import 'package:flutter_test/flutter_test.dart';

import 'package:home_kitchen/mixins/date.dart';

void main() {
  test('timeIndicators should be to show date value', () {
    final today = Date.formDartTime(DateTime.now());
    final yesterday = Date.formDartTime(DateTime.now().add(Duration(days: -1)));
    final tommorow = Date.formDartTime(DateTime.now().add(Duration(days: 1)));

    print(today.timeIndicators());
    assert(today.timeIndicators() == "Today");

    print(yesterday.timeIndicators());
    assert(yesterday.timeIndicators() == "Yesterday");

    print(tommorow.timeIndicators());
    assert(tommorow.timeIndicators() == "Tomorrow");
  });

  test('Checks for yesterday', () {
    final today = Date.formDartTime(DateTime.now());
    final yesterday = Date.formDartTime(DateTime.now().add(Duration(days: -1)));
    final tommorow = Date.formDartTime(DateTime.now().add(Duration(days: 1)));

    assert(today.isYesterday() != true);
    assert(yesterday.isYesterday() == true);
    assert(tommorow.isYesterday() != true);
  });

  test('Checks for today', () {
    final today = Date.formDartTime(DateTime.now());
    final yesterday = Date.formDartTime(DateTime.now().add(Duration(days: -1)));
    final tommorow = Date.formDartTime(DateTime.now().add(Duration(days: 1)));

    assert(today.isToday() == true);
    assert(yesterday.isToday() != true);
    assert(tommorow.isToday() != true);
  });

  test('Checks for tommorow', () {
    final today = Date.formDartTime(DateTime.now());
    final yesterday = Date.formDartTime(DateTime.now().add(Duration(days: -1)));
    final tommorow = Date.formDartTime(DateTime.now().add(Duration(days: 1)));

    assert(today.isTomorrow() != true);
    assert(yesterday.isTomorrow() != true);
    assert(tommorow.isTomorrow() == true);
  });
}
