import 'package:flutter_test/flutter_test.dart';

import 'package:rxdart/rxdart.dart';

void main() {
  test('rxdart listener', () {
    final _subject = BehaviorSubject<int>();
    _subject.stream.listen((number) => print(number == 1));
    _subject.stream.listen(print);

    _subject.add(1);
  });
}
