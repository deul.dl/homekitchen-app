import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:home_kitchen/order_status_repository/order_status_repository.dart';
import 'package:meta/meta.dart';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'localization.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class ReceivedNotification {
  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  final int id;
  final String title;
  final String body;
  final String payload;
}

class PushNotification {
  PushNotification({this.orderStatusRepository, this.languageCode});

  final OrderStatusRepository orderStatusRepository;
  final String languageCode;

  NotificationDetails get platformChannelSpecifics {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails('1', 'Notification', 'homekitchen',
            importance: Importance.max,
            priority: Priority.high,
            ticker: 'ticker');
    return NotificationDetails(android: androidPlatformChannelSpecifics);
  }

  Map<String, String> get localizationOfNotification {
    if (languageCode == 'ru') {
      return localization['ru'];
    } else {
      return localization['en'];
    }
  }

  Future<void> init() async {
    FirebaseMessaging.onMessage.listen(_firebaseMessagingBackgroundHandler);
  }

  Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
    print('Got a message whilst in the foreground!');
    print('Message data: ${message.toString()}');

    if (message?.notification != null) {
      _showNotification(message.notification);

      orderStatusRepository.put(OrderStatus(
          orderUUID: message.data['order_uuid'],
          status: message.data['status']));

      print('Message also contained a notification: ${message.notification}');
    }
  }

  Future<void> _showNotification(RemoteNotification notification) async {
    print(notification.title);
    await flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        localizationOfNotification[notification.title],
        localizationOfNotification[notification.body],
        platformChannelSpecifics,
        payload: 'item x');
  }
}
