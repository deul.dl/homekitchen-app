import 'package:flutter/material.dart';
import 'package:home_kitchen/theme/colors.dart';

ThemeData homeKitchenTheme = ThemeData(
    primaryColor: HomeKitchenColors.blackColor,
    buttonColor: HomeKitchenColors.blackColor,
    backgroundColor: HomeKitchenColors.whiteLilac,
    bottomAppBarColor: Colors.red,
    errorColor: Colors.redAccent,
    splashColor: HomeKitchenColors.blackColor,
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.red,
    ),
    textTheme: TextTheme(
        button: TextStyle(color: Colors.white),
        headline4: TextStyle(color: Colors.black)));
