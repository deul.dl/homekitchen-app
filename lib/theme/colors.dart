import 'package:flutter/material.dart';

class HomeKitchenColors {
  static const _whiteLilacPrimaryValue = 0xffeff1f8;
  static const _blackPrimaryValue = 0xff2e323d;

  static const MaterialColor whiteLilac = MaterialColor(
    _whiteLilacPrimaryValue,
    <int, Color>{
      50: Color.fromRGBO(239, 241, 248, .1),
      100: Color.fromRGBO(239, 241, 248, .2),
      200: Color.fromRGBO(239, 241, 248, .3),
      300: Color.fromRGBO(239, 241, 248, .4),
      400: Color.fromRGBO(239, 241, 248, .5),
      500: Color.fromRGBO(239, 241, 248, .6),
      600: Color.fromRGBO(239, 241, 248, .7),
      700: Color.fromRGBO(239, 241, 248, .8),
      800: Color.fromRGBO(239, 241, 248, .9),
      900: Color.fromRGBO(239, 241, 248, 1),
    },
  );

  static const MaterialColor blackColor = MaterialColor(
    _blackPrimaryValue,
    <int, Color>{
      50: Color.fromRGBO(46, 50, 61, .1),
      100: Color.fromRGBO(46, 50, 61, .2),
      200: Color.fromRGBO(46, 50, 61, .3),
      300: Color.fromRGBO(46, 50, 61, .4),
      400: Color.fromRGBO(46, 50, 61, .5),
      500: Color.fromRGBO(46, 50, 61, .6),
      600: Color.fromRGBO(46, 50, 61, .7),
      700: Color.fromRGBO(46, 50, 61, .8),
      800: Color.fromRGBO(46, 50, 61, .9),
      900: Color.fromRGBO(46, 50, 61, 1),
    },
  );
}
