import 'package:flutter/material.dart';

import 'package:home_kitchen/utils/format_price.dart';

class CommonSettings extends InheritedWidget {
  CommonSettings({
    Key key,
    @required this.countryCode,
    @required Widget child,
  })  : assert(countryCode != null),
        assert(child != null),
        super(key: key, child: child);

  final String countryCode;

  static CommonSettings of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<CommonSettings>();
  }

  @override
  Widget get child => super.child;

  PriceFormat get formatCurrency => PriceFormat(countryCode: countryCode);

  @override
  bool updateShouldNotify(CommonSettings old) => countryCode != old.countryCode;
}
