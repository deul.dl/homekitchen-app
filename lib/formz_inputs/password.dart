import 'package:formz/formz.dart';

enum PasswordValidationError { empty, shortLength }

class Password extends FormzInput<String, PasswordValidationError> {
  const Password.pure() : super.pure('');
  const Password.dirty([String value = '']) : super.dirty(value);

  @override
  PasswordValidationError validator(String value) {
    if (value?.isNotEmpty != true) {
      return PasswordValidationError.empty;
    } else if (value.length < 6) {
      return PasswordValidationError.shortLength;
    } else
      return null;
  }
}
