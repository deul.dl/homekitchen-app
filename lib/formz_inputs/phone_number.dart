import 'package:formz/formz.dart';

enum PhoneNumberValidationError { empty, isNotFormat }

class ZPhoneNumber extends FormzInput<String, PhoneNumberValidationError> {
  const ZPhoneNumber.pure() : super.pure('');
  const ZPhoneNumber.dirty([String value = '']) : super.dirty(value);

  String get phoneNumberWithPlus => '+$value';

  bool _isPhoneNumber(String em) {
    String p =
        r"^((?:9[679]|8[035789]|6[789]|5[90]|42|3[578]|2[1-689])|9[0-58]|8[1246]|6[0-6]|5[1-8]|4[013-9]|3[0-469]|2[70]|7|1)(?:\W*\d){0,13}\d$";

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }

  @override
  PhoneNumberValidationError validator(String value) {
    if (value?.isNotEmpty != true) {
      return PhoneNumberValidationError.empty;
    } else if (!_isPhoneNumber(value)) {
      return PhoneNumberValidationError.isNotFormat;
    } else if (value.length < 11 || value.length > 15) {
      return PhoneNumberValidationError.isNotFormat;
    } else {
      return null;
    }
  }
}
