import 'package:formz/formz.dart';

enum FullNameValidationError { empty }

class FirstName extends FormzInput<String, FullNameValidationError> {
  const FirstName.pure() : super.pure('');
  const FirstName.dirty([String value = '']) : super.dirty(value);

  @override
  FullNameValidationError validator(String value) {
    return value?.isNotEmpty == true ? null : FullNameValidationError.empty;
  }
}

class LastName extends FormzInput<String, FullNameValidationError> {
  const LastName.pure() : super.pure('');
  const LastName.dirty([String value = '']) : super.dirty(value);

  @override
  FullNameValidationError validator(String value) {
    return value?.isNotEmpty == true ? null : FullNameValidationError.empty;
  }
}
