import 'package:formz/formz.dart';

enum IINValidationError { empty, shortLength }

class IIN extends FormzInput<String, IINValidationError> {
  const IIN.pure() : super.pure('');
  const IIN.dirty([String value = '']) : super.dirty(value);

  @override
  IINValidationError validator(String value) {
    if (value?.isNotEmpty != true) {
      return IINValidationError.empty;
    } else if (value.length < 10) {
      return IINValidationError.shortLength;
    } else
      return null;
  }
}
