import 'package:formz/formz.dart';

enum EmailValidationError { empty, isNotEmail }

class Email extends FormzInput<String, EmailValidationError> {
  const Email.pure() : super.pure('');
  const Email.dirty([String value = '']) : super.dirty(value);

  @override
  EmailValidationError validator(String value) {
    if (value?.isNotEmpty != true) {
      return EmailValidationError.empty;
    } else if (!_isEmail(value)) {
      return EmailValidationError.isNotEmail;
    } else {
      return null;
    }
  }

  bool _isEmail(String em) {
    String p =
        r"^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$";

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(em);
  }
}
