import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'item_category.dart';

@immutable
abstract class ItemCategoryEvent {
  Stream<ItemCategoryState> applyAsync(
      {ItemCategoryState currentState, ItemCategoryBloc bloc});
}

class LoadCategories extends ItemCategoryEvent {
  LoadCategories();

  @override
  String toString() => 'LoadCategories';

  @override
  Stream<ItemCategoryState> applyAsync(
      {ItemCategoryState currentState, ItemCategoryBloc bloc}) async* {
    try {
      yield ItemCategoryInProgress();
      final categories = await bloc.categoryRepository.loadData();
      yield ItemCategoryLoadSuccess(categories: categories);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadCategories', error: _, stackTrace: stackTrace);
      yield ItemCategoryFailure(_?.toString());
    }
  }
}
