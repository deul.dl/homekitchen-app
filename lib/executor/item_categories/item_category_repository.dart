import 'dart:async';

import 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/client/client.dart';

class ItemCategoryRepository {
  ItemCategoryRepository(client) : this._client = client;

  final WebClient _client;

  Future<dynamic> loadData() {
    return _client
        .get('categories')
        .then((res) => res['data']['categories'])
        .then((categories) => (categories as List)
            .map((category) => Category.fromJson(category))
            .toList());
  }
}
