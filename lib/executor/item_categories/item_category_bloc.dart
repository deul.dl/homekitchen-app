import 'dart:async';

import 'package:home_kitchen/executor/item_categories/item_category_repository.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'item_category.dart';

class ItemCategoryBloc extends Bloc<ItemCategoryEvent, ItemCategoryState> {
  ItemCategoryBloc({@required this.categoryRepository}) : super(null);
  final ItemCategoryRepository categoryRepository;

  @override
  Stream<ItemCategoryState> mapEventToState(
    ItemCategoryEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
