import 'package:equatable/equatable.dart';

import 'package:home_kitchen/models/models.dart';

abstract class ItemCategoryState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  ItemCategoryState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class ItemCategoryInProgress extends ItemCategoryState {
  ItemCategoryInProgress();

  @override
  String toString() => 'ItemCategoryInProgress';
}

class ItemCategoryLoadSuccess extends ItemCategoryState {
  final List<Category> categories;

  ItemCategoryLoadSuccess({this.categories}) : super([categories]);

  @override
  String toString() => 'ItemCategoryLoadSuccess { categories: $categories }';
}

class ItemCategoryFailure extends ItemCategoryState {
  final String errorMessage;

  ItemCategoryFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ItemCategoryError { errorMessage: $errorMessage }';
}
