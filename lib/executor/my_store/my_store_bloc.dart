import 'dart:async';

import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'my_store.dart';

class MyStoreBloc extends Bloc<MyStoreEvent, MyStoreState> {
  MyStoreBloc({@required this.storeRepository}) : super(null);
  final StoreRepository storeRepository;

  @override
  Stream<MyStoreState> mapEventToState(
    MyStoreEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
