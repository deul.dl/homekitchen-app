import 'package:home_kitchen/models/store_delivery.dart';
import 'package:meta/meta.dart';

import 'edit_delivery_price.dart';

@immutable
abstract class EditDeliveryPriceEvent {
  Stream<EditDeliveryPriceState> applyAsync(
      {EditDeliveryPriceState currentState, EditDeliveryPriceBloc bloc});
}

class UpdateStoreDeliveryPrice extends EditDeliveryPriceEvent {
  final StoreDelivery storeDelivery;

  UpdateStoreDeliveryPrice({@required this.storeDelivery});

  @override
  Stream<EditDeliveryPriceState> applyAsync(
      {EditDeliveryPriceState currentState,
      EditDeliveryPriceBloc bloc}) async* {
    try {
      yield EditDeliveryPriceInProgress();
      await bloc.storeDeliveryRepository
          .updateStoreDeliveryPrice(storeDelivery);

      yield EditDeliveryPriceLoadSuccess();
    } catch (_) {
      yield EditDeliveryPriceFailure(_?.toString());
    }
  }
}
