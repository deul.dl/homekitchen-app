import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/edit_delivery_price/edit_delivery_price_screen.dart';

import 'package:home_kitchen/models/store_delivery.dart';

import 'edit_delivery_price.dart';

class EditDeliveryPricePage extends StatelessWidget {
  EditDeliveryPricePage({Key key, this.storeDelivery}) : super(key: key);

  final StoreDelivery storeDelivery;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
              context, 'deliverySettings.EditDeliveryPrice.AppBar.Title')),
        ),
        body: EditDeliveryPriceScreen(
            storeDelivery: storeDelivery,
            onSave: (num value) {
              BlocProvider.of<EditDeliveryPriceBloc>(context).add(
                  UpdateStoreDeliveryPrice(
                      storeDelivery: storeDelivery.copy(price: value)));
            }));
  }
}
