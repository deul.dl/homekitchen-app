import 'package:equatable/equatable.dart';

abstract class EditDeliveryPriceState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  EditDeliveryPriceState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class EditDeliveryPriceLoadSuccess extends EditDeliveryPriceState {
  EditDeliveryPriceLoadSuccess();

  @override
  String toString() => 'EditDeliveryPriceLoadSuccess {}';
}

class EditDeliveryPriceInProgress extends EditDeliveryPriceState {
  EditDeliveryPriceInProgress();

  @override
  String toString() => 'EditDeliveryPriceLoadSuccess {}';
}

class EditDeliveryPriceFailure extends EditDeliveryPriceState {
  final errorMessage;
  EditDeliveryPriceFailure(this.errorMessage);

  @override
  String toString() =>
      'EditDeliveryPriceFailure { errorMessage: $errorMessage }';
}
