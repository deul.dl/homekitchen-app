import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/delivery_settings.dart';
import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/edit_delivery_price/edit_delivery_price_event.dart';

import 'edit_delivery_price.dart';

class EditDeliveryPriceBloc
    extends Bloc<EditDeliveryPriceEvent, EditDeliveryPriceState> {
  EditDeliveryPriceBloc({@required this.storeDeliveryRepository}) : super(null);
  final StoreDeliveryRepository storeDeliveryRepository;

  @override
  Stream<EditDeliveryPriceState> mapEventToState(
    EditDeliveryPriceEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
