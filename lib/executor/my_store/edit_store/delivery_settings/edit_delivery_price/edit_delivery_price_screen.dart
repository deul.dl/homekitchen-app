import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/delivery_settings.dart';
import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/edit_delivery_price/edit_delivery_price.dart';
import 'package:home_kitchen/models/store_delivery.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/custom_form_input.dart';
import 'package:home_kitchen/widgets/form_top.dart';

class EditDeliveryPriceScreen extends StatefulWidget {
  EditDeliveryPriceScreen({Key key, this.storeDelivery, @required this.onSave})
      : super(key: key);

  final StoreDelivery storeDelivery;
  final Function onSave;
  _EditDeliveryPriceState createState() => _EditDeliveryPriceState();
}

class _EditDeliveryPriceState extends State<EditDeliveryPriceScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  num _price;

  StoreDelivery get storeDelivery => widget.storeDelivery;

  @override
  Widget build(BuildContext context) {
    return BlocListener<EditDeliveryPriceBloc, EditDeliveryPriceState>(
        listener: (context, state) {
          if (state is EditDeliveryPriceFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
          }
          if (state is EditDeliveryPriceLoadSuccess) {
            Navigator.pop(context);
          }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FormTop(
                    title: FlutterI18n.translate(context,
                        'deliverySettings.EditDeliveryPrice.Form.Title'),
                    child: Column(
                      children: <Widget>[
                        CustomFormInput(
                          keyboardType: TextInputType.number,
                          initialValue: storeDelivery == null
                              ? ''
                              : storeDelivery.price.toString(),
                          onSaved: (val) => _price = num.parse(val),
                          label: FlutterI18n.translate(context,
                              'deliverySettings.EditDeliveryPrice.Form.Price.Label'),
                          validator: (val) {
                            return val.trim().isEmpty
                                ? FlutterI18n.translate(context,
                                    'deliverySettings.EditDeliveryPrice.Form.Price.Error')
                                : null;
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                      alignment: Alignment.bottomCenter,
                      child: ExpandedFlatButton(
                          label: FlutterI18n.translate(context, 'common.Save'),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();

                              widget.onSave(_price);
                            }
                          }))
                ],
              ),
            )));
  }
}
