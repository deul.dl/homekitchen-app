import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/edit_delivery_price/edit_delivery_price.dart';

import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/edit_delivery_price/edit_delivery_price_page.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'delivery_settings.dart';

class DeliverySettingsScreen extends StatefulWidget {
  DeliverySettingsScreen({
    Key key,
  }) : super(key: key);

  @override
  _EditDeliveryState createState() => _EditDeliveryState();
}

class _EditDeliveryState extends State<DeliverySettingsScreen> {
  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return BlocBuilder<DeliverySettingBloc, DeliverySettingState>(
        builder: (context, state) {
      if (state is DeliverySettingLoadSuccess) {
        return Column(children: <Widget>[
          Container(
              color: Colors.white,
              child: ListTile(
                  title: Text(
                    FlutterI18n.translate(context, 'deliverySettings.Price'),
                  ),
                  trailing: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: <Widget>[
                      Text(
                          formatCurrency
                              .printPrice(state.storeDelivery.printPrice),
                          style: TextStyle(fontSize: 14)),
                      SizedBox(
                        width: 5,
                      ),
                      Icon(
                        Icons.chevron_right,
                      )
                    ],
                  ),
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          BlocProvider<EditDeliveryPriceBloc>(
                              create: (context) => EditDeliveryPriceBloc(
                                  storeDeliveryRepository: RepositoryProvider
                                      .of<StoreDeliveryRepository>(context)),
                              child: EditDeliveryPricePage(
                                storeDelivery: state.storeDelivery,
                              )))))),
        ]);
      } else if (state is DeliverySettingFailure) {
        return ErrorScreen(errorMessage: state.errorMessage, onReload: _load);
      } else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    });
  }

  void _load() {
    BlocProvider.of<DeliverySettingBloc>(context).add(LoadStoreDelivery());
  }
}
