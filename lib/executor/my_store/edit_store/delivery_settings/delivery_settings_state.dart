import 'package:equatable/equatable.dart';
import 'package:home_kitchen/models/store_delivery.dart';

const HOME_KITCHEN_DELIVERY = 'hk_delivery_service';
const COOKER_DELIVERY = 'ck_delivery_service';

abstract class DeliverySettingState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  DeliverySettingState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class DeliverySettingLoadSuccess extends DeliverySettingState {
  DeliverySettingLoadSuccess(this.storeDelivery) : super([storeDelivery]);
  final StoreDelivery storeDelivery;

  @override
  String toString() =>
      'DeliverySettingLoadSuccess { storeDelivery: $storeDelivery }';
}

class DeliverySettingFailure extends DeliverySettingState {
  final errorMessage;
  DeliverySettingFailure(this.errorMessage);

  @override
  String toString() => 'DeliverySettingFailure { errorMessage: $errorMessage }';
}
