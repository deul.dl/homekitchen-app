import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'delivery_settings_screen.dart';

class DeliverySettingsPage extends StatelessWidget {
  DeliverySettingsPage({
    Key key,
  }) : super(key: key);
  static const String routeName = "/delivery-settings";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              FlutterI18n.translate(context, 'deliverySettings.AppBar.Title')),
        ),
        body: DeliverySettingsScreen());
  }
}
