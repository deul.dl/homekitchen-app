import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:meta/meta.dart';

import 'package:home_kitchen/models/store_delivery.dart';
import 'delivery_settings.dart';

class DeliverySettingBloc
    extends Bloc<DeliverySettingEvent, DeliverySettingState> {
  DeliverySettingBloc({@required this.storeDeliveryRepository}) : super(null) {
    _storeDeliverySubscription = storeDeliveryRepository
        .storeDeliveryStream()
        .listen((StoreDelivery storeDelivery) {
      add(UpdateStoreDeliver(storeDelivery));
    });
  }
  StreamSubscription<StoreDelivery> _storeDeliverySubscription;
  final StoreDeliveryRepository storeDeliveryRepository;

  @override
  Stream<DeliverySettingState> mapEventToState(
    DeliverySettingEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    _storeDeliverySubscription?.cancel();
    await super.close();
  }
}
