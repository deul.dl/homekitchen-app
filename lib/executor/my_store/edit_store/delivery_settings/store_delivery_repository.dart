import 'dart:async';
import 'dart:core';

import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/models/store_delivery.dart';
import 'package:rxdart/rxdart.dart';

abstract class StoreRepository {
  Future<dynamic> getStoreDelivery();

  Future<dynamic> updateStoreDeliveryPrice(StoreDelivery storeDelivery);

  Future<dynamic> changeTypeOfDelivery(StoreDelivery storeDelivery);

  Stream<StoreDelivery> storeDeliveryStream();
}

class StoreDeliveryRepository extends StoreRepository {
  StoreDeliveryRepository(client) : this._client = client;

  final WebClient _client;

  final _subject = BehaviorSubject<StoreDelivery>();

  @override
  Stream<StoreDelivery> storeDeliveryStream() async* {
    yield* _subject.stream;
  }

  @override
  Future<dynamic> getStoreDelivery() async {
    return _client
        .get('executor/stories/store/delivery')
        .then((res) => res['data'])
        .then((storeDelivery) => StoreDelivery.fromJson(storeDelivery));
  }

  @override
  Future<dynamic> updateStoreDeliveryPrice(StoreDelivery storeDelivery) async {
    return _client
        .put('executor/stories/store/delivery/price',
            {'price': storeDelivery.price})
        .then((res) => res["data"])
        .then((storeDelivery) {
          final _storeDelivery = StoreDelivery.fromJson(storeDelivery);

          _subject.add(_storeDelivery);
          return _storeDelivery;
        });
  }

  @override
  Future<dynamic> changeTypeOfDelivery(StoreDelivery storeDelivery) async {
    return _client
        .put('executor/stories/store/delivery/change-service',
            {'type': storeDelivery.type})
        .then((res) => res["data"])
        .then((storeDelivery) {
          final _storeDelivery = StoreDelivery.fromJson(storeDelivery);

          _subject.add(_storeDelivery);
          return _storeDelivery;
        });
  }
}
