import 'package:home_kitchen/models/store_delivery.dart';

import 'delivery_settings.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DeliverySettingEvent {
  Stream<DeliverySettingState> applyAsync(
      {DeliverySettingState currentState, DeliverySettingBloc bloc});
}

class LoadStoreDelivery extends DeliverySettingEvent {
  LoadStoreDelivery();

  @override
  Stream<DeliverySettingState> applyAsync(
      {DeliverySettingState currentState, DeliverySettingBloc bloc}) async* {
    try {
      final storeDelivery =
          await bloc.storeDeliveryRepository.getStoreDelivery();
      yield DeliverySettingLoadSuccess(storeDelivery as StoreDelivery);
    } catch (_) {
      yield DeliverySettingFailure(_?.toString());
    }
  }
}

class UpdateStoreDeliver extends DeliverySettingEvent {
  final StoreDelivery storeDelivery;

  UpdateStoreDeliver(this.storeDelivery);

  @override
  Stream<DeliverySettingState> applyAsync(
      {DeliverySettingState currentState, DeliverySettingBloc bloc}) async* {
    try {
      yield DeliverySettingLoadSuccess(storeDelivery);
    } catch (_) {
      yield DeliverySettingFailure(_?.toString());
    }
  }
}
