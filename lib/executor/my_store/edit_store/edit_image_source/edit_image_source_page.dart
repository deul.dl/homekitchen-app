import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/my_store/widgets/edit_image_source_screen.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditImageSourcePage extends StatelessWidget {
  EditImageSourcePage({
    Key key,
    this.store,
  }) : super(key: key);
  final Store store;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              FlutterI18n.translate(context, 'myStore.EditImage.AppBar.Title')),
        ),
        body: BlocListener<MyStoreBloc, MyStoreState>(
            listener: (context, state) {
              if (state is MyStoreFailure) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
              }
              if (state is MyStoreLoadSuccess) {
                Navigator.pop(context);
              }
            },
            child: EditImageSourceScreen(
                isEdit: true,
                store: store,
                onSave: (File file) {
                  BlocProvider.of<MyStoreBloc>(context)
                      .add(UploadImageOfStore(file: file));
                })));
  }
}
