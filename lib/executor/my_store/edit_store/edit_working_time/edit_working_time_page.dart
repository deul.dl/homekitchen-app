import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/my_store/widgets/edit_working_time_screen.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditWorkingTimePage extends StatelessWidget {
  EditWorkingTimePage({Key key, this.store}) : super(key: key);

  final Store store;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
              context, 'myStore.EditWorkingTime.AppBar.Title')),
        ),
        body: EditWorkingTimeScreen(
            store: store,
            isEdit: true,
            onSave: (int start, int end) {
              BlocProvider.of<MyStoreBloc>(context).add(
                  UpdateMyStore(store.copy(startedAt: start, endedAt: end)));
            }));
  }
}
