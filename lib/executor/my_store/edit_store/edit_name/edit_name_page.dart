import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/my_store/widgets/edit_name_screen.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditNamePage extends StatelessWidget {
  EditNamePage({Key key, this.store}) : super(key: key);

  final Store store;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              FlutterI18n.translate(context, 'myStore.EditName.AppBar.Title')),
        ),
        body: EditNameScreen(
            store: store,
            isEdit: true,
            onSave: (String value) {
              BlocProvider.of<MyStoreBloc>(context)
                  .add(UpdateMyStore(store.copy(name: value)));
            }));
  }
}
