import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/custom_form_input.dart';
import 'package:home_kitchen/widgets/form_top.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditDescriptionScreen extends StatefulWidget {
  EditDescriptionScreen(
      {Key key, this.store, bool isEdit, @required this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final StoreModel store;

  final Function onSave;
  _EditDescriptionState createState() => _EditDescriptionState();
}

class _EditDescriptionState extends State<EditDescriptionScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _description;

  get store => widget.isEdit ? widget.store as Store : widget.store as NewStore;

  @override
  Widget build(BuildContext context) {
    return BlocListener<MyStoreBloc, MyStoreState>(
        listener: (context, state) {
          if (state is MyStoreFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
          }
          if (state is MyStoreLoadSuccess) {
            Navigator.pop(context);
          }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FormTop(
                    title: FlutterI18n.translate(
                        context, 'myStore.EditDescription.Title'),
                    child: CustomFormInput(
                      initialValue:
                          widget.store == null ? '' : store.description,
                      onSaved: (val) => _description = val,
                      label: FlutterI18n.translate(context,
                          'myStore.EditDescription.Form.Description.Label'),
                      maxLines: 8,
                      hintText: FlutterI18n.translate(context,
                          'myStore.EditDescription.Form.Description.Placeholder'),
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: ExpandedFlatButton(
                        label: widget.isEdit
                            ? FlutterI18n.translate(context, 'common.Save')
                            : FlutterI18n.translate(context, 'common.Next'),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();

                            widget.onSave(_description);
                          }
                        }),
                  )
                ],
              ),
            )));
  }
}
