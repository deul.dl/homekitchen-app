import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/image_upload.dart';
import 'package:home_kitchen/widgets/form_top.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditImageSourceScreen extends StatefulWidget {
  EditImageSourceScreen({
    Key key,
    this.store,
    bool isEdit,
    @required this.onSave,
  })  : this.isEdit = isEdit ?? false,
        super(key: key);
  final StoreModel store;
  final bool isEdit;
  final Function onSave;
  _EditImageSourceState createState() => _EditImageSourceState();
}

class _EditImageSourceState extends State<EditImageSourceScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  File _file;
  String _error;

  get store => widget.isEdit ? widget.store as Store : widget.store as NewStore;

  @override
  void initState() {
    if (!widget.isEdit)
      setState(() {
        _file = store == null ? null : store.file;
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FormTop(
                title:
                    FlutterI18n.translate(context, 'myStore.EditImage.Title'),
                error: _error,
                child: ImageUpload(
                    placeholderImage: widget.isEdit ? store.imageSource : null,
                    initialValue: _file,
                    height: 200,
                    description: FlutterI18n.translate(
                        context, 'myStore.EditImage.ImageUpload.Description'),
                    onUploaded: (File file) {
                      setState(() => _file = file);
                    }),
              ),
              Container(
                alignment: Alignment.bottomCenter,
                child: ExpandedFlatButton(
                    label: widget.isEdit
                        ? FlutterI18n.translate(context, 'common.Save')
                        : FlutterI18n.translate(context, 'common.Next'),
                    onPressed: () {
                      if (_file != null || widget.isEdit) {
                        widget.onSave(_file);
                      } else {
                        setState(() => _error = 'Image is empty.');
                      }
                    }),
              )
            ],
          ),
        ));
  }
}
