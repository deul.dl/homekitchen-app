import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/custom_form_input.dart';
import 'package:home_kitchen/widgets/form_top.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditPhoneNumberScreen extends StatefulWidget {
  EditPhoneNumberScreen(
      {Key key, this.store, bool isEdit, @required this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final StoreModel store;
  final Function onSave;
  _EditPhoneNumberState createState() => _EditPhoneNumberState();
}

class _EditPhoneNumberState extends State<EditPhoneNumberScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _phoneNumber;

  get store => widget.isEdit ? widget.store as Store : widget.store as NewStore;

  @override
  Widget build(BuildContext context) {
    return BlocListener<MyStoreBloc, MyStoreState>(
        listener: (context, state) {
          if (state is MyStoreFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
          }
          if (state is MyStoreLoadSuccess) {
            Navigator.pop(context);
          }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FormTop(
                    title: FlutterI18n.translate(
                        context, 'myStore.EditPhoneNumber.Title'),
                    child: Column(
                      children: <Widget>[
                        CustomFormInput(
                          initialValue:
                              widget.store == null ? '' : store.phoneNumber,
                          onSaved: (val) => _phoneNumber = val,
                          label: FlutterI18n.translate(context,
                              'myStore.EditPhoneNumber.Form.PhoneNumber.Label'),
                          keyboardType: TextInputType.phone,
                          validator: (val) {
                            return val.trim().isEmpty
                                ? FlutterI18n.translate(context,
                                    'myStore.EditPhoneNumber.Form.PhoneNumber.Required')
                                : null;
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: ExpandedFlatButton(
                        label: widget.isEdit
                            ? FlutterI18n.translate(context, 'common.Save')
                            : FlutterI18n.translate(context, 'common.Next'),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();

                            widget.onSave(_phoneNumber);
                          }
                        }),
                  )
                ],
              ),
            )));
  }
}
