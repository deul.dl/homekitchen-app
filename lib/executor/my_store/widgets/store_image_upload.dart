import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:home_kitchen/models/models.dart';

class StoreImageUpload extends StatelessWidget {
  StoreImageUpload({Key key, this.store, this.onUpload}) : super(key: key);
  final Store store;
  final Function onUpload;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (store.imageSource.isNotEmpty
          ? store.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        CachedNetworkImage(
          imageUrl: urlImage,
          height: 150,
          width: 150,
          placeholder: (context, url) =>
              Image.asset('assets/images/placeholder.jpg'),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
        Positioned(
            bottom: -10,
            right: -15,
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.circle,
                ),
                child: IconButton(
                  color: Colors.white,
                  icon: Icon(Icons.file_upload),
                  onPressed: onUpload,
                ))),
      ],
    );
  }
}
