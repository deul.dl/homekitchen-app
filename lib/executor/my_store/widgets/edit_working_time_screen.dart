import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:time_range_picker/time_range_picker.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class EditWorkingTimeScreen extends StatefulWidget {
  EditWorkingTimeScreen(
      {Key key, this.store, bool isEdit, @required this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final StoreModel store;
  final Function onSave;
  _EditWorkingTimeState createState() => _EditWorkingTimeState();
}

class _EditWorkingTimeState extends State<EditWorkingTimeScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  int _startedAt, _endedAt;
  String _errorMessage = '';

  get store => widget.isEdit ? widget.store as Store : widget.store as NewStore;
  @override
  void initState() {
    if (store.startedAt != null && store.endedAt != null) {
      _startedAt = store.startedAt;
      _endedAt = store.endedAt;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MyStoreBloc, MyStoreState>(
        listener: (context, state) {
          if (state is MyStoreFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
          }
          if (state is MyStoreLoadSuccess) {
            Navigator.pop(context);
          }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FormTop(
                    title: FlutterI18n.translate(
                        context, 'myStore.EditWorkingTime.Title'),
                    child: Column(
                      children: <Widget>[
                        TimeRangeInput(
                          initialTimeRange:
                              store.startedAt != null && store.endedAt != null
                                  ? [store.startedAt, store.endedAt]
                                  : [],
                          onChange: (int started, int ended) {
                            _startedAt = started;
                            _endedAt = ended;
                          },
                          errorMessage: _errorMessage,
                        )
                      ],
                    ),
                  ),
                  Container(
                      alignment: Alignment.bottomCenter,
                      child: ExpandedFlatButton(
                          label: widget.isEdit
                              ? FlutterI18n.translate(context, 'common.Save')
                              : FlutterI18n.translate(context, 'common.Next'),
                          onPressed: () {
                            if (_startedAt != null && _startedAt != null) {
                              widget.onSave(_startedAt, _endedAt);
                            } else {
                              setState(() {
                                _errorMessage = FlutterI18n.translate(context,
                                    'myStore.EditWorkingTime.Form.WorkingTime.Error');
                              });
                            }
                          }))
                ],
              ),
            )));
  }
}

class TimeRangeInput extends StatefulWidget {
  TimeRangeInput({
    Key key,
    this.initialTimeRange,
    this.errorMessage,
    this.onChange,
  }) : super(key: key);

  final String errorMessage;
  final List<int> initialTimeRange;
  final Function(int, int) onChange;

  @override
  _TimeRangeState createState() => _TimeRangeState();
}

class _TimeRangeState extends State<TimeRangeInput> {
  TimeRange _timeRange;

  String get _timeRangeText => _timeRange != null
      ? '${_timeRange.startTime.format(context)} -${_timeRange.endTime.format(context)}'
      : FlutterI18n.translate(context, 'timeRange.Placeholder');

  @override
  void initState() {
    if (widget.initialTimeRange.isNotEmpty) {
      final startedAt = convertTimeOfDayFromSeconds(widget.initialTimeRange[0]);
      final endedAt = convertTimeOfDayFromSeconds(widget.initialTimeRange[1]);
      setState(() {
        _timeRange = TimeRange(startTime: startedAt, endTime: endedAt);
      });
    }
    super.initState();
  }

  TimeOfDay convertTimeOfDayFromSeconds(int seconds) {
    final secondOfMinutes = (seconds % 3600);
    final minute = secondOfMinutes / 60;
    final hour = (seconds - secondOfMinutes) / 3600;

    return TimeOfDay(hour: hour.toInt(), minute: minute.toInt());
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      InkWell(
          onTap: () async {
            TimeRange result = await showTimeRangePicker(
              context: context,
              start: _timeRange?.startTime,
              end: _timeRange?.endTime,
            );
            setState(() => _timeRange = result);

            final startSeconds = (result.startTime.minute * 60) +
                (result.startTime.hour * 60 * 60);
            final endSeconds =
                (result.endTime.minute * 60) + (result.endTime.hour * 60 * 60);
            await widget.onChange(startSeconds, endSeconds);
          },
          child: Container(
            height: 60,
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    bottom: BorderSide(
                  color: Theme.of(context).primaryColor,
                  width: 1,
                ))),
            child: Row(
              children: [
                Icon(Icons.timer),
                SizedBox(
                  width: 10,
                ),
                Text(
                  _timeRangeText,
                )
              ],
            ),
          )),
      Text(
        widget.errorMessage,
        style: TextStyle(
          color: Colors.red,
        ),
        textAlign: TextAlign.start,
      )
    ]);
  }
}
