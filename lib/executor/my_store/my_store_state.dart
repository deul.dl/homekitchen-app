import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:home_kitchen/addresses/addresses.dart';

@immutable
abstract class MyStoreState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  MyStoreState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class MyStoreInProgress extends MyStoreState {
  MyStoreInProgress();

  @override
  String toString() => 'MyStoreInProgress';
}

class MyStoreLoadSuccess extends MyStoreState {
  MyStoreLoadSuccess(this.store) : super([store]);
  final Store store;
  @override
  String toString() => 'MyStoreLoadSuccess { store: $store }';
}

class MyStoreFailure extends MyStoreState {
  final String errorMessage;

  MyStoreFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'MyStoreError { errorMessage: $errorMessage }';
}
