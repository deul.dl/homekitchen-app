import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'create_store.dart';

class CreateStoreBloc extends Bloc<CreateStoreEvent, CreateStoreState> {
  CreateStoreBloc({@required this.storeRepository}) : super(null);
  final StoreRepository storeRepository;

  @override
  Stream<CreateStoreState> mapEventToState(
    CreateStoreEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'CreateStoreBloc', error: _, stackTrace: stackTrace);
      yield state.copyWith(store: state.store, errorMessage: _.toString());
    }
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
