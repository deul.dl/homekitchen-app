import 'dart:io';

import 'package:formz/formz.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

import 'package:home_kitchen/executor/my_store/widgets/edit_name_screen.dart';
import 'package:home_kitchen/executor/my_store/widgets/edit_description_screen.dart';
import 'package:home_kitchen/executor/my_store/widgets/edit_phone_number_screen.dart';
import 'package:home_kitchen/executor/my_store/widgets/edit_image_source_screen.dart';
import 'package:home_kitchen/executor/my_store/widgets/edit_working_time_screen.dart';
import 'package:home_kitchen/theme/colors.dart';
import 'confirm_done_screen.dart';
import 'done_page.dart';

class CreateStorePage extends StatefulWidget {
  static const String routeName = '/create-store';

  @override
  _CreateStoreState createState() => _CreateStoreState();
}

class _CreateStoreState extends State<CreateStorePage> {
  final _formsPageViewController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: SafeArea(
            top: true,
            child: BlocListener<CreateStoreBloc, CreateStoreState>(
                listener: (context, state) {
              if (state.status.isSubmissionSuccess) {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (_) => DonePage()));
              }
              if (state.status.isSubmissionFailure) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(content: Text(state.errorMessage)),
                  );
              }
            }, child: BlocBuilder<CreateStoreBloc, CreateStoreState>(
                    builder: (context, state) {
              final NewStore store =
                  (state is CreateStoreState) ? state.store : NewStore();

              final List _forms = [
                WillPopScope(
                  key: Key('new_name'),
                  onWillPop: () => Future.sync(this.onWillPop),
                  child: EditNameScreen(
                      store: store,
                      onSave: (String value) {
                        BlocProvider.of<CreateStoreBloc>(context)
                            .add(ChangeStore(store: store.copy(name: value)));
                        _nextFormStep();
                      }),
                ),
                WillPopScope(
                    key: Key('new_description'),
                    onWillPop: () => Future.sync(this.onWillPop),
                    child: EditDescriptionScreen(
                      store: store,
                      onSave: (String value) {
                        BlocProvider.of<CreateStoreBloc>(context).add(
                            ChangeStore(store: store.copy(description: value)));
                        _nextFormStep();
                      },
                    )),
                WillPopScope(
                  key: Key('new_phone_number'),
                  onWillPop: () => Future.sync(this.onWillPop),
                  child: EditPhoneNumberScreen(
                    store: store,
                    onSave: (String value) {
                      BlocProvider.of<CreateStoreBloc>(context).add(
                          ChangeStore(store: store.copy(phoneNumber: value)));
                      _nextFormStep();
                    },
                  ),
                ),
                WillPopScope(
                  key: Key('new_working_time'),
                  onWillPop: () => Future.sync(this.onWillPop),
                  child: EditWorkingTimeScreen(
                    store: store,
                    onSave: (int startedAt, endedAt) {
                      BlocProvider.of<CreateStoreBloc>(context).add(ChangeStore(
                          store: store.copy(
                              startedAt: startedAt, endedAt: endedAt)));
                      _nextFormStep();
                    },
                  ),
                ),
                WillPopScope(
                  key: Key('image_source'),
                  onWillPop: () => Future.sync(this.onWillPop),
                  child: EditImageSourceScreen(
                      store: store,
                      onSave: (File file) {
                        BlocProvider.of<CreateStoreBloc>(context)
                            .add(ChangeStore(store: store.copy(file: file)));
                        _nextFormStep();
                      }),
                ),
                WillPopScope(
                  key: Key('confirm_done'),
                  onWillPop: () => Future.sync(this.onWillPop),
                  child: ConfirmDoneScreen(
                      state: state,
                      store: store,
                      onCreateStore: () =>
                          BlocProvider.of<CreateStoreBloc>(context)
                              .add(CreateStore())),
                ),
              ];

              return Container(
                  child: PageView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: _forms.length,
                      controller: _formsPageViewController,
                      itemBuilder: (BuildContext context, int index) {
                        return _forms[index];
                      }));
            }))));
  }

  void _nextFormStep() {
    _formsPageViewController.nextPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  bool onWillPop() {
    if (_formsPageViewController.page.round() ==
        _formsPageViewController.initialPage) return true;

    _formsPageViewController.previousPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );

    return false;
  }
}
