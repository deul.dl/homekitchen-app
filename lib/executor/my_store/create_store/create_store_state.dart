import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import 'create_store.dart';

class CreateStoreState extends Equatable {
  CreateStoreState(
      {this.store, this.status = FormzStatus.pure, this.errorMessage})
      : super();

  final NewStore store;
  final FormzStatus status;
  final String errorMessage;

  @override
  List<Object> get props => ([store, status, errorMessage]);

  @override
  String toString() =>
      'CreateStoreState { store: $store, status: $status, errorMessage: $errorMessage }';

  CreateStoreState copyWith(
      {FormzStatus status, NewStore store, final String errorMessage}) {
    return CreateStoreState(
        status: status ?? this.status,
        errorMessage: errorMessage ?? this.errorMessage,
        store: store ?? this.store);
  }
}
