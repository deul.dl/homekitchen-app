import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/item_child.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class ConfirmDoneScreen extends StatefulWidget {
  ConfirmDoneScreen(
      {Key key,
      @required this.store,
      @required this.state,
      @required this.onCreateStore})
      : super(key: key);

  final StoreModel store;

  final Function onCreateStore;
  final CreateStoreState state;

  @override
  _ConfirmDoneState createState() => _ConfirmDoneState();
}

class _ConfirmDoneState extends State<ConfirmDoneScreen> {
  NewStore get store => widget.store as NewStore;

  get renderWorkingTime =>
      '${store.startTime.format(context)} - ${store.endTime.format(context)}';

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                color: Colors.white,
                alignment: Alignment.center,
                child: Image.file(
                  store.file,
                  height: 150,
                  width: 250,
                ),
              ),
              ItemChild(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    FlutterI18n.translate(
                        context, 'myStore.ConfirmCreateStore.StoreName'),
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  Text(store.name)
                ],
              )),
              ItemChild(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    FlutterI18n.translate(
                        context, 'myStore.ConfirmCreateStore.PhoneNumber'),
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  Text(store.phoneNumber)
                ],
              )),
              ItemChild(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    FlutterI18n.translate(
                        context, 'myStore.ConfirmCreateStore.WorkingTime'),
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  Text(renderWorkingTime)
                ],
              )),
              ItemChild(
                  child: Container(
                alignment: Alignment.topLeft,
                child: Text(store.description),
              )),
            ],
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              child: widget.state.status.isSubmissionInProgress
                  ? CircularProgressIndicator()
                  : ExpandedFlatButton(
                      label: FlutterI18n.translate(context,
                          'myStore.ConfirmCreateStore.Button.CreateStore'),
                      onPressed: () {
                        widget.onCreateStore();
                      },
                    )),
        ],
      ),
    );
  }
}
