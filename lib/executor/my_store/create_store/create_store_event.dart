import 'dart:async';
import 'dart:io';
import 'package:formz/formz.dart';

import 'package:meta/meta.dart';

import 'create_store.dart';

@immutable
abstract class CreateStoreEvent {
  Stream<CreateStoreState> applyAsync(
      {CreateStoreState currentState, CreateStoreBloc bloc});
}

class ChangeStore extends CreateStoreEvent {
  final NewStore store;

  ChangeStore({@required this.store});

  @override
  String toString() => 'ChangeStore { store: $store }';

  @override
  Stream<CreateStoreState> applyAsync(
      {CreateStoreState currentState, CreateStoreBloc bloc}) async* {
    await Future.delayed(Duration(microseconds: 200));
    yield CreateStoreState(store: store);
  }
}

class CreateStore extends CreateStoreEvent {
  CreateStore();

  @override
  String toString() => 'CreateStore {}';

  @override
  Stream<CreateStoreState> applyAsync(
      {CreateStoreState currentState, CreateStoreBloc bloc}) async* {
    try {
      yield CreateStoreState(
          store: currentState.store,
          status: FormzStatus.submissionInProgress,
          errorMessage: '');
      await bloc.storeRepository.create(currentState.store);
      yield CreateStoreState(
          store: currentState.store, status: FormzStatus.submissionSuccess);
    } on HttpException catch (err) {
      yield CreateStoreState(
          store: currentState.store,
          status: FormzStatus.submissionFailure,
          errorMessage: err?.message);
    }
  }
}
