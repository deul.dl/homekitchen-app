import 'package:flutter/material.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class DonePage extends StatelessWidget {
  DonePage({Key key, this.account, this.onNext}) : super(key: key);

  static const String routeName = '/create-store';

  final Account account;
  final Function onNext;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () =>
              Navigator.popUntil(context, ModalRoute.withName('/')),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 200),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.store,
                        color: Colors.green,
                        size: 120,
                      ),
                      Text(
                        'Successful',
                        style: TextStyle(
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            color: Colors.black54),
                      ),
                    ])),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                child: ExpandedFlatButton(
                    onPressed: () =>
                        Navigator.popUntil(context, ModalRoute.withName('/')),
                    label: 'Let go!'))
          ]),
    );
  }
}
