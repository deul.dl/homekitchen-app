import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/executor/my_store/my_store.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'home_view.dart';
import 'settings_view.dart';

class MyStoreScreen extends StatefulWidget {
  MyStoreScreen({Key key, this.tabController, this.store}) : super(key: key);
  final Store store;

  final TabController tabController;

  @override
  _MyStoreState createState() => _MyStoreState();
}

class _MyStoreState extends State<MyStoreScreen> {
  @override
  void initState() {
    BlocProvider.of<ItemsBloc>(context).add(InitItems());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MyStoreBloc, MyStoreState>(builder: (context, state) {
      if (state is MyStoreLoadSuccess) {
        return Container(
          color: Theme.of(context).backgroundColor,
          child: TabBarView(
            controller: widget.tabController,
            children: <Widget>[
              HomeView(
                store: state.store ?? widget.store,
              ),
              SettingsView(
                store: state.store ?? widget.store,
              )
            ],
          ),
        );
      } else if (state is MyStoreFailure) {
        return ErrorScreen(
            errorMessage: state.errorMessage,
            onReload: () => BlocProvider.of<MyStoreBloc>(context));
      } else {
        return Center(child: CircularProgressIndicator());
      }
    });
  }
}
