import 'dart:io';
import 'dart:async';

import 'package:meta/meta.dart';

import 'my_store.dart';

@immutable
abstract class MyStoreEvent {
  Stream<MyStoreState> applyAsync(
      {MyStoreState currentState, MyStoreBloc bloc});
}

class LoadStore extends MyStoreEvent {
  LoadStore();

  @override
  String toString() => 'LoadStore {}';

  @override
  Stream<MyStoreState> applyAsync(
      {MyStoreState currentState, MyStoreBloc bloc}) async* {
    try {
      print('store');
      yield MyStoreInProgress();
      final store = await bloc.storeRepository.getStore();
      print(store);
      yield MyStoreLoadSuccess(store as Store);
    } on HttpException catch (err) {
      yield MyStoreFailure(err?.message);
    }
  }
}

class SwitchMyStore extends MyStoreEvent {
  final String id;

  SwitchMyStore({@required this.id});

  @override
  String toString() => 'SwitchMyStore { id: $id }';

  @override
  Stream<MyStoreState> applyAsync(
      {MyStoreState currentState, MyStoreBloc bloc}) async* {
    try {
      final store = await bloc.storeRepository.switchStore();
      yield MyStoreLoadSuccess(store as Store);
    } on HttpException catch (err) {
      yield MyStoreFailure(err?.message);
    }
  }
}

class UploadImageOfStore extends MyStoreEvent {
  final File file;

  UploadImageOfStore({@required this.file});

  @override
  String toString() => 'UploadImageOfStore { $file: file }';

  @override
  Stream<MyStoreState> applyAsync(
      {MyStoreState currentState, MyStoreBloc bloc}) async* {
    try {
      yield MyStoreInProgress();
      final store = await bloc.storeRepository.updateImage(file);

      yield MyStoreLoadSuccess(store);
    } on HttpException catch (err) {
      yield MyStoreFailure(err?.message);
    }
  }
}

class UpdateMyStore extends MyStoreEvent {
  final Store store;

  UpdateMyStore(this.store);

  @override
  String toString() => 'UpdateMyStore { store: $store }';

  @override
  Stream<MyStoreState> applyAsync(
      {MyStoreState currentState, MyStoreBloc bloc}) async* {
    try {
      yield MyStoreInProgress();

      final newStore = await bloc.storeRepository.update(store);
      yield MyStoreLoadSuccess(newStore);
    } on HttpException catch (err) {
      yield MyStoreFailure(err?.message);
    }
  }
}
