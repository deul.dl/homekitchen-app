import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';

class CreateStoreScreen extends StatelessWidget {
  CreateStoreScreen({Key key, this.account, this.onNext}) : super(key: key);

  final Account account;
  final Function onNext;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      width: double.infinity,
      color: Theme.of(context).backgroundColor,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 200),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.store,
                        color: Colors.green,
                        size: 120,
                      ),
                      Text(
                        FlutterI18n.translate(
                            context, 'myStore.Onboarding.Description'),
                        style: TextStyle(
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            color: Colors.black54),
                      ),
                    ])),
            ExpandedFlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/create-store');
                },
                label: FlutterI18n.translate(
                    context, 'myStore.Onboarding.OpenStore'))
          ]),
    ));
  }
}
