import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/custom_form_input.dart';
import 'package:home_kitchen/widgets/form_widget_select.dart';
import 'package:home_kitchen/widgets/expanded_flat_button.dart';

import 'package:home_kitchen/executor/my_store/my_store.dart';
import 'package:home_kitchen/executor/item_categories/item_category.dart';

import 'package:home_kitchen/executor/models/models.dart';

class EditItemScreen extends StatefulWidget {
  EditItemScreen({Key key, this.item, bool isEdit, this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final Item item;
  final Function onSave;

  @override
  _EditItemState createState() => _EditItemState();
}

class _EditItemState extends State<EditItemScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _name, _description, _categoryId;
  num _price, _cost;

  Item get item => widget.item;

  @override
  void initState() {
    if (item != null) {
      setState(() => _categoryId = item.categoryId.toString());
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> _args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    if (_args != null && _args['categoryId'] != null) {
      setState(() => _categoryId = _args['categoryId'].toString());
    }

    return ConstrainedBox(
        constraints: BoxConstraints(
          minHeight:
              MediaQuery.of(context).size.height - (widget.isEdit ? 55 : 110),
        ),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    CustomFormInput(
                      //
                      label: FlutterI18n.translate(
                          context, 'items.EditItem.Form.Name.Label'),
                      initialValue: item == null ? '' : item.name,
                      onSaved: (val) => _name = val,
                      validator: (val) {
                        return val.trim().isEmpty
                            ? FlutterI18n.translate(
                                context, 'items.EditItem.Form.Name.Error')
                            : null;
                      },
                    ),
                    CategorySelect(
                      categoryID: _categoryId,
                      onChanged: (val) => setState(() => _categoryId = val),
                    ),
                    CustomFormInput(
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))
                      ],
                      keyboardType: TextInputType.number,
                      label: FlutterI18n.translate(
                          context, 'items.EditItem.Form.Price.Label'),
                      initialValue: item == null ? '' : item.price.toString(),
                      onSaved: (val) => _price = num.parse(val),
                      validator: (val) {
                        return val.trim().isEmpty
                            ? FlutterI18n.translate(
                                context, 'items.EditItem.Form.Price.Error')
                            : null;
                      },
                    ),
                    CustomFormInput(
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))
                      ],
                      keyboardType: TextInputType.number,
                      label: FlutterI18n.translate(
                          context, 'items.EditItem.Form.Cost.Label'),
                      initialValue: item == null ? '' : item.cost.toString(),
                      onSaved: (val) => _cost = num.parse(val),
                      validator: (val) {
                        return val.trim().isEmpty
                            ? FlutterI18n.translate(
                                context, 'items.EditItem.Form.Cost.Error')
                            : null;
                      },
                    ),
                    CustomFormInput(
                      label: FlutterI18n.translate(
                          context, 'items.EditItem.Form.Description.Label'),
                      initialValue: item == null ? '' : item.description,
                      onSaved: (val) => _description = val,
                      maxLines: 5,
                      validator: (val) {
                        return val.trim().isEmpty
                            ? FlutterI18n.translate(context,
                                'items.EditItem.Form.Description.Error')
                            : null;
                      },
                    ),
                  ],
                ),
              ),
              LimitedBox(
                  maxHeight: 200,
                  child: ExpandedFlatButton(
                    label: widget.isEdit
                        ? FlutterI18n.translate(context, 'common.Save')
                        : FlutterI18n.translate(context, 'common.Next'),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();

                        widget.onSave(Item(
                            uuid: widget.isEdit ? item.uuid : null,
                            storeUUID: item?.storeUUID ??
                                (_args['store'] as Store).uuid,
                            name: _name,
                            description: _description,
                            price: _price,
                            cost: _cost,
                            categoryId: int.parse(_categoryId)));
                      }
                    },
                  ))
            ],
          ),
        ));
  }
}

class CategorySelect extends StatelessWidget {
  CategorySelect({
    @required this.onChanged,
    @required this.categoryID,
  });
  final Function(dynamic) onChanged;
  final String categoryID;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ItemCategoryBloc, ItemCategoryState>(
        builder: (BuildContext context, ItemCategoryState state) {
      return FormWidgetSelect(
          label: FlutterI18n.translate(
              context, 'items.EditItem.Form.Category.Label'),
          value: categoryID,
          onChanged: onChanged,
          validator: (val) {
            return val == null
                ? FlutterI18n.translate(
                    context, 'items.EditItem.Form.Category.Error')
                : null;
          },
          options: (state as ItemCategoryLoadSuccess)
              .categories
              .map((Category category) => DropdownMenuItem<String>(
                    key: Key(category.id.toString()),
                    child: Text(
                      FlutterI18n.translate(
                          context, 'category.${category.name}'),
                    ),
                    value: category.id.toString(),
                  ))
              .toList());
    });
  }
}
