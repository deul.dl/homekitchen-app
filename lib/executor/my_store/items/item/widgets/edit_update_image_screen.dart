import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/image_upload.dart';
import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/executor/models/models.dart';

class EditUpdateImageScreen extends StatefulWidget {
  EditUpdateImageScreen({Key key, this.item, bool isEdit, this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final Item item;
  final bool isEdit;
  final Function(File file) onSave;
  @override
  _EditUpdateImageState createState() => _EditUpdateImageState();
}

class _EditUpdateImageState extends State<EditUpdateImageScreen> {
  File _file;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        ImageUpload(
            initialValue: widget.isEdit ? _file : null,
            height: 200,
            placeholderImage: widget.item?.imageSource == null
                ? null
                : widget.item.imageSource,
            description: FlutterI18n.translate(
                context, 'items.EditItem.ImageSource.Description'),
            onUploaded: (File file) {
              setState(() {
                _file = file;
              });
            }),
        ExpandedFlatButton(
          label: FlutterI18n.translate(context, 'common.Save'),
          onPressed: () {
            widget.onSave(_file);
          },
        )
      ],
    ));
  }
}
