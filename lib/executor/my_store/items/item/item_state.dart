import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class ItemState extends Equatable {
  final List propss;

  ItemState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class ItemLoadInProgress extends ItemState {
  ItemLoadInProgress();

  @override
  String toString() => 'ItemLoadInProgress {}';
}

class ItemLoadSuccess extends ItemState {
  ItemLoadSuccess();

  @override
  String toString() => 'ItemsLoadSuccess {}';
}

class ItemFailure extends ItemState {
  final String errorMessage;

  ItemFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ItemFailure { errorMessage: $errorMessage }';
}
