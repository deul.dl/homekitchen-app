import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/my_store/items/items.dart';

import 'new_item.dart';

class NewItemPage extends StatefulWidget {
  static const String routeName = '/new-item';

  @override
  _NewItemState createState() => _NewItemState();
}

class _NewItemState extends State<NewItemPage> {
  _NewItemState();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(FlutterI18n.translate(context, 'items.NewItem.Title'))),
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: BlocListener<NewItemBloc, NewItemState>(
                listener: (BuildContext context, NewItemState state) {
              if (state is NewLoadSuccess) {
                Navigator.of(context).pushReplacementNamed('/edit-item',
                    arguments: {
                      'itemUUID': state.item.uuid,
                      'previousRoute': '/new-item'
                    });
              }
              if (state is NewItemFailure) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
              }
            }, child: BlocBuilder<NewItemBloc, NewItemState>(
                    builder: (BuildContext context, NewItemState state) {
              return EditItemScreen(
                onSave: (Item item) {
                  BlocProvider.of<NewItemBloc>(context)
                      .add(SubmitItem(item: item));
                },
              );
            }))));
  }
}
