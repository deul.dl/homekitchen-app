import 'package:equatable/equatable.dart';

import 'new_item.dart';

abstract class NewItemState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  NewItemState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class NewLoadInProgress extends NewItemState {
  NewLoadInProgress();

  @override
  String toString() => 'NewLoadInProgress {}';
}

class NewLoadSuccess extends NewItemState {
  final Item item;

  NewLoadSuccess({this.item}) : super([item]);

  @override
  String toString() => 'NewLoadSuccess { item: $item }';
}

class NewItemFailure extends NewItemState {
  final String errorMessage;

  NewItemFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'NewItemFailure { errorMessage: $errorMessage }';
}
