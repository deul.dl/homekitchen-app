import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:home_kitchen/executor/my_store/items/items.dart';

import 'new_item.dart';

class NewItemBloc extends Bloc<NewItemEvent, NewItemState> {
  final ItemsRepository itemsRepository;
  NewItemBloc({this.itemsRepository}) : super(null);

  @override
  Stream<NewItemState> mapEventToState(
    NewItemEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
