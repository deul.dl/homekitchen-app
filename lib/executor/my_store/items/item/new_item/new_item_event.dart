import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/executor/my_store/items/items.dart';

import 'new_item.dart';

@immutable
abstract class NewItemEvent {
  Stream<NewItemState> applyAsync(
      {NewItemState currentState, NewItemBloc bloc});
}

class SubmitItem extends NewItemEvent {
  SubmitItem({@required this.item});
  final Item item;

  @override
  String toString() => 'SubmitItem { item: $item }';

  @override
  Stream<NewItemState> applyAsync(
      {NewItemState currentState, NewItemBloc bloc}) async* {
    try {
      yield NewLoadInProgress();
      final Item createdItem = await bloc.itemsRepository.add(item);
      yield NewLoadSuccess(item: createdItem);
    } catch (_) {
      yield NewItemFailure(_?.toString());
    }
  }
}
