import 'dart:io';

import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'item.dart';

@immutable
abstract class ItemEvent {
  Stream<ItemState> applyAsync({ItemState currentState, ItemBloc bloc});
}

class UpdateItem extends ItemEvent {
  final Item item;

  UpdateItem(this.item);

  @override
  String toString() => 'UpdateItem { item: $item }';

  @override
  Stream<ItemState> applyAsync({ItemState currentState, ItemBloc bloc}) async* {
    try {
      yield ItemLoadInProgress();
      await bloc.itemsRepository.update(item);
      yield ItemLoadSuccess();
    } catch (_, stackTrace) {
      developer.log('$_', name: 'UpdateItem', error: _, stackTrace: stackTrace);
      yield ItemFailure(_?.toString());
    }
  }
}

class UploadItemImage extends ItemEvent {
  final String uuid;
  final File file;

  UploadItemImage({@required this.uuid, @required this.file});

  @override
  String toString() => 'UploadItemImage { uuid: $uuid, file: $file }';

  @override
  Stream<ItemState> applyAsync({ItemState currentState, ItemBloc bloc}) async* {
    try {
      yield ItemLoadInProgress();
      await bloc.itemsRepository.uploadImage(uuid, file);
      yield ItemLoadSuccess();
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'UploadItemImage', error: _, stackTrace: stackTrace);
      yield ItemFailure(_?.toString());
    }
  }
}
