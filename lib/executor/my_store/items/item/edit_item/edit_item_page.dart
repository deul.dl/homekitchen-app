import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/my_store/items/items.dart';
import 'package:home_kitchen/theme/colors.dart';

import 'edit_item.dart';

class EditItemPage extends StatefulWidget {
  EditItemPage({Key key}) : super(key: key);

  static const String routeName = '/edit-item';

  _EditItemState createState() => _EditItemState();
}

class _EditItemState extends State<EditItemPage>
    with SingleTickerProviderStateMixin {
  static TabController _tabItemsController;

  @override
  void initState() {
    _tabItemsController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> arguments =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final state = BlocProvider.of<ItemsBloc>(context).state;

    if (state is ItemsInProgreess) {
      return Container(
          color: HomeKitchenColors.whiteLilac,
          alignment: Alignment.center,
          child: CircularProgressIndicator());
    }

    final Item item = (state as ItemsLoadSuccess).items.firstWhere(
        (element) => element.uuid == arguments['itemUUID'],
        orElse: () => null);

    if (arguments['previousRoute'] != null) {
      _tabItemsController.animateTo(1);
    }

    return Scaffold(
        appBar: AppBar(
            title: Text(item == null ? '' : item.name),
            bottom: TabBar(
                controller: _tabItemsController,
                tabs: [
                  Tab(
                      text: FlutterI18n.translate(
                          context, 'items.EditItem.Tab.FirstTab')),
                  Tab(
                      text: FlutterI18n.translate(
                          context, 'items.EditItem.Tab.SecondTab'))
                ],
                isScrollable: false)),
        body: _body(context, item));
  }

  Widget _body(BuildContext context, Item item) {
    return BlocListener<ItemBloc, ItemState>(
        listener: (BuildContext context, ItemState state) {
          if (state is ItemLoadSuccess) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text('Updated successful!')));
          }
          if (state is ItemFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
          }
        },
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: TabBarView(controller: _tabItemsController, children: [
              EditItemScreen(
                item: item,
                isEdit: true,
                onSave: (Item item) =>
                    BlocProvider.of<ItemBloc>(context).add(UpdateItem(item)),
              ),
              EditUpdateImageScreen(
                item: item,
                onSave: (file) {
                  BlocProvider.of<ItemBloc>(context)
                      .add(UploadItemImage(uuid: item.uuid, file: file));
                },
              )
            ])));
  }

  @override
  void dispose() {
    _tabItemsController.dispose();
    super.dispose();
  }
}
