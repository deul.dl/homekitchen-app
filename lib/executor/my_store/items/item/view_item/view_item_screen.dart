import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/executor/models/models.dart';

import 'package:home_kitchen/executor/my_store/items/items.dart';

class ViewItemScreen extends StatefulWidget {
  ViewItemScreen({Key key, this.item}) : super(key: key);

  final Item item;

  @override
  _ViewItemState createState() => _ViewItemState();
}

class _ViewItemState extends State<ViewItemScreen> {
  _ViewItemState();

  Item get item => widget.item;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (item != null
          ? ((item.imageSource.isNotEmpty
              ? item.imageSource
              : DotEnv().env['PLACEHOLDER_IMAGE']))
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const padding = EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0);
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return Container(
        width: double.infinity,
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
            child: CachedNetworkImage(
              imageUrl: urlImage,
              fit: BoxFit.cover,
              width: double.infinity,
              height: 400,
              placeholder: (context, url) =>
                  Image.asset('assets/images/placeholder.jpg'),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Padding(
            padding: padding,
            child: Row(children: [
              Text(
                formatCurrency.printPrice(item.price),
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )
            ]),
          ),
          Padding(
            padding: padding,
            child: Text(
              item.name,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
            padding: padding,
            child: Text(item.description),
          ),
          Padding(
              padding: padding,
              child: Row(
                children: <Widget>[
                  Text(FlutterI18n.translate(context, 'items.Publicity')),
                  Switch(
                    onChanged: (bool isTurnedOn) {
                      BlocProvider.of<ItemsBloc>(context).add(SwitchItemOfItems(
                          uuid: item.uuid, isSwitched: isTurnedOn));
                    },
                    value: item.isSwitchedOn,
                    activeColor: Theme.of(context).primaryColor,
                  )
                ],
              ))
        ]));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
