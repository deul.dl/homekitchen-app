import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/executor/models/models.dart';
import 'package:home_kitchen/executor/my_store/items/items.dart';

import 'view_item_screen.dart';

class ViewItemPage extends StatefulWidget {
  ViewItemPage();

  static const String routeName = '/view-item';

  @override
  _ViewItemState createState() => _ViewItemState();
}

class _ViewItemState extends State<ViewItemPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> _args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;

    return BlocBuilder<ItemsBloc, ItemsState>(builder: (context, state) {
      final Item item = (state as ItemsLoadSuccess).items.firstWhere(
          (element) => element.uuid == _args['itemUUID'],
          orElse: () => null);

      return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: Text(item.name),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () => Navigator.of(context).pushNamed('/edit-item',
                    arguments: {'itemUUID': item.uuid}))
          ],
        ),
        body: ViewItemScreen(
          item: item,
        ),
      );
    });
  }
}
