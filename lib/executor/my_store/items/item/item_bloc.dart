import 'dart:async';
import 'dart:developer' as developer;
import 'package:bloc/bloc.dart';

import 'package:meta/meta.dart';

import 'item.dart';

class ItemBloc extends Bloc<ItemEvent, ItemState> {
  ItemBloc({@required this.itemsRepository}) : super(null);

  final ItemsRepository itemsRepository;

  @override
  Stream<ItemState> mapEventToState(
    ItemEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'ItemBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
