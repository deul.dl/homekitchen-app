import 'package:flutter/material.dart';

import 'package:flutter_i18n/flutter_i18n.dart';

import 'items_screen.dart';

class ItemsPage extends StatelessWidget {
  ItemsPage({Key key}) : super(key: key);

  static const String routeName = '/my-items';

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> _args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final category = _args['category'];
    return Scaffold(
        appBar: AppBar(
          title:
              Text(FlutterI18n.translate(context, 'category.${category.name}')),
        ),
        body: ItemsScreen(),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () => Navigator.of(context).pushNamed('/new-item',
                    arguments: {
                      'categoryId': category.id,
                      'store': _args['store']
                    })));
  }
}
