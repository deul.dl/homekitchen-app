import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/executor/my_store/my_store.dart';
import 'package:home_kitchen/theme/colors.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'widgets/list_item_card.dart';

class ItemsScreen extends StatefulWidget {
  ItemsScreen({Key key}) : super(key: key);

  @override
  _ItemsState createState() => _ItemsState();
}

class _ItemsState extends State<ItemsScreen> {
  _ItemsState();

  @override
  Widget build(BuildContext context) {
    _load();

    return Container(
        width: double.infinity,
        color: HomeKitchenColors.whiteLilac,
        child: BlocListener<ItemsBloc, ItemsState>(listener: (context, state) {
          if (state is ItemsLoadSuccess && state.errorMessage.isNotEmpty) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(content: Text(state.errorMessage)),
              );
          }
        }, child: BlocBuilder<ItemsBloc, ItemsState>(
            builder: (BuildContext context, ItemsState state) {
          if (state is ItemsLoadSuccess) {
            final _arg = ModalRoute.of(context).settings.arguments
                as Map<String, dynamic>;
            final category = _arg['category'];

            if (state.items.isEmpty) {
              return Container();
            }

            final items = state.items
                .where(
                  (item) => item.categoryId == category.id,
                )
                .toList();

            return ListView.separated(
                itemBuilder: (BuildContext context, int index) {
                  return ListItemCard(
                      item: items[index],
                      onTap: () {
                        Navigator.of(context).pushNamed('/item',
                            arguments: {'item': items[index].uuid});
                      });
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 1),
                itemCount: items.length);
          } else if (state is ItemsFailure) {
            return ErrorScreen(
                errorMessage: state.errorMessage, onReload: _load);
          } else {
            return Container(
              alignment: Alignment.center,
              color: Colors.white,
              child: CircularProgressIndicator(),
            );
          }
        })));
  }

  void _load() {
    final _args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final categoryId = _args['categoryId'];

    BlocProvider.of<ItemsBloc>(context).add(LoadItems(categoroId: categoryId));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
