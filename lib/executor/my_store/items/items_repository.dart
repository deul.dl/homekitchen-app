import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:home_kitchen/client/client.dart';

import 'items.dart';

abstract class Repository {
  final _controller = StreamController<Item>();

  Stream<Item> get items async* {
    yield* _controller.stream;
  }

  Future<dynamic> loadData(int categoryId);

  Future<dynamic> add(Item item);

  Future<dynamic> update(Item item);

  Future<dynamic> uploadImage(String uuid, File file);

  Future<dynamic> turnOff(String uuid);

  Future<dynamic> turnOn(String uuid);
}

class ItemsRepository extends Repository {
  ItemsRepository(client) : this._client = client;

  final WebClient _client;

  @override
  Future<dynamic> loadData(int categoryId) async {
    return _client
        .get('executor/items')
        .then((res) => res["data"]["items"])
        .then((items) => items == null
            ? null
            : (items as List).map((item) => Item.fromJson(item)).toList());
  }

  @override
  Future<dynamic> add(Item item) async {
    return _client
        .post('executor/items/add', {
          'name': item.name,
          'description': item.description,
          'price': item.price,
          'cost': item.cost,
          'categoryId': item.categoryId,
          'isTurnOn': true,
          'storeUUID': item.storeUUID,
        })
        .then((res) => Item.fromJson(res['data']))
        .then((item) {
          _controller.add(item);
          return item;
        });
  }

  @override
  Future<dynamic> update(Item item) async {
    return _client
        .put('executor/items/${item.uuid}', {
          'name': item.name,
          'description': item.description,
          'price': item.price,
          'cost': item.cost,
          'categoryId': item.categoryId,
          'isTurnOn': true,
        })
        .then((res) => res['data'])
        .then((item) {
          if (item != null) {
            _controller.add(Item.fromJson(item));
          }
          return item;
        });
  }

  @override
  Future<dynamic> uploadImage(String uuid, File file) async {
    return _client
        .multipat()
        .put('executor/items/$uuid/upload', null, file)
        .then((res) => res['data'])
        .then((item) {
      if (item != null) {
        _controller.add(Item.fromJson(item));
      }
      return item;
    });
  }

  @override
  Future<dynamic> turnOff(String uuid) async {
    return _client
        .put('executor/items/$uuid/switch', null)
        .then((res) => res['data'])
        .then((item) {
      if (item != null) {
        _controller.add(Item.fromJson(item));
      }
      return item;
    });
  }

  @override
  Future<dynamic> turnOn(String uuid) async {
    return _client
        .put('executor/items/$uuid/switch', null)
        .then((res) => res['data'])
        .then((item) {
      if (item != null) {
        _controller.add(Item.fromJson(item));
      }
      return item;
    });
  }
}
