import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'items.dart';

@immutable
abstract class ItemsEvent {
  Stream<ItemsState> applyAsync({ItemsState currentState, ItemsBloc bloc});
}

class InitItems extends ItemsEvent {
  @override
  Stream<ItemsState> applyAsync(
      {ItemsState currentState, ItemsBloc bloc}) async* {
    yield ItemsInProgreess();
  }
}

class LoadItems extends ItemsEvent {
  final int categoroId;

  LoadItems({@required this.categoroId});

  @override
  String toString() => 'LoadItems { categoroId: $categoroId }';

  @override
  Stream<ItemsState> applyAsync(
      {ItemsState currentState, ItemsBloc bloc}) async* {
    try {
      final items = await bloc.itemsRepository.loadData(categoroId);

      yield ItemsLoadSuccess(items: (items as List<Item>) ?? []);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'LoadItems', error: _, stackTrace: stackTrace);
      yield ItemsFailure(_?.toString());
    }
  }
}

class UpdateItems extends ItemsEvent {
  final Item item;

  UpdateItems(this.item);

  @override
  String toString() => 'UpdateItems { item: $item }';

  @override
  Stream<ItemsState> applyAsync(
      {ItemsState currentState, ItemsBloc bloc}) async* {
    try {
      if (currentState is ItemsInProgreess) {
        yield ItemsLoadSuccess(items: [item]);
        return;
      }

      final oldItems = (currentState as ItemsLoadSuccess).items;
      final foundItem = (currentState as ItemsLoadSuccess).items.firstWhere(
          (element) => element.uuid == item.uuid,
          orElse: () => null);
      if (foundItem == null) {
        final List<Item> items = List.from(oldItems)..add(item);
        yield ItemsLoadSuccess(items: items);
      } else {
        final items =
            oldItems.map((e) => e.uuid == item.uuid ? item : e).toList();

        yield ItemsLoadSuccess(items: items);
      }
    } catch (_) {
      yield ItemsLoadSuccess(
          items: (currentState as ItemsLoadSuccess).items,
          errorMessage: _?.toString());
    }
  }
}

class SwitchItemOfItems extends ItemsEvent {
  final String uuid;
  final bool isSwitched;

  SwitchItemOfItems({@required this.uuid, @required this.isSwitched});

  @override
  String toString() => 'SwitchItem { uuid: $uuid, isSwitched: $isSwitched }';

  @override
  Stream<ItemsState> applyAsync(
      {ItemsState currentState, ItemsBloc bloc}) async* {
    try {
      if (isSwitched) {
        await bloc.itemsRepository.turnOff(uuid);
      } else {
        await bloc.itemsRepository.turnOn(uuid);
      }
    } catch (_) {
      yield ItemsLoadSuccess(
          items: (currentState as ItemsLoadSuccess).items,
          errorMessage: _?.toString());
    }
  }
}
