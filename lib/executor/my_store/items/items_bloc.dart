import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'items.dart';

class ItemsBloc extends Bloc<ItemsEvent, ItemsState> {
  ItemsBloc({@required this.itemsRepository}) : super(ItemsInProgreess()) {
    _itemSubscription?.cancel();
    _itemSubscription = itemsRepository.items.listen((Item item) {
      add(UpdateItems(item));
    });
  }

  final ItemsRepository itemsRepository;
  StreamSubscription<Item> _itemSubscription;

  @override
  Stream<ItemsState> mapEventToState(
    ItemsEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'ItemsBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() async {
    await _itemSubscription?.cancel();
    await super.close();
  }
}
