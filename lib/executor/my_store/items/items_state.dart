import 'package:equatable/equatable.dart';

import 'item/item.dart';

abstract class ItemsState extends Equatable {
  final List propss;

  ItemsState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class ItemsInProgreess extends ItemsState {
  ItemsInProgreess();

  @override
  String toString() => 'ItemsInProgreess {}';
}

class ItemsLoadSuccess extends ItemsState {
  final List<Item> items;
  final String errorMessage;

  ItemsLoadSuccess({this.items, this.errorMessage = ''})
      : super([items, errorMessage]);

  @override
  String toString() =>
      'ItemsLoadSuccess { items: $items, errorMessage: $errorMessage }';
}

class ItemsFailure extends ItemsState {
  final String errorMessage;

  ItemsFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ItemsFailure { errorMessage: $errorMessage }';
}
