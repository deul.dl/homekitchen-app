import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/models/item.dart';

import 'package:home_kitchen/executor/my_store/items/items.dart';

class ListItemCard extends StatefulWidget {
  ListItemCard({@required this.item, @required this.onTap});

  final Item item;
  final Function onTap;
  @override
  _ListItemCardState createState() => _ListItemCardState();
}

class _ListItemCardState extends State<ListItemCard> {
  Item get item => widget.item;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (item.imageSource.isEmpty
          ? DotEnv().env['PLACEHOLDER_IMAGE']
          : item.imageSource);

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return Card(
        elevation: 0,
        child: Padding(
            padding: EdgeInsets.only(top: 10),
            child: Row(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: urlImage,
                  height: 100,
                  width: 100,
                  placeholder: (context, url) =>
                      Image.asset('assets/images/placeholder.jpg'),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
                SizedBox(width: 20),
                Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        item.name,
                        style: TextStyle(
                          fontSize: 21,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                          width: MediaQuery.of(context).size.width - 135,
                          child: Text(
                            item.description,
                            style: TextStyle(color: Colors.black54),
                            maxLines: 8,
                            overflow: TextOverflow.ellipsis,
                          )),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        formatCurrency.printPrice(item.price),
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Row(
                        children: <Widget>[
                          TextButton.icon(
                            style: TextButton.styleFrom(
                              backgroundColor: Colors.black,
                            ),
                            onPressed: () => Navigator.of(context).pushNamed(
                                '/edit-item',
                                arguments: {'itemUUID': item.uuid}),
                            label: Text(
                              FlutterI18n.translate(context, 'common.Edit'),
                              style: TextStyle(color: Colors.white),
                            ),
                            icon: Icon(
                              Icons.edit,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          TextButton.icon(
                            style: TextButton.styleFrom(
                              backgroundColor: Colors.black,
                            ),
                            onPressed: () => Navigator.of(context).pushNamed(
                                '/view-item',
                                arguments: {'itemUUID': item.uuid}),
                            label: Text(
                              FlutterI18n.translate(context, 'common.View'),
                              style: TextStyle(color: Colors.white),
                            ),
                            icon: Icon(
                              Icons.link,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(FlutterI18n.translate(
                              context, 'items.Publicity')),
                          Switch(
                              value: item.isSwitchedOn,
                              activeColor: Theme.of(context).primaryColor,
                              onChanged: (bool isTurnedOn) {
                                BlocProvider.of<ItemsBloc>(context).add(
                                    SwitchItemOfItems(
                                        uuid: item.uuid,
                                        isSwitched: isTurnedOn));
                              })
                        ],
                      )
                    ])
              ],
            )));
  }
}
