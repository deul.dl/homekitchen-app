import 'dart:io';
import 'dart:async';
import 'dart:core';

import 'package:home_kitchen/client/client.dart';
import 'package:rxdart/rxdart.dart';

import 'my_store.dart';

abstract class StoreRepository {
  Future<dynamic> getStore();

  Future<dynamic> create(NewStore store);

  Future<dynamic> update(Store store);

  Future<dynamic> switchStore();

  Future<dynamic> updateImage(File file);

  Stream<Store> storeStream();
}

class MyStoreRepository implements StoreRepository {
  MyStoreRepository({client})
      : this._client = client,
        this._subject = BehaviorSubject<Store>();

  final WebClient _client;
  final BehaviorSubject<Store> _subject;

  @override
  Stream<Store> storeStream() async* {
    yield* _subject.stream;
  }

  @override
  Future<dynamic> getStore() async {
    return _client
        .get('executor/stories/store')
        .then((res) => Store.fromJson(res['data']['store']))
        .then((store) {
      _subject.add(store);
      return store;
    }).catchError((error) => null);
  }

  @override
  Future<dynamic> create(NewStore store) async {
    return _client
        .multipat()
        .post(
            'executor/stories/create',
            {
              'name': store.name,
              'description': store.description,
              'phoneNumber': store.phoneNumber,
              'startedAt': store.startedAt.toString(),
              'endedAt': store.endedAt.toString(),
            },
            store.file)
        .then((res) => res['data']['store'])
        .then((store) {
      _subject.add(Store.fromJson(store));

      return Store.fromJson(store);
    });
  }

  @override
  Future<dynamic> switchStore() async {
    return _client
        .put('executor/stories/store/switch', null)
        .then((res) => res['data']['store'])
        .then((store) {
      _subject.add(Store.fromJson(store));

      return Store.fromJson(store);
    });
  }

  @override
  Future<dynamic> update(Store store) async {
    return _client
        .put('executor/stories/store', {
          'name': store.name,
          'description': store.description,
          'phoneNumber': store.phoneNumber,
          'isTurnOn': store.isTurnedOn,
          'startedAt': store.startedAt,
          'endedAt': store.endedAt
        })
        .then((res) => res['data']['store'])
        .then((store) {
          _subject.add(Store.fromJson(store));

          return Store.fromJson(store);
        });
  }

  @override
  Future<dynamic> updateImage(File file) async {
    return _client
        .multipat()
        .put('executor/stories/store/upload', null, file)
        .then((res) => res['data']['store'])
        .then((store) => Store.fromJson(store))
        .then((store) {
      return store;
    });
  }
}
