import 'my_store.dart';

class ItemsScreenArguments {
  final List<Item> items;

  ItemsScreenArguments({this.items});
}
