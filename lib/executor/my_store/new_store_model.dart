import 'dart:io';

import 'package:flutter/material.dart';
import 'package:home_kitchen/executor/models/models.dart';

class NewStore extends StoreModel {
  final String name;
  final String phoneNumber;
  final String description;
  final int startedAt;
  final int endedAt;
  final File file;

  NewStore(
      {this.file,
      this.name,
      this.phoneNumber,
      this.description = '',
      this.startedAt,
      this.endedAt});

  @override
  List<Object> get props =>
      [file, name, phoneNumber, description, startedAt, endedAt];

  TimeOfDay get startTime => convertTimeOfDayFromSeconds(startedAt);

  TimeOfDay get endTime => convertTimeOfDayFromSeconds(endedAt);

  NewStore copy(
      {String name,
      String description,
      String phoneNumber,
      int startedAt,
      int endedAt,
      File file}) {
    return NewStore(
      name: name ?? this.name,
      description: description ?? this.description,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      file: file ?? this.file,
      startedAt: startedAt ?? this.startedAt,
      endedAt: endedAt ?? this.endedAt,
    );
  }
}
