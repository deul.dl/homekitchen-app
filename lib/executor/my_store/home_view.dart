import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_kitchen/addresses/addresses.dart';

import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:home_kitchen/executor/item_categories/item_category.dart';

class HomeView extends StatelessWidget {
  HomeView({Key key, this.store}) : super(key: key);

  final Store store;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ItemCategoryBloc, ItemCategoryState>(
        builder: (BuildContext context, ItemCategoryState state) {
      if (state is ItemCategoryLoadSuccess) {
        return Container(
          child: ListView.separated(
            itemCount: state.categories.length,
            separatorBuilder: (context, index) => Divider(
              height: 1,
            ),
            itemBuilder: (BuildContext context, int index) {
              final Category category = state.categories[index];

              return Container(
                  color: Colors.white,
                  child: ListTile(
                    onTap: () => Navigator.of(context).pushNamed('/my-items',
                        arguments: {'category': category, 'store': store}),
                    title: Text(FlutterI18n.translate(
                        context, 'category.${category.name}')),
                    trailing: Wrap(
                      children: [Icon(Icons.chevron_right)],
                    ),
                  ));
            },
          ),
        );
      } else if (state is ItemCategoryFailure) {
        return ErrorScreen(
            errorMessage: state.errorMessage,
            onReload: () => BlocProvider.of<ItemCategoryBloc>(context)
                .add(LoadCategories()));
      } else {
        return Container(
          alignment: Alignment.center,
          color: Colors.white,
          child: CircularProgressIndicator(),
        );
      }
    });
  }
}
