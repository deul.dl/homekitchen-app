import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/my_store/edit_store/edit_image_source/edit_image_source.dart';
import 'package:home_kitchen/executor/my_store/edit_store/edit_name/edit_name.dart';
import 'package:home_kitchen/executor/my_store/edit_store/edit_phone_number/edit_phone_number.dart';
import 'package:home_kitchen/executor/my_store/edit_store/edit_description/edit_description.dart';
import 'package:home_kitchen/executor/my_store/edit_store/edit_working_time/edit_working_time_page.dart';

import 'widgets/store_image_upload.dart';
import 'my_store.dart';

class SettingsView extends StatelessWidget {
  SettingsView({Key key, this.store}) : super(key: key);
  final Store store;

  @override
  Widget build(BuildContext context) {
    return BlocListener<MyStoreBloc, MyStoreState>(
        listener: (context, state) {
          if (state is MyStoreFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(content: Text(state.errorMessage)),
              );
          }
        },
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 15),
                child: StoreImageUpload(
                    store: store,
                    onUpload: () => Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                EditImageSourcePage(
                                  store: store,
                                )))),
              ),
              Container(
                  color: Colors.white,
                  child: ListTile(
                    tileColor: Colors.white,
                    shape: Border(
                      bottom: BorderSide(width: 2, color: Colors.black),
                    ),
                    title: Text(
                      FlutterI18n.translate(
                          context, 'myStore.Settings.StoreName'),
                    ),
                    trailing: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: <Widget>[
                        Text(store.name, style: TextStyle(fontSize: 14)),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.chevron_right,
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => EditNamePage(
                              store: store,
                            ))),
                  )),
              Container(
                  color: Colors.white,
                  child: ListTile(
                    tileColor: Colors.white,
                    shape: Border(
                      bottom: BorderSide(width: 1, color: Colors.black54),
                    ),
                    title: Text(
                      FlutterI18n.translate(
                          context, 'myStore.Settings.PhoneNumber'),
                    ),
                    trailing: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: <Widget>[
                        Text(store.phoneNumber, style: TextStyle(fontSize: 14)),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.chevron_right,
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => EditPhoneNumberPage(
                              store: store,
                            ))),
                  )),
              Container(
                  color: Colors.white,
                  child: ListTile(
                    shape: Border(
                      bottom: BorderSide(width: 1, color: Colors.black54),
                    ),
                    tileColor: Colors.white,
                    title: Text(
                      FlutterI18n.translate(
                          context, 'myStore.Settings.WorkingTime'),
                    ),
                    trailing: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: <Widget>[
                        Text(
                            '${store.startTime.format(context)} - ${store.endTime.format(context)}',
                            style: TextStyle(fontSize: 14)),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          Icons.chevron_right,
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => EditWorkingTimePage(
                              store: store,
                            ))),
                  )),
              Container(
                  color: Colors.white,
                  child: ListTile(
                    tileColor: Colors.white,
                    shape: Border(
                      bottom: BorderSide(width: 1, color: Colors.black54),
                    ),
                    title: Text(
                      FlutterI18n.translate(context, 'myStore.Settings.TurnOn'),
                    ),
                    trailing: Switch(
                      value: store.isTurnedOn,
                      onChanged: (bool isTurnedOn) {
                        BlocProvider.of<MyStoreBloc>(context)
                            .add(SwitchMyStore(id: store.uuid));
                      },
                      activeColor: Theme.of(context).primaryColor,
                    ),
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => EditPhoneNumberPage(
                              store: store,
                            ))),
                  )),
              Container(
                  color: Colors.white,
                  child: ListTile(
                    tileColor: Colors.white,
                    shape: Border(
                      bottom: BorderSide(width: 1, color: Colors.black54),
                    ),
                    title: Text(
                      FlutterI18n.translate(
                          context, 'myStore.Settings.DeliveryService'),
                    ),
                    trailing: Icon(
                      Icons.chevron_right,
                    ),
                    onTap: () => Navigator.of(context).pushNamed(
                      '/delivery-settings',
                    ),
                  )),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                alignment: Alignment.topLeft,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      FlutterI18n.translate(
                          context, 'myStore.Settings.Description'),
                      style: Theme.of(context).textTheme.subtitle2,
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      store.description,
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    IconButton(
                      onPressed: () => Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  EditDescriptionPage(
                                    store: store,
                                  ))),
                      icon: Icon(Icons.edit),
                    )
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
