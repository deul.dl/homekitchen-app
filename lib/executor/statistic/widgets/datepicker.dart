import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  DatePicker({Key key, this.onChangeDate}) : super(key: key);
  final Function(DateTime, DateTime) onChangeDate;

  @override
  _DateState createState() => _DateState();
}

class _DateState extends State<DatePicker> {
  bool _isRange = false;
  DateTime _from, _to;
  String _groupValue = '1d';
  @override
  void initState() {
    super.initState();
    final now = DateTime.now();
    _from = DateTime(now.year, now.month, now.day);
    _to = DateTime(now.year, now.month, now.day);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Column(
          children: [_listTab(context), if (_isRange) _rangeInput(context)],
        ));
  }

  Widget _listTab(BuildContext context) {
    return CupertinoSegmentedControl(
      groupValue: _groupValue,
      onValueChanged: _onDateChanged,
      borderColor: Theme.of(context).primaryColor,
      children: {
        '1d': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              '1d',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
              ),
            )),
        '7d': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              '7d',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
              ),
            )),
        '1m': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              '1m',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
              ),
            )),
        '3m': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              '3m',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
              ),
            )),
        '6m': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              '6m',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
              ),
            )),
        '1y': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              '1y',
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
              ),
            )),
        'range': Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Icon(
              Icons.date_range,
              color: Theme.of(context).primaryColor,
            )),
      },
    );
  }

  Widget _rangeInput(BuildContext context) {
    return DateRangeInput(
      startedAt: _from.toString(),
      endedAt: _to.toString(),
      onChange: widget.onChangeDate,
    );
  }

  void _onDateChanged(val) {
    final now = DateTime.now();

    setState(() => _groupValue = val);
    if (val == '1d') {
      setState(() {
        _isRange = false;
        _to = DateTime(now.year, now.month, now.day).add(Duration(hours: 24));
      });
    } else if (val == '7d') {
      setState(() {
        _isRange = false;
        _to = DateTime(now.year, now.month, now.day).add(Duration(days: 7));
      });
    } else if (val == '1m') {
      setState(() {
        _isRange = false;
        _to = DateTime(now.year, now.month, now.day).add(Duration(days: 30));
      });
    } else if (val == '3m') {
      setState(() {
        _isRange = false;
        _to =
            DateTime(now.year, now.month, now.day).add(Duration(days: 30 * 3));
      });
    } else if (val == '6m') {
      setState(() {
        _isRange = false;
        _to =
            DateTime(now.year, now.month, now.day).add(Duration(days: 30 * 6));
      });
    } else if (val == '1y') {
      setState(() {
        _isRange = false;
        _to = DateTime(now.year, now.month, now.day).add(Duration(days: 365));
      });
    } else {
      setState(() {
        _isRange = true;
      });
    }

    widget.onChangeDate(_from, _to);
  }
}

class DateRangeInput extends StatefulWidget {
  DateRangeInput({
    Key key,
    this.startedAt,
    this.endedAt,
    this.onChange,
  }) : super(key: key);

  final String startedAt;
  final String endedAt;
  final Function(DateTime, DateTime) onChange;

  @override
  _DateRangeState createState() => _DateRangeState();
}

class _DateRangeState extends State<DateRangeInput> {
  DateTimeRange _dateRange;

  String get _dateRangeText {
    final from = DateFormat.MMMMd().format(_dateRange.start);
    final to = DateFormat.MMMMd().format(_dateRange.end);
    return '$from - $to';
  }

  @override
  void initState() {
    final startedAt = DateTime.parse(widget.startedAt);
    final endedAt = DateTime.parse(widget.endedAt);
    setState(() {
      _dateRange = DateTimeRange(start: startedAt, end: endedAt);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        child: InkWell(
            onTap: () async {
              DateTimeRange result = await showDateRangePicker(
                  context: context,
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2100),
                  initialDateRange: _dateRange);
              setState(() => _dateRange = result ?? _dateRange);

              await widget.onChange(_dateRange.start, _dateRange.end);
            },
            child: Container(
              height: 60,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(
                    color: Theme.of(context).primaryColor,
                    width: 1,
                  ))),
              child: Row(
                children: [
                  Icon(Icons.date_range),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    _dateRangeText,
                  )
                ],
              ),
            )));
  }
}
