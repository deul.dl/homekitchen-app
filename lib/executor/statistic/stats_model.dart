import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

import 'package:home_kitchen/mixins/date.dart';

@immutable
class StatsData extends Equatable {
  final Date day;
  final int orderCounts;
  final num revenue;
  final num profit;

  StatsData({
    this.orderCounts,
    this.revenue,
    this.profit,
    this.day,
  });

  @override
  List<Object> get props => [orderCounts, revenue, profit, day];

  factory StatsData.fromJson(Map<String, dynamic> json) {
    return StatsData(
      orderCounts: json['orderCounts'] as int,
      revenue: json['revenue'] as num,
      profit: json['profit'] as num,
      day: Date.formDartTime(DateTime.parse(json['day'] as String)
          .add(DateTime.now().timeZoneOffset)),
    );
  }
}
