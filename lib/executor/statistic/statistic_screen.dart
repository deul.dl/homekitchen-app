import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/executor/statistic/statistic.dart';
import 'package:home_kitchen/executor/statistic/widgets/datepicker.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import 'stats_model.dart';

class StatisticScreen extends StatefulWidget {
  @override
  _StatisticState createState() => _StatisticState();
}

class _StatisticState extends State<StatisticScreen> {
  _StatisticState();

  TooltipBehavior _tooltipBehavior;
  StatisticBloc _statisticBloc;

  @override
  void initState() {
    final now = DateTime.now();
    final _from = DateTime(now.year, now.month, now.day)
        .add(Duration(hours: DateTime.now().timeZoneOffset.inHours))
        .toUtc()
        .toIso8601String();
    final _to = DateTime(now.year, now.month, now.day)
        .add(Duration(hours: DateTime.now().timeZoneOffset.inHours))
        .toUtc()
        .toIso8601String();
    _statisticBloc = StatisticBloc(
        statisticRepository:
            StatisticRepository(RepositoryProvider.of<WebClient>(context)))
      ..add(LoadStatsData(_from, _to));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StatisticBloc, StatisticState>(
        bloc: _statisticBloc,
        builder: (
          BuildContext context,
          StatisticState state,
        ) {
          return Container(
            child: Column(
              children: [
                DatePicker(
                  onChangeDate: (DateTime from, DateTime to) {
                    final _from = from
                        .add(Duration(
                            hours: DateTime.now().timeZoneOffset.inHours))
                        .toUtc()
                        .toIso8601String();
                    final _to = to
                        .add(Duration(
                            hours: DateTime.now().timeZoneOffset.inHours))
                        .toUtc()
                        .toIso8601String();

                    _statisticBloc.add(LoadStatsData(_from, _to));
                  },
                ),
                _buildStats(state)
              ],
            ),
          );
        });
  }

  Widget _buildStats(StatisticState state) {
    if (state is StatisticLoadSuccess) {
      if (state is StatisticLoadSuccess && state.listOfStats.isEmpty)
        return Center(
          child: Icon(
            Icons.bar_chart,
            size: 150,
            color: Colors.black12,
          ),
        );
      else
        return Expanded(child: _sparkChart(state));
    } else if (state is StatisticFailure) {
      return ErrorScreen(errorMessage: state.errorMessage, onReload: () => {});
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }

  Widget _sparkChart(StatisticLoadSuccess state) {
    return SfCartesianChart(
      primaryXAxis: CategoryAxis(),
      legend: Legend(isVisible: true),
      tooltipBehavior: _tooltipBehavior,
      series: <FastLineSeries<StatsData, dynamic>>[
        FastLineSeries<StatsData, dynamic>(
            name: FlutterI18n.translate(context, 'statistic.Revenue',
                translationParams: {'value': state.revenue.toString()}),
            dataSource: state.listOfStats,
            xValueMapper: (StatsData stats, _) => stats.day.toFormat('MMMd'),
            yValueMapper: (StatsData stats, _) => stats.revenue,
            dataLabelSettings: DataLabelSettings(isVisible: true)),
        FastLineSeries<StatsData, dynamic>(
            name: FlutterI18n.translate(context, 'statistic.Profit',
                translationParams: {'value': state.profit.toString()}),
            dataSource: state.listOfStats,
            xValueMapper: (StatsData stats, _) => stats.day.toFormat('MMMd'),
            yValueMapper: (StatsData stats, _) => stats.profit,
            dataLabelSettings: DataLabelSettings(isVisible: true)),
        FastLineSeries<StatsData, dynamic>(
            name: FlutterI18n.translate(context, 'statistic.OrderCounts',
                translationParams: {'value': state.orderCounts.toString()}),
            dataSource: state.listOfStats,
            xValueMapper: (StatsData stats, _) => stats.day.toFormat('MMMd'),
            yValueMapper: (StatsData stats, _) => stats.orderCounts,
            dataLabelSettings: DataLabelSettings(isVisible: true)),
      ],
      primaryYAxis: NumericAxis(edgeLabelPlacement: EdgeLabelPlacement.shift),
    );
  }
}
