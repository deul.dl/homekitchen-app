import 'dart:async';

import 'package:meta/meta.dart';

import 'statistic.dart';

@immutable
abstract class StatisticEvent {
  Stream<StatisticState> applyAsync(
      {StatisticState currentState, StatisticBloc bloc});
}

class LoadStatsData extends StatisticEvent {
  LoadStatsData(this.from, this.to);
  final String from;
  final String to;

  @override
  String toString() => 'LoadStatsData { from: $from, to: $to }';

  @override
  Stream<StatisticState> applyAsync(
      {StatisticState currentState, StatisticBloc bloc}) async* {
    try {
      yield StatisticInProgress();

      final listOfStats =
          await bloc.statisticRepository.getDataByPeriod(from, to);

      yield StatisticLoadSuccess(listOfStats: listOfStats ?? []);
    } catch (_) {
      yield StatisticFailure(_?.toString());
    }
  }
}
