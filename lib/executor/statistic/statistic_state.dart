import 'package:equatable/equatable.dart';
import 'package:home_kitchen/executor/statistic/stats_model.dart';

import 'package:home_kitchen/models/models.dart';

abstract class StatisticState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  StatisticState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class StatisticLoadSuccess extends StatisticState {
  final List<StatsData> listOfStats;

  StatisticLoadSuccess({this.listOfStats}) : super([listOfStats]);

  num get orderCounts {
    return listOfStats.fold(
        0,
        (previousValue, StatsData element) =>
            previousValue + element.orderCounts);
  }

  num get profit {
    return listOfStats.fold(0,
        (previousValue, StatsData element) => previousValue + element.profit);
  }

  num get revenue {
    return listOfStats.fold(0,
        (previousValue, StatsData element) => previousValue + element.revenue);
  }

  @override
  String toString() => 'StatisticLoadSuccess { listOfStats: $listOfStats }';
}

class StatisticInProgress extends StatisticState {
  StatisticInProgress() : super();

  @override
  String toString() => 'StatisticError {}';
}

class StatisticFailure extends StatisticState {
  final String errorMessage;

  StatisticFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'StatisticError { errorMessage: $errorMessage }';
}
