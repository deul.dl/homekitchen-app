import 'dart:async';

import 'package:home_kitchen/executor/statistic/stats_model.dart';
import 'package:home_kitchen/client/client.dart';

class StatisticRepository {
  StatisticRepository(client) : this._client = client;

  final WebClient _client;

  Future<dynamic> getDataByPeriod(String from, String to) {
    return _client
        .post('executor/statistic/period', {'from': from, 'to': to})
        .then((res) => res['data'])
        .then((listOfStats) => listOfStats == null
            ? null
            : (listOfStats as List)
                .map((stats) => StatsData.fromJson(stats))
                .toList());
  }
}
