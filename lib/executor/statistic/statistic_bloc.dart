import 'dart:async';

import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'statistic.dart';

class StatisticBloc extends Bloc<StatisticEvent, StatisticState> {
  StatisticBloc({@required this.statisticRepository}) : super(null);
  final StatisticRepository statisticRepository;

  @override
  Stream<StatisticState> mapEventToState(
    StatisticEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
