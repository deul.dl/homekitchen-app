import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/executor/signup/signup_screen.dart';
import 'package:home_kitchen/executor/signup/signup.dart';

class SignUpPage extends StatelessWidget {
  SignUpPage({Key key, this.type}) : super(key: key);
  final String type;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: SafeArea(
            top: true,
            child: BlocProvider<SignUpBloc>(
                create: (_) => SignUpBloc(
                      authRepository:
                          RepositoryProvider.of<AuthRepository>(context),
                    ),
                child: Container(child: SignUpScreen(type: type)))));
  }
}
