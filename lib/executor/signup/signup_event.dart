import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';

abstract class SignUpEvent extends Equatable {
  const SignUpEvent();

  @override
  List<Object> get props => [];
}

class SignUpUsernameChanged extends SignUpEvent {
  const SignUpUsernameChanged({this.firstName, this.lastNae});

  final String firstName;
  final String lastNae;

  @override
  List<Object> get props => [firstName, lastNae];
}

class SignUpFullNameChanged extends SignUpEvent {
  const SignUpFullNameChanged(this.firstName, this.lastName);

  final String firstName;
  final String lastName;

  @override
  List<Object> get props => [firstName, lastName];
}

class SignUpEmailChanged extends SignUpEvent {
  const SignUpEmailChanged(this.email);

  final String email;

  @override
  List<Object> get props => [email];
}

class SignUpIinChanged extends SignUpEvent {
  const SignUpIinChanged(this.iin);

  final String iin;

  @override
  List<Object> get props => [iin];
}

class SignUpPasswordChanged extends SignUpEvent {
  const SignUpPasswordChanged(this.password);

  final String password;

  @override
  List<Object> get props => [password];
}

class SignUpLocationChanged extends SignUpEvent {
  const SignUpLocationChanged({this.city, this.position, this.countryCode});

  final Position position;
  final String city;
  final String countryCode;

  @override
  List<Object> get props => [city, position, countryCode];
}

class SignUpPhoneNumberChanged extends SignUpEvent {
  const SignUpPhoneNumberChanged(this.phoneNumber);

  final String phoneNumber;

  @override
  List<Object> get props => [phoneNumber];
}

class SignUpSubmitted extends SignUpEvent {
  const SignUpSubmitted(this.code);

  final String code;

  @override
  List<Object> get props => [code];
}
