import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/widgets/custom_text_input.dart';

import 'package:home_kitchen/executor/signup/signup.dart';

class EditPasswordForm extends StatefulWidget {
  EditPasswordForm({Key key, this.state, this.onSave}) : super(key: key);

  final SignUpState state;
  final Function onSave;

  @override
  _EditPasswordState createState() => _EditPasswordState();
}

class _EditPasswordState extends State<EditPasswordForm> {
  TextEditingController _passwordContoller;
  bool _obscureText = false;

  SignUpState get state => widget.state;

  @override
  void initState() {
    _passwordContoller = TextEditingController(text: state.password.value);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FormTop(
            title: FlutterI18n.translate(context, 'signup.EditPassword.Title'),
            child: CustomTextInput(
                maxLines: 1,
                controller: _passwordContoller,
                obscureText: _obscureText,
                onChanged: (val) => BlocProvider.of<SignUpBloc>(context)
                    .add(SignUpPasswordChanged(val)),
                decoration: InputDecoration(
                  suffix: InkWell(
                      child: _obscureText
                          ? Icon(Icons.visibility)
                          : Icon(Icons.visibility_off),
                      onTap: () {
                        setState(() => _obscureText = !_obscureText);
                      }),
                  labelText: FlutterI18n.translate(
                      context, 'signup.EditPassword.Form.Password.Label'),
                  labelStyle: TextStyle(color: Colors.black),
                  errorText: state.password.invalid
                      ? FlutterI18n.translate(
                          context, 'signup.EditPassword.Form.Password.Error')
                      : null,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.black,
                  )),
                )),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 10),
            alignment: Alignment.bottomCenter,
            child: ExpandedFlatButton(
                label: FlutterI18n.translate(context, 'common.Next'),
                onPressed: () {
                  BlocProvider.of<SignUpBloc>(context)
                      .add(SignUpPasswordChanged(state.password.value));
                  widget.onSave();
                }),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    _passwordContoller.dispose();
    super.dispose();
  }
}
