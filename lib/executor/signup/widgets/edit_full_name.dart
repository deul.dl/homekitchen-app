import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/widgets/custom_text_input.dart';

import 'package:home_kitchen/executor/signup/signup.dart';

class EditFullNameForm extends StatefulWidget {
  EditFullNameForm({Key key, this.state, this.onSave}) : super(key: key);
  final SignUpState state;
  final Function onSave;
  @override
  _EditFullNameState createState() => _EditFullNameState();
}

class _EditFullNameState extends State<EditFullNameForm> {
  TextEditingController _firstNameContoller;
  TextEditingController _lastNameContoller;

  SignUpState get state => widget.state;

  @override
  void initState() {
    _firstNameContoller = TextEditingController(text: state.firstName.value);
    _lastNameContoller = TextEditingController(text: state.firstName.value);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FormTop(
              title:
                  FlutterI18n.translate(context, 'signup.EditFullName.Title'),
              child: Column(
                children: <Widget>[
                  CustomTextInput(
                    maxLines: 1,
                    controller: _firstNameContoller,
                    onChanged: (val) => BlocProvider.of<SignUpBloc>(context)
                        .add(SignUpFullNameChanged(val, state.lastName.value)),
                    label: FlutterI18n.translate(
                        context, 'signup.EditFullName.Form.FirstName.Label'),
                    error: state.firstName.invalid
                        ? FlutterI18n.translate(
                            context, 'signup.EditFullName.Form.FirstName.Error')
                        : null,
                  ),
                  CustomTextInput(
                    maxLines: 1,
                    controller: _lastNameContoller,
                    onChanged: (val) => BlocProvider.of<SignUpBloc>(context)
                        .add(SignUpFullNameChanged(state.firstName.value, val)),
                    label: FlutterI18n.translate(
                        context, 'signup.EditFullName.Form.LastName.Label'),
                    error: state.lastName.invalid
                        ? FlutterI18n.translate(
                            context, 'signup.EditFullName.Form.LastName.Error')
                        : null,
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 10),
              alignment: Alignment.bottomCenter,
              child: ExpandedFlatButton(
                  label: FlutterI18n.translate(context, 'common.Next'),
                  onPressed: () {
                    BlocProvider.of<SignUpBloc>(context).add(
                        SignUpFullNameChanged(
                            state.firstName.value, state.lastName.value));
                    widget.onSave();
                  }),
            )
          ],
        ));
  }

  @override
  void dispose() {
    _firstNameContoller.dispose();
    _lastNameContoller.dispose();
    super.dispose();
  }
}
