import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/widgets/custom_text_input.dart';

import 'package:home_kitchen/executor/signup/signup.dart';

class EditEmailForm extends StatefulWidget {
  EditEmailForm({Key key, this.state, Function this.onSave}) : super(key: key);

  final SignUpState state;
  final Function onSave;

  @override
  _EditEmailState createState() => _EditEmailState();
}

class _EditEmailState extends State<EditEmailForm> {
  TextEditingController _emailContoller;

  SignUpState get state => widget.state;

  @override
  void initState() {
    _emailContoller = TextEditingController(text: state.email.value);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FormTop(
              title: FlutterI18n.translate(context, 'signup.EditEmail.Title'),
              child: CustomTextInput(
                maxLines: 1,
                autofocus: true,
                controller: _emailContoller,
                initialValue: state.email == null ? '' : state.email.value,
                onChanged: (val) => BlocProvider.of<SignUpBloc>(context)
                    .add(SignUpEmailChanged(val)),
                label: FlutterI18n.translate(
                    context, 'signup.EditEmail.Form.Email.Label'),
                error: state.email.invalid
                    ? FlutterI18n.translate(
                        context, 'signup.EditEmail.Form.Email.Error')
                    : null,
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 10),
              alignment: Alignment.bottomCenter,
              child: ExpandedFlatButton(
                  label: FlutterI18n.translate(context, 'common.Next'),
                  onPressed: () {
                    BlocProvider.of<SignUpBloc>(context)
                        .add(SignUpEmailChanged(state.email.value));
                    widget.onSave();
                  }),
            )
          ],
        ));
  }

  @override
  void dispose() {
    _emailContoller.dispose();
    super.dispose();
  }
}
