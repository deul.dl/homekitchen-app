import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/widgets/error_sreen.dart';

import 'location.dart';
import 'edit_location.dart';

class LocationSelect extends SearchDelegate<String> {
  final Bloc<EditLocationEvent, EditLocationState> editLocationBloc;

  LocationSelect({this.editLocationBloc});

  @override
  List<Widget> buildActions(BuildContext context) {
    return [IconButton(icon: Icon(Icons.clear), onPressed: () => query = '')];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.chevron_left),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return BlocBuilder<EditLocationBloc, EditLocationState>(
        bloc: editLocationBloc,
        builder: (context, EditLocationState state) {
          if (state is EditLocationLoaded) {
            if (query.isNotEmpty) {
              editLocationBloc.add(SearchedCities(search: query));
            }
            return ListView(
                children: state.locations
                    .map((Location location) => ListTile(
                        leading: Icon(Icons.location_city),
                        onTap: () => close(context, location.name),
                        title: Text(
                          location.name,
                          style: TextStyle(color: Colors.black),
                        )))
                    .toList());
          } else if (state is EditLocationFailure) {
            return ErrorScreen(
                errorMessage: state.errorMessage,
                onReload: () => editLocationBloc.add(InitialedCities()));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }

  @override
  Widget buildResults(BuildContext context) => null;
}
