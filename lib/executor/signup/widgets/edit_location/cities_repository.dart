import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

import 'package:home_kitchen/executor/signup/widgets/edit_location/location.dart';

abstract class CityRepositoryBase {
  Future<List<Location>> getCitiesByCounty(String country, {int offset});

  Future<List<Location>> search(String search, {String country, int offset});

  close();
}

class CityRepository extends CityRepositoryBase {
  http.Client _http;
  final String _url = 'api.geonames.org';

  CityRepository() {
    this._http = http.Client();
  }

  @override
  Future<List<Location>> getCitiesByCounty(String country,
      {int offset = 50}) async {
    final startRow = offset - offset;
    final cities = await _http
        .get(Uri.http(_url, '/searchJSON', {
          'username': 'deul.dl',
          'country': country,
          'startRow': startRow.toString(),
          'maxRows': offset.toString(),
          'style': 'SHORT',
          'lang': 'en'
        }))
        .then((response) => convert.jsonDecode(response.body))
        .then((result) => (result['geonames'] as List)
            .map((e) => Location.fromJsonMap(e))
            .toList());

    return cities;
  }

  @override
  Future<List<Location>> search(String name,
      {String country, int offset = 50}) async {
    final int startRow = offset - offset;
    final List<Location> cities = await _http
        .get(Uri.http(_url, '/searchJSON', {
          'name': name,
          'username': 'deul.dl',
          'country': country,
          'startRow': startRow.toString(),
          'maxRows': offset.toString(),
          'style': 'SHORT'
        }))
        .then((response) => convert.jsonDecode(response.body))
        .then((result) => (result['geonames'] as List)
            .map((e) => Location.fromJsonMap(e))
            .toList());

    return cities;
  }

  void close() async {
    _http.close();
  }
}
