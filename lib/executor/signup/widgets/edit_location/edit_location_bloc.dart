import 'dart:async';
import 'dart:io';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

import 'location.dart';
import 'cities_repository.dart';
import 'edit_location_state.dart';
import 'edit_location_event.dart';

class EditLocationBloc extends Bloc<EditLocationEvent, EditLocationState> {
  EditLocationBloc({@required this.countryCode}) : super(null) {
    add(InitialedCities());
  }
  final CityRepository _cityRepository = CityRepository();
  final String countryCode;

  @override
  Stream<EditLocationState> mapEventToState(
    EditLocationEvent event,
  ) async* {
    try {
      if (event is InitialedCities) {
        yield* _mapEditLocationInitialedCitiesToState(state, event);
      } else if (event is GotCities) {
        yield* _mapEditLocationGotCitiesToState(state, event);
      } else if (event is SearchedCities) {
        yield* _mapEditLocationSearchedCitiesToState(state, event);
      }
    } catch (err) {
      EditLocationFailure(err.toString());
    }
  }

  Stream<EditLocationState> _mapEditLocationInitialedCitiesToState(
    EditLocationState state,
    InitialedCities event,
  ) async* {
    try {
      final List<Location> locations =
          await _cityRepository.getCitiesByCounty(countryCode);
      yield EditLocationLoaded(locations: locations);
    } on HttpException catch (e) {
      yield EditLocationFailure(e?.toString());
    }
  }

  Stream<EditLocationState> _mapEditLocationGotCitiesToState(
    EditLocationState state,
    GotCities event,
  ) async* {
    try {
      final List<Location> locations = await _cityRepository
          .getCitiesByCounty(countryCode, offset: event.offset);

      yield EditLocationLoaded(
          locations: (state as EditLocationLoaded).locations
            ..contains(locations),
          offset: event.offset);
    } on HttpException catch (e) {
      yield EditLocationFailure(e?.toString());
    }
  }

  Stream<EditLocationState> _mapEditLocationSearchedCitiesToState(
    EditLocationState state,
    SearchedCities event,
  ) async* {
    try {
      final List<Location> locations =
          await _cityRepository.search(event.search, country: countryCode);
      yield EditLocationLoaded(locations: locations);
    } on HttpException catch (e) {
      yield EditLocationFailure(e?.toString());
    }
  }
}
