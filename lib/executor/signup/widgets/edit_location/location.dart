import 'package:equatable/equatable.dart';

class Location extends Equatable {
  final String name;
  final num lat;
  final num lng;

  Location({this.name, this.lat, this.lng});

  @override
  List<Object> get props => [name, lat, lng];

  Location.fromJsonMap(Map<String, dynamic> map)
      : name = map['name'],
        lat = num.parse(map['lat']),
        lng = num.parse(map['lng']);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name;
    data['lat'] = lat;
    data['lng'] = lng;
    return data;
  }
}
