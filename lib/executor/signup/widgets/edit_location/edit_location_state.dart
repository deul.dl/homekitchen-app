import 'package:equatable/equatable.dart';

import 'location.dart';

abstract class EditLocationState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  EditLocationState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class EditLocationLoaded extends EditLocationState {
  final List<Location> locations;
  final int offset;

  EditLocationLoaded({this.locations, this.offset = 50})
      : super([locations, offset]);

  @override
  String toString() =>
      'EditLocationLoadSuccess { locations: $locations. offset: $offset }';
}

class EditLocationFailure extends EditLocationState {
  final String errorMessage;

  EditLocationFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'EditLocationError { errorMessage: $errorMessage }';
}
