import 'package:meta/meta.dart';

class EditLocationEvent {}

class InitialedCities extends EditLocationEvent {
  InitialedCities();
}

class GotCities extends EditLocationEvent {
  final int offset;

  GotCities({@required this.offset});
}

class SearchedCities extends EditLocationEvent {
  final String search;

  SearchedCities({@required this.search});
}
