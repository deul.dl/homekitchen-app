import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/location/location.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/executor/signup/signup.dart';

import 'package:home_kitchen/executor/signup/widgets/edit_location/location_select.dart';
import 'package:home_kitchen/executor/signup/widgets/edit_location/edit_location.dart';

class EditLocationForm extends StatelessWidget {
  EditLocationForm({Key key, this.signUpState, this.onSave}) : super(key: key);

  final SignUpState signUpState;
  final Function onSave;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationBloc, LocationState>(builder: (context, state) {
      if (signUpState.city.value == '' && state is LocationSuccess) {
        BlocProvider.of<SignUpBloc>(context).add(SignUpLocationChanged(
            city: state.locality,
            position: state.position,
            countryCode: state.countryCode));
      }

      final location = (state as LocationSuccess);

      return Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FormTop(
                  title: FlutterI18n.translate(
                      context, 'signup.EditLocation.Title'),
                  description: FlutterI18n.translate(
                      context, 'signup.EditLocation.Description'),
                  child: Column(children: [
                    Text(
                      signUpState.city.value,
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () {
                            showSearch(
                                context: context,
                                delegate: LocationSelect(
                                    editLocationBloc: EditLocationBloc(
                                  countryCode: signUpState.countryCode ??
                                      location.countryCode,
                                ))).then((value) => BlocProvider.of<SignUpBloc>(
                                    context)
                                .add(SignUpLocationChanged(
                                    city: value ?? location.locality,
                                    position: location.position,
                                    countryCode: location.countryCode)));
                          },
                          child: Text(
                            FlutterI18n.translate(
                                context, 'signup.EditLocation.ChooseOther'),
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.blue[700],
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ]),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 100),
                  alignment: Alignment.bottomCenter,
                  child: _CircleButton(
                    icon: Icon(
                      Icons.done,
                      color: Colors.green,
                    ),
                    onClick: () {
                      onSave();
                    },
                  ),
                )
              ]));
    });
  }
}

class _CircleButton extends StatelessWidget {
  _CircleButton({@required this.onClick, @required this.icon, this.title});

  final Function onClick;
  final Widget icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onClick,
      child: icon,
      shape: CircleBorder(),
      elevation: 2.0,
      fillColor: Colors.white,
      padding: const EdgeInsets.all(25.0),
    );
  }
}
