import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:formz/formz.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/executor/signup/signup.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignUpBloc({
    @required AuthRepository authRepository,
  })  : assert(authRepository != null),
        _authRepository = authRepository,
        super(SignUpState());

  final AuthRepository _authRepository;

  @override
  Stream<SignUpState> mapEventToState(
    SignUpEvent event,
  ) async* {
    if (event is SignUpFullNameChanged) {
      yield _mapFullNameChangedToState(event, state);
    } else if (event is SignUpEmailChanged) {
      yield _mapEmailChangedToState(event, state);
    } else if (event is SignUpPasswordChanged) {
      yield _mapPasswordChangedToState(event, state);
    } else if (event is SignUpLocationChanged) {
      yield _mapLocationChangedToState(event, state);
    } else if (event is SignUpPhoneNumberChanged) {
      yield _mapPhoneNumberChangedToState(event, state);
    } else if (event is SignUpSubmitted) {
      yield* _mapSignUpSubmittedToState(event, state);
    }
  }

  SignUpState _mapFullNameChangedToState(
    SignUpFullNameChanged event,
    SignUpState state,
  ) {
    final firstName = FirstName.dirty(event.firstName);
    final lastName = LastName.dirty(event.lastName);

    return state.copyWith(
      firstName: firstName,
      lastName: lastName,
      status: Formz.validate([firstName, lastName]),
    );
  }

  SignUpState _mapEmailChangedToState(
    SignUpEmailChanged event,
    SignUpState state,
  ) {
    final email = Email.dirty(event.email);

    return state.copyWith(
      email: email,
      status: Formz.validate([email, state.firstName, state.lastName]),
    );
  }

  SignUpState _mapPasswordChangedToState(
    SignUpPasswordChanged event,
    SignUpState state,
  ) {
    final password = Password.dirty(event.password);
    return state.copyWith(
      password: password,
      status: Formz.validate([
        password,
        state.firstName,
        state.lastName,
        state.email,
      ]),
    );
  }

  SignUpState _mapLocationChangedToState(
    SignUpLocationChanged event,
    SignUpState state,
  ) {
    final city = City.dirty(event.city);

    return state.copyWith(
      position: event.position,
      city: city,
      countryCode: event.countryCode,
      status: Formz.validate([
        state.password,
        state.firstName,
        state.lastName,
        state.email,
        city,
      ]),
    );
  }

  SignUpState _mapPhoneNumberChangedToState(
    SignUpPhoneNumberChanged event,
    SignUpState state,
  ) {
    final phoneNumber = ZPhoneNumber.dirty(event.phoneNumber);

    return state.copyWith(
      phoneNumber: phoneNumber,
      status: Formz.validate([
        phoneNumber,
      ]),
    );
  }

  Stream<SignUpState> _mapSignUpSubmittedToState(
    SignUpSubmitted event,
    SignUpState state,
  ) async* {
    yield state.copyWith(status: FormzStatus.valid);
    if (state.status.isValidated) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);
      try {
        print(state.phoneNumber.phoneNumberWithPlus);
        final timeZoneOffset = DateTime.now().timeZoneOffset;
        await _authRepository.signUp(
          phoneNumber: state.phoneNumber.phoneNumberWithPlus,
          password: state.password.value,
          firstName: state.firstName.value,
          lastName: state.lastName.value,
          email: state.email.value,
          code: event.code,
          city: state.city.value,
          longitude: state.position.longitude,
          latitude: state.position.latitude,
          countryCode: state.countryCode,
          timezone:
              '${DateTime.now().timeZoneName}(${timeZoneOffset.inHours}:${timeZoneOffset.inMinutes})',
        );

        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } on HttpException catch (err) {
        yield state.copyWith(
            status: FormzStatus.submissionFailure, errorMessage: err?.message);
      }
    }
  }
}
