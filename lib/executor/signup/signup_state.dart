import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:geolocator/geolocator.dart';

import 'package:home_kitchen/executor/signup/signup.dart';

class SignUpState extends Equatable {
  const SignUpState(
      {this.status = FormzStatus.pure,
      this.firstName = const FirstName.pure(),
      this.lastName = const LastName.pure(),
      this.email = const Email.pure(),
      this.password = const Password.pure(),
      this.city = const City.pure(),
      this.position,
      this.countryCode,
      this.phoneNumber = const ZPhoneNumber.pure(),
      this.errorMessage});

  final FormzStatus status;
  final FirstName firstName;
  final LastName lastName;
  final Email email;
  final Password password;
  final City city;
  final Position position;
  final ZPhoneNumber phoneNumber;
  final String countryCode;
  final String errorMessage;

  SignUpState copyWith(
      {FormzStatus status,
      FirstName firstName,
      LastName lastName,
      Email email,
      Password password,
      City city,
      Position position,
      String countryCode,
      ZPhoneNumber phoneNumber,
      String errorMessage}) {
    return SignUpState(
        status: status ?? this.status,
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        email: email ?? this.email,
        password: password ?? this.password,
        city: city ?? this.city,
        position: position ?? this.position,
        countryCode: countryCode ?? this.countryCode,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  List<Object> get props => [
        status,
        firstName,
        lastName,
        email,
        password,
        phoneNumber,
        position,
        countryCode,
        city,
        errorMessage
      ];
}
