import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/widgets/verificate_phone_number/veritifacate_phone_number.dart';

import 'package:home_kitchen/executor/signup/widgets/edit_email.dart';
import 'package:home_kitchen/executor/signup/widgets/edit_full_name.dart';
import 'package:home_kitchen/executor/signup/widgets/edit_password.dart';
import 'package:home_kitchen/executor/signup/widgets/edit_location/edit_location.dart';

import 'package:home_kitchen/executor/signup/signup.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({Key key, this.type = SIGNUP_TYPE}) : super(key: key);
  final String type;

  static final _formsPageViewController = PageController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VerificatePhoneNumberBloc>(
        create: (_) => VerificatePhoneNumberBloc(
              authRepository: RepositoryProvider.of<AuthRepository>(context),
            ),
        child: forms(context));
  }

  Widget forms(BuildContext context) {
    return BlocListener<SignUpBloc, SignUpState>(
        listener: (BuildContext context, SignUpState state) {
      if (state.status.isSubmissionFailure) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(content: Text(state.errorMessage)),
          );
      }

      if (state.status.isSubmissionSuccess) {
        Navigator.popUntil(context, ModalRoute.withName('/'));
      }
    }, child: BlocBuilder<SignUpBloc, SignUpState>(
            builder: (BuildContext context, SignUpState state) {
      return PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _formsPageViewController,
        children: [
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('fullNameForm'),
            child: EditFullNameForm(
              state: state,
              onSave: () {
                if (state.status.isValidated) {
                  _nextFormStep();
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              },
            ),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('emailForm'),
            child: EditEmailForm(
              state: state,
              onSave: () {
                if (state.status.isValidated) {
                  _nextFormStep();
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              },
            ),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('passwordForm'),
            child: EditPasswordForm(
              state: state,
              onSave: () {
                if (state.status.isValidated) {
                  _nextFormStep();
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              },
            ),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('locationForm'),
            child: EditLocationForm(
                signUpState: state,
                onSave: () {
                  if (state.status.isValidated) {
                    _nextFormStep();
                  }
                }),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('phoneNumberForm'),
            child: VeritificatePhoneNumberForm(
                type: type,
                phoneNumber: state.phoneNumber,
                onChangePhoneNumber: (val) =>
                    BlocProvider.of<SignUpBloc>(context)
                        .add(SignUpPhoneNumberChanged(val)),
                onSave: () {
                  if (state.status.isValidated) {
                    _nextFormStep();
                    FocusScope.of(context).requestFocus(new FocusNode());
                  }
                }),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('codeForm'),
            child: VeritificateCodeForm(
              type: SIGNUP_TYPE,
              phoneNumber: state.phoneNumber,
              onSave: (String value) {
                BlocProvider.of<SignUpBloc>(context)
                    .add(SignUpSubmitted(value));
              },
            ),
          )
        ],
      );
    }));
  }

  void _nextFormStep() {
    _formsPageViewController.nextPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  bool _onWillPop() {
    if (_formsPageViewController.page.round() ==
        _formsPageViewController.initialPage) return true;

    _formsPageViewController.previousPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );

    return false;
  }
}
