import 'package:equatable/equatable.dart';

import 'package:home_kitchen/executor/login/login.dart';

class LoginState extends Equatable {
  const LoginState(
      {this.status = FormzStatus.pure,
      this.phoneNumber = const ZPhoneNumber.pure(),
      this.password = const Password.pure(),
      this.errorMessage});

  final FormzStatus status;
  final ZPhoneNumber phoneNumber;
  final Password password;
  final String errorMessage;

  LoginState copyWith({
    FormzStatus status,
    ZPhoneNumber phoneNumber,
    Password password,
    String errorMessage,
  }) {
    return LoginState(
        status: status ?? this.status,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        password: password ?? this.password,
        errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  List<Object> get props => [status, phoneNumber, password, errorMessage];
}
