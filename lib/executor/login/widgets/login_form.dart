import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/executor/login/login.dart';
import 'package:home_kitchen/executor/signup/signup_page.dart';
import 'package:home_kitchen/widgets/custom_text_input.dart';
import 'package:home_kitchen/widgets/phone_number_input.dart';
import 'package:home_kitchen/widgets/verificate_phone_number/veritifacate_phone_number.dart';

class LoginForm extends StatelessWidget {
  LoginForm({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              const SnackBar(content: Text('Authentication Failure')),
            );
        }
      },
      child: Align(
        alignment: const Alignment(0, -1 / 3),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(bottom: 30),
                child: Image.asset(
                  'assets/images/logo.png',
                  key: const Key('logo'),
                  width: 150,
                )),
            TextError(),
            _UsernameInput(),
            PasswordInput(),
            const Padding(padding: EdgeInsets.all(12)),
            _LoginButton(),
            const Padding(padding: EdgeInsets.all(12)),
            _SignupButton()
          ],
        ),
      ),
    );
  }
}

class TextError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 20,
        child: BlocBuilder<LoginBloc, LoginState>(
          builder: (context, state) {
            if (state.status.isSubmissionFailure &&
                state.errorMessage != null) {
              return Text(
                state.errorMessage,
                style: TextStyle(color: Colors.red),
                textAlign: TextAlign.center,
              );
            }
            return SizedBox(
              height: 14,
            );
          },
        ));
  }
}

class _UsernameInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) =>
          previous.phoneNumber != current.phoneNumber,
      builder: (context, state) {
        return PhoneNumberInput(
          errorText: state.phoneNumber.invalid
              ? FlutterI18n.translate(context, 'common.PhoneNumber.Error')
              : null,
          onInputChanged: (number) {
            context.bloc<LoginBloc>().add(LoginUsernameChanged(number));
          },
        );
      },
    );
  }
}

class PasswordInput extends StatefulWidget {
  @override
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
        buildWhen: (previous, current) => previous.password != current.password,
        builder: (context, state) {
          return CustomTextInput(
              key: const Key('loginForm_passwordInput_textField'),
              maxLines: 1,
              obscureText: _obscureText,
              onChanged: (val) => BlocProvider.of<LoginBloc>(context)
                  .add(LoginPasswordChanged(val)),
              decoration: InputDecoration(
                fillColor: Colors.white,
                filled: true,
                suffix: InkWell(
                    child: _obscureText
                        ? Icon(Icons.visibility)
                        : Icon(Icons.visibility_off),
                    onTap: () {
                      setState(() => _obscureText = !_obscureText);
                    }),
                labelText:
                    FlutterI18n.translate(context, 'login.Form.Password.Label'),
                labelStyle: TextStyle(color: Colors.black),
                errorText: state.password.invalid
                    ? FlutterI18n.translate(
                        context, 'login.Form.Password.Error')
                    : null,
                focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                  color: Colors.black,
                )),
              ));
        });
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : SizedBox(
                height: 48,
                width: double.infinity,
                child: TextButton(
                  key: const Key('loginForm_continue_raisedButton'),
                  style: TextButton.styleFrom(
                    backgroundColor: Theme.of(context).primaryColor,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'common.Login'),
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (state.status.isValidated) {
                      context.bloc<LoginBloc>().add(const LoginSubmitted());
                    }
                  },
                ));
      },
    );
  }
}

class _SignupButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : SizedBox(
                height: 48,
                width: double.infinity,
                child: OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    backgroundColor: Colors.white,
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                    ),
                    side: BorderSide(color: Theme.of(context).primaryColor),
                  ),
                  key: const Key('loginForm_continue_signupButton'),
                  child: Text(
                    FlutterI18n.translate(context, 'common.Signup'),
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          SignUpPage(type: SIGNUP_TYPE))),
                ));
      },
    );
  }
}
