import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/block_screens/ban_screen.dart';
import 'package:home_kitchen/block_screens/in_moderation_screen.dart';
import 'package:home_kitchen/block_screens/refuse_screen.dart';

import 'package:home_kitchen/widgets/bottom_navigator.dart';
import 'package:home_kitchen/accounts/accounts.dart';
import 'package:home_kitchen/executor/my_store/my_store.dart';
import 'package:home_kitchen/executor/orders/orders.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:home_kitchen/executor/statistic/statistic.dart';

import 'package:home_kitchen/executor/main/main.dart';

const IN_MODERATION = 'in_moderation';
const BANNED = 'banned';
const REFUSED = 'refused';
const VERIFICATED = ' verificated';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.authState}) : super(key: key);

  static const String routeName = '/home';

  final AuthState authState;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  static TabController _tabOrdersController;
  static TabController _tabMyStoreController;

  int _tabMyStoreIndex = 0;

  static const List<Tab> _myStoreTabs = <Tab>[
    Tab(
      text: 'Home',
    ),
    Tab(text: 'Settings'),
  ];

  @override
  void initState() {
    super.initState();
    _load();

    _tabOrdersController = TabController(vsync: this, length: 3);
    _tabMyStoreController = TabController(length: 2, vsync: this);
  }

  Widget _selectedAppBar(MainLoadSuccess state) {
    switch (state.screenIndex) {
      case MY_ORDERS_INDEX:
        {
          return AppBar(
              title: Text(FlutterI18n.translate(context, 'main.AppBar.Orders')),
              bottom: TabBar(
                  isScrollable: true,
                  controller: _tabOrdersController,
                  tabs: <Tab>[
                    Tab(
                        text: FlutterI18n.translate(
                            context, 'order.TabBar.Overview')),
                    Tab(
                        text: FlutterI18n.translate(
                            context, 'order.TabBar.InProgress')),
                    Tab(
                        text: FlutterI18n.translate(
                            context, 'order.TabBar.Completed')),
                  ]));
        }
      case STORE_INDEX:
        {
          if (state.store != null)
            return AppBar(
              title: Text(state.store.name),
              bottom: TabBar(
                controller: _tabMyStoreController,
                tabs: _myStoreTabs,
                onTap: (index) {
                  setState(() => _tabMyStoreIndex = index);
                },
              ),
            );

          return AppBar(
            iconTheme: IconThemeData(color: Colors.black),
            backgroundColor: Theme.of(context).backgroundColor,
            shadowColor: Colors.transparent,
          );
        }
      case ACCOUNT_INDEX:
        {
          return AppBar(
            backgroundColor: Colors.black,
            title: Text(FlutterI18n.translate(context, 'main.AppBar.Account')),
          );
        }
      default:
        {
          return AppBar(
            backgroundColor: Colors.black,
            title: Text('Home'),
          );
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.authState.user.status == IN_MODERATION) {
      return InModerationScreen();
    }

    if (widget.authState.user.status == BANNED) {
      return BanScreen();
    }

    if (widget.authState.user.status == REFUSED) {
      return RefuseScreen();
    }

    return BlocBuilder<MainBloc, MainState>(builder: (context, state) {
      if (state is MainFailure) {
        return Scaffold(
            body:
                ErrorScreen(errorMessage: state.errorMessage, onReload: _load));
      }

      if (state is MainLoadSuccess) {
        return Scaffold(
          appBar: _selectedAppBar(state),
          body: SafeArea(
            child: Container(
                color: Theme.of(context).backgroundColor,
                child: _selectedScreen(state)),
          ),
          bottomNavigationBar: BottomNavigation(
            onChangeScreen: (int index) =>
                BlocProvider.of<MainBloc>(context).add(SreenChanged(index)),
            selectedIndex: state.screenIndex,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.dashboard),
                label: FlutterI18n.translate(
                    context, 'main.BottomNavigator.Dashboard'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.list),
                label: FlutterI18n.translate(
                    context, 'main.BottomNavigator.Orders'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.store),
                label: FlutterI18n.translate(
                    context, 'main.BottomNavigator.MyStore'),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_circle),
                label: FlutterI18n.translate(
                    context, 'main.BottomNavigator.Account'),
              ),
            ],
          ),
          floatingActionButton: state.screenIndex == STORE_INDEX &&
                  state.store != null &&
                  _tabMyStoreIndex == 0
              ? FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () => Navigator.of(context).pushNamed('/new-item',
                      arguments: {'store': state.store}))
              : null,
        );
      }

      return Container(
        alignment: Alignment.center,
        color: Colors.white,
        child: CircularProgressIndicator(),
      );
    });
  }

  Widget _selectedScreen(MainLoadSuccess state) {
    if (state.screenIndex == HOME_INDEX && state.store != null) {
      return StatisticScreen();
    }
    if (state.screenIndex == MY_ORDERS_INDEX) {
      return OrdersScreen(
        tabController: _tabOrdersController,
      );
    }
    if (state.screenIndex == MY_ORDERS_INDEX) {
      return OrdersScreen(
        tabController: _tabOrdersController,
      );
    }
    if (state.screenIndex == STORE_INDEX && state.store != null) {
      return MyStoreScreen(
        tabController: _tabMyStoreController,
        store: state.store,
      );
    }
    if (state.screenIndex == ACCOUNT_INDEX) {
      return AccountScreen();
    }

    return CreateStoreScreen();
  }

  void _load() {
    BlocProvider.of<MainBloc>(context).add(LoadMain());
    BlocProvider.of<MyStoreBloc>(context).add(LoadStore());
  }

  @override
  void dispose() {
    _tabOrdersController?.dispose();
    _tabMyStoreController?.dispose();
    super.dispose();
  }
}
