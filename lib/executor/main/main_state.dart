import 'package:equatable/equatable.dart';

import 'package:home_kitchen/models/store.dart';

abstract class MainState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  MainState(this.propss);

  @override
  List<Object> get props => (propss ?? []);
}

class MainLoadSuccess extends MainState {
  MainLoadSuccess({this.store, this.screenIndex = 0, this.loading = true})
      : super([store, screenIndex, loading]);

  final Store store;
  final bool loading;
  final int screenIndex;

  @override
  String toString() => '''MainLoadSuccess { 
    store: $store,
    screenIndex: $screenIndex,
    loading: $loading
  }''';
}

class MainFailure extends MainState {
  final String errorMessage;

  MainFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'MainFailure { errorMessage: $errorMessage }';
}
