import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'package:home_kitchen/models/store.dart';

import 'main.dart';

@immutable
abstract class MainEvent {
  Stream<MainState> applyAsync({MainState currentState, MainBloc bloc});
}

class LoadMain extends MainEvent {
  LoadMain();

  @override
  String toString() => 'LoadMain {}';

  @override
  Stream<MainState> applyAsync({MainState currentState, MainBloc bloc}) async* {
    try {
      yield MainLoadSuccess(
        screenIndex: HOME_INDEX,
        loading: false,
      );
    } catch (_, stackTrace) {
      developer.log('$_', name: 'LoadMain', error: _, stackTrace: stackTrace);
      yield MainFailure(_?.toString());
    }
  }
}

class SreenChanged extends MainEvent {
  SreenChanged(this.screenIndex);

  final int screenIndex;

  @override
  String toString() => 'SreenChanged { screenIndex: $screenIndex }';

  @override
  Stream<MainState> applyAsync({MainState currentState, MainBloc bloc}) async* {
    try {
      final state = currentState as MainLoadSuccess;
      yield MainLoadSuccess(
          store: state.store, loading: state.loading, screenIndex: screenIndex);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'SreenChanged', error: _, stackTrace: stackTrace);
      yield MainFailure(_?.toString());
    }
  }
}

class UpdatedStore extends MainEvent {
  UpdatedStore(this.store);
  final Store store;

  @override
  String toString() => 'UpdatedStore { store: $store }';

  @override
  Stream<MainState> applyAsync({MainState currentState, MainBloc bloc}) async* {
    try {
      yield MainLoadSuccess(
        store: store,
        screenIndex: (currentState as MainLoadSuccess).screenIndex,
        loading: (currentState as MainLoadSuccess).loading,
      );
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'UpdatedStore', error: _, stackTrace: stackTrace);
      yield MainFailure(_?.toString());
    }
  }
}
