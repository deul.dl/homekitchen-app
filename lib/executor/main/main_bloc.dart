import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/executor/my_store/my_store.dart';

import 'main.dart';

const int HOME_INDEX = 0;
const int MY_ORDERS_INDEX = 1;
const int STORE_INDEX = 2;
const int ACCOUNT_INDEX = 3;

class MainBloc extends Bloc<MainEvent, MainState> {
  final StoreRepository storeRepository;
  StreamSubscription<Store> _storeSubscription;

  MainBloc({@required this.storeRepository}) : super(null) {
    _storeSubscription = storeRepository.storeStream().listen((store) {
      add(UpdatedStore(store));
    });
  }

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'MainBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() async {
    _storeSubscription?.cancel();
    await super.close();
  }
}
