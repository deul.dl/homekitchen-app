import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/executor/orders/orders.dart';

import 'order_details.dart';

class OrderDetailsBloc extends Bloc<OrderDetailsEvent, OrderDetailsState> {
  OrderDetailsBloc(
      {@required this.orderStatusRepository, @required this.orderRepository})
      : super(OrderDetailsInProgress());
  final OrderStatusRepositoryBase orderStatusRepository;
  final OrderRepositoryBase orderRepository;

  @override
  Stream<OrderDetailsState> mapEventToState(
    OrderDetailsEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }
}
