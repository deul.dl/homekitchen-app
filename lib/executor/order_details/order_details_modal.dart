import 'package:flutter/cupertino.dart';
import 'package:home_kitchen/executor/models/models.dart';
import 'package:home_kitchen/mixins/date.dart';

@immutable
class OrderDetails extends Order {
  OrderDetails({
    uuid,
    number,
    description,
    address,
    type,
    status,
    totalPrice,
    createdAt,
    storeUUID,
    executorUUID,
    customerUUID,
    addressUUID,
    this.customer,
  }) : super(
            uuid: uuid,
            addressUUID: addressUUID,
            storeUUID: storeUUID,
            executorUUID: executorUUID,
            customerUUID: customerUUID,
            number: number,
            description: description,
            address: address,
            status: status,
            totalPrice: totalPrice,
            createdAt: createdAt);

  final Customer customer;

  @override
  // TODO: implement props
  List<Object> get props => [
        uuid,
        storeUUID,
        number,
        description,
        address,
        status,
        totalPrice,
        createdAt,
        customer,
      ];

  factory OrderDetails.fromJson(Map<String, dynamic> json) {
    return OrderDetails(
      uuid: json['uuid'] as String,
      storeUUID: json['storeUUID'] as String,
      executorUUID: json['executorUUID'] as String,
      customerUUID: json['customerUUID'] as String,
      addressUUID: json['addressUUID'] as String,
      status: json['status'] as String,
      description: json['description'] as String,
      totalPrice: json['totalPrice'] as num,
      address:
          json['address'] != null ? Address.fromJson(json['address']) : null,
      customer: Customer.fromJson(json['customer']),
      createdAt: Date.formDartTime(
          DateTime.parse(json['time']).add(DateTime.now().timeZoneOffset)),
    );
  }

  OrderDetails updateStatus(String status) {
    return OrderDetails(
      uuid: uuid,
      storeUUID: storeUUID,
      executorUUID: executorUUID,
      customerUUID: customerUUID,
      addressUUID: addressUUID,
      status: status,
      description: description,
      totalPrice: totalPrice,
      address: address,
      customer: customer,
      createdAt: createdAt,
    );
  }
}
