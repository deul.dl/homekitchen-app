import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';

import 'package:home_kitchen/executor/order_details/widgets/bottom_container/bottom_container.dart';
import 'package:home_kitchen/executor/orders/orders.dart';

import 'order_details_screen.dart';

class OrderDetailsPage extends StatelessWidget {
  OrderDetailsPage({Key key}) : super(key: key);

  static const String routeName = '/order-details';

  @override
  Widget build(BuildContext context) {
    final _args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final String orderUUID = _args['orderUUID'];

    return BlocBuilder<OrdersBloc, OrdersState>(
        builder: (BuildContext context, OrdersState state) {
      final order = (state as OrdersLoadSuccess).orders.firstWhere(
          (element) => element.uuid == orderUUID,
          orElse: () => null);
      return Scaffold(
        appBar: AppBar(
          title: Text(
            'Order',
          ),
        ),
        body: OrderDetailsScreen(order: order),
        bottomSheet: BottomContainer(order: order),
      );
    });
  }
}
