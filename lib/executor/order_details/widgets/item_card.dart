import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/models/models.dart';

class ItemCard extends StatelessWidget {
  ItemCard({Key key, @required this.item}) : super(key: key);
  final OrderItem item;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (item.imageSource.isNotEmpty
          ? item.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0,
        child: Row(children: <Widget>[
          // Image.network(urlImage, height: 100, width: 100),
          SizedBox(width: 20),
          Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.name,
                  style: TextStyle(
                    fontSize: 21,
                  ),
                ),
                Text(
                  FlutterI18n.translate(context, 'order.ItemCard.Count',
                      translationParams: {'count': item.count.toString()}),
                  style: TextStyle(fontSize: 16),
                ),
                Text(
                  FlutterI18n.translate(context, 'order.ItemCard.Price',
                      translationParams: {'price': item.price.toString()}),
                  style: TextStyle(fontSize: 16),
                ),
              ])
        ]));
  }
}
