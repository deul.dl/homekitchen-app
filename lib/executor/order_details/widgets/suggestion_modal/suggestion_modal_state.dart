import 'package:equatable/equatable.dart';

abstract class SuggestionModalState extends Equatable {
  final List<Object> propss;
  SuggestionModalState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class SuggestionModalInProgress extends SuggestionModalState {
  @override
  String toString() => 'SuggestionModalInProgress';
}

class SuggestionModalLoadSuccess extends SuggestionModalState {
  SuggestionModalLoadSuccess() : super([]);

  @override
  String toString() => 'SuggestionModalLoadSuccess {}';
}

class SuggestionModalFailed extends SuggestionModalState {
  final String errorMessage;

  SuggestionModalFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'SuggestionModalFailed';
}
