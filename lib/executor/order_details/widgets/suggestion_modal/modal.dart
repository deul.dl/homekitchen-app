import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/orders/orders.dart';
import 'package:home_kitchen/executor/order_details/order_details.dart';

import 'suggestion_modal.dart';

class SuggestionModal extends StatefulWidget {
  final Order order;

  SuggestionModal({Key key, @required this.order}) : super(key: key);

  @override
  _SuggestionState createState() => _SuggestionState();
}

class _SuggestionState extends State<SuggestionModal> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        content: BlocListener<SuggestionModalBloc, SuggestionModalState>(
            listener: (context, state) {
              if (state is SuggestionModalLoadSuccess) {
                Navigator.of(context).pop();
              }
            },
            child: Container(
                height: 100,
                child: Column(children: [
                  SizedBox(
                      width: double.infinity,
                      child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.greenAccent,
                          ),
                          child:
                              Text('OK', style: TextStyle(color: Colors.white)),
                          onPressed: () {
                            BlocProvider.of<SuggestionModalBloc>(context)
                                .add(Suggest(
                              orderUUID: widget.order.uuid,
                            ));
                          })),
                  SizedBox(
                      width: double.infinity,
                      child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.redAccent,
                          ),
                          child: Text('Cancel',
                              style: TextStyle(color: Colors.white)),
                          onPressed: () => Navigator.pop(context))),
                ]))));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
