import 'dart:async';

import 'package:meta/meta.dart';

import 'suggestion_modal.dart';

@immutable
abstract class SuggestionModalEvent {
  Stream<SuggestionModalState> applyAsync(
      {SuggestionModalState currentState, SuggestionModalBloc bloc});
}

class Suggest extends SuggestionModalEvent {
  final String orderUUID;

  Suggest({@required this.orderUUID});

  @override
  String toString() => 'SendedOrder { orderUUID: $orderUUID }';

  @override
  Stream<SuggestionModalState> applyAsync(
      {SuggestionModalState currentState, SuggestionModalBloc bloc}) async* {
    try {
      yield SuggestionModalInProgress();
      await bloc.orderStatusRepository.acceptOrder(orderUUID);
      yield SuggestionModalLoadSuccess();
    } catch (_) {
      yield SuggestionModalFailed(_?.toString());
    }
  }
}
