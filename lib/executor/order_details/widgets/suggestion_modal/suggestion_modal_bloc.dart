import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/order_status_repository/order_status_repository.dart';

import 'suggestion_modal.dart';

class SuggestionModalBloc
    extends Bloc<SuggestionModalEvent, SuggestionModalState> {
  final OrderStatusRepositoryBase orderStatusRepository;
  SuggestionModalBloc({@required this.orderStatusRepository}) : super(null);

  @override
  Stream<SuggestionModalState> mapEventToState(
    SuggestionModalEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }
}
