import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';

import 'package:home_kitchen/executor/orders/orders.dart';
import 'package:meta/meta.dart';

import 'refusing_modal.dart';

class RefusingModalBloc extends Bloc<RefusingModalEvent, RefusingModalState> {
  final OrderStatusRepositoryBase orderStatusRepository;
  RefusingModalBloc({@required this.orderStatusRepository}) : super(null);

  @override
  Stream<RefusingModalState> mapEventToState(
    RefusingModalEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }
}
