import 'dart:async';
import 'dart:developer' as developer;
import 'package:meta/meta.dart';

import 'refusing_modal.dart';

@immutable
abstract class RefusingModalEvent {
  Stream<RefusingModalState> applyAsync(
      {RefusingModalState currentState, RefusingModalBloc bloc});
}

class RejectOrder extends RefusingModalEvent {
  final String orderUUID;

  RejectOrder({@required this.orderUUID});

  @override
  String toString() => 'RejectOrder { orderUUID: $orderUUID }';

  @override
  Stream<RefusingModalState> applyAsync(
      {RefusingModalState currentState, RefusingModalBloc bloc}) async* {
    try {
      yield RefusingModalInProgress();
      await bloc.orderStatusRepository.rejectOrder(orderUUID);
      yield RefusingModalLoadSuccess();
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'RejectOrder', error: _, stackTrace: stackTrace);
      yield RefusingModalFailed(_?.toString());
    }
  }
}
