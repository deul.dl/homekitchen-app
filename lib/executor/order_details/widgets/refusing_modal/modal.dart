import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/executor/orders/orders.dart';

import 'refusing_modal.dart';

class RefuseModal extends StatefulWidget {
  final Order order;
  final Function onSave;

  RefuseModal({Key key, @required this.order, @required this.onSave})
      : super(key: key);

  @override
  _RefuseState createState() => _RefuseState();
}

class _RefuseState extends State<RefuseModal> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        content: BlocListener<RefusingModalBloc, RefusingModalState>(
            listener: (context, state) {
              if (state is RefusingModalLoadSuccess) {
                Navigator.of(context).pop();
              }
            },
            child: Container(
                height: 100,
                child: Column(children: [
                  SizedBox(
                      width: double.infinity,
                      child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.greenAccent,
                          ),
                          child:
                              Text('OK', style: TextStyle(color: Colors.white)),
                          onPressed: () {
                            BlocProvider.of<RefusingModalBloc>(context)
                                .add(RejectOrder(orderUUID: widget.order.uuid));
                          })),
                  SizedBox(
                      width: double.infinity,
                      child: TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.redAccent,
                          ),
                          child: Text('Cancel',
                              style: TextStyle(color: Colors.white)),
                          onPressed: () => Navigator.pop(context))),
                ]))));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
