import 'package:equatable/equatable.dart';

abstract class RefusingModalState extends Equatable {
  final List<Object> propss;
  RefusingModalState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class RefusingModalInProgress extends RefusingModalState {
  @override
  String toString() => 'RefusingModalInProgress';
}

class RefusingModalLoadSuccess extends RefusingModalState {
  RefusingModalLoadSuccess() : super([]);

  @override
  String toString() => 'RefusingModalLoadSuccess {}';
}

class RefusingModalFailed extends RefusingModalState {
  final String errorMessage;

  RefusingModalFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'RefusingModalFailed';
}
