import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:home_kitchen/order_status_repository/order_status_repository.dart';

import 'package:home_kitchen/executor/order_details/widgets/refusing_modal/refusing_modal.dart';
import 'package:home_kitchen/executor/order_details/widgets/suggestion_modal/suggestion_modal.dart';

import 'package:home_kitchen/executor/order_details/order_details.dart';

class BottomContainer extends StatelessWidget {
  final OrderDetails order;

  BottomContainer({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
        height: 150,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(15), topLeft: Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
              ),
            ]),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                    FlutterI18n.translate(
                        context, 'order.BottomContainer.Client'),
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                ClipOval(
                  child: Container(
                    color: Colors.green,
                    child: IconButton(
                        icon: Icon(
                          Icons.phone,
                          color: Colors.white,
                        ),
                        onPressed: () => _makePhoneCall(
                            'tel:${order.customer.phoneNumber}')),
                  ),
                )
              ],
            ),
            Padding(
                padding: EdgeInsets.only(top: 30),
                child: _renderStatusButton(context, order))
          ],
        ));
  }

  Widget _renderStatusButton(BuildContext context, OrderDetails order) {
    switch (order.status) {
      case RECEIVED_STATUS:
        {
          return _renderRejectAndAcceptButton(context);
        }
        break;
      case ACCEPTED_STATUS:
        {
          return _StatusButton(
            text: Text(
              FlutterI18n.translate(
                  context, 'order.BottomContainer.StartCooking'),
              style: Theme.of(context).textTheme.button,
            ),
            color: Colors.blueAccent,
            onPressed: () =>
                BlocProvider.of<OrderDetailsBloc>(context).add(ChangeStatus(
              status: PROCEESSED_STATUS,
              order: order,
            )),
          );
        }
        break;
      case PROCEESSED_STATUS:
        {
          return _StatusButton(
            text: Text(
              FlutterI18n.translate(context, 'order.BottomContainer.Done'),
              style: Theme.of(context).textTheme.button,
            ),
            color: Colors.blueAccent,
            onPressed: () =>
                BlocProvider.of<OrderDetailsBloc>(context).add(ChangeStatus(
              status: DONE_STATUS,
              order: order,
            )),
          );
        }
        break;
      case DONE_STATUS:
        {
          return _StatusButton(
            text: Text(
              FlutterI18n.translate(
                  context, 'order.BottomContainer.OrderWasPaid'),
              style: Theme.of(context).textTheme.button,
            ),
            color: Colors.blueAccent,
            onPressed: () =>
                BlocProvider.of<OrderDetailsBloc>(context).add(ChangeStatus(
              status: TRANSACTION_STATUS,
              order: order,
            )),
          );
        }
        break;

      default:
        {
          return Text(FlutterI18n.translate(
              context, 'order.BottomContainer.OrderComplited'));
        }
        break;
    }
  }

  Widget _renderRejectAndAcceptButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _SmallButton(
            color: Colors.greenAccent,
            onPressed: () => _showSuggestionDialog(context),
            text: Text(
              FlutterI18n.translate(context, 'order.BottomContainer.Accept'),
              style: Theme.of(context).textTheme.button,
            )),
        _SmallButton(
          color: Colors.redAccent,
          onPressed: () => _showRefusingModal(context),
          text: Text(
            FlutterI18n.translate(context, 'order.BottomContainer.Reject'),
            style: Theme.of(context).textTheme.button,
          ),
        )
      ],
    );
  }

  Future<Widget> _showSuggestionDialog(BuildContext context) {
    final _orderStatusRepository =
        RepositoryProvider.of<OrderStatusRepository>(context);
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return BlocProvider(
              create: (_) => SuggestionModalBloc(
                  orderStatusRepository: _orderStatusRepository),
              child: SuggestionModal(
                order: order,
              ));
        });
  }

  Future<Widget> _showRefusingModal(BuildContext context) {
    final _orderStatusRepository =
        RepositoryProvider.of<OrderStatusRepository>(context);
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return BlocProvider(
              create: (_) => RefusingModalBloc(
                  orderStatusRepository: _orderStatusRepository),
              child: RefuseModal(
                order: order,
                onSave: () => {},
              ));
        });
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class _SmallButton extends StatelessWidget {
  final Color color;
  final Widget text;

  final Function onPressed;

  _SmallButton({this.text, this.color, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 48,
        width: 150,
        child: TextButton(
            style: TextButton.styleFrom(backgroundColor: color),
            onPressed: onPressed,
            child: text));
  }
}

class _StatusButton extends StatelessWidget {
  final Color color;
  final Widget text;

  final Function onPressed;

  _StatusButton({this.text, this.color, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 48,
        width: double.infinity,
        child: TextButton(
            style: TextButton.styleFrom(backgroundColor: color),
            onPressed: onPressed,
            child: text));
  }
}
