import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/order_details/order_details.dart';

class DescriptionView extends StatelessWidget {
  final OrderDetails order;

  DescriptionView({Key key, @required this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(
              color: Theme.of(context).backgroundColor,
              width: 1.0,
            ),
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
        alignment: Alignment.topLeft,
        child: Column(children: <Widget>[
          Text(
            FlutterI18n.translate(context, 'order.DescriptionView.Description'),
            style: Theme.of(context).textTheme.subtitle2,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            order.description,
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ]));
  }
}
