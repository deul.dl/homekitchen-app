import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/executor/order_details/order_details.dart';

class DatetimeView extends StatelessWidget {
  final OrderDetails order;

  DatetimeView({Key key, @required this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(
            color: Theme.of(context).backgroundColor,
            width: 1.0,
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
      alignment: Alignment.topLeft,
      child: Row(
        children: <Widget>[
          Text(order.createdAt.toFormat('MMM d, h:mm'),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
        ],
      ),
    );
  }
}
