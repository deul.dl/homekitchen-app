import 'dart:async';
import 'package:meta/meta.dart';

import 'order_details.dart';

@immutable
abstract class OrderDetailsEvent {
  Stream<OrderDetailsState> applyAsync(
      {OrderDetailsState currentState, OrderDetailsBloc bloc});
}

class LoadOrderDetails extends OrderDetailsEvent {
  LoadOrderDetails(this.orderUUID);
  final String orderUUID;
  @override
  String toString() => 'LoadOrderDetails { orderUUID: $orderUUID }';

  @override
  Stream<OrderDetailsState> applyAsync(
      {OrderDetailsState currentState, OrderDetailsBloc bloc}) async* {
    try {
      yield OrderDetailsInProgress();
      final orderItems = await bloc.orderRepository.getOrderItems(orderUUID);
      final orderPaymentMethod =
          await bloc.orderRepository.getOrderPaymentMethod(orderUUID);

      yield OrderDetailsLoadSuccess(
          orderItems: orderItems as List<OrderItem>,
          orderPaymentMethod: orderPaymentMethod as OrderPaymentMethod);
    } catch (_) {
      yield OrderDetailsFailed(_?.toString());
    }
  }
}

class ChangeStatus extends OrderDetailsEvent {
  ChangeStatus({@required this.status, @required this.order});

  final String status;
  final OrderDetails order;

  @override
  String toString() => 'LoadOrderDetails { status: $status, order: $order}';

  @override
  Stream<OrderDetailsState> applyAsync(
      {OrderDetailsState currentState, OrderDetailsBloc bloc}) async* {
    try {
      switch (status) {
        case ACCEPTED_STATUS:
          {
            await bloc.orderStatusRepository.acceptOrder(order.uuid);
          }
          break;
        case PROCEESSED_STATUS:
          {
            await bloc.orderStatusRepository.startOrder(order.uuid);
          }
          break;
        case DONE_STATUS:
          {
            await bloc.orderStatusRepository.doneOrder(order.uuid);
          }
          break;
        default:
          {
            await bloc.orderStatusRepository.paidOrder(order.uuid);
          }
          break;
      }

      yield (currentState as OrderDetailsLoadSuccess).copyWith();
    } catch (_) {
      yield OrderDetailsFailed(_?.toString());
    }
  }
}
