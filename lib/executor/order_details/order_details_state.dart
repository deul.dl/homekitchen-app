import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:home_kitchen/addresses/addresses.dart';

abstract class OrderDetailsState extends Equatable {
  final List<Object> propss;
  OrderDetailsState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

@immutable
class OrderDetailsInProgress extends OrderDetailsState {
  @override
  String toString() => 'OrderDetailsInProgress';
}

@immutable
class OrderDetailsLoadSuccess extends OrderDetailsState {
  OrderDetailsLoadSuccess({this.orderItems, this.orderPaymentMethod})
      : super([orderItems, orderPaymentMethod]);

  final List<OrderItem> orderItems;
  final OrderPaymentMethod orderPaymentMethod;

  @override
  String toString() => '''
    OrderDetailsLoadSuccess {
      orderItems: $orderItems,
      orderPaymentMethod: $orderPaymentMethod,
    }''';

  OrderDetailsLoadSuccess copyWith(
      {List<OrderItem> orderItems,
      OrderPaymentMethod orderPaymentMethod,
      String errorMessage}) {
    return OrderDetailsLoadSuccess(
      orderItems: orderItems ?? this.orderItems,
      orderPaymentMethod: orderPaymentMethod ?? this.orderPaymentMethod,
    );
  }
}

@immutable
class OrderDetailsFailed extends OrderDetailsState {
  final String errorMessage;

  OrderDetailsFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'OrderDetailsFailed { errorMessage: $errorMessage }';
}
