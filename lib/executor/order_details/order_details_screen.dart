import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:home_kitchen/widgets/price_list_item.dart';

import 'widgets/address_view.dart';
import 'widgets/item_card.dart';
import 'widgets/datetime_view.dart';

import 'order_details.dart';

class OrderDetailsScreen extends StatefulWidget {
  OrderDetailsScreen({Key key, this.order}) : super(key: key);

  final OrderDetails order;

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetailsScreen> {
  OrderDetails get order => widget.order;

  @override
  void initState() {
    _load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderDetailsBloc, OrderDetailsState>(
        builder: (BuildContext context, OrderDetailsState state) {
      if (state is OrderDetailsLoadSuccess) {
        return Container(
          color: Theme.of(context).backgroundColor,
          child: Column(
            children: [
              Expanded(
                child: ListView(
                  children: state.orderItems
                      .map((item) =>
                          ItemCard(key: Key(item.itemUUID), item: item))
                      .toList(),
                ),
              ),
              if (order.address != null) AddressView(order: order),
              DatetimeView(order: order),
              //DescriptionView(order: order),
              Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(
                        color: Theme.of(context).backgroundColor,
                        width: 1.0,
                      ),
                    ),
                  ),
                  child: ListTile(
                    tileColor: Colors.white,
                    trailing: Icon(Icons.payment),
                    title: Text(FlutterI18n.translate(
                        context, 'order.${state.orderPaymentMethod.type}')),
                  )),
              PriceListItem(
                  label: Text(
                    FlutterI18n.translate(context, 'order.OrderTotal.Label'),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  amount: state.orderPaymentMethod.amount),
              PriceListItem(
                  label: Text(
                    FlutterI18n.translate(context, 'order.DeliveryPrice.Label'),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  amount: state.orderPaymentMethod.deliveryPrice),
              PriceListItem(
                  label: Text(
                    FlutterI18n.translate(context, 'order.TotalPrice.Label'),
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  amount: order.totalPrice),

              SizedBox(
                height: 150,
              )
            ],
          ),
        );
      } else if (state is OrderDetailsFailed) {
        return ErrorScreen(errorMessage: state.errorMessage, onReload: _load);
      } else {
        return Center(child: CircularProgressIndicator());
      }
    });
  }

  void _load() {
    BlocProvider.of<OrderDetailsBloc>(context)
        .add(LoadOrderDetails(order.uuid));
  }
}
