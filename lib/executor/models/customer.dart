import 'package:equatable/equatable.dart';

class Customer extends Equatable {
  final String id;
  final String phoneNumber;
  final String name;

  Customer({this.id, this.phoneNumber, this.name});

  @override
  List<Object> get props => [id, phoneNumber, name];

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      phoneNumber: json['phoneNumber'] as String,
    );
  }
}
