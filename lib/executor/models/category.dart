import 'models.dart';

class MyCategoryWithItems extends Category {
  MyCategoryWithItems({name, imageSource, description, this.items})
      : super(name: name, imageSource: imageSource);

  final List<Item> items;
}
