import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/client/client.dart';

import 'package:home_kitchen/order_status_repository/order_status_repository.dart';

import 'package:home_kitchen/theme/theme.dart';

import 'package:home_kitchen/executor/main/main.dart';
import 'package:home_kitchen/executor/my_store/my_store.dart';
import 'package:home_kitchen/executor/orders/orders.dart';
import 'package:home_kitchen/executor/order_details/order_details.dart'
    as orderDetails;
import 'package:home_kitchen/accounts/accounts.dart';
import 'package:home_kitchen/executor/item_categories/item_category.dart';
import 'package:home_kitchen/executor/my_store/edit_store/delivery_settings/delivery_settings.dart';

class HomeKitchenExecutor extends StatelessWidget {
  HomeKitchenExecutor({this.authState});

  final AuthState authState;

  @override
  Widget build(BuildContext context) {
    final languageCode =
        WidgetsBinding.instance.window.locale.languageCode == 'ru'
            ? 'ru'
            : 'en';
    final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
      translationLoader: NamespaceFileTranslationLoader(
        namespaces: [
          'common',
          'main',
          'signup',
          'login',
          'profile',
          'verificatePhoneNumber',
          'myStore',
          'items',
          'category',
          'order',
          'block',
          'settings',
          'account',
          'uploadImage',
          'deliverySettings',
          'timeRange',
          'statistic'
        ],
        useCountryCode: false,
        basePath: 'assets/locales',
        fallbackDir: languageCode,
        forcedLocale: Locale(languageCode),
      ),
      missingTranslationHandler: (key, locale) {
        print('--- Missing Key: $key, languageCode: ${locale.languageCode}');
      },
    );
    WidgetsFlutterBinding.ensureInitialized();

    final _myStoreRepository =
        MyStoreRepository(client: RepositoryProvider.of<WebClient>(context));
    final _itemsRepository =
        ItemsRepository(RepositoryProvider.of<WebClient>(context));
    final _ordersRepository =
        OrderRepository(RepositoryProvider.of<WebClient>(context));
    final _storeDeliveryRepository =
        StoreDeliveryRepository(RepositoryProvider.of<WebClient>(context));
    final _myStoreProvider = RepositoryProvider<MyStoreRepository>(
      create: (context) => _myStoreRepository,
    );

    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider(create: (context) => _storeDeliveryRepository),
          _myStoreProvider,
          RepositoryProvider<ItemsRepository>(
            create: (context) => _itemsRepository,
          ),
          RepositoryProvider<OrderRepository>(
            create: (context) => _ordersRepository,
          ),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider<MyStoreBloc>(
              create: (_) => MyStoreBloc(storeRepository: _myStoreRepository),
            ),
            BlocProvider<ItemCategoryBloc>(
                create: (_) => ItemCategoryBloc(
                    categoryRepository: ItemCategoryRepository(
                        RepositoryProvider.of<WebClient>(context)))
                  ..add(LoadCategories())),
            BlocProvider<ItemsBloc>(
                create: (_) => ItemsBloc(itemsRepository: _itemsRepository)),
            BlocProvider<OrdersBloc>(
                create: (_) => OrdersBloc(
                    orderRepository: _ordersRepository,
                    orderStatusRepository:
                        RepositoryProvider.of<OrderStatusRepository>(context))),
          ],
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: homeKitchenTheme,
            home: MultiBlocProvider(
              providers: [
                BlocProvider<MainBloc>(
                    create: (_) =>
                        MainBloc(storeRepository: _myStoreRepository)),
              ],
              child: MainPage(authState: authState),
            ),
            routes: <String, WidgetBuilder>{
              CreateStorePage.routeName: (BuildContext context) => BlocProvider<
                      CreateStoreBloc>(
                  create: (context) => CreateStoreBloc(
                      storeRepository:
                          RepositoryProvider.of<MyStoreRepository>(context)),
                  child: CreateStorePage()),
              ItemsPage.routeName: (BuildContext context) => ItemsPage(),
              NewItemPage.routeName: (BuildContext context) =>
                  BlocProvider<NewItemBloc>(
                      create: (_) => NewItemBloc(
                          itemsRepository:
                              RepositoryProvider.of<ItemsRepository>(context)),
                      child: NewItemPage()),
              EditItemPage.routeName: (BuildContext context) =>
                  BlocProvider<ItemBloc>(
                      create: (_) => ItemBloc(
                          itemsRepository:
                              RepositoryProvider.of<ItemsRepository>(context)),
                      child: EditItemPage()),
              ViewItemPage.routeName: (BuildContext context) =>
                  BlocProvider<ItemBloc>(
                      create: (context) => ItemBloc(
                          itemsRepository:
                              RepositoryProvider.of<ItemsRepository>(context)),
                      child: ViewItemPage()),
              orderDetails.OrderDetailsPage.routeName: (BuildContext context) =>
                  BlocProvider<orderDetails.OrderDetailsBloc>(
                      create: (context) => orderDetails.OrderDetailsBloc(
                          orderStatusRepository:
                              RepositoryProvider.of<OrderStatusRepository>(
                                  context),
                          orderRepository: _ordersRepository),
                      child: orderDetails.OrderDetailsPage()),
              // Account
              SettingsPage.routeName: (BuildContext context) => SettingsPage(),
              ProfilePage.routeName: (BuildContext context) => ProfilePage(),
              DeliverySettingsPage.routeName: (BuildContext context) =>
                  BlocProvider<DeliverySettingBloc>(
                      create: (context) => DeliverySettingBloc(
                          storeDeliveryRepository: _storeDeliveryRepository)
                        ..add(LoadStoreDelivery()),
                      child: DeliverySettingsPage()),
            },
            localizationsDelegates: [
              flutterI18nDelegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
          ),
        ));
  }
}
