import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'package:home_kitchen/executor/order_details/order_details.dart';

import 'orders.dart';

@immutable
abstract class OrdersEvent {
  Stream<OrdersState> applyAsync({OrdersState currentState, OrdersBloc bloc});
}

class LoadOrders extends OrdersEvent {
  @override
  String toString() => 'LoadOrders';

  LoadOrders();

  @override
  Stream<OrdersState> applyAsync(
      {OrdersState currentState, OrdersBloc bloc}) async* {
    try {
      yield OrdersInProgress();
      final orders = await bloc.orderRepository.getOrders();

      yield OrdersLoadSuccess(orders: (orders as List<OrderDetails>) ?? []);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'LoadOrders', error: _, stackTrace: stackTrace);
      yield OrdersFailure(_?.toString());
    }
  }
}

class OrdersStatusChanged extends OrdersEvent {
  final OrderStatus orderStatus;

  @override
  String toString() => 'OrdersStatusChanged { orderStatus: $orderStatus }';

  OrdersStatusChanged(this.orderStatus);

  @override
  Stream<OrdersState> applyAsync(
      {OrdersState currentState, OrdersBloc bloc}) async* {
    try {
      final orders = (currentState as OrdersLoadSuccess)
          .orders
          .map((OrderDetails e) => e.uuid == orderStatus.orderUUID
              ? e.updateStatus(orderStatus.status)
              : e)
          .toList();

      yield OrdersLoadSuccess(orders: orders);
    } catch (_) {
      yield OrdersFailure(_?.toString());
    }
  }
}
