import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/executor/orders/orders_repository.dart';
import 'package:home_kitchen/order_status_repository/order_status_repository.dart';

import 'orders.dart';

class OrdersBloc extends Bloc<OrdersEvent, OrdersState> {
  OrdersBloc({this.orderRepository, this.orderStatusRepository})
      : super(OrdersInProgress()) {
    _orderStatuses?.cancel();
    _orderStatuses =
        orderStatusRepository.statuses.listen((OrderStatus orderStatus) {
      if (orderStatus.status == RECEIVED_STATUS) {
        add(LoadOrders());
      } else {
        add(OrdersStatusChanged(orderStatus));
      }
    });
  }

  StreamSubscription<OrderStatus> _orderStatuses;

  final OrderRepositoryBase orderRepository;
  final OrderStatusRepositoryBase orderStatusRepository;

  @override
  Stream<OrdersState> mapEventToState(
    OrdersEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    _orderStatuses?.cancel();
    await super.close();
  }
}
