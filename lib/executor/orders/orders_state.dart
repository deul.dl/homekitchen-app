import 'package:equatable/equatable.dart';

import 'package:home_kitchen/executor/order_details/order_details.dart';

abstract class OrdersState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  OrdersState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class OrdersInProgress extends OrdersState {
  OrdersInProgress();

  @override
  String toString() => 'OrdersInProgress {}';
}

class OrdersLoadSuccess extends OrdersState {
  final List<OrderDetails> orders;

  OrdersLoadSuccess({this.orders}) : super(orders);

  @override
  String toString() => 'OrdersLoadSuccess { orders: $orders }';

  List<OrderDetails> get currentOrders => orders
      .where((OrderDetails order) =>
          (order.status == RECEIVED_STATUS ||
              order.status == ACCEPTED_STATUS) &&
          order.status != COMPLETED_STATUS)
      .toList();

  List<OrderDetails> get inProgressOrders => orders
      .where((OrderDetails order) =>
          !(order.status == ACCEPTED_STATUS ||
              order.status == RECEIVED_STATUS) &&
          order.status != COMPLETED_STATUS)
      .toList();

  List<OrderDetails> get completedOrders => orders
      .where((OrderDetails order) => order.status == COMPLETED_STATUS)
      .toList();
}

class OrdersFailure extends OrdersState {
  final String errorMessage;

  OrdersFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'OrdersError { errorMessage: $errorMessage }';
}
