import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'widgets/orders_list.dart';
import 'orders.dart';

class OrdersScreen extends StatefulWidget {
  OrdersScreen({Key key, this.tabController}) : super(key: key);

  final TabController tabController;

  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<OrdersScreen> {
  @override
  void initState() {
    _load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: BlocBuilder<OrdersBloc, OrdersState>(
            builder: (BuildContext context, OrdersState state) {
          if (state is OrdersLoadSuccess) {
            return TabBarView(controller: widget.tabController, children: [
              OrderList(
                orders: state.currentOrders,
                onReload: _load,
              ),
              OrderList(
                orders: state.inProgressOrders,
                onReload: _load,
              ),
              OrderList(
                orders: state.completedOrders,
                onReload: _load,
              ),
            ]);
          } else if (state is OrdersFailure) {
            return ErrorScreen(
                errorMessage: state.errorMessage, onReload: _load);
          } else {
            return Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            );
          }
        }));
  }

  void _load() {
    BlocProvider.of<OrdersBloc>(context).add(LoadOrders());
  }

  @override
  void dispose() {
    super.dispose();
  }
}
