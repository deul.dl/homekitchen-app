import 'dart:async';

import 'package:home_kitchen/executor/models/models.dart';
import 'package:home_kitchen/executor/order_details/order_details.dart';

import 'package:home_kitchen/client/client.dart';

abstract class OrderRepositoryBase {
  Future<dynamic> getOrders() async {}

  Future<dynamic> getOrderItems(String orderUUID);

  Future<dynamic> getOrderPaymentMethod(String orderUUID);
}

class OrderRepository extends OrderRepositoryBase {
  OrderRepository(client) : this._client = client;

  final WebClient _client;

  @override
  Future<dynamic> getOrders() async {
    return _client.get('executor/orders').then((res) {
      return res['data']['orders'];
    }).then((orders) => orders == null
        ? null
        : (orders as List)
            .map((orderUUID) => OrderDetails.fromJson(orderUUID))
            .toList());
  }

  @override
  Future<dynamic> getOrderItems(String ordeUUID) async {
    return _client
        .get('executor/orders/$ordeUUID/items')
        .then((res) => res['data']['orderItems'])
        .then((orderItems) => orderItems == null
            ? null
            : (orderItems as List)
                .map((orderUUID) => OrderItem.fromJson(orderUUID))
                .toList());
  }

  @override
  Future<dynamic> getOrderPaymentMethod(String ordeUUID) async {
    return _client
        .get('executor/orders/$ordeUUID/payment')
        .then((res) => res['data']['orderPaymentMethod'])
        .then((orderPaymentMethod) => orderPaymentMethod == null
            ? null
            : OrderPaymentMethod.fromJson(orderPaymentMethod));
  }
}
