import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

import 'package:home_kitchen/widgets/empty_screen.dart';
import 'package:home_kitchen/executor/order_details/order_details.dart';

import 'order_card.dart';

class HeadingItem implements ListItem {
  HeadingItem(this.heading);

  final String heading;
}

abstract class ListItem {}

class OrderList extends StatelessWidget {
  OrderList({Key key, this.orders, this.onReload}) : super(key: key);

  final List<OrderDetails> orders;
  final Function onReload;

  @override
  Widget build(BuildContext context) {
    if (orders.isEmpty) {
      return EmptyScreen(onReload: onReload);
    }

    final sectionList =
        groupBy(orders, (order) => order.createdAt.timeIndicators())
            .entries
            .expand((order) {
      return [HeadingItem(order.key), ...order.value];
    }).toList();

    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        final item = sectionList[index];

        if (item is HeadingItem) {
          return ListTile(
            title: Text(item.heading,
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black54,
                    fontWeight: FontWeight.bold)),
          );
        }
        final OrderDetails order = sectionList[index] as OrderDetails;

        return OrderCard(
            index: index,
            key: Key(order.uuid),
            order: order,
            onTap: () => Navigator.of(context).pushNamed('/order-details',
                arguments: {'orderUUID': order.uuid}));
      },
      itemCount: sectionList.length,
    );
  }
}
