import 'dart:async';
import 'dart:convert';

import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'mulitpart_request.dart';

String CSRF_TOKEN = '_csrfToken';

class WebClient {
  WebClient();

  http.Client _client = http.Client();
  Map<String, String> headers = {};

  final storage = new FlutterSecureStorage();

  Future<dynamic> loadCookies() async {
    String csrfToken = await storage.read(key: CSRF_TOKEN);

    headers['cookie'] = csrfToken;
  }

  Future<dynamic> deleteSession() async {
    await storage.delete(key: CSRF_TOKEN);
  }

  Future<dynamic> get(String url) async {
    final http.Response response = await _client.get(
      Uri.parse(DotEnv().env['SERVER_API_URL'] + url),
      headers: headers,
    );

    if (response.statusCode >= 400 && response.statusCode <= 499) {
      print(headers['cookie']);
      print(json.decode(response.body)['message']);
      print(url);
      throw HttpException(json.decode(response.body)['message']);
    }

    try {
      updateCookie(response);
      return json.decode(utf8.decode(response.bodyBytes));
    } catch (exception) {
      throw HttpException('An error occurred');
    }
  }

  Future<dynamic> post(String url, dynamic data) async {
    final http.Response response = await _client.post(
      Uri.parse(DotEnv().env['SERVER_API_URL'] + url),
      body: jsonEncode({
        'data': data,
      }),
      headers: {'Content-Type': 'application/json', ...headers},
    );

    if (response.statusCode >= 400 && response.statusCode <= 499) {
      throw HttpException(json.decode(response.body)['message']);
    }

    try {
      updateCookie(response);
      return json.decode(utf8.decode(response.bodyBytes));
    } catch (exception) {
      throw HttpException('An error occurred');
    }
  }

  Future<dynamic> put(String url, dynamic data) async {
    final http.Response response = await _client.put(
      Uri.parse(DotEnv().env['SERVER_API_URL'] + url),
      body: jsonEncode({
        'data': data,
      }),
      headers: {'Content-Type': 'application/json', ...headers},
    );

    if (response.statusCode >= 400 && response.statusCode <= 499) {
      throw HttpException(json.decode(response.body)['message']);
    }

    try {
      updateCookie(response);
      return json.decode(utf8.decode(response.bodyBytes));
    } catch (exception) {
      throw HttpException('An error occurred');
    }
  }

  Future<dynamic> delete(String url, String uuid) async {
    final http.Response response = await _client.delete(
      Uri.parse(DotEnv().env['SERVER_API_URL'] + url + '/$uuid'),
      headers: {'Content-Type': 'application/json', ...headers},
    );

    if (response.statusCode >= 400) {
      throw HttpException(json.decode(response.body)['message']);
    }

    try {
      updateCookie(response);
      final jsonResponse = json.decode(response.body);
      return jsonResponse;
    } catch (exception) {
      throw HttpException('An error occurred');
    }
  }

  MultipartRequest multipat() {
    return MultipartRequest(headers);
  }

  Future<void> updateCookie(http.Response response) async {
    String rawCookie = response.headers['set-cookie'];

    if (rawCookie != null && !rawCookie.contains('csrfToken=;')) {
      int index = rawCookie.indexOf(';');
      headers['cookie'] =
          (index == -1) ? rawCookie : rawCookie.substring(0, index);
      await storage.write(key: CSRF_TOKEN, value: headers['cookie']);
    }
  }
}
