import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class MultipartRequest {
  MultipartRequest(this.headers);

  final headers;

  Future<dynamic> put(String url, Map<String, String> data, File file) async {
    final response = await _send(url, 'PUT', data, file);

    if (response.statusCode >= 400 && response.statusCode <= 499) {
      throw HttpException(json.decode(response.body)['message']);
    }

    try {
      final jsonResponse = json.decode(utf8.decode(response.bodyBytes));
      return jsonResponse;
    } catch (exception) {
      throw HttpException('An error occurred');
    }
  }

  Future<dynamic> post(String url, Map<String, String> data, File file) async {
    final response = await _send(url, 'POST', data, file);

    if (response.statusCode >= 400 && response.statusCode <= 499) {
      throw HttpException(json.decode(response.body)['message']);
    }

    try {
      final jsonResponse = json.decode(response.body);
      return jsonResponse;
    } catch (exception) {
      throw HttpException('An error occurred');
    }
  }

  Future<http.Response> _send(
      String url, String method, Map<String, String> data, File file) async {
    final http.MultipartRequest request = http.MultipartRequest(
      method,
      Uri.parse(DotEnv().env['SERVER_API_URL'] + url),
    )..headers.addAll(headers);

    if (data != null) {
      request.fields.addAll(data);
    }

    if (file != null) {
      request.files.add(await http.MultipartFile.fromPath('file', file.path));
    }

    return http.Response.fromStream(await request.send());
  }
}
