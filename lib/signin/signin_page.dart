import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/signin/signin_screen.dart';
import 'package:home_kitchen/signin/signin.dart';

class SignInPage extends StatelessWidget {
  SignInPage({Key key, this.isPopMain = true, this.hideExecutorButton = false})
      : super(key: key);
  final isPopMain;
  final hideExecutorButton;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: SafeArea(
            top: true,
            child: BlocProvider<SignInBloc>(
                create: (_) => SignInBloc(
                      authRepository:
                          RepositoryProvider.of<AuthRepository>(context),
                    ),
                child: SignInScreen(
                  hideExecutorButton: hideExecutorButton,
                  onNavigation: () {
                    if (isPopMain) {
                      Navigator.popUntil(context, ModalRoute.withName('/'));
                    } else {
                      Navigator.pop(context);
                    }
                  },
                ))));
  }
}
