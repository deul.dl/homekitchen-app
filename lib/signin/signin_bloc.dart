import 'dart:async';
import 'dart:io';

import 'package:formz/formz.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/signin/signin.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc({
    @required AuthRepository authRepository,
  })  : assert(authRepository != null),
        _authRepository = authRepository,
        super(SignInState());

  final AuthRepository _authRepository;

  @override
  Stream<SignInState> mapEventToState(
    SignInEvent event,
  ) async* {
    if (event is SignInPhoneNumberChanged) {
      yield _mapPhoneNumberChangedToState(event, state);
    } else if (event is SignInSubmitted) {
      yield* _mapSignInSubmittedToState(event, state);
    }
  }

  SignInState _mapPhoneNumberChangedToState(
    SignInPhoneNumberChanged event,
    SignInState state,
  ) {
    final phoneNumber = ZPhoneNumber.dirty(event.phoneNumber);
    return state.copyWith(
      phoneNumber: phoneNumber,
      status: Formz.validate([phoneNumber]),
    );
  }

  Stream<SignInState> _mapSignInSubmittedToState(
    SignInSubmitted event,
    SignInState state,
  ) async* {
    yield state.copyWith(status: FormzStatus.valid);
    if (state.status.isValidated) {
      try {
        yield state.copyWith(status: FormzStatus.submissionInProgress);
        await _authRepository.logInCustomer(
            phoneNumber: state.phoneNumber.phoneNumberWithPlus,
            code: event.code);

        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } on HttpException catch (err) {
        yield state.copyWith(
            status: FormzStatus.submissionFailure, errorMessage: err?.message);
      }
    }
  }
}
