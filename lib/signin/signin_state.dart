import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/signin/signin.dart';

class SignInState extends Equatable {
  const SignInState(
      {this.status = FormzStatus.pure,
      this.phoneNumber = const ZPhoneNumber.pure(),
      this.errorMessage});

  final FormzStatus status;
  final ZPhoneNumber phoneNumber;
  final String errorMessage;

  SignInState copyWith(
      {FormzStatus status, ZPhoneNumber phoneNumber, String errorMessage}) {
    return SignInState(
        status: status ?? this.status,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  List<Object> get props => [phoneNumber, status, errorMessage];
}
