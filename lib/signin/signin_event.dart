import 'package:equatable/equatable.dart';

abstract class SignInEvent extends Equatable {
  const SignInEvent();

  @override
  List<Object> get props => [];
}

class SignInPhoneNumberChanged extends SignInEvent {
  const SignInPhoneNumberChanged(this.phoneNumber);

  final String phoneNumber;

  @override
  List<Object> get props => [phoneNumber];
}

class SignInSubmitted extends SignInEvent {
  const SignInSubmitted(this.code);

  final String code;

  @override
  List<Object> get props => [code];
}
