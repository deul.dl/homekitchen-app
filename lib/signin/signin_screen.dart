import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/widgets/verificate_phone_number/veritifacate_phone_number.dart';

import 'package:home_kitchen/signin/signin.dart';

class SignInScreen extends StatelessWidget {
  SignInScreen(
      {Key key,
      this.onNavigation,
      this.hideExecutorButton = false,
      bool showEnterAsCook})
      : this.showEnterAsCook = showEnterAsCook ?? true,
        super(key: key);

  final _formsPageViewController = PageController();
  final Function onNavigation;
  final bool showEnterAsCook;
  final bool hideExecutorButton;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VerificatePhoneNumberBloc>(
        create: (_) => VerificatePhoneNumberBloc(
              authRepository: RepositoryProvider.of<AuthRepository>(context),
            ),
        child: _forms(context));
  }

  Widget _forms(BuildContext context) {
    return BlocListener<SignInBloc, SignInState>(
        listener: (BuildContext context, SignInState state) {
      if (state.status.isSubmissionSuccess) {
        onNavigation();
      } else if (state.status.isSubmissionFailure) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(content: Text(state.errorMessage)));
      }
    }, child: BlocBuilder<SignInBloc, SignInState>(
            builder: (BuildContext context, SignInState state) {
      return PageView(
        controller: _formsPageViewController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('phoneNumberForm'),
            child: VeritificatePhoneNumberForm(
                hideExecutorButton: hideExecutorButton,
                type: SIGNIN_TYPE,
                phoneNumber: state.phoneNumber,
                onChangePhoneNumber: (val) =>
                    BlocProvider.of<SignInBloc>(context)
                        .add(SignInPhoneNumberChanged(val)),
                onSave: () {
                  if (state.status.isValidated) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    _nextFormStep();
                  }
                }),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('codeForm'),
            child: VeritificateCodeForm(
              type: 'authorization',
              phoneNumber: state.phoneNumber,
              onSave: (String value) {
                if (state.status.isValidated) {
                  context.bloc<SignInBloc>().add(SignInSubmitted(value));
                }
              },
            ),
          ),
        ],
      );
    }));
  }

  void _nextFormStep() {
    _formsPageViewController.nextPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  bool _onWillPop() {
    if (_formsPageViewController.page.round() ==
        _formsPageViewController.initialPage) return true;

    _formsPageViewController.previousPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );

    return false;
  }
}
