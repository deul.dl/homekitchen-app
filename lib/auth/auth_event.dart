import 'package:equatable/equatable.dart';

import 'package:home_kitchen/auth/auth.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AuthStatusChanged extends AuthEvent {
  const AuthStatusChanged(this.status);

  final AuthStatus status;

  @override
  List<Object> get props => [status];
}

class AuthCustomerChanged extends AuthEvent {}

class AuthExecutorChanged extends AuthEvent {}

class AuthUserUpdated extends AuthEvent {}

class AuthLogoutRequested extends AuthEvent {}
