import 'package:equatable/equatable.dart';

import 'package:home_kitchen/auth/auth.dart';

class AuthState extends Equatable {
  const AuthState._({
    this.status = AuthStatus.unknown,
    this.user,
  });

  const AuthState.unknown() : this._();

  const AuthState.authenticated(User user)
      : this._(status: AuthStatus.authenticated, user: user);

  const AuthState.unauthenticated()
      : this._(status: AuthStatus.unauthenticated);

  final AuthStatus status;
  final User user;

  bool get isAuth => status == AuthStatus.authenticated;

  bool get isExecutor => isAuth && user != null && user.currentRole == EXECUTOR;

  bool get isCustomer => isAuth && user != null && user.currentRole == CUSTOMER;

  bool hasRoles(String name) => user.roles.any((element) => element == name);

  @override
  List<Object> get props => [status, user];
}
