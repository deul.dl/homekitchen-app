import 'dart:async';
import 'package:home_kitchen/client/client.dart';
import 'package:rxdart/subjects.dart';

import 'package:meta/meta.dart';

import 'package:rxdart/rxdart.dart';

enum AuthStatus { unknown, authenticated, unauthenticated }

class AuthRepository {
  AuthRepository(client) : this._http = client;

  final _authController = BehaviorSubject<AuthStatus>();

  final WebClient _http;

  Stream<AuthStatus> get status async* {
    await Future<void>.delayed(const Duration(
      seconds: 1,
    ));

    yield* _authController.stream;
  }

  Future<dynamic> logInExecutor({
    @required String phoneNumber,
    @required String password,
  }) async {
    assert(phoneNumber != null);
    assert(password != null);

    return _http
        .post('auth/login', {
          'phoneNumber': phoneNumber,
          'key': password,
        })
        .then((value) {})
        .then((value) => _authController.add(AuthStatus.authenticated));
  }

  Future<void> logInCustomer(
      {@required String phoneNumber, @required String code}) async {
    assert(phoneNumber != null);
    assert(code != null);
    return _http
        .post('auth/login',
            {'phoneNumber': phoneNumber, 'key': code, 'type': 'sms_code'})
        .then((value) {})
        .then((value) => _authController.add(AuthStatus.authenticated));
  }

  Future<void> sendCode(String phoneNumber, String type) async {
    assert(phoneNumber != null);

    return _http.post('auth/send-code/$type', {
      'phoneNumber': phoneNumber,
    });
  }

  Future<void> signUp({
    @required String phoneNumber,
    @required String password,
    @required String firstName,
    @required String lastName,
    @required String email,
    @required String code,
    @required String city,
    @required String countryCode,
    num longitude,
    num latitude,
    @required String timezone,
  }) async {
    assert(phoneNumber != null);
    assert(password != null);
    assert(firstName != null);
    assert(lastName != null);
    assert(email != null);
    assert(code != null);
    assert(countryCode != null);
    assert(latitude != null);
    assert(longitude != null);
    assert(city != null);
    assert(timezone != null);

    return _http
        .post('auth/signup', {
          'phoneNumber': phoneNumber,
          'password': password,
          'firstName': firstName,
          'lastName': lastName,
          'email': email,
          'code': code,
          'city': city,
          'longitude': longitude,
          'latitude': latitude,
          'locationCode': countryCode,
          'timezone': timezone,
        })
        .then((value) {})
        .then((value) => _authController.add(AuthStatus.authenticated));
  }

  Future<void> changeRole(
    String role,
  ) async {
    assert(role != null);

    return _http.put('account/change-role', {
      'role': role,
    }).then((res) {
      _authController.add(AuthStatus.authenticated);
    });
  }

  Future<dynamic> logOut() async {
    await _http.deleteSession();

    return _http
        .post('auth/logout', null)
        .then((res) => _authController.add(AuthStatus.unauthenticated))
        .catchError((err) => print(err));
  }

  void dispose() => _authController.close();
}
