import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/models/models.dart';

import 'package:home_kitchen/user/user_repository.dart';
import 'package:home_kitchen/auth/auth.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc({
    @required AuthRepository authRepository,
    @required UserRepository userRepository,
    @required FirebaseMessaging firebaseMessaging,
  })  : assert(authRepository != null),
        assert(userRepository != null),
        assert(firebaseMessaging != null),
        _authRepository = authRepository,
        _userRepository = userRepository,
        _firebaseMessaging = firebaseMessaging,
        super(const AuthState.unknown()) {
    _authStatusSubscription = _authRepository.status.listen(
      (status) => add(AuthStatusChanged(status)),
    );

    _userStateSubscription =
        _userRepository.user.listen((event) => add(AuthUserUpdated()));
  }

  final FirebaseMessaging _firebaseMessaging;

  final AuthRepository _authRepository;
  final UserRepository _userRepository;
  StreamSubscription<AuthStatus> _authStatusSubscription;
  StreamSubscription<UserState> _userStateSubscription;

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is AuthStatusChanged) {
      yield await _mapAuthStatusChangedToState(event);
    } else if (event is AuthLogoutRequested) {
      await _userRepository.putDevices(null);
      await _authRepository.logOut();
    } else if (event is AuthUserUpdated) {
      yield* _mapAuthUserUpdateToState(event);
    } else if (event is AuthCustomerChanged) {
      yield AuthState.unknown();
      await _authRepository.changeRole(CUSTOMER);
    }
  }

  @override
  Future<void> close() {
    _authStatusSubscription?.cancel();
    _userStateSubscription?.cancel();
    _authRepository?.dispose();
    return super.close();
  }

  Stream<AuthState> _mapAuthUserUpdateToState(AuthUserUpdated event) async* {
    final user = await _tryGetUser();
    yield user != null
        ? AuthState.authenticated(user)
        : const AuthState.unauthenticated();
  }

  Future<AuthState> _mapAuthStatusChangedToState(
    AuthStatusChanged event,
  ) async {
    switch (event.status) {
      case AuthStatus.unauthenticated:
        return const AuthState.unauthenticated();
      case AuthStatus.authenticated:
        final user = await _tryGetUser();

        if (user != null) {
          try {
            final fmtToken = await _firebaseMessaging.getToken();

            await _userRepository.putDevices(fmtToken);
            return AuthState.authenticated(user);
          } catch (err) {
            return const AuthState.unauthenticated();
          }
        }
        return const AuthState.unauthenticated();
      default:
        return const AuthState.unknown();
    }
  }

  Future<User> _tryGetUser() async {
    try {
      final user = await _userRepository.getUser();

      return user;
    } on Exception {
      return null;
    }
  }
}
