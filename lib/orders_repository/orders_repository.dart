abstract class OrdersRepositoryBase {
  Future<void> getOrders();

  Future<void> pastOrders();
}

class OrdersRepository extends OrdersRepositoryBase {
  @override
  Future<void> getOrders() async {}

  @override
  Future<void> pastOrders() async {}
}
