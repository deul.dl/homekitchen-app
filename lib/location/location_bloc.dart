import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';

import 'location.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  LocationBloc() : super(LocationSuccess());

  @override
  Stream<LocationState> mapEventToState(
    LocationEvent event,
  ) async* {
    try {
      yield* event.applyAsync(state: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LocationBloc', error: _, stackTrace: stackTrace);
    }
  }
}
