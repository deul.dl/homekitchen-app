import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';

class LocationState extends Equatable {
  @override
  List<Object> get props => [];
}

class LocationSuccess extends LocationState {
  LocationSuccess(
      {this.position,
      this.street,
      this.country,
      this.locality,
      this.countryCode = 'US',
      this.permession = false});

  final Position position;
  final String street;
  final String country;
  final String locality;
  final bool permession;
  final String countryCode;

  @override
  List<Object> get props =>
      [position, street, country, locality, permession, countryCode];
}

class LocationFailed extends LocationState {
  LocationFailed(this.errorMessage);

  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}
