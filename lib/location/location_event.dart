import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/location/location.dart';

@immutable
abstract class LocationEvent {
  Stream<LocationState> applyAsync({LocationState state, LocationBloc bloc});
}

class LocationSendedPermession extends LocationEvent {
  @override
  String toString() => 'LocationSendedPermession {}';

  @override
  Stream<LocationState> applyAsync(
      {LocationState state, LocationBloc bloc}) async* {
    try {
      LocationPermission permission = await Geolocator.checkPermission();

      if (permission.index == 0) {
        await Geolocator.requestPermission();
        yield LocationSuccess(permession: permission.index == 1);
      }
    } catch (_) {
      yield LocationFailed(_?.toString());
    }
  }
}

class LocationGotPosition extends LocationEvent {
  @override
  String toString() => 'LocationGotPosition {}';

  @override
  Stream<LocationState> applyAsync(
      {LocationState state, LocationBloc bloc}) async* {
    try {
      if ((state as LocationSuccess).permession) {
        bloc.add(LocationSendedPermession());
      }

      final Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);

      List<Placemark> p = await placemarkFromCoordinates(
          position.latitude, position.longitude,
          localeIdentifier: 'en');

      Placemark place = p[0];

      yield LocationSuccess(
          position: position,
          street: place.street,
          country: place.country,
          locality: place.locality,
          countryCode: place.isoCountryCode,
          permession: (state as LocationSuccess).permession);
    } catch (_) {
      yield LocationFailed(_?.toString());
    }
  }
}
