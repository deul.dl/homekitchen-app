import 'package:equatable/equatable.dart';

abstract class VerificateEvent extends Equatable {
  const VerificateEvent();

  @override
  List<Object> get props => [];
}

class VerificatePhoneNumberSubmitted extends VerificateEvent {
  final String phoneNumber;
  final String type;

  VerificatePhoneNumberSubmitted(this.phoneNumber, this.type);
}

class VeritifacatePhoneNumberChanged extends VerificateEvent {
  const VeritifacatePhoneNumberChanged(this.phoneNumber);

  final String phoneNumber;
}
