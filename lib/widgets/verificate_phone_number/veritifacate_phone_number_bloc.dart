import 'dart:async';
import 'dart:io';

import 'package:home_kitchen/accounts/profile/update_phone_number/update_phone_number.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'veritifacate_phone_number.dart';

const SIGNUP_TYPE = 'signup';
const SIGNIN_TYPE = 'login';
const CHECK_TYPE = 'check';

class VerificatePhoneNumberBloc
    extends Bloc<VerificateEvent, VerificatePhoneNumberState> {
  VerificatePhoneNumberBloc({
    @required AuthRepository authRepository,
  })  : assert(authRepository != null),
        _authRepository = authRepository,
        super(VerificatePhoneNumberState());

  final AuthRepository _authRepository;

  @override
  Stream<VerificatePhoneNumberState> mapEventToState(
    VerificateEvent event,
  ) async* {
    if (event is VeritifacatePhoneNumberChanged) {
      yield* mapPhoneNumberToStateChanged(event);
    }
    if (event is VerificatePhoneNumberSubmitted) {
      yield* mapSendPhoneNumberToState(event);
    }
  }

  Stream<VerificatePhoneNumberState> mapPhoneNumberToStateChanged(
    VeritifacatePhoneNumberChanged event,
  ) async* {
    final phoneNumber = ZPhoneNumber.dirty(event.phoneNumber);
    yield state.copyWith(
      phoneNumber: phoneNumber,
      status: Formz.validate([phoneNumber]),
    );
  }

  Stream<VerificatePhoneNumberState> mapSendPhoneNumberToState(
    VerificatePhoneNumberSubmitted event,
  ) async* {
    yield state.copyWith(status: FormzStatus.valid);
    if (state.status.isValidated) {
      try {
        yield state.copyWith(status: FormzStatus.submissionInProgress);
        await _authRepository.sendCode(event.phoneNumber, event.type);
        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } on HttpException catch (e) {
        yield state.copyWith(
            status: FormzStatus.submissionFailure, errorMessage: e?.message);
      }
    }
  }
}
