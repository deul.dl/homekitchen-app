import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:home_kitchen/executor/login/login_page.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/formz_inputs/formz_inputs.dart';
import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/widgets/phone_number_input.dart';

import 'veritifacate_phone_number.dart';

class VeritificatePhoneNumberForm extends StatefulWidget {
  VeritificatePhoneNumberForm(
      {Key key,
      this.phoneNumber,
      this.onChangePhoneNumber,
      this.onSave,
      this.type = "check",
      this.hideExecutorButton = false})
      : super(key: key);

  final ZPhoneNumber phoneNumber;
  final String type;
  final Function onSave;
  final Function onChangePhoneNumber;
  final bool hideExecutorButton;

  @override
  _VeritificatePhoneNumberState createState() =>
      _VeritificatePhoneNumberState();
}

class _VeritificatePhoneNumberState extends State<VeritificatePhoneNumberForm> {
  TextEditingController _phoneNumberContoller;

  @override
  void initState() {
    _phoneNumberContoller =
        TextEditingController(text: widget.phoneNumber.value);
    BlocProvider.of<VerificatePhoneNumberBloc>(context)
        .add(VeritifacatePhoneNumberChanged(widget.phoneNumber.value));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<VerificatePhoneNumberBloc, VerificatePhoneNumberState>(
        listener: (BuildContext context, VerificatePhoneNumberState state) {
      if (state.status.isSubmissionFailure) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(SnackBar(content: Text('${state.errorMessage}')));
      }
      if (state.status.isSubmissionSuccess) {
        widget.onSave();
      }
    }, child:
            BlocBuilder<VerificatePhoneNumberBloc, VerificatePhoneNumberState>(
                builder:
                    (BuildContext context, VerificatePhoneNumberState state) {
      return Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FormTop(
                title: FlutterI18n.translate(
                    context, "verificatePhoneNumber.EditPhoneNumber.Title"),
                error: state.errorMessage,
                child: PhoneNumberInput(
                  textFieldController: _phoneNumberContoller,
                  onInputChanged: (value) {
                    widget.onChangePhoneNumber(value);
                    BlocProvider.of<VerificatePhoneNumberBloc>(context)
                        .add(VeritifacatePhoneNumberChanged(value));
                  },
                  errorText: state.phoneNumber.invalid
                      ? FlutterI18n.translate(context,
                          "verificatePhoneNumber.EditPhoneNumber.Form.PhoneNumber.Error")
                      : null,
                ),
              ),
              Column(children: [
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.bottomCenter,
                  child: ExpandedFlatButton(
                      label: FlutterI18n.translate(context, "common.Next"),
                      onPressed: () {
                        BlocProvider.of<VerificatePhoneNumberBloc>(context).add(
                            VerificatePhoneNumberSubmitted(
                                widget.phoneNumber.phoneNumberWithPlus,
                                widget.type));
                      }),
                ),
                if (widget.type == SIGNIN_TYPE && !widget.hideExecutorButton)
                  Container(
                      padding: EdgeInsets.only(bottom: 10),
                      alignment: Alignment.bottomCenter,
                      child: SizedBox(
                          height: 48,
                          width: double.infinity,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0),
                              ),
                              backgroundColor: Colors.white,
                              side: BorderSide(
                                  color: Theme.of(context).primaryColor),
                            ),
                            child: Text(
                              FlutterI18n.translate(
                                  context, 'common.EnterAsCook'),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor),
                            ),
                            onPressed: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        LoginPage())),
                          )))
              ])
            ],
          ));
    }));
  }
}
