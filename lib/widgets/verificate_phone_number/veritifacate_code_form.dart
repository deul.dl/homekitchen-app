import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/formz_inputs/formz_inputs.dart';
import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';

import 'veritifacate_phone_number.dart';

class VeritificateCodeForm extends StatefulWidget {
  VeritificateCodeForm(
      {Key key,
      this.phoneNumber,
      this.onSave,
      this.type = 'check',
      this.showEnterAsCook})
      : super(key: key);

  final ZPhoneNumber phoneNumber;
  final String type;
  final Function onSave;
  final Function showEnterAsCook;

  @override
  _VeritificateCodeState createState() => _VeritificateCodeState();
}

class _VeritificateCodeState extends State<VeritificateCodeForm> {
  String _code;
  bool _onEditing = true;

  Timer _timer;
  int _start = 0;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  void startTimer() {
    setState(() => _start = 2 * 60);
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer?.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<VerificatePhoneNumberBloc, VerificatePhoneNumberState>(
        listener: (BuildContext context, VerificatePhoneNumberState state) {
      if (state.status.isSubmissionFailure) {
        ScaffoldMessenger.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(SnackBar(content: Text('${state.errorMessage}')));
      }
      if (state.status.isSubmissionSuccess) {
        widget.onSave(_code);
      }
    }, child:
            BlocBuilder<VerificatePhoneNumberBloc, VerificatePhoneNumberState>(
                builder: (context, state) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FormTop(
                title: FlutterI18n.translate(
                    context, 'verificatePhoneNumber.EditCode.Title'),
                error: state.errorMessage,
                child: Column(children: [
                  VerificationCode(
                    textStyle:
                        TextStyle(fontSize: 20.0, color: Colors.red[900]),
                    underlineColor: Colors.amber,
                    keyboardType: TextInputType.number,
                    length: 6,
                    // clearAll is NOT required, you can delete it
                    // takes any widget, so you can implement your design
                    clearAll: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        FlutterI18n.translate(context, 'common.ClearAll'),
                        style: TextStyle(
                            fontSize: 14.0,
                            decoration: TextDecoration.underline,
                            color: Colors.blue[700]),
                      ),
                    ),
                    onCompleted: (String value) {
                      setState(() {
                        _code = value;
                      });
                      widget.onSave(value);
                    },
                    onEditing: (bool value) {
                      setState(() {
                        _onEditing = value;
                      });
                      if (!_onEditing) FocusScope.of(context).unfocus();
                    },
                  ),
                  Padding(
                      padding: EdgeInsets.only(top: 40),
                      child: _start == 0
                          ? _ReloadButton(onTap: () {
                              _sendAgainCode();
                            })
                          : Text(
                              FlutterI18n.translate(context,
                                  'verificatePhoneNumber.EditCode.Pause',
                                  translationParams: {
                                    'timeseconds': _start.toString(),
                                  }),
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                            )),
                ])),
            Container(
              padding: EdgeInsets.only(bottom: 10),
              alignment: Alignment.bottomCenter,
              child: ExpandedFlatButton(
                label: FlutterI18n.translate(context, 'common.Next'),
                onPressed: () => widget.onSave(_code),
              ),
            )
          ],
        ),
      );
    }));
  }

  void _sendAgainCode() {
    startTimer();
    BlocProvider.of<VerificatePhoneNumberBloc>(context).add(
        VerificatePhoneNumberSubmitted(widget.phoneNumber.value, widget.type));
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }
}

class _ReloadButton extends StatelessWidget {
  _ReloadButton({this.onTap});

  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: Column(
          children: [
            Text(
              FlutterI18n.translate(
                  context, 'verificatePhoneNumber.SendAgainSms'),
              style: TextStyle(
                  color: Colors.blueAccent,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            Icon(
              Icons.repeat,
              color: Colors.blue,
              size: 24,
            ),
          ],
        ));
  }
}
