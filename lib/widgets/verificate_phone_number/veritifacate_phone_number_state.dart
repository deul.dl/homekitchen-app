import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/formz_inputs/formz_inputs.dart';

class VerificatePhoneNumberState extends Equatable {
  const VerificatePhoneNumberState({
    this.status = FormzStatus.pure,
    this.phoneNumber = const ZPhoneNumber.pure(),
    this.errorMessage,
  });

  final FormzStatus status;
  final String errorMessage;
  final ZPhoneNumber phoneNumber;

  VerificatePhoneNumberState copyWith(
      {FormzStatus status, ZPhoneNumber phoneNumber, String errorMessage}) {
    return VerificatePhoneNumberState(
        status: status ?? this.status,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  List<Object> get props => [phoneNumber, status, errorMessage];
}
