import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/widgets/online_status.dart';

class AuthorAvatar extends StatelessWidget {
  AuthorAvatar({Key key, @required this.store}) : super(key: key);

  final Store store;

  final defaulWidth = 40.0;
  final defaultHeight = 40.0;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (store.imageSource.isNotEmpty
          ? store.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  Widget build(BuildContext context) {
    return SizedBox(
        height: 50,
        child: Row(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(25.0),
              child: CachedNetworkImage(
                imageUrl: urlImage,
                width: defaulWidth,
                height: defaultHeight,
                placeholder: (context, url) =>
                    Image.asset('assets/images/placeholder.jpg'),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            SizedBox(
              width: 9.0,
            ),
            OnlineStatus(store: store),
            SizedBox(
              width: 4.0,
            ),
            Expanded(
                child: Text(store.name,
                    style: TextStyle(
                      fontSize: 18,
                      //fontWeight: FontWeight.bold,
                    ))),
          ],
        ));
  }
}
