import 'package:flutter/material.dart';

class Counter extends StatelessWidget {
  Counter(
      {@required this.count, @required this.onPlus, @required this.onMinus});

  final int count;
  final Function onPlus;
  final Function onMinus;

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      roundButton(icon: Icon(Icons.remove, size: 16), onPressed: onMinus),
      Text(count.toString()),
      roundButton(icon: Icon(Icons.add, size: 16), onPressed: onPlus),
    ]);
  }

  Widget roundButton({Icon icon, Function onPressed}) {
    return RawMaterialButton(
      onPressed: onPressed,
      child: icon,
      shape: CircleBorder(),
      elevation: 2.0,
      fillColor: Colors.white70,
      constraints: BoxConstraints(minWidth: 60.0, minHeight: 36.0),
      padding: const EdgeInsets.all(5.0),
    );
  }
}
