import 'package:flutter/material.dart';

import 'package:home_kitchen/models/item.dart';
import 'package:home_kitchen/common_settings.dart';

class ListItemCard extends StatelessWidget {
  ListItemCard({@required this.item, @required this.onTap});

  final Item item;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return GestureDetector(
      onTap: onTap,
      child: Card(
          elevation: 0,
          child: Row(
            children: <Widget>[
              Image.network(item.imageSource, height: 100, width: 100),
              SizedBox(width: 20),
              Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.name,
                      style: TextStyle(
                        fontSize: 21,
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    SizedBox(
                        width: MediaQuery.of(context).size.width - 135,
                        child: Text(
                          item.description,
                          style: TextStyle(color: Colors.black54),
                          maxLines: 8,
                          overflow: TextOverflow.ellipsis,
                        )),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      formatCurrency.printPrice(item.price),
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    )
                  ])
            ],
          )),
    );
  }
}
