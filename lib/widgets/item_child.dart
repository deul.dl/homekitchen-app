import 'package:flutter/material.dart';

class ItemChild extends StatelessWidget {
  ItemChild({Key key, @required this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(
              color: Theme.of(context).backgroundColor,
              width: 1.0,
            ),
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15.0),
        child: child);
  }
}
