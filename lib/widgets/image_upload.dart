import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:image_picker/image_picker.dart';

class ImageUpload extends StatefulWidget {
  ImageUpload({
    this.description,
    this.placeholderImage,
    this.height,
    this.onUploaded,
    File initialValue,
  }) : this.initialValue = initialValue ?? null;

  final String placeholderImage;
  final String description;
  final double height;
  final double heightButton = 48;
  final Function(File file) onUploaded;
  final File initialValue;

  @override
  _ImageUploadState createState() => _ImageUploadState();
}

class _ImageUploadState extends State<ImageUpload> {
  _ImageUploadState() : _picker = ImagePicker();

  final ImagePicker _picker;
  Future<File> _imageFile;

  @override
  void initState() {
    super.initState();

    if (widget.initialValue != null) {
      setState(() {
        _imageFile = Future.sync(() => widget.initialValue);
      });
    }
  }

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (widget.placeholderImage.isNotEmpty
          ? widget.placeholderImage
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<File>(
        future: _imageFile,
        initialData: widget.initialValue,
        builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Column(
                children: [
                  if (widget.placeholderImage != null && _imageFile == null)
                    Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: CachedNetworkImage(
                          imageUrl: urlImage,
                          width: 200,
                          height: 200,
                          placeholder: (context, url) =>
                              Image.asset('assets/images/placeholder.jpg'),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        )),
                  _button(FlutterI18n.translate(
                    context,
                    'uploadImage.Buttons.ChooseImage',
                  )),
                  if (widget.description != null)
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(widget.description),
                    ),
                  if (widget.placeholderImage == null)
                    SizedBox(
                      height: widget.height,
                    )
                ],
              );
            case ConnectionState.done:
              return Column(
                children: [
                  Padding(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Image.file(
                        snapshot.data,
                        width: double.infinity,
                        height: widget.height,
                      )),
                  _button(FlutterI18n.translate(
                    context,
                    'uploadImage.Buttons.UploadImage',
                  )),
                ],
              );
            default:
              if (snapshot.hasError) {
                return Text(
                  FlutterI18n.translate(context, 'uploadImage.Error',
                      translationParams: {'error': snapshot.error}),
                  textAlign: TextAlign.center,
                );
              } else {
                return Text(
                  FlutterI18n.translate(context, 'uploadImage.Empty'),
                  textAlign: TextAlign.center,
                );
              }
          }
        });
  }

  Widget _button(String title) {
    return SizedBox(
        height: widget.heightButton,
        width: double.infinity,
        child: TextButton.icon(
          onPressed: () => _onImageButtonPressed(ImageSource.gallery),
          style: TextButton.styleFrom(
            backgroundColor: Theme.of(context).primaryColor,
          ),
          icon: Icon(
            Icons.file_upload,
            color: Colors.white,
          ),
          label: Text(
            title,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ));
  }

  void _onImageButtonPressed(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    widget.onUploaded(File(pickedFile.path));
    setState(() {
      _imageFile = Future.sync(() => File(pickedFile.path));
    });
  }
}
