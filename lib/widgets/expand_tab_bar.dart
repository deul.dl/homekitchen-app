import 'package:flutter/material.dart';

class ExpandTabBar extends StatefulWidget {
  ExpandTabBar({Key key, @required this.tabs, @required this.controller})
      : super(key: key);

  final List<Widget> tabs;
  final TabController controller;

  @override
  _ExpandTabBarState createState() => _ExpandTabBarState();
}

class _ExpandTabBarState extends State<ExpandTabBar> {
  _ExpandTabBarState();

  @override
  Widget build(BuildContext context) {
    Widget tabBar = TabBar(
      controller: widget.controller,
      tabs: widget.tabs,
      labelColor: Theme.of(context).primaryColor,
      indicatorColor: Theme.of(context).primaryColor,
      unselectedLabelColor: Colors.grey,
      labelStyle: TextStyle(
        fontWeight: FontWeight.bold,
      ),
      unselectedLabelStyle: TextStyle(
        fontWeight: FontWeight.bold,
      ),
    );

    tabBar = Container(height: 48, color: Colors.white, child: tabBar);

    return Expanded(child: tabBar);
  }
}
