import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget implements PreferredSizeWidget {
  SearchBar(
      {Key key,
      @required this.height,
      this.bottom,
      this.onNextSearch,
      showSearch})
      : this.showSearch = showSearch ?? false,
        super(key: key);

  final double height;
  final bool showSearch;
  Function onNextSearch;
  Widget bottom;

  @override
  _SearchBarState createState() => _SearchBarState(bottom, onNextSearch);

  @override
  Size get preferredSize => Size.fromHeight(height);
}

class _SearchBarState extends State<SearchBar> {
  Widget bottom;
  Function onNextSearch;
  _SearchBarState(this.bottom, this.onNextSearch);

  @override
  Widget build(BuildContext context) {
    Widget searchBar = Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        if (widget.showSearch)
          Flexible(
            child: _search(context),
          ),
        if (!widget.showSearch) Flexible(child: header(context)),
        if (bottom != null) bottom
      ],
    );

    return SafeArea(top: true, child: searchBar);
  }

  Widget _search(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      //clipBehavior: ,
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 7),
      child: InkWell(
        onTap: onNextSearch,
        child: Card(
          elevation: 3.0,
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.search,
                    color: Colors.black54,
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget header(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 25),
        child: Row(children: [
          Text(
            'HOME KITHCEN',
            style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold),
          )
        ]));
  }
}
