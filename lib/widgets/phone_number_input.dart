import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/services.dart';

import 'package:home_kitchen/location/location.dart';

class PhoneNumberInput extends StatelessWidget {
  PhoneNumberInput(
      {Key key,
      this.hintText,
      this.errorText,
      this.onInputChanged,
      this.textFieldController})
      : super(key: key);
  final String hintText;
  final String errorText;
  final Function onInputChanged;
  final TextEditingController textFieldController;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LocationBloc, LocationState>(
        builder: (BuildContext context, LocationState state) {
      return Container(
          margin: EdgeInsets.only(bottom: 10),
          child: TextField(
            controller: textFieldController,
            onChanged: onInputChanged,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))
            ],
            decoration: InputDecoration(
                prefix: Text('+  '),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context).primaryColor, width: 1.0),
                ),
                errorText: errorText,
                hintText: hintText ??
                    FlutterI18n.translate(context, 'common.PhoneNumber.Label'),
                helperText: FlutterI18n.translate(
                    context, 'common.PhoneNumber.Helper')),
          ));
    });
  }
}
