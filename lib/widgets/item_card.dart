import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:home_kitchen/customer/cart/widgets/cart_actions.dart';

import 'package:home_kitchen/models/item.dart';
import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/theme/colors.dart';

class ItemCard extends StatelessWidget {
  ItemCard(
      {Key key,
      @required this.item,
      double height,
      double width,
      this.showCartButton = false,
      margin,
      @required this.onTap})
      : this.margin = margin ?? EdgeInsets.only(right: 10.0),
        this.width = width ?? 150,
        this.height = height ?? 150,
        super(key: key);

  final double height;
  final double width;
  final EdgeInsets margin;
  final Item item;
  final bool showCartButton;
  final Function onTap;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (item.imageSource.isNotEmpty
          ? item.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    Widget card = Card(
      margin: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: urlImage,
            alignment: Alignment.center,
            fit: BoxFit.cover,
            height: height,
            placeholder: (context, url) =>
                Image.asset('assets/images/placeholder.jpg'),
            errorWidget: (context, url, error) => Icon(Icons.error),
            width: width,
          ),
          SizedBox(
            height: 14,
          ),
          _footer(context)
        ],
      ),
    );

    return GestureDetector(
      onTap: onTap,
      child: card,
    );
  }

  Widget _footer(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return Container(
        padding: EdgeInsets.only(left: 3),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  width: 150,
                  child: Text(
                    item.name,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                  )),
              Text(formatCurrency.printPrice(item.price),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      height: 1.5, fontWeight: FontWeight.bold, fontSize: 16)),
            ],
          ),
          if (showCartButton)
            IconButton(
                icon: Icon(
                  Icons.shopping_basket,
                  color: HomeKitchenColors.blackColor,
                ),
                onPressed: () => CartActions.of(context).addItemToCart(item))
        ]));
  }
}
