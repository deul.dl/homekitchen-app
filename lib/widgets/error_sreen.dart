import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ErrorScreen extends StatelessWidget {
  ErrorScreen({Key key, @required this.errorMessage, @required this.onReload})
      : super(key: key);

  final String errorMessage;
  final Function onReload;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(errorMessage,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.red,
                )),
            SizedBox(
              height: 20,
            ),
            TextButton.icon(
              style: TextButton.styleFrom(backgroundColor: Colors.blue),
              label: Text(FlutterI18n.translate(context, 'common.Reload'),
                  style: TextStyle(color: Colors.white)),
              icon: Icon(Icons.repeat, color: Colors.white),
              onPressed: onReload,
            )
          ],
        ));
  }
}
