import 'package:flutter/material.dart';

import 'package:home_kitchen/models/category.dart';

class CategoryListCard extends StatelessWidget {
  CategoryListCard({Key key, @required this.category, @required this.onTap})
      : super(key: key);

  final Category category;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: Theme.of(context).backgroundColor,
              width: 1.0,
            ),
          ),
        ),
        child: ListTile(
            onTap: onTap,
            trailing: Icon(Icons.keyboard_arrow_right),
            title: Text(
              category.name,
              style: TextStyle(fontSize: 16.0),
            )));
  }
}
