import 'package:flutter/material.dart';

class FormWidgetSelect extends StatefulWidget {
  FormWidgetSelect(
      {Key key,
      this.options,
      this.placeholder,
      this.label,
      this.onChanged,
      this.onSaved,
      this.validator,
      this.value})
      : super(key: key);

  final List<DropdownMenuItem<String>> options;
  final String value;
  final String placeholder;
  final Function onChanged;
  final Function onSaved;
  final Function validator;
  final String label;

  @override
  _FormWidgetSelectState createState() => _FormWidgetSelectState();
}

class _FormWidgetSelectState extends State<FormWidgetSelect> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: DropdownButtonFormField<String>(
          value: widget.value,
          decoration: InputDecoration(
              fillColor: Colors.white,
              filled: true,
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                color: Theme.of(context).primaryColor,
              )),
              hintText: widget.placeholder,
              labelText: widget.label),
          icon: Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          onChanged: widget.onChanged,
          onSaved: widget.onSaved,
          items: widget.options,
          validator: widget.validator,
        ));
  }
}
