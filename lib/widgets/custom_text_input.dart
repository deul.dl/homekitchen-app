import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextInput extends StatelessWidget {
  CustomTextInput(
      {Key key,
      bool autofocus,
      this.initialValue,
      this.label,
      this.error,
      this.controller,
      this.onChanged,
      keyboardType,
      this.inputFormatters,
      this.hintText,
      this.maxLines,
      this.decoration,
      bool obscureText,
      this.prefix,
      this.helper})
      : this.autofocus = autofocus ?? false,
        this.obscureText = obscureText ?? false,
        this.keyboardType = keyboardType ?? TextInputType.text,
        super(key: key);

  final String helper;
  final String label;
  final String error;
  final String initialValue;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final Function onChanged;
  final List<TextInputFormatter> inputFormatters;
  final bool autofocus;
  final String hintText;
  final int maxLines;
  final InputDecoration decoration;
  final bool obscureText;
  final Widget prefix;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: TextField(
          controller: controller,
          key: key,
          obscureText: obscureText,
          autofocus: autofocus,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters,
          onChanged: onChanged,
          maxLines: maxLines,
          decoration: decoration == null
              ? InputDecoration(
                  prefix: prefix,
                  fillColor: Colors.white,
                  labelStyle: TextStyle(color: Colors.black),
                  filled: true,
                  helperText: helper,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.black,
                  )),
                  errorText: error,
                  labelText: label,
                  hintText: hintText)
              : decoration,
        ));
  }
}
