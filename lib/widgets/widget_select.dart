import 'package:flutter/material.dart';

class WidgetSelect extends StatefulWidget {
  WidgetSelect(
      {Key key,
      this.options,
      this.placeholder,
      this.label,
      this.onChanged,
      this.onSaved,
      this.value})
      : super(key: key);

  final List<DropdownMenuItem<String>> options;
  final String value;
  final String placeholder;
  final Function onChanged;
  final Function onSaved;
  final String label;

  @override
  _WidgetSelectState createState() => _WidgetSelectState();
}

class _WidgetSelectState extends State<WidgetSelect> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: DropdownButton<String>(
          dropdownColor: Colors.black,
          value: widget.value,
          onChanged: widget.onChanged,
          items: widget.options,
        ));
  }
}
