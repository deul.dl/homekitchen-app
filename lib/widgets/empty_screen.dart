import 'package:flutter/material.dart';

class EmptyScreen extends StatelessWidget {
  EmptyScreen(
      {Key key,
      this.onReload,
      this.icon = const Icon(
        Icons.list_alt,
        color: Colors.black12,
        size: 150,
      ),
      this.text = 'There is no past order!'})
      : super(key: key);

  final Function onReload;
  final Icon icon;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Column(children: [
        SizedBox(
          height: 40,
        ),
        icon,
        SizedBox(
          height: 12,
        ),
        Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 16, color: Colors.black12),
        ),
        SizedBox(
          height: 20,
        ),
        if (onReload != null)
          TextButton.icon(
            style: TextButton.styleFrom(
              backgroundColor: Colors.blue,
            ),
            label: Text('Reload', style: TextStyle(color: Colors.white)),
            icon: Icon(Icons.repeat, color: Colors.white),
            onPressed: onReload,
          )
      ]),
    );
  }
}
