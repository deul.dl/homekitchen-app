import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomFormInput extends StatelessWidget {
  CustomFormInput(
      {bool autofocus,
      this.initialValue,
      this.label,
      this.onChanged,
      this.onSaved,
      this.controller,
      this.keyboardType,
      this.validator,
      this.inputFormatters,
      this.maxLines,
      this.decoration,
      this.hintText,
      int maxLength,
      this.prefix,
      this.helper})
      : this.autofocus = autofocus ?? false,
        this.maxLength = maxLength ?? null;

  final String helper;
  final String Function(String) validator;
  final String label;
  final TextEditingController controller;
  TextInputType keyboardType = TextInputType.text;
  final Function onChanged;
  final Function onSaved;
  final List<TextInputFormatter> inputFormatters;
  final bool autofocus;
  final String initialValue;
  final int maxLength;
  final int maxLines;
  final InputDecoration decoration;
  final String hintText;
  final Widget prefix;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 10),
        child: TextFormField(
          initialValue: initialValue,
          autofocus: autofocus,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters,
          controller: controller,
          onChanged: onChanged,
          onSaved: onSaved,
          validator: validator,
          maxLength: maxLength,
          maxLines: maxLines,
          decoration: decoration == null
              ? InputDecoration(
                  prefix: prefix,
                  fillColor: Colors.white,
                  labelStyle: TextStyle(color: Colors.black),
                  filled: true,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.black,
                  )),
                  labelText: label,
                  hintText: hintText)
              : decoration,
        ));
  }
}
