import 'package:flutter/material.dart';

class FormTop extends StatelessWidget {
  FormTop(
      {@required this.child,
      @required this.title,
      this.description,
      this.error});

  final Widget child;
  final String title;
  final String description;
  final String error;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (title != null)
          Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: Text(title,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 21.0,
                )),
          ),
        if (description != null)
          Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: Text(description),
          ),
        if (error != null)
          Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: Text(
              error,
              style: TextStyle(color: Colors.red),
            ),
          ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 30.0),
          child: child,
        )
      ],
    );
  }
}
