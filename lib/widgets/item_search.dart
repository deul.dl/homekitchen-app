import 'package:flutter/material.dart';

class ItemSearch extends SearchDelegate<Object> {
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.search),
        onPressed: null
      )
    ];
  }

  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: null
    );
  }

  Widget buildResults(BuildContext context) {
    return Container();
  }

  Widget buildSuggestions(BuildContext context) {
    return Container();
  }
}