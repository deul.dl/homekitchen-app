import 'package:flutter/material.dart';

import 'package:home_kitchen/common_settings.dart';

class PriceListItem extends StatelessWidget {
  final Text label;
  final num amount;

  PriceListItem({Key key, @required this.amount, @required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(
              color: Theme.of(context).backgroundColor,
              width: 1.0,
            ),
          ),
        ),
        alignment: Alignment.topLeft,
        child: ListTile(
          minLeadingWidth: 2,
          tileColor: Colors.white,
          title: label,
          trailing: Text(
            formatCurrency.printPrice(amount),
            style: Theme.of(context).textTheme.headline6,
          ),
        ));
  }
}
