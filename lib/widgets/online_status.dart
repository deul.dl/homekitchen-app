import 'package:flutter/material.dart';
import 'package:home_kitchen/addresses/addresses.dart';

class OnlineStatus extends StatelessWidget {
  OnlineStatus({Key key, this.store}) : super(key: key);
  final Store store;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 5,
        height: 5,
        decoration: ShapeDecoration(
          color: store.status ? Colors.green : Colors.red,
          shape: CircleBorder(),
        ));
  }
}
