import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/models/order.dart';

class OrderCard extends StatelessWidget {
  OrderCard({Key key, this.order, this.onTap, this.index}) : super(key: key);

  final Order order;
  final int index;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    Widget content =
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      Text('#$index', style: Theme.of(context).textTheme.subtitle1),
      SizedBox(
        height: 3,
      ),
      Text(
        FlutterI18n.translate(context, 'order.Status.${order.status}'),
        style: Theme.of(context).textTheme.subtitle1,
      ),
      SizedBox(
        height: 3,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            order.createdAt.toFormat('MMMMd HH::mm:ss'),
            style: TextStyle(
              color: Colors.black54,
            ),
          ),
          Text(
            formatCurrency.printPrice(order.totalPrice),
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      )
    ]);

    return GestureDetector(
        onTap: onTap,
        child: Card(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: content,
        )));
  }
}
