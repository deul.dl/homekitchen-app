import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

import 'package:home_kitchen/customer/order_details/order_details.dart';

import 'order_card.dart';

class HeadingItem implements ListItem {
  HeadingItem(this.heading);

  final String heading;
}

abstract class ListItem {}

class OrderList extends StatelessWidget {
  OrderList({@required this.orders});
  final List<OrderDetails> orders;

  @override
  Widget build(BuildContext context) {
    final sectionList =
        groupBy(orders, (order) => order.createdAt.timeIndicators())
            .entries
            .expand((order) {
      return [HeadingItem(order.key), ...order.value];
    }).toList();

    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        final item = sectionList[index];

        if (item is HeadingItem) {
          return ListTile(
            title: Text(item.heading,
                style: TextStyle(
                    fontSize: 18.0,
                    color: Colors.black54,
                    fontWeight: FontWeight.bold)),
          );
        }
        final OrderDetails order = item as OrderDetails;
        return OrderCard(
            index: index,
            onTap: () {
              Navigator.of(context).pushNamed('/order-details',
                  arguments: {'orderUUID': order.uuid});
            },
            key: Key(order.uuid),
            order: order);
      },
      itemCount: sectionList.length,
    );
  }
}
