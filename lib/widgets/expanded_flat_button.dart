import 'package:flutter/material.dart';

class ExpandedFlatButton extends StatelessWidget {
  ExpandedFlatButton({@required this.onPressed, @required this.label});

  final String label;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 48,
        width: double.infinity,
        child: TextButton(
            style: TextButton.styleFrom(
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0),
              ),
              backgroundColor: Theme.of(context).primaryColor,
            ),
            child: Text(
              label,
              style: TextStyle(color: Colors.white),
            ),
            onPressed: onPressed));
  }
}
