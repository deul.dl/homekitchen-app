import 'package:flutter/material.dart';

const unselectedLabelStyle = TextStyle(color: Colors.black87);

class BottomNavigation extends StatefulWidget {
  BottomNavigation({
    Key key,
    @required this.items,
    @required this.selectedIndex,
    @required this.onChangeScreen,
  }) : super(key: key);

  final int selectedIndex;
  final List<BottomNavigationBarItem> items;
  final Function onChangeScreen;

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  _BottomNavigationState();

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: widget.selectedIndex,
      selectedItemColor: Theme.of(context).bottomAppBarColor,
      unselectedItemColor: Colors.black87,
      unselectedLabelStyle: unselectedLabelStyle,
      showUnselectedLabels: true,
      onTap: widget.onChangeScreen,
      type: BottomNavigationBarType.fixed,
      items: widget.items,
    );
  }
}
