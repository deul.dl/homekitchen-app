import 'package:intl/intl.dart';

abstract class EquatableDateTime extends DateTime {
  EquatableDateTime(
    int year, [
    int month = 1,
    int day = 1,
    int hour = 0,
    int minute = 0,
    int second = 0,
    int millisecond = 0,
    int microsecond = 0,
  ]) : super(year, month, day, hour, minute, second, millisecond, microsecond);

  EquatableDateTime.formDartTime(DateTime dateTime)
      : super(
            dateTime.year,
            dateTime.month,
            dateTime.day,
            dateTime.hour,
            dateTime.minute,
            dateTime.second,
            dateTime.millisecond,
            dateTime.microsecond);

  List<Object> get props =>
      [year, month, day, hour, minute, second, millisecond, microsecond];

  String timeIndicators();

  bool isToday();

  bool isYesterday();

  bool isTomorrow();

  String toFormat(String format);
}

class Date extends EquatableDateTime {
  Date(
    int year, [
    int month = 1,
    int day = 1,
    int hour = 0,
    int minute = 0,
    int second = 0,
    int millisecond = 0,
    int microsecond = 0,
  ]) : super(year, month, day, hour, minute, second, millisecond, microsecond);

  Date.formDartTime(DateTime dateTime)
      : super(
            dateTime.year,
            dateTime.month,
            dateTime.day,
            dateTime.hour,
            dateTime.minute,
            dateTime.second,
            dateTime.millisecond,
            dateTime.microsecond);

  String timeIndicators() {
    if (this.isToday()) {
      return 'Today';
    } else if (this.isYesterday()) {
      return 'Yesterday';
    } else if (this.isTomorrow()) {
      return 'Tomorrow';
    } else {
      return DateFormat.MMMMd().format(this);
    }
  }

  bool isToday() {
    final now = DateTime.now();

    return now.year == this.year &&
        now.month == this.month &&
        now.day == this.day;
  }

  bool isYesterday() {
    final now = DateTime.now();

    return DateTime(this.year, this.month, this.day)
            .difference(DateTime(now.year, now.month, now.day))
            .inDays ==
        -1;
  }

  bool isTomorrow() {
    final now = DateTime.now();

    return DateTime(this.year, this.month, this.day)
            .difference(DateTime(now.year, now.month, now.day))
            .inDays ==
        1;
  }

  String toFormat(String format) {
    return DateFormat(format).format(this);
  }
}
