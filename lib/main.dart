import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:home_kitchen/client/client.dart';
import 'package:rxdart/subjects.dart';
import 'package:flutter/services.dart';

import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:home_kitchen/executor/orders/orders.dart';
import 'package:home_kitchen/location/location.dart';
import 'package:home_kitchen/push_notification/push_notification.dart';
import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/customer/customer.dart';
import 'package:home_kitchen/executor/executor.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/onboarding/onboarding.dart';

/// Streams are created so that app can respond to notification-related events
/// since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
    BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
    BehaviorSubject<String>();

const MethodChannel platform =
    MethodChannel('dexterx.dev/flutter_local_notifications_example');

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message");
}

Future<void> main() async {
  const apiEnv = String.fromEnvironment('API_ENV', defaultValue: 'prod');

  await DotEnv().load('.$apiEnv.env');

  WidgetsFlutterBinding.ensureInitialized();
  final client = WebClient();
  await client.loadCookies();
  await Firebase.initializeApp();

  await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');

  final initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
  );
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  runApp(App(client: client));
}

class App extends StatelessWidget {
  App({Key key, this.client}) : super(key: key);

  final WebClient client;

  @override
  Widget build(BuildContext context) {
    final firebaseMessaging = FirebaseMessaging.instance;

    final userRepository = UserRepository(client);
    final authRepository = AuthRepository(client);
    final orderStatusRepository = OrderStatusRepository(client);

    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<AuthRepository>(
            create: (context) => authRepository,
          ),
          RepositoryProvider<UserRepository>(
            create: (context) => userRepository,
          ),
          RepositoryProvider<WebClient>(
            create: (context) => client,
          ),
          RepositoryProvider<OrderStatusRepository>(
            create: (context) => orderStatusRepository,
          ),
        ],
        child: MultiBlocProvider(providers: [
          BlocProvider<LocationBloc>(
            create: (_) => LocationBloc()..add(LocationGotPosition()),
          ),
          BlocProvider(
              create: (_) => AuthBloc(
                  firebaseMessaging: firebaseMessaging,
                  userRepository: userRepository,
                  authRepository: authRepository)
                ..add(AuthUserUpdated()))
        ], child: HomeKitchenApp(firebaseMessaging, orderStatusRepository)));
  }
}

class HomeKitchenApp extends StatefulWidget {
  HomeKitchenApp(this.firebaseMessaging, this.orderStatusRepository);

  final FirebaseMessaging firebaseMessaging;
  final OrderStatusRepository orderStatusRepository;

  @override
  _HomeKitchenAppState createState() => _HomeKitchenAppState();
}

class _HomeKitchenAppState extends State<HomeKitchenApp> {
  @override
  void initState() {
    PushNotification(
            languageCode: WidgetsBinding.instance.window.locale.languageCode,
            orderStatusRepository: widget.orderStatusRepository)
        .init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, authState) {
          switch (authState.status) {
            case AuthStatus.authenticated:
            case AuthStatus.unauthenticated:
              return _renderView(context, authState);

            default:
              return Onboarding();
          }
        },
      ),
    );
  }

  static Widget _renderView(BuildContext context, AuthState authState) {
    return BlocBuilder<LocationBloc, LocationState>(
        builder: (context, locationState) {
      if (locationState is LocationSuccess) {
        return CommonSettings(
            countryCode: locationState.countryCode,
            child: UserRoom(
              key: Key('UserRoom'),
              state: authState,
            ));
      } else {
        return Container(
          color: Colors.white,
          child: Column(children: [
            Icon(Icons.location_city),
            SizedBox(
              height: 20,
            ),
            Text('Allow locatiion'),
            TextButton(
                onPressed: () => BlocProvider.of<LocationBloc>(context)
                    .add(LocationGotPosition()),
                child: Text('Get permession'))
          ]),
        );
      }
    });
  }
}

class UserRoom extends StatelessWidget {
  UserRoom({Key key, this.state}) : super(key: key);

  final AuthState state;

  @override
  Widget build(BuildContext context) {
    if (state.user != null && state.user.currentRole == EXECUTOR) {
      return HomeKitchenExecutor(authState: state);
    }

    return HomeKitchenCustomer(state: state);
  }
}
