import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';

const url = 'https://t.me/hhhkitchen';

class BanScreen extends StatelessWidget {
  BanScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      width: double.infinity,
      color: Theme.of(context).backgroundColor,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
                padding: EdgeInsets.only(top: 200),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.pan_tool,
                        color: Theme.of(context).primaryColor,
                        size: 120,
                      ),
                      Text(
                        FlutterI18n.translate(context, 'block.Ban.Title'),
                        style: TextStyle(
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            color: Colors.black54),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        FlutterI18n.translate(context, 'block.Ban.Description'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.black54),
                      ),
                    ])),
            ExpandedFlatButton(
                onPressed: _openTelegram,
                label: FlutterI18n.translate(context, 'common.Support'))
          ]),
    );
  }

  void _openTelegram() async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
