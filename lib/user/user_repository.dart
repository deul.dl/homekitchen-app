import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/client/client.dart';

abstract class UserRepositoryBase {
  Future<User> getUser();

  Future<dynamic> updatePhoneNumber(
      {@required String phoneNumber,
      @required String oldPhoneNumber,
      @required String code});

  Future<dynamic> updateEmail(String email);

  Future<dynamic> updateFullName({
    @required String firstName,
    @required String lastName,
  });

  Future<dynamic> putDevices(String fmtToken);
}

enum UserState { update }

class UserRepository extends UserRepositoryBase {
  UserRepository(client) : this._client = client;

  WebClient _client;

  final _controller = StreamController<UserState>();

  Stream<UserState> get user async* {
    yield* _controller.stream;
  }

  @override
  Future<User> getUser() async {
    return _client.get('user/get').then((user) => User.fromJson(user['data']));
  }

  @override
  Future<dynamic> putDevices(String token) async {
    return _client.put('user/devices', {'fmtToken': token});
  }

  @override
  Future<dynamic> updatePhoneNumber({
    @required String phoneNumber,
    @required String code,
    @required String oldPhoneNumber,
  }) async {
    assert(phoneNumber != null);
    assert(code != null);
    assert(oldPhoneNumber != null);
    return _client.put('user/phone-number', {
      'phoneNumber': phoneNumber,
      'oldPhoneNumber': oldPhoneNumber,
      'code': code
    }).then((res) {
      _controller.add(UserState.update);
      return res['data'];
    });
  }

  @override
  Future<dynamic> updateEmail(String email) async {
    assert(email != null);
    return _client
        .put('user/email', {'email': email}).then((res) => res['data']);
  }

  @override
  Future<dynamic> updateFullName({
    @required String firstName,
    @required String lastName,
  }) async {
    assert(firstName != null);
    assert(lastName != null);
    return _client.put('user/full-name', {
      'firstName': firstName,
      'lastName': lastName,
    }).then((res) => res['data']);
  }
}
