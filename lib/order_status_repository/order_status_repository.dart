import 'dart:async';
import 'package:rxdart/rxdart.dart';

import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/customer/main/home/home.dart';

abstract class OrderStatusRepositoryBase {
  final _controller = BehaviorSubject<OrderStatus>();

  Stream<OrderStatus> get statuses async* {
    yield* _controller.stream;
  }

  Future<dynamic> getOrderStatuses(String orderUUID);

  Future<dynamic> rejectOrder(String orderUUID);

  Future<dynamic> acceptOrder(String orderUUID);

  Future<dynamic> startOrder(String orderUUID);

  Future<dynamic> doneOrder(String orderUUID);

  Future<dynamic> paidOrder(String orderUUID);

  Future<dynamic> completeOrder(String orderUUID);
}

class OrderStatusRepository extends OrderStatusRepositoryBase {
  OrderStatusRepository(client) : this._client = client;

  final WebClient _client;

  Future<void> put(OrderStatus status) async {
    _controller.add(status);
  }

  @override
  Future<dynamic> getOrderStatuses(String ordeUUID) async {
    return _client
        .get('customer/orders/$ordeUUID/statuses')
        .then((res) => res['data']['orderStatuses'])
        .then((orderItems) => orderItems == null
            ? null
            : (orderItems as List)
                .map((orderUUID) => OrderStatus.fromJson(orderUUID))
                .toList());
  }

  @override
  Future<dynamic> completeOrder(String orderUUID) async {
    return _client.put('customer/orders/$orderUUID/complete', null).then((res) {
      _controller.add(OrderStatus.fromJson(res['data']));
    });
  }

  @override
  Future<dynamic> rejectOrder(String orderUUID) async {
    return _client.put('executor/orders/$orderUUID/reject', null).then((res) {
      _controller.add(OrderStatus.fromJson(res['data']));
    });
  }

  @override
  Future<dynamic> acceptOrder(String orderUUID) async {
    return _client.put('executor/orders/$orderUUID/accept', null).then((res) {
      _controller.add(OrderStatus.fromJson(res['data']));
    });
  }

  @override
  Future<dynamic> startOrder(String orderUUID) async {
    return _client.put('executor/orders/$orderUUID/start', null).then((res) {
      _controller.add(OrderStatus.fromJson(res['data']));
    });
  }

  @override
  Future<dynamic> doneOrder(String orderUUID) async {
    return _client.put('executor/orders/$orderUUID/done', null).then((res) {
      _controller.add(OrderStatus.fromJson(res['data']));
    });
  }

  @override
  Future<dynamic> paidOrder(String orderUUID) async {
    return _client.put('executor/orders/$orderUUID/pay', null).then((res) {
      _controller.add(OrderStatus.fromJson(res['data']));
    });
  }
}
