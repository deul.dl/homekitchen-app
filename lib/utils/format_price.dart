import 'package:intl/intl.dart';

class PriceFormat {
  PriceFormat({countryCode}) {
    if (countryCode == 'KZ') {
      this._currencyFormat = NumberFormat.currency(
          name: 'currency', decimalDigits: 2, symbol: '₸', locale: 'ru');
    } else if (countryCode == 'RU') {
      this._currencyFormat = NumberFormat.currency(
          name: 'currency', decimalDigits: 2, symbol: 'руб.', locale: 'ru');
    } else {
      this._currencyFormat = _currencyFormat = NumberFormat.currency(
          name: 'dollars', decimalDigits: 2, symbol: '\$');
    }
  }

  NumberFormat _currencyFormat;

  String printPrice(amount) => _currencyFormat.format(amount);
}
