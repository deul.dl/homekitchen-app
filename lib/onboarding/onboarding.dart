import 'package:flutter/material.dart';

class Onboarding extends StatelessWidget {
  Onboarding({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: Colors.white,
      child: Image.asset(
        'assets/images/logo.png',
        key: const Key('splash_bloc_image'),
        width: 250,
      ),
    );
  }
}
