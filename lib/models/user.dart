import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

const CUSTOMER = 'Customer';
const EXECUTOR = 'Executor';

@immutable
class User extends Equatable {
  final String uuid;
  final String email;
  final String phoneNumber;
  final String currentRole;
  final List<String> roles;
  final String firstName;
  final String lastName;
  final String status;

  User(
      {uuid,
      this.phoneNumber,
      this.email,
      this.currentRole,
      this.roles,
      this.firstName,
      this.lastName,
      this.status})
      : this.uuid = uuid ?? Uuid().v4();

  @override
  List<Object> get props => [
        uuid,
        phoneNumber,
        email,
        status,
        roles,
        currentRole,
        firstName,
        lastName
      ];

  String get fullName => '$firstName $lastName';

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      uuid: json['uuid'] as String,
      phoneNumber: json['phoneNumber'] as String,
      email: json['email'] as String,
      currentRole: json['currentRole'] as String,
      status: json['status'] as String,
      roles: (json['roles'] as List<dynamic>).map((e) => e as String).toList(),
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['uuid'] = this.uuid;
    data['phoneNumber'] = this.phoneNumber;
    data['email'] = this.email;
    data['status'] = this.status;
    data['currentRole'] = this.currentRole;
    data['roles'] = this.roles;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;

    return data;
  }
}
