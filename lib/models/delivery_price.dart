import 'package:equatable/equatable.dart';

class DeliveryPrice extends Equatable {
  final String uuid;
  final String countryCode;
  final String country;
  final String city;
  final num price;
  final String createdAt;
  final String updatedAt;

  DeliveryPrice(
      {this.uuid,
      this.countryCode,
      this.country,
      this.city,
      this.price,
      this.createdAt,
      this.updatedAt});

  @override
  // TODO: implement props
  List<Object> get props =>
      [uuid, countryCode, country, city, price, createdAt, updatedAt];

  @override
  String toString() {
    return '''StatusUpdates {
      uuid:  $uuid,
      countryCode: $countryCode,
      country: $country,
      city: $city,
      price: $price,
      createdAt: $createdAt,
      updatedAt: $updatedAt
    }''';
  }

  factory DeliveryPrice.fromJson(Map<String, dynamic> json) {
    return DeliveryPrice(
      uuid: json['uuid'] as String,
      countryCode: json['countryCode'] as String,
      country: json['country'] as String,
      city: json['city'] as String,
      price: json['price'] as num,
      createdAt: json['createdAt'] as String,
      updatedAt: json['updatedAt'] as String,
    );
  }

  DeliveryPrice copy({
    String uuid,
    String countryCode,
    String country,
    String city,
    double price,
  }) {
    return DeliveryPrice(
      uuid: uuid ?? this.uuid,
      countryCode: countryCode ?? this.countryCode,
      country: country ?? this.country,
      city: city ?? this.city,
      price: price ?? this.price,
    );
  }
}
