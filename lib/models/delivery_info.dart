import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'address.dart';
import 'payment_method.dart';

@immutable
class DeliveryInfo extends Equatable {
  final double cash_return;
  final Address address;
  final PaymentMethod payment_method;

  DeliveryInfo({this.address, this.cash_return, this.payment_method});

  @override
  List<Object> get props => [cash_return, address, payment_method];

  @override
  String toString() {
    return '''DeliveryInfo {
      cash_return: $cash_return,
      address: $address,
      payment_method: $payment_method,
    }''';
  }

  factory DeliveryInfo.fromJson(Map<String, dynamic> json) {
    return DeliveryInfo(
        address:
            json['address'] != null ? Address.fromJson(json['address']) : null,
        cash_return: json['cash_return'],
        payment_method: json['payment_method'] != null
            ? PaymentMethod.fromJson(json['payment_method'])
            : json['payment_method']);
  }
}
