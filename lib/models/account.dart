import 'package:uuid/uuid.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
class Account extends Equatable {
  final String id;
  final String photoUrl;
  final String firstName;
  final String lastName;
  final String phoneNumber;
  final String email;

  Account(
      {id,
      this.firstName,
      this.lastName,
      this.phoneNumber,
      this.email,
      this.photoUrl})
      : this.id = id ?? Uuid().v4();

  String get fullName => '${firstName} ${lastName}';

  @override
  List<Object> get props =>
      [id, firstName, lastName, phoneNumber, email, photoUrl];

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
      id: json['id'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      phoneNumber: json['phoneNumber'] as String,
      email: json['email'] as String,
      photoUrl: json['email'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['phoneNumber'] = this.phoneNumber;
    data['email'] = this.email;
    data['photoUrl'] = this.photoUrl;

    return data;
  }
}
