import 'package:equatable/equatable.dart';

class StoreDelivery extends Equatable {
  final String uuid;
  final String storeUUID;
  final String userUUID;
  final String type;
  final num price;
  final String deliveryPriceUUID;
  final String createdAt;
  final String updatedAt;

  StoreDelivery(
      {this.uuid,
      this.storeUUID,
      this.userUUID,
      this.type,
      this.price,
      this.deliveryPriceUUID,
      this.createdAt,
      this.updatedAt});

  @override
  // TODO: implement props
  List<Object> get props => [
        uuid,
        storeUUID,
        userUUID,
        type,
        price,
        deliveryPriceUUID,
        createdAt,
        updatedAt
      ];

  dynamic get printPrice => price == null ? 0 : price;

  @override
  String toString() {
    return '''StatusUpdates {
      uuid:  $uuid,
      storeUUID: $storeUUID,
      userUUID: $userUUID,
      type: $type,
      price: $price,
      deliveryPriceUUID: $deliveryPriceUUID,
      createdAt: $createdAt,
      updatedAt: $updatedAt
    }''';
  }

  factory StoreDelivery.fromJson(Map<String, dynamic> json) {
    return StoreDelivery(
      uuid: json['uuid'] as String,
      storeUUID: json['storeUUID'] as String,
      userUUID: json['userUUID'] as String,
      type: json['type'] as String,
      price: json['price'] as num,
      deliveryPriceUUID: json['deliveryPriceUUID'] as String,
      createdAt: json['createdAt'] as String,
      updatedAt: json['updatedAt'] as String,
    );
  }

  StoreDelivery copy(
      {String uuid,
      String storeUUID,
      String userUUID,
      String type,
      num price,
      StoreDelivery deliveryPriceUUID}) {
    return StoreDelivery(
      uuid: uuid ?? this.uuid,
      storeUUID: storeUUID ?? this.storeUUID,
      userUUID: userUUID ?? this.userUUID,
      type: type ?? this.type,
      price: price ?? this.price,
      deliveryPriceUUID: deliveryPriceUUID ?? this.deliveryPriceUUID,
    );
  }
}
