import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class PaymentMethod extends Equatable {
  final int id;
  final String name;
  final String type;
  PaymentMethod({this.id, this.name, this.type});

  @override
  List<Object> get props => [id, name, type];

  @override
  String toString() {
    return '''PaymentMethod {
      id: $id,
      name: $name,
      type: $type,
    }''';
  }

  factory PaymentMethod.fromJson(Map<String, dynamic> json) {
    return PaymentMethod(
      id: json['id'],
      name: json['name'],
      type: json['type'],
    );
  }
}
