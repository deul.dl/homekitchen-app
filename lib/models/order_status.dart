import 'package:equatable/equatable.dart';

class OrderStatus extends Equatable {
  final String orderUUID;
  final String status;

  OrderStatus({this.orderUUID, this.status});

  @override
  List<Object> get props => [orderUUID, status];

  factory OrderStatus.fromJson(Map<String, dynamic> json) {
    return OrderStatus(
      orderUUID: json['orderUUID'] as String,
      status: json['status'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['orderUUID'] = this.orderUUID;
    data['status'] = this.status;
    return data;
  }
}
