import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class StatusUpdates extends Equatable {
  final String status;
  final String createdAt;

  StatusUpdates({this.createdAt, this.status});

  @override
  List<Object> get props => [createdAt, status];

  DateTime get createdAtToDataTime => DateTime.parse(createdAt);

  @override
  String toString() {
    return '''StatusUpdates {
      createdAt: $createdAt,
      status: $status,
    }''';
  }

  factory StatusUpdates.fromJson(Map<String, dynamic> json) {
    return StatusUpdates(
      createdAt: json['createdAt'],
      status: json['status'],
    );
  }
}
