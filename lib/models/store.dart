import 'package:flutter/material.dart';
import 'package:home_kitchen/models/store_delivery.dart';
import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class StoreModel extends Equatable {
  TimeOfDay convertTimeOfDayFromSeconds(seconds) {
    final secondOfMinutes = seconds % 3600;
    final minute = secondOfMinutes / 60;
    final hour = (seconds - secondOfMinutes) / 3600;

    return TimeOfDay(hour: hour.toInt(), minute: minute.toInt());
  }
}

@immutable
class Store extends StoreModel {
  final String uuid;
  final String name;
  final String imageSource;
  final String phoneNumber;
  final String description;
  final String userUUID;
  final bool isTurnedOn;
  final int startedAt;
  final int endedAt;

  Store(
      {uuid,
      this.name,
      this.imageSource,
      this.phoneNumber,
      this.description,
      this.userUUID,
      this.isTurnedOn,
      this.startedAt,
      this.endedAt})
      : this.uuid = uuid ?? Uuid().v4();

  @override
  List<Object> get props => [
        uuid,
        name,
        imageSource,
        phoneNumber,
        description,
        userUUID,
        isTurnedOn,
        startedAt,
        endedAt
      ];

  TimeOfDay get startTime => convertTimeOfDayFromSeconds(startedAt);

  TimeOfDay get endTime => convertTimeOfDayFromSeconds(endedAt);

  bool get status {
    final now = TimeOfDay.now();
    final nowOfTime = (now.hour * 3600) + (now.minute * 60);

    return startedAt < nowOfTime && nowOfTime > endedAt;
  }

  factory Store.fromJson(Map<String, dynamic> json) {
    return Store(
      uuid: json['uuid'] as String,
      name: json['name'] as String,
      imageSource: json['imageSource'] as String,
      phoneNumber: json['phoneNumber'] as String,
      description: json['description'] as String,
      userUUID: json['userUUID'] as String,
      isTurnedOn: json['isTurnOn'] as bool,
      startedAt: json['startedAt'] as int,
      endedAt: json['endedAt'] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['uuid'] = this.uuid;
    data['name'] = this.name;
    data['imageSource'] = this.imageSource;
    data['phoneNumber'] = this.phoneNumber;
    data['description'] = this.description;
    data['userUUID'] = this.userUUID;
    data['isTurnedOn'] = this.isTurnedOn;
    data['startedAt'] = this.startedAt;
    data['endedAt'] = this.endedAt;

    return data;
  }

  Store copy(
      {String uuid,
      String name,
      String description,
      String phoneNumber,
      String imageSource,
      StoreDelivery storeDelivery,
      int startedAt,
      int endedAt}) {
    return Store(
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        description: description ?? this.description,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        imageSource: imageSource ?? this.description,
        userUUID: userUUID ?? this.userUUID,
        isTurnedOn: isTurnedOn,
        startedAt: startedAt ?? this.startedAt,
        endedAt: endedAt ?? this.endedAt);
  }
}
