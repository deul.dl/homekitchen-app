import 'package:home_kitchen/models/item.dart';

class CartItem extends Item {
  CartItem(
      {String uuid,
      String storeUUID,
      String name,
      num price,
      String description,
      String imageSource,
      String userUUID,
      int count,
      num cost}
      // this.created_at
      )
      : this.count = count ?? 1,
        super(
            uuid: uuid,
            storeUUID: storeUUID,
            name: name,
            description: description,
            price: price,
            userUUID: userUUID,
            imageSource: imageSource,
            cost: cost);

  int count;

  num get totalPrice => price * count;

  num get totalCost => cost * count;

  CartItem increment() {
    return CartItem(
        uuid: uuid,
        count: count + 1,
        name: name,
        price: price,
        cost: cost,
        description: description,
        userUUID: userUUID,
        storeUUID: storeUUID,
        imageSource: imageSource);
  }

  CartItem descrement() {
    return CartItem(
        uuid: uuid,
        count: count - 1,
        name: name,
        price: price,
        cost: cost,
        description: description,
        userUUID: userUUID,
        storeUUID: storeUUID,
        imageSource: imageSource);
  }

  @override
  List<Object> get props => [
        uuid,
        userUUID,
        description,
        storeUUID,
        cost,
        count,
        name,
        price,
        imageSource
      ];

  @override
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['itemUUID'] = this.uuid;
    data['count'] = this.count;
    data['price'] = this.price;
    data['cost'] = this.cost;
    data['name'] = this.name;

    return data;
  }

  Item getItem() {
    return Item(
        uuid: uuid,
        name: name,
        price: price,
        cost: cost,
        description: description,
        imageSource: imageSource,
        storeUUID: storeUUID);
  }
}
