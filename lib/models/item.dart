import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ItemModel extends Equatable {}

@immutable
class Item extends ItemModel {
  final String uuid;
  final String name;
  final int categoryId;
  final String storeUUID;
  final num price;
  final num cost;
  final String description;
  final String userUUID;
  final String imageSource;
  final bool isSwitchedOn;
  // final EquatableDateTime created_at;

  Item(
      {uuid,
      this.name,
      this.categoryId,
      this.price,
      this.cost,
      this.userUUID,
      this.storeUUID,
      this.description,
      this.isSwitchedOn,
      imageSource}
      // this.created_at
      )
      : this.uuid = uuid ?? Uuid().v4(),
        this.imageSource = imageSource ?? null;

  @override
  List<Object> get props => [
        uuid,
        name,
        categoryId,
        price,
        cost,
        description,
        imageSource,
        isSwitchedOn,
        storeUUID,
        userUUID
        // created_at
      ];

  @override
  String toString() {
    return '''Geo {
      uuid: $uuid,
      name: $name,
      categoryId: $categoryId,
      price: $price,
      cost: $cost,
      storeUUID: $storeUUID,
      description: $description,
      imageSource: $imageSource,
      userUUID: $userUUID,
      isSwitchedOn: $isSwitchedOn
    }''';
  }

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
        uuid: json['uuid'] as String,
        name: json['name'] as String,
        categoryId: json['categoryID'] as int,
        price: json['price'] as num,
        cost: json['cost'] as num,
        storeUUID: json['storeUUID'] as String,
        description: json['description'] as String,
        imageSource: json['imageSource'] as String,
        userUUID: json['userUUID'] as String,
        isSwitchedOn: json['isTurnOn'] as bool);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['uuid'] = this.uuid ?? Uuid().v4();
    data['name'] = this.name;
    data['categoryId'] = this.categoryId;
    data['price'] = this.price;
    data['cost'] = this.cost;
    data['description'] = this.description;
    data['imageSource'] = this.imageSource;
    data['isSwitchedOn'] = this.isSwitchedOn;
    data['storeUUID'] = this.storeUUID;
    data['userUUID'] = this.userUUID;
    return data;
  }

  Item copyWith(
      {String name,
      num price,
      num cost,
      String description,
      String imageSource,
      String storeUUID,
      bool isSwitchedOn}) {
    return Item(
        uuid: this.uuid,
        name: name ?? this.name,
        categoryId: categoryId ?? this.categoryId,
        description: description ?? this.description,
        storeUUID: storeUUID ?? this.storeUUID,
        price: price ?? this.price,
        cost: cost ?? this.cost,
        userUUID: userUUID ?? this.userUUID,
        imageSource: imageSource ?? this.imageSource,
        isSwitchedOn: isSwitchedOn ?? this.isSwitchedOn);
  }
}
