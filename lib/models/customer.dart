import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class Customer extends Equatable {
  final int id;
  final String name;
  final String phone_number;

  Customer({this.id, this.name, this.phone_number});

  @override
  List<Object> get props => [id, name, phone_number];

  @override
  String toString() {
    return '''Customer {
      id: $id,
      name: $name,
      phone_number: $phone_number,
    }''';
  }

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      id: json['id'],
      name: json['name'],
      phone_number: json['phone_number'],
    );
  }
}
