import 'package:equatable/equatable.dart';
import 'package:uuid/uuid.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/models/address.dart';
import 'package:home_kitchen/mixins/date.dart';

const RECEIVED_STATUS = 'received';
const ACCEPTED_STATUS = 'accepted';
const PROCEESSED_STATUS = 'processing';
const DONE_STATUS = 'ready';
const TRANSACTION_STATUS = 'paid';
const COMPLETED_STATUS = 'completed';
const CANCELED_STATUS = 'cancelled';
const REFUSED_STATUS = 'rejected';

const SPECIAL_ORDER = 'special_order';
const SIMPLE_ORDER = 'simple_order';

const DELIVERY_TYPE = 'delivery';
const IN_STORE_TYPE = 'pick_up';

@immutable
class Order extends Equatable {
  final String uuid;
  final String number;
  final String description;
  final Address address;
  final String type;
  final String status;
  final num totalPrice;
  final String executorUUID;
  final String customerUUID;
  final String storeUUID;
  final String addressUUID;
  final EquatableDateTime createdAt;

  Order(
      {uuid,
      this.number,
      this.description,
      this.address,
      this.type,
      this.status,
      this.totalPrice,
      this.executorUUID,
      this.customerUUID,
      this.storeUUID,
      this.addressUUID,
      this.createdAt})
      : this.uuid = uuid ?? Uuid().v4();

  @override
  List<Object> get props =>
      [uuid, number, description, address, type, status, totalPrice, createdAt];

  String get showTimeIfToday => createdAt.isToday()
      ? createdAt.toFormat('hh:mm aaa')
      : createdAt.toFormat('MMMMd');

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      uuid: json['uuid'] as String,
      number: json['number'] as String,
      status: json['status'] as String,
      description: json['description'] as String,
      address: json['address'] as Address,
      type: json['type'] as String,
      totalPrice: json['totalPrice'] as num,
      createdAt: json['createdAt'] as EquatableDateTime,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['uuid'] = this.uuid;
    data['number'] = this.number;
    data['description'] = this.description;
    data['address'] = this.address;
    data['type'] = this.type;
    data['status'] = this.status;
    data['totalPrice'] = this.totalPrice;
    data['createdAt'] = this.createdAt;

    return data;
  }
}
