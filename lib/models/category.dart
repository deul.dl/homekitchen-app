import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
class Category extends Equatable {
  final int id;
  final String imageSource;
  final String name;
  // final EquatableDateTime created_at;

  Category({
    id,
    this.imageSource,
    this.name,
  }) : this.id = id;

  @override
  List<Object> get props => [
        id,
        imageSource,
        name,
        // created_at
      ];

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: json['ID'] as int,
      name: json['Name'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['ID'] = this.id;
    data['Name'] = this.name;
    data['ImageSource'] = this.imageSource;

    return data;
  }
}
