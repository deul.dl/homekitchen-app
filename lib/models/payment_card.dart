import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class PaymentCard extends Equatable {
  final String id;
  final String number;
  final int code;
  final String name;
  final String date;
  final bool isSave;

  PaymentCard({id, this.number, this.code, this.name, this.date, isSave})
      : this.id = id ?? Uuid().v4(),
        this.isSave = isSave && false;

  String get printLastNumber => '**** **** **** ${getLastNumber()}';

  String getLastNumber() => number.substring(number.length - 5);

  @override
  List<Object> get props => [
        id,
        number,
        code,
        name,
        date,
      ];

  factory PaymentCard.fromJson(Map<String, dynamic> json) {
    return PaymentCard(
      id: json['id'] as String,
      number: json['number'] as String,
      code: json['code'] as int,
      name: json['name'] as String,
      date: json['date'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = this.id;
    data['number'] = this.number;
    data['code'] = this.code;
    data['name'] = this.name;
    data['date'] = this.date;

    return data;
  }
}
