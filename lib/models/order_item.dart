import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class OrderItemBase extends Equatable {
  final String orderUUID;
  OrderItemBase({orderUUID}) : this.orderUUID = orderUUID ?? Uuid().v4();
}

@immutable
class OrderItem extends OrderItemBase {
  final String name;
  final String itemUUID;
  final int count;
  final String imageSource;
  final num price;

  OrderItem({
    orderUUID,
    this.name,
    this.itemUUID,
    this.count,
    this.imageSource,
    this.price,
  }) : super(orderUUID: orderUUID);

  @override
  List<Object> get props =>
      [orderUUID, imageSource, name, itemUUID, count, price];

  num get totalPrice => count * price;

  factory OrderItem.fromJson(Map<String, dynamic> json) {
    return OrderItem(
      orderUUID: json['orderUUID'] as String,
      name: json['name'] as String,
      imageSource: json['imageSource'] as String,
      itemUUID: json['itemUUId'] as String,
      count: json['count'] as int,
      price: json['price'] as num,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['orderUUID'] = this.orderUUID;
    data['name'] = this.name;
    data['imageSource'] = this.imageSource;
    data['itemUUID'] = this.itemUUID;
    data['price'] = this.price;
    data['count'] = this.count;
    return data;
  }
}
