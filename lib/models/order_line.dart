import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class OrderLine extends Equatable {
  final int id;
  final String displayName;
  final int count;
  final double totalPrice;

  OrderLine({this.displayName, this.count, this.totalPrice, this.id});

  @override
  List<Object> get props => [id, displayName, count, totalPrice];

  @override
  String toString() {
    return '''OrderLine {
      id: $id,
      display_name: $displayName,
      count: $count,
      total_price: $totalPrice,
    }''';
  }

  factory OrderLine.fromJson(Map<String, dynamic> json) {
    return OrderLine(
      id: json['id'],
      displayName: json['displayName'],
      count: json['count'],
      totalPrice: json['totalPrice'],
    );
  }
}
