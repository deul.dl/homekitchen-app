import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class OrderPaymentMethod extends Equatable {
  final String uuid;
  final String type;
  final num totalPrice;
  final num deliveryPrice;
  final num amount;
  OrderPaymentMethod(
      {this.uuid, this.totalPrice, this.deliveryPrice, this.amount, this.type});

  @override
  List<Object> get props => [uuid, totalPrice, deliveryPrice, amount, type];

  @override
  String toString() {
    return '''OrderPaymentMethod {
      uuid: $uuid,
      totalPrice: $totalPrice,
      deliveryPrice: $deliveryPrice,
      amount:  $amount,
      type: $type,
    }''';
  }

  factory OrderPaymentMethod.fromJson(Map<String, dynamic> json) {
    return OrderPaymentMethod(
        uuid: json['uuid'],
        amount: json['amount'],
        totalPrice: json['totalPrice'],
        deliveryPrice: json['deliveryPrice'],
        type: json['type']);
  }
}
