import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class Executor extends Equatable {
  final String uiid;
  final String firstName;
  final String lastName;
  final String address;

  Executor({
    this.uiid,
    @required this.firstName,
    @required this.lastName,
    this.address,
  });

  String get fullName => '$lastName $firstName';

  @override
  List<Object> get props => [uiid, firstName, lastName, address];

  factory Executor.fromJson(Map<String, dynamic> json) {
    return Executor(
      uiid: json['uiid'] as String,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      address: json['address'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['uiid'] = this.uiid;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['address'] = this.address;

    return data;
  }
}
