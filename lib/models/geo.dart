import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
class Geo extends Equatable {
  final String type;
  final List coordinates;

  Geo({this.type, this.coordinates});

  @override
  List<Object> get props => [type, coordinates];

  @override
  String toString() {
    return '''Geo {
      type: $type,
      coordinates: $coordinates,
    }''';
  }

  factory Geo.fromJson(Map<String, dynamic> json) {
    return Geo(
      type: json['type'],
      coordinates: json['coordinates'],
    );
  }
}
