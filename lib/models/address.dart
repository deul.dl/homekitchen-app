import 'package:uuid/uuid.dart';
import 'package:equatable/equatable.dart';

class Address extends Equatable {
  final String uuid;
  final String street;
  final String house;
  final String apartment;
  final String porch;
  final String city;
  final String country;
  final bool primary;

  Address(
      {uuid,
      this.street,
      this.house,
      this.apartment,
      this.porch,
      this.city,
      this.country,
      primary}
      // this.created_at
      )
      : this.uuid = uuid ?? Uuid().v4(),
        this.primary = primary ?? false;

  String get printOnlyAddress => [street, house, apartment, porch].join(', ');

  Address removeDefault() {
    return Address(
        uuid: uuid,
        street: street,
        house: house,
        apartment: apartment,
        porch: porch,
        city: city,
        country: country,
        primary: false);
  }

  Address setDefault() {
    return Address(
        uuid: uuid,
        street: street,
        house: house,
        apartment: apartment,
        porch: porch,
        city: city,
        country: country,
        primary: true);
  }

  @override
  List<Object> get props =>
      [uuid, street, house, apartment, porch, city, country, primary];

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      uuid: json['uuid'] as String,
      street: json['street'] as String,
      house: json['house'] as String,
      apartment: json['apartment'] as String,
      porch: json['porch'] as String,
      city: json['city'] as String,
      country: json['country'] as String,
      primary: json['primary'] as bool,
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['uuid'] = this.uuid ?? Uuid().v4();
    data['street'] = this.street;
    data['house'] = this.house;
    data['apartment'] = this.apartment;
    data['porch'] = this.porch;
    data['city'] = this.city;
    data['country'] = this.country;
    data['primary'] = this.primary;

    return data;
  }

  Address copyWith({
    String uuid,
    String street,
    String house,
    String apartment,
    String porch,
    String city,
    String country,
    bool primary,
  }) {
    return Address(
      uuid: uuid ?? this.uuid,
      street: street ?? this.street,
      house: house ?? this.house,
      apartment: apartment ?? this.apartment,
      porch: porch ?? this.porch,
      city: city ?? this.city,
      country: country ?? this.country,
      primary: primary ?? this.primary,
    );
  }
}
