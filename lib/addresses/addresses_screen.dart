import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/widgets/error_sreen.dart';

import 'package:home_kitchen/addresses/widgets/empty_screen.dart';
import 'package:home_kitchen/addresses/addresses.dart';

class AddressesScreen extends StatefulWidget {
  AddressesScreen({Key key}) : super(key: key);

  @override
  _AddressesState createState() => _AddressesState();
}

class _AddressesState extends State<AddressesScreen> {
  @override
  void initState() {
    super.initState();
    this._load();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        color: Theme.of(context).backgroundColor,
        child: BlocBuilder<AddressesBloc, AddressesState>(builder: (
          BuildContext context,
          AddressesState currentState,
        ) {
          if (currentState is AddressesLoadSuccess) {
            currentState.addresses.sort((a, b) => b.primary ? 1 : 0);

            if (currentState.addresses.length > 0) {
              return ListView(
                  children: currentState.addresses
                      .map((address) => AddressItem(
                            key: Key(address.uuid),
                            address: address,
                          ))
                      .toList());
            } else {
              return EmptyScreen();
            }
          } else if (currentState is AddressesError) {
            return ErrorScreen(
                errorMessage: currentState.errorMessage, onReload: _load);
          } else {
            return EmptyScreen();
          }
        }));
  }

  void _load() {
    BlocProvider.of<AddressesBloc>(context).add(LoadAddresses());
  }

  @override
  dispose() {
    super.dispose();
  }
}
