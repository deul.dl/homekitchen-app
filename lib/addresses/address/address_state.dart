import 'package:equatable/equatable.dart';

import 'package:home_kitchen/addresses/addresses.dart';

abstract class AddressState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  AddressState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class AddressUninitialized extends AddressState {
  AddressUninitialized();

  @override
  String toString() => 'AddressUninitialized';
}

class AddressLoadSuccess extends AddressState {
  AddressLoadSuccess({this.addresses = const []}) : super([addresses]);

  final List<Address> addresses;

  Address getPrimary() => addresses.firstWhere((element) => element.primary,
      orElse: () => addresses[0]);

  @override
  String toString() => 'AddressLoadSuccess { addresses: $addresses }';
}

class AddressInProgress extends AddressState {
  AddressInProgress({this.addresses = const []}) : super([addresses]);

  final List<Address> addresses;

  Address getPrimary() => addresses.firstWhere((element) => element.primary,
      orElse: () => addresses[0]);

  @override
  String toString() => 'AddressInProgress { addresses: $addresses }';
}

class AddressError extends AddressState {
  final String errorMessage;

  AddressError(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'AddressError { errorMessage: $errorMessage }';
}
