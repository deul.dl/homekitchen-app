import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/addresses/addresses.dart';

import 'package:home_kitchen/addresses/address/widgets/add_edit_screen.dart';

class NewAddressPage extends StatelessWidget {
  static const String routeName = '/new-address';

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height - 80;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title:
            Text(FlutterI18n.translate(context, 'addresses.NewAddress.Title')),
      ),
      body: SingleChildScrollView(
          child: Container(
              height: height,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15.0),
              child: AddEditAddressScreen(
                onSave: (values) {
                  BlocProvider.of<AddressBloc>(context)
                      .add(AddAddress(address: values));
                },
              ))),
    );
  }
}
