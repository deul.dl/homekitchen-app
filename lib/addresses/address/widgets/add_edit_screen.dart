import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/widgets/custom_form_input.dart';
import 'package:home_kitchen/widgets/expanded_flat_button.dart';

import 'package:home_kitchen/addresses/addresses.dart';

class AddEditAddressScreen extends StatefulWidget {
  AddEditAddressScreen({Key key, this.onSave, bool isEditing, Address address})
      : this.isEditing = isEditing ?? false,
        this.address = address ?? null,
        super(key: key);

  final bool isEditing;
  final Function onSave;
  final Address address;

  @override
  _AddEditScreenScreen createState() => _AddEditScreenScreen();
}

class _AddEditScreenScreen extends State<AddEditAddressScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _street, _house, _apartment, _porch;
  bool _primary = false;

  @override
  void initState() {
    if (widget.isEditing)
      setState(() {
        _primary = widget.address.primary;
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddressBloc, AddressState>(
        listener: (context, state) {
          if (state is AddressLoadSuccess) {
            Navigator.of(context).pop();
          }
        },
        child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Expanded(
                    child: Column(
                  children: <Widget>[
                    CustomFormInput(
                      initialValue:
                          widget.isEditing ? widget.address.street : '',
                      autofocus: !widget.isEditing,
                      onSaved: (val) => _street = val,
                      label: FlutterI18n.translate(
                          context, 'addresses.Form.Street.Label'),
                      validator: (val) {
                        return val.trim().isEmpty
                            ? FlutterI18n.translate(
                                context, 'addresses.Form.Street.Error')
                            : null;
                      },
                    ),
                    CustomFormInput(
                      initialValue:
                          widget.isEditing ? widget.address.house : '',
                      autofocus: !widget.isEditing,
                      onSaved: (val) => _house = val,
                      label: FlutterI18n.translate(
                          context, 'addresses.Form.House.Label'),
                      validator: (val) {
                        return val.trim().isEmpty
                            ? FlutterI18n.translate(
                                context, 'addresses.Form.House.Error')
                            : null;
                      },
                    ),
                    CustomFormInput(
                      initialValue:
                          widget.isEditing ? widget.address.apartment : '',
                      autofocus: !widget.isEditing,
                      onSaved: (val) => _apartment = val,
                      label: FlutterI18n.translate(
                          context, 'addresses.Form.Apartament.Label'),
                    ),
                    CustomFormInput(
                      initialValue:
                          widget.isEditing ? widget.address.porch : '',
                      autofocus: !widget.isEditing,
                      onSaved: (val) => _porch = val,
                      label: FlutterI18n.translate(
                          context, 'addresses.Form.Porch.Label'),
                    ),
                    Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(FlutterI18n.translate(
                                context, 'addresses.Form.SetDefaultDescribe')),
                            Switch(
                              value: _primary,
                              onChanged: (val) =>
                                  setState(() => _primary = val),
                              activeColor: Theme.of(context).primaryColor,
                            ),
                          ],
                        ))
                  ],
                )),
                _footer(context)
              ],
            )));
  }

  Widget _footer(BuildContext context) {
    final logicalSize = MediaQuery.of(context).size;
    final double height = logicalSize.height;

    return BlocBuilder<AddressBloc, AddressState>(
        builder: (BuildContext context, AddressState state) {
      return Container(
        padding: EdgeInsets.only(top: height - 520),
        alignment: Alignment.bottomCenter,
        child: state is AddressInProgress
            ? const CircularProgressIndicator()
            : ExpandedFlatButton(
                label: widget.isEditing
                    ? FlutterI18n.translate(context, 'common.Edit')
                    : FlutterI18n.translate(context, 'common.Add'),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();

                    final address = !widget.isEditing
                        ? Address(
                            street: _street,
                            house: _house,
                            apartment: _apartment,
                            porch: _porch,
                            primary: _primary)
                        : widget.address.copyWith(
                            uuid: widget.address.uuid,
                            country: widget.address.country,
                            city: widget.address.city,
                            street: _street,
                            house: _house,
                            apartment: _apartment,
                            porch: _porch,
                            primary: _primary);

                    widget.onSave(address);
                  }
                }),
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
