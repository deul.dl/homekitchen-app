import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/addresses/addresses.dart';

class AddressBloc extends Bloc<AddressEvent, AddressState> {
  AddressBloc({this.addressRepository}) : super(AddressUninitialized());
  final AddressesRepository addressRepository;
  @override
  Stream<AddressState> mapEventToState(
    AddressEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
