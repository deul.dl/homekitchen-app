import 'dart:async';

import 'package:meta/meta.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';

import 'package:home_kitchen/addresses/addresses.dart';

@immutable
abstract class AddressEvent {
  Stream<AddressState> applyAsync(
      {AddressState currentState, AddressBloc bloc});
}

class AddAddress extends AddressEvent {
  AddAddress({@required this.address});

  final Address address;

  @override
  String toString() => 'AddAddress  {address: $address}';

  @override
  Stream<AddressState> applyAsync(
      {AddressState currentState, AddressBloc bloc}) async* {
    try {
      yield AddressInProgress();
      final Position position = await Geolocator.getLastKnownPosition();

      List<Placemark> p = await placemarkFromCoordinates(
          position.latitude, position.longitude,
          localeIdentifier: 'en');

      Placemark place = p[0];

      await bloc.addressRepository
          .add(address.copyWith(city: place.locality, country: place.country));
      yield AddressLoadSuccess();
    } catch (_) {
      yield AddressError(_?.toString());
    }
  }
}

class EditAddress extends AddressEvent {
  EditAddress({@required this.address});

  final Address address;

  @override
  String toString() => 'EditAddress {address: $address}';

  @override
  Stream<AddressState> applyAsync(
      {AddressState currentState, AddressBloc bloc}) async* {
    try {
      yield AddressInProgress();
      await bloc.addressRepository.update(address);
      yield AddressLoadSuccess();
    } catch (_) {
      yield AddressError(_?.toString());
    }
  }
}
