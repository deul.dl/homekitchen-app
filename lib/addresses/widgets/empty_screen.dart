import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class EmptyScreen extends StatelessWidget {
  EmptyScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        SizedBox(
          height: 200,
        ),
        Text(
          FlutterI18n.translate(context, 'addresses.Empty.Description'),
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        SizedBox(
          height: 10,
        ),
        Icon(
          Icons.expand_more,
          size: 150,
          color: Colors.black12,
        )
      ]),
    );
  }
}
