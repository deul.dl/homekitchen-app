import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/addresses/addresses.dart';

class AddressItem extends StatelessWidget {
  AddressItem({Key key, this.address}) : super(key: key);

  final Address address;

  @override
  Widget build(BuildContext context) {
    final Widget card = Card(
        color: Colors.white,
        elevation: 2,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          if (address.primary) _containerDefault(context),
          _body(context)
        ]));

    return Dismissible(
      key: Key(address.uuid),
      child: card,
      onDismissed: (direction) {
        BlocProvider.of<AddressesBloc>(context)
            .add(DeleteAddress(address: address));
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text('Removed')));
      },
    );
  }

  Widget _containerDefault(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(10.0)),
          color: Colors.red.shade100,
        ),
        child: Text(FlutterI18n.translate(context, 'addresses.List.Default'),
            style: TextStyle(
                color: Colors.redAccent,
                fontWeight: FontWeight.bold,
                fontSize: 12)));
  }

  Widget _body(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(
            vertical: address.primary ? 10 : 20, horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              address.printOnlyAddress,
              style: TextStyle(
                fontSize: 14,
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                InkWell(
                  child: Text(
                    FlutterI18n.translate(context, 'addresses.List.Edit'),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.blue),
                  ),
                  onTap: () => Navigator.of(context)
                      .pushNamed('/edit-address', arguments: address),
                ),
                if (!address.primary)
                  Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: InkWell(
                        child: Text(
                          FlutterI18n.translate(
                              context, 'addresses.List.SetDefault'),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Colors.blue),
                        ),
                        onTap: () => BlocProvider.of<AddressesBloc>(context)
                            .add(SetPrimaryAddress(address: address)),
                      ))
              ],
            )
          ],
        ));
  }
}
