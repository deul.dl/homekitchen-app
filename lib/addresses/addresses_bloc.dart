import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'addresses.dart';

class AddressesBloc extends Bloc<AddressesEvent, AddressesState> {
  AddressesBloc({
    this.addressRepository,
  }) : super(AddressesUninitialized()) {
    _addressSubscription?.cancel();
    _addressSubscription = addressRepository.addresses.listen((address) {
      add(LoadAddresses());
    });
  }
  final AddressesRepository addressRepository;
  StreamSubscription<Address> _addressSubscription;

  @override
  Stream<AddressesState> mapEventToState(
    AddressesEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() {
    _addressSubscription?.cancel();
    return super.close();
  }
}
