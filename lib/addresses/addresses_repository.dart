import 'dart:async';
import 'dart:core';

import 'addresses.dart';

class AddressesRepository {
  AddressesRepository(client) : this._http = client;

  final _controller = StreamController<Address>();
  final _http;

  Stream<Address> get addresses async* {
    yield* _controller.stream;
  }

  Future<dynamic> getAddresses() async {
    return _http.get('customer/addresses').then((res) => res['data']).then(
        (addresses) => addresses == null
            ? null
            : (addresses as List<dynamic>)
                .map((address) => Address.fromJson(address))
                .toList());
  }

  Future<dynamic> add(Address address) async {
    return _http
        .post('customer/addresses/add-address', {
          'city': address.city,
          'street': address.street,
          'country': address.country,
          'apartment': address.apartment,
          'house': address.house,
          'porch': address.porch,
          'primary': address.primary
        })
        .then((res) => res['data']['address'])
        .then((address) => Address.fromJson(address))
        .then((address) {
          _controller.add(address);
          return address;
        });
  }

  Future<dynamic> remove(Address address) async {
    return _http.delete('customer/addresses', address.uuid).then((res) {
      _controller.add(address);
      return address;
    });
  }

  Future<dynamic> update(Address address) async {
    return _http
        .put('customer/addresses/${address.uuid}', {
          'uuid': address.uuid,
          'city': address.city,
          'street': address.street,
          'country': address.country,
          'apartment': address.apartment,
          'house': address.house,
          'porch': address.porch,
          'primary': address.primary,
        })
        .then((res) => res['data']['address'])
        .then((address) => Address.fromJson(address))
        .then((address) {
          _controller.add(address);
          return address;
        });
  }

  Future<dynamic> setPrimary(Address address) async {
    return _http
        .put('customer/addresses/${address.uuid}/primary', null)
        .then((res) {
      _controller.add(address);
      return address;
    });
  }
}
