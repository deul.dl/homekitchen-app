import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import 'addresses.dart';

abstract class AddressesState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  AddressesState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

@immutable
class AddressesInProgress extends AddressesState {
  AddressesInProgress();

  @override
  String toString() => 'AddressesInProgress {}';
}

@immutable
class AddressesUninitialized extends AddressesState {
  AddressesUninitialized();

  @override
  String toString() => 'AddressesUninitialized {}';
}

@immutable
class AddressesLoadSuccess extends AddressesState {
  AddressesLoadSuccess({this.addresses = const []}) : super([addresses]);

  final List<Address> addresses;

  Address getPrimary() =>
      addresses.firstWhere((element) => element.primary, orElse: () => null);

  @override
  String toString() => 'AddressesLoadSuccess { addresses: $addresses }';
}

@immutable
class AddressesError extends AddressesState {
  final String errorMessage;

  AddressesError(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'AddressesError { errorMessage: $errorMessage }';
}
