import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'addresses.dart';

@immutable
abstract class AddressesEvent {
  Stream<AddressesState> applyAsync(
      {AddressesState currentState, AddressesBloc bloc});
}

class LoadAddresses extends AddressesEvent {
  LoadAddresses();

  @override
  String toString() => 'LoadAddresses {}';

  @override
  Stream<AddressesState> applyAsync(
      {AddressesState currentState, AddressesBloc bloc}) async* {
    try {
      yield AddressesInProgress();
      final addresses = await bloc.addressRepository.getAddresses();
      yield AddressesLoadSuccess(
        addresses: (addresses as List<Address>) ?? [],
      );
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadAddresses', error: _, stackTrace: stackTrace);
      yield AddressesError(_?.toString());
    }
  }
}

class DeleteAddress extends AddressesEvent {
  DeleteAddress({@required this.address});

  final Address address;

  @override
  String toString() => 'DeleteAddress { address: $address }';

  @override
  Stream<AddressesState> applyAsync(
      {AddressesState currentState, AddressesBloc bloc}) async* {
    try {
      await bloc.addressRepository.remove(address);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'DeleteAddress', error: _, stackTrace: stackTrace);
      yield AddressesError(_?.toString());
    }
  }
}

class SetPrimaryAddress extends AddressesEvent {
  SetPrimaryAddress({@required this.address});

  final Address address;

  @override
  String toString() => 'SetPrimaryAddress { address: $address }';

  @override
  Stream<AddressesState> applyAsync(
      {AddressesState currentState, AddressesBloc bloc}) async* {
    try {
      await bloc.addressRepository.setPrimary(address);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'SetPrimaryAddress', error: _, stackTrace: stackTrace);
      yield AddressesError(_?.toString());
    }
  }
}

class UpdateAddress extends AddressesEvent {
  UpdateAddress({@required this.address});

  final Address address;

  @override
  String toString() => 'UpdateAddress { address: $address }';

  @override
  Stream<AddressesState> applyAsync(
      {AddressesState currentState, AddressesBloc bloc}) async* {
    try {
      final addresses = (currentState as AddressesLoadSuccess)
          .addresses
          .map((e) =>
              e.uuid == address.uuid ? address.setDefault() : e.removeDefault())
          .toList();
      yield AddressesLoadSuccess(addresses: addresses);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'UpdateAddress', error: _, stackTrace: stackTrace);
      yield AddressesError(_?.toString());
    }
  }
}
