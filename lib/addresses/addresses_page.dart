import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/addresses/addresses_screen.dart';

class AddressesPage extends StatelessWidget {
  static const String routeName = '/addresses';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(FlutterI18n.translate(context, 'addresses.Title')),
      ),
      body: AddressesScreen(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context).pushNamed('/new-address'),
        child: Icon(Icons.add),
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }
}
