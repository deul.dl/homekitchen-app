import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/executor/login/login.dart';

import 'package:home_kitchen/executor/signup/signup_page.dart';
import 'package:home_kitchen/accounts/enter_executor_page.dart';
import 'package:home_kitchen/signin/signin_page.dart';
import 'package:home_kitchen/widgets/verificate_phone_number/veritifacate_phone_number.dart';

class AccountScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Localizations.localeOf(context).countryCode;
    return BlocBuilder<AuthBloc, AuthState>(
        builder: (BuildContext context, AuthState auth) {
      if (!auth.isAuth) {
        return Container(
            child: Column(children: [
          Text(FlutterI18n.translate(context, 'account.IsNotPermession')),
          SizedBox(
            height: 20,
          ),
          TextButton(
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => SignInPage())),
              child: Text(FlutterI18n.translate(context, 'account.Enter')))
        ]));
      }
      final double height = 56.0 * renderTiles(context, auth).length;
      return Column(children: [
        Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  color: Theme.of(context).backgroundColor,
                  width: 1.0,
                ),
              ),
            ),
            height: height,
            child: ListView(
              children: ListTile.divideTiles(
                context: context,
                tiles: renderTiles(context, auth),
              ).toList(),
            ))
      ]);
    });
  }

  List<Widget> renderTiles(BuildContext context, AuthState auth) {
    return [
      if (auth.isExecutor)
        ListTile(
          title: Text(FlutterI18n.translate(context, 'account.Menu.Profile')),
          onTap: () => Navigator.of(context).pushNamed('/profile'),
          trailing: Icon(Icons.chevron_right),
        ),
      if (auth.isCustomer)
        ListTile(
          title: Text(
              FlutterI18n.translate(context, 'account.Menu.ShippingAddress')),
          onTap: () => Navigator.of(context).pushNamed('/addresses'),
          trailing: Icon(Icons.chevron_right),
        ),
      // if (auth.isCustomer)
      //   ListTile(
      //     title: Text('History Orders'),
      //     onTap: () => Navigator.of(context).pushNamed('/past-orders'),
      //     trailing: Icon(Icons.chevron_right),
      //   ),
      if (auth.isCustomer && !auth.hasRoles(EXECUTOR))
        Container(
          color: Colors.blue,
          child: ListTile(
            title: Text(
              FlutterI18n.translate(context, 'account.Menu.BecameChef'),
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SignUpPage(
                      type: CHECK_TYPE,
                    ))),
          ),
        ),
      if (auth.hasRoles(EXECUTOR))
        Container(
          color: Colors.white,
          child: ListTile(
            title: Text(
              FlutterI18n.translate(context, 'account.Menu.ChangeRole',
                  translationParams: {'role': auth.user.currentRole}),
              style: TextStyle(fontSize: 16),
            ),
            trailing: Switch(
                value: auth.isExecutor,
                onChanged: (bool isExecutor) {
                  if (auth.isCustomer && auth.hasRoles(EXECUTOR)) {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => BlocProvider<LoginBloc>(
                            create: (_) => LoginBloc(
                                authRepository:
                                    RepositoryProvider.of<AuthRepository>(
                                        context))
                              ..add(LoginUsernameChanged(
                                  auth.user.phoneNumber.substring(1))),
                            child: EnterExecutorOnlyPasswordPage())));
                  } else {
                    BlocProvider.of<AuthBloc>(context)
                        .add(AuthCustomerChanged());
                  }
                }),
          ),
        ),
      ListTile(
        title: Text(FlutterI18n.translate(context, 'account.Menu.Settings')),
        onTap: () => Navigator.of(context).pushNamed('/settings'),
        trailing: Icon(Icons.chevron_right),
      )
    ];
  }
}
