import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/form_top.dart';

import 'package:home_kitchen/executor/login/login.dart';
import 'package:home_kitchen/widgets/custom_text_input.dart';

class EnterExecutorOnlyPasswordPage extends StatelessWidget {
  EnterExecutorOnlyPasswordPage({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: BlocListener<LoginBloc, LoginState>(
            listener: (context, state) {
              if (state.status.isSubmissionFailure) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(content: Text(state.errorMessage)),
                  );
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FormTop(
                      title: FlutterI18n.translate(context, 'login.Title'),
                      description:
                          FlutterI18n.translate(context, 'login.Describtion'),
                      child: PasswordInput()),
                  _LoginButton(),
                ],
              ),
            )));
  }
}

class PasswordInput extends StatefulWidget {
  PasswordInput();

  @override
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
        buildWhen: (previous, current) => previous.password != current.password,
        builder: (context, state) {
          return Column(children: [
            if (state.errorMessage != null)
              Container(
                  margin: EdgeInsets.only(bottom: 20),
                  alignment: Alignment.center,
                  child: Text(
                    state.errorMessage,
                    style: TextStyle(color: Colors.red),
                    textAlign: TextAlign.center,
                  )),
            CustomTextInput(
                key: const Key('loginForm_passwordInput_textField'),
                maxLines: 1,
                obscureText: _obscureText,
                onChanged: (val) => BlocProvider.of<LoginBloc>(context)
                    .add(LoginPasswordChanged(val)),
                decoration: InputDecoration(
                  filled: true,
                  suffix: InkWell(
                      child: _obscureText
                          ? Icon(Icons.visibility)
                          : Icon(Icons.visibility_off),
                      onTap: () {
                        setState(() => _obscureText = !_obscureText);
                      }),
                  labelText: FlutterI18n.translate(
                      context, 'login.Form.Password.Label'),
                  labelStyle: TextStyle(color: Colors.black),
                  errorText: state.password.invalid
                      ? FlutterI18n.translate(
                          context, 'login.Form.Password.Error')
                      : null,
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    color: Colors.black,
                  )),
                ))
          ]);
        });
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : SizedBox(
                height: 48,
                width: double.infinity,
                child: TextButton(
                  key: const Key('loginForm_continue_raisedButton'),
                  style: TextButton.styleFrom(
                    backgroundColor: Theme.of(context).primaryColor,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'common.Login'),
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    context.bloc<LoginBloc>().add(const LoginSubmitted());
                  },
                ));
      },
    );
  }
}
