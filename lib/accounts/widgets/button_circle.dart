import 'package:flutter/material.dart';

class ButtonCircle extends StatelessWidget {
  ButtonCircle(
      {@required this.onClick, @required this.icon, @required this.title});

  final Function onClick;
  final Widget icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      RawMaterialButton(
        onPressed: onClick,
        child: icon,
        shape: CircleBorder(),
        elevation: 2.0,
        fillColor: Colors.white,
        padding: const EdgeInsets.all(25.0),
      ),
      SizedBox(
        height: 5,
      ),
      Text(
        title,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ]);
  }
}
