import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/accounts/profile/profile.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/widgets/custom_form_input.dart';

class EditFullNameScreen extends StatefulWidget {
  EditFullNameScreen({Key key, bool isEdit, this.user, this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final User user;
  final Function onSave;

  @override
  _EditFullNameState createState() => _EditFullNameState();
}

class _EditFullNameState extends State<EditFullNameScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _firstName;
  String _lastName;

  @override
  void initState() {
    _firstName = widget.user.firstName;
    _lastName = widget.user.lastName;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileBloc, ProfileState>(
        listener: (BuildContext context, ProfileState state) {
      if (state is ProfileLoadSuccess) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(content: Text(state.message)),
          );
        widget.onSave();
      } else if (state is ProfileFailure) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(content: Text(state.errorMessage)),
          );
      }
    }, child: BlocBuilder<ProfileBloc, ProfileState>(
            builder: (BuildContext context, ProfileState state) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FormTop(
                  child: Column(
                    children: <Widget>[
                      CustomFormInput(
                        initialValue:
                            widget.user == null ? '' : widget.user.firstName,
                        onSaved: (val) => _firstName = val,
                        label: FlutterI18n.translate(context,
                            'profile.EditFullName.Form.FirstName.Label'),
                        validator: (val) {
                          return val.trim().isEmpty
                              ? FlutterI18n.translate(context,
                                  'profile.EditFullName.Form.FirstName.Error')
                              : null;
                        },
                      ),
                      CustomFormInput(
                        initialValue:
                            widget.user == null ? '' : widget.user.lastName,
                        onSaved: (val) => _lastName = val,
                        label: FlutterI18n.translate(context,
                            'profile.EditFullName.Form.LastName.Label'),
                        validator: (val) {
                          return val.trim().isEmpty
                              ? FlutterI18n.translate(context,
                                  'profile.EditFullName.Form.FirstName.Error')
                              : null;
                        },
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.bottomCenter,
                  child: ExpandedFlatButton(
                      label: FlutterI18n.translate(context, 'common.Save'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          BlocProvider.of<ProfileBloc>(context).add(
                              UpdateFullName(
                                  user: User(
                                      firstName: _firstName,
                                      lastName: _lastName)));
                        }
                      }),
                )
              ],
            )),
      );
    }));
  }
}
