import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/accounts/profile/profile.dart';

import 'widgets/edit_full_name.dart';

class UpdateFullNamePage extends StatelessWidget {
  UpdateFullNamePage({Key key, @required this.user}) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text(
            FlutterI18n.translate(context, 'profile.EditFullName.Title'),
          ),
        ),
        body: BlocProvider<ProfileBloc>(
            create: (_) => ProfileBloc(
                authBloc: BlocProvider.of<AuthBloc>(context),
                userRepository: RepositoryProvider.of<UserRepository>(context)),
            child: EditFullNameScreen(
              isEdit: true,
              user: user,
              onSave: () {
                Navigator.pop(context);
              },
            )));
  }
}
