import 'dart:async';
import 'dart:developer' as developer;
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/user/user_repository.dart';
import 'package:home_kitchen/auth/auth.dart';

import 'profile.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final UserRepositoryBase userRepository;
  final AuthBloc authBloc;

  ProfileBloc({@required this.userRepository, @required this.authBloc})
      : super(ProfileUninitialized());

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'ProfileBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }
}
