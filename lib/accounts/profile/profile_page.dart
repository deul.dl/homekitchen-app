import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'profile_screen.dart';

class ProfilePage extends StatelessWidget {
  ProfilePage({Key key}) : super(key: key);
  static const routeName = '/profile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text(FlutterI18n.translate(context, 'profile.Title')),
        ),
        body: ProfileScreen());
  }
}
