import 'dart:async';
import 'dart:io';

import 'package:home_kitchen/auth/auth_event.dart';
import 'package:meta/meta.dart';

import 'profile.dart';

@immutable
abstract class ProfileEvent {
  Stream<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc});
}

class UnProfileEvent extends ProfileEvent {
  @override
  Stream<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc}) async* {
    yield ProfileUninitialized();
  }
}

class UpdateFullName extends ProfileEvent {
  final User user;

  UpdateFullName({@required this.user});

  @override
  String toString() => 'UpdateFullName { user: $user }';

  @override
  Stream<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc}) async* {
    try {
      await bloc.userRepository
          .updateFullName(firstName: user.firstName, lastName: user.lastName)
          .whenComplete(() => bloc.authBloc.add(AuthUserUpdated()));
      yield ProfileLoadSuccess('Updated full name is successfully!');
    } on HttpException catch (_) {
      yield ProfileFailure(_?.toString());
    }
  }
}

class UpdateEmail extends ProfileEvent {
  final String email;

  UpdateEmail({@required this.email});

  @override
  String toString() => 'UpdateEmail { email: $email }';

  @override
  Stream<ProfileState> applyAsync(
      {ProfileState currentState, ProfileBloc bloc}) async* {
    try {
      await bloc.userRepository
          .updateEmail(email)
          .whenComplete(() => bloc.authBloc.add(AuthUserUpdated()));

      yield ProfileLoadSuccess('Successed updated!');
    } on HttpException catch (_) {
      yield ProfileFailure(_?.toString());
    }
  }
}
