import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/accounts/profile/update_full_name/update_full_name_page.dart';
import 'package:home_kitchen/accounts/profile/update_email/update_email_page.dart';
import 'package:home_kitchen/accounts/profile/update_phone_number/update_phone_number.dart';
import 'package:home_kitchen/auth/auth.dart';

import 'profile.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfileScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(
        builder: (BuildContext context, AuthState currentState) {
      final double height = 56.0 * 3;

      return Container(
          color: Theme.of(context).backgroundColor,
          child: Column(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      bottom: BorderSide(
                        color: Theme.of(context).backgroundColor,
                        width: 1.0,
                      ),
                    ),
                  ),
                  height: height,
                  child: ListView(
                    children: ListTile.divideTiles(
                      context: context,
                      tiles: [
                        ListTile(
                            title: Text(FlutterI18n.translate(
                                context, 'profile.List.FullName')),
                            trailing: Wrap(children: [
                              Text(
                                currentState.user.fullName,
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 14),
                              ),
                              Icon(Icons.chevron_right),
                            ]),
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        BlocProvider<ProfileBloc>(
                                            create: (_) => ProfileBloc(
                                                authBloc:
                                                    BlocProvider.of<AuthBloc>(
                                                        context),
                                                userRepository:
                                                    RepositoryProvider.of<
                                                            UserRepository>(
                                                        context)),
                                            child: UpdateFullNamePage(
                                              user: currentState.user,
                                            ))))),
                        ListTile(
                            title: Text(FlutterI18n.translate(
                                context, 'profile.List.PhoneNumber')),
                            trailing: Wrap(children: [
                              Text(
                                currentState.user.phoneNumber,
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 14),
                              ),
                              Icon(Icons.chevron_right),
                            ]),
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        BlocProvider<ProfileBloc>(
                                            create: (_) => ProfileBloc(
                                                authBloc:
                                                    BlocProvider.of<AuthBloc>(
                                                        context),
                                                userRepository:
                                                    RepositoryProvider.of<
                                                            UserRepository>(
                                                        context)),
                                            child: UpdatePhoneNumberPage(
                                              user: currentState.user,
                                            ))))),
                        ListTile(
                            title: Text(FlutterI18n.translate(
                                context, 'profile.List.Email')),
                            trailing: Wrap(children: [
                              Text(
                                currentState.user.email,
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 14),
                              ),
                              Icon(Icons.chevron_right),
                            ]),
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        UpdateEmailPage(
                                            user: currentState.user)))),
                      ],
                    ).toList(),
                  ))
            ],
          ));
    });
  }
}
