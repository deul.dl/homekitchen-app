import 'package:equatable/equatable.dart';

abstract class ProfileState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  ProfileState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class ProfileUninitialized extends ProfileState {
  ProfileUninitialized();

  @override
  String toString() => 'ProfileUninitialized {}';
}

class ProfileLoadSuccess extends ProfileState {
  ProfileLoadSuccess(this.message) : super();

  final message;

  @override
  String toString() => 'ProfileLoadSuccess { message: $message }';
}

class ProfileFailure extends ProfileState {
  final String errorMessage;

  ProfileFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ProfileError { errorMessage: $errorMessage }';
}
