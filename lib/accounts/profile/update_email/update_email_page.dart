import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/accounts/profile/profile.dart';

import 'widgets/edit_email.dart';

class UpdateEmailPage extends StatelessWidget {
  UpdateEmailPage({Key key, @required this.user}) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title:
              Text(FlutterI18n.translate(context, 'profile.EditEmail.Title')),
        ),
        body: BlocProvider<ProfileBloc>(
            create: (_) => ProfileBloc(
                authBloc: BlocProvider.of<AuthBloc>(context),
                userRepository: RepositoryProvider.of<UserRepository>(context)),
            child: EditEmailScreen(
              isEdit: true,
              user: user,
              onSave: () => Navigator.pop(context),
            )));
  }
}
