import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/accounts/profile/profile.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/form_top.dart';
import 'package:home_kitchen/widgets/custom_form_input.dart';

class EditEmailScreen extends StatefulWidget {
  EditEmailScreen({Key key, bool isEdit, this.user, this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final User user;
  final Function onSave;

  @override
  _EditEmailState createState() => _EditEmailState();
}

class _EditEmailState extends State<EditEmailScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _email;

  @override
  void initState() {
    _email = widget.user.email;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileBloc, ProfileState>(
        listener: (BuildContext context, ProfileState state) {
      if (state is ProfileLoadSuccess) {
        widget.onSave();
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(content: Text(state.message)),
          );
      } else if (state is ProfileFailure) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(content: Text(state.errorMessage)),
          );
      }
    }, child: BlocBuilder<ProfileBloc, ProfileState>(
            builder: (BuildContext context, ProfileState state) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FormTop(
                  child: Column(
                    children: <Widget>[
                      CustomFormInput(
                        initialValue: widget.user.email ?? '',
                        onSaved: (val) => _email = val,
                        label: FlutterI18n.translate(
                            context, 'profile.EditEmail.Form.Email.Label'),
                        validator: (val) {
                          return val.trim().isEmpty
                              ? FlutterI18n.translate(
                                  context, 'profile.EditEmail.Form.Email.Error')
                              : null;
                        },
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.bottomCenter,
                  child: ExpandedFlatButton(
                      label: FlutterI18n.translate(context, 'common.Save'),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          BlocProvider.of<ProfileBloc>(context)
                              .add(UpdateEmail(email: _email));
                        }
                      }),
                )
              ],
            )),
      );
    }));
  }
}
