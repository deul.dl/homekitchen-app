import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/accounts/profile/profile.dart';
import 'package:home_kitchen/widgets/verificate_phone_number/veritifacate_phone_number.dart';

import 'package:home_kitchen/accounts/profile/update_phone_number/update_phone_number.dart';

class UpdatePhoneNumberScreen extends StatefulWidget {
  UpdatePhoneNumberScreen({Key key, @required this.user}) : super(key: key);

  final User user;

  @override
  _UpdatePhoneNumberState createState() => _UpdatePhoneNumberState();
}

class _UpdatePhoneNumberState extends State<UpdatePhoneNumberScreen> {
  final _formsPageViewController = PageController();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<VerificatePhoneNumberBloc>(
        create: (_) => VerificatePhoneNumberBloc(
              authRepository: RepositoryProvider.of<AuthRepository>(context),
            ),
        child: forms(context));
  }

  Widget forms(BuildContext context) {
    return BlocListener<UpdatePhoneNumberBloc, UpdatePhoneNumberState>(
        listener: (BuildContext context, UpdatePhoneNumberState state) {
      if (state.status.isSubmissionSuccess) {
        Navigator.pop(context);
      } else if (state.status.isSubmissionFailure) {
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(content: Text(state.message)));
      }
    }, child: BlocBuilder<UpdatePhoneNumberBloc, UpdatePhoneNumberState>(
            builder: (BuildContext context, UpdatePhoneNumberState state) {
      return PageView(
        controller: _formsPageViewController,
        physics: new NeverScrollableScrollPhysics(),
        children: [
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('phoneNumberForm'),
            child: VeritificatePhoneNumberForm(
              phoneNumber: state.phoneNumber.value == ''
                  ? ZPhoneNumber.dirty(widget.user.phoneNumber)
                  : state.phoneNumber,
              onChangePhoneNumber: (val) =>
                  BlocProvider.of<UpdatePhoneNumberBloc>(context)
                      .add(UpdatePhoneNumberChanged(val)),
              onSave: () {
                if (state.status.isValidated) {
                  _nextFormStep();
                  FocusScope.of(context).requestFocus(new FocusNode());
                }
              },
            ),
          ),
          WillPopScope(
            onWillPop: () => Future.sync(this._onWillPop),
            key: Key('codeForm'),
            child: VeritificateCodeForm(
              phoneNumber: state.phoneNumber.value == ''
                  ? ZPhoneNumber.dirty(widget.user.phoneNumber)
                  : state.phoneNumber,
              onSave: (String code) {
                if (state.status.isValidated) {
                  BlocProvider.of<UpdatePhoneNumberBloc>(context).add(
                      UpdatePhoneNumberSubmitted(
                          code, widget.user.phoneNumber));
                }
              },
            ),
          ),
        ],
      );
    }));
  }

  void _nextFormStep() {
    _formsPageViewController.nextPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  bool _onWillPop() {
    if (_formsPageViewController.page.round() ==
        _formsPageViewController.initialPage) return true;

    _formsPageViewController.previousPage(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
    );

    return false;
  }
}
