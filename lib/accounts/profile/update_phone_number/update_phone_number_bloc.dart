import 'dart:async';

import 'package:formz/formz.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/accounts/profile/update_phone_number/update_phone_number.dart';

class UpdatePhoneNumberBloc
    extends Bloc<UpdatePhoneNumberEvent, UpdatePhoneNumberState> {
  UpdatePhoneNumberBloc({
    @required AuthRepository authRepository,
    @required UserRepository userRepository,
  })  : assert(authRepository != null),
        _authRepository = authRepository,
        _userRepository = userRepository,
        super(UpdatePhoneNumberState());

  final AuthRepository _authRepository;
  final UserRepository _userRepository;

  @override
  Stream<UpdatePhoneNumberState> mapEventToState(
    UpdatePhoneNumberEvent event,
  ) async* {
    if (event is UpdatePhoneNumberChanged) {
      yield _mapPhoneNumberChangedToState(event, state);
    } else if (event is UpdatePhoneNumberSubmitted) {
      yield* _mapUpdatePhoneNumberSubmittedToState(event, state);
    }
  }

  UpdatePhoneNumberState _mapPhoneNumberChangedToState(
    UpdatePhoneNumberChanged event,
    UpdatePhoneNumberState state,
  ) {
    final phoneNumber = ZPhoneNumber.dirty(event.phoneNumber);

    return state.copyWith(
      phoneNumber: phoneNumber,
      status: Formz.validate([phoneNumber]),
    );
  }

  Stream<UpdatePhoneNumberState> _mapUpdatePhoneNumberSubmittedToState(
    UpdatePhoneNumberSubmitted event,
    UpdatePhoneNumberState state,
  ) async* {
    yield state.copyWith(status: FormzStatus.valid);
    if (state.status.isValidated) {
      try {
        print(event.phoneNumber);
        yield state.copyWith(status: FormzStatus.submissionInProgress);
        await _userRepository.updatePhoneNumber(
            phoneNumber: state.phoneNumber.phoneNumberWithPlus,
            code: event.code,
            oldPhoneNumber: event.phoneNumber);

        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } on Exception catch (_) {
        yield state.copyWith(
            status: FormzStatus.submissionFailure, message: _?.toString());
      }
    }
  }
}
