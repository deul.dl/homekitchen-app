import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/auth/auth.dart';

import 'package:home_kitchen/accounts/profile/update_phone_number/update_phone_number_screen.dart';
import 'package:home_kitchen/accounts/profile/update_phone_number/update_phone_number.dart';

class UpdatePhoneNumberPage extends StatelessWidget {
  UpdatePhoneNumberPage({Key key, this.user}) : super(key: key);
  final User user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        body: SafeArea(
            top: true,
            child: BlocProvider<UpdatePhoneNumberBloc>(
                create: (_) => UpdatePhoneNumberBloc(
                      authRepository:
                          RepositoryProvider.of<AuthRepository>(context),
                      userRepository:
                          RepositoryProvider.of<UserRepository>(context),
                    ),
                child: Container(child: UpdatePhoneNumberScreen(user: user)))));
  }
}
