import 'package:flutter/material.dart';

import 'package:home_kitchen/accounts/profile/profile.dart';

import 'package:home_kitchen/widgets/expanded_flat_button.dart';
import 'package:home_kitchen/widgets/custom_form_input.dart';

class EditPhoneNumberScreen extends StatefulWidget {
  EditPhoneNumberScreen(
      {Key key, bool isEdit, this.user, @required this.onSave})
      : this.isEdit = isEdit ?? false,
        super(key: key);

  final bool isEdit;
  final User user;
  final Function onSave;

  @override
  _EditPhoneNumberState createState() => _EditPhoneNumberState();
}

class _EditPhoneNumberState extends State<EditPhoneNumberScreen> {
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _phoneNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    CustomFormInput(
                      keyboardType: TextInputType.phone,
                      initialValue:
                          widget.user == null ? '' : widget.user.phoneNumber,
                      onSaved: (val) => _phoneNumber = val,
                      label: 'Phone Number',
                      validator: (val) {
                        return val.trim().isEmpty ? 'required' : null;
                      },
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(bottom: 10),
                alignment: Alignment.bottomCenter,
                child: ExpandedFlatButton(
                    label: widget.isEdit ? 'Next' : 'Save',
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        widget.onSave(_phoneNumber);
                      }
                    }),
              )
            ],
          )),
    );
  }
}
