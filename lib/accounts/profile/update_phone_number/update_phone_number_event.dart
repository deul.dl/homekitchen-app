import 'package:equatable/equatable.dart';

abstract class UpdatePhoneNumberEvent extends Equatable {
  const UpdatePhoneNumberEvent();

  @override
  List<Object> get props => [];
}

class UpdatePhoneNumberChanged extends UpdatePhoneNumberEvent {
  const UpdatePhoneNumberChanged(this.phoneNumber);

  final String phoneNumber;

  @override
  List<Object> get props => [phoneNumber];
}

class UpdatePhoneNumberSubmitted extends UpdatePhoneNumberEvent {
  const UpdatePhoneNumberSubmitted(this.code, this.phoneNumber);

  final String code;
  final String phoneNumber;

  @override
  List<Object> get props => [code, phoneNumber];
}
