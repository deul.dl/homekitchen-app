import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import 'package:home_kitchen/signin/signin.dart';

class UpdatePhoneNumberState extends Equatable {
  const UpdatePhoneNumberState({
    this.status = FormzStatus.pure,
    this.phoneNumber = const ZPhoneNumber.pure(),
    this.message,
  });

  final FormzStatus status;
  final ZPhoneNumber phoneNumber;
  final String message;

  UpdatePhoneNumberState copyWith(
      {FormzStatus status, ZPhoneNumber phoneNumber, String message}) {
    return UpdatePhoneNumberState(
        status: status ?? this.status,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        message: message ?? this.message);
  }

  @override
  List<Object> get props => [phoneNumber, message, status];
}
