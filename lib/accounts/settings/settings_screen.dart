import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:home_kitchen/auth/auth.dart';

class SettingsScreen extends StatefulWidget {
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    final double height = 56.0 * 2;
    return Container(
        color: Theme.of(context).backgroundColor,
        child: Column(children: [
          Container(
              height: height,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                  bottom: BorderSide(
                    color: Theme.of(context).backgroundColor,
                    width: 1.0,
                  ),
                ),
              ),
              child: ListView(
                children: ListTile.divideTiles(
                  context: context,
                  tiles: [
                    ListTile(
                      title: Text('Version'),
                      trailing: Text('1.0.7'),
                    ),
                  ],
                ).toList(),
              )),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: 20),
            height: 48,
            child: TextButton.icon(
                style: TextButton.styleFrom(
                  backgroundColor: Theme.of(context).primaryColor,
                ),
                icon: Icon(Icons.exit_to_app, color: Colors.white),
                onPressed: () {
                  BlocProvider.of<AuthBloc>(context).add(AuthLogoutRequested());
                  Navigator.pushReplacementNamed(context, '/');
                },
                label: Text(FlutterI18n.translate(context, 'settings.Logout'),
                    style: Theme.of(context).textTheme.button)),
          )
        ]));
  }
}
