import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'settings_screen.dart';

class SettingsPage extends StatelessWidget {
  static const String routeName = '/settings';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text(FlutterI18n.translate(context, 'settings.Title')),
        ),
        body: SettingsScreen());
  }
}
