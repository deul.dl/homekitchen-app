import 'dart:async';
import 'dart:developer' as developer;
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/orders_repository/orders_repository.dart';

import 'past_orders.dart';

class PastOrdersBloc extends Bloc<PastOrdersEvent, PastOrdersState> {
  // todo: check singleton for logic in project
  final OrdersRepositoryBase ordersRepository;

  PastOrdersBloc({@required this.ordersRepository}) : super(null);

  @override
  Stream<PastOrdersState> mapEventToState(
    PastOrdersEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'PastOrdersBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
