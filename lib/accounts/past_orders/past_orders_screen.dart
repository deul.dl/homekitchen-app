import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:home_kitchen/widgets/empty_screen.dart';

import 'package:home_kitchen/widgets/order_list/order_list.dart';

import 'past_orders.dart';

class PastOrdersScreen extends StatefulWidget {
  PastOrdersScreen({Key key, @required this.tabController}) : super(key: key);

  final TabController tabController;

  @override
  _PastOrdersState createState() => _PastOrdersState();
}

class _PastOrdersState extends State<PastOrdersScreen> {
  @override
  void initState() {
    this._load();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PastOrdersBloc, PastOrdersState>(
        builder: (BuildContext context, PastOrdersState state) {
      if (state is PastOrdersLoadSuccess) {
        if (state.orders.length != 0) {
          return TabBarView(controller: widget.tabController, children: [
            simpleOrders(state.orders),
            specialOrders(state.orders),
          ]);
        } else {
          return EmptyScreen();
        }
      } else if (state is PastOrdersFailed) {
        return ErrorScreen(errorMessage: state.errorMessage, onReload: _load);
      } else {
        return EmptyScreen();
      }
    });
  }

  Widget simpleOrders(List<Order> orders) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: OrderList(orders: orders),
    );
  }

  Widget specialOrders(List<Order> orders) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: OrderList(orders: orders),
    );
  }

  void _load() {
    BlocProvider.of<PastOrdersBloc>(context).add(PastOrdersLoaded());
  }
}
