import 'package:flutter/material.dart';

import 'past_orders_screen.dart';

class PastOrdersPage extends StatefulWidget {
  PastOrdersPage({Key key}) : super(key: key);

  static const routeName = '/past-orders';

  @override
  _PastOrdersState createState() => _PastOrdersState();
}

class _PastOrdersState extends State<PastOrdersPage>
    with SingleTickerProviderStateMixin {
  static TabController _tabOrdersController;

  static const List<Tab> _ordersTabs = <Tab>[
    Tab(text: 'Simple'),
    Tab(text: 'Special'),
  ];

  @override
  void initState() {
    _tabOrdersController =
        TabController(length: _ordersTabs.length, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('PastOrders'),
          bottom: TabBar(
              isScrollable: true,
              controller: _tabOrdersController,
              tabs: _ordersTabs),
        ),
        body: PastOrdersScreen(
          tabController: _tabOrdersController,
        ));
  }
}
