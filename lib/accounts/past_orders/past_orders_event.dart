import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';

import 'past_orders.dart';

@immutable
abstract class PastOrdersEvent {
  Stream<PastOrdersState> applyAsync(
      {PastOrdersState currentState, PastOrdersBloc bloc});
}

class PastOrdersLoaded extends PastOrdersEvent {
  String getString() => 'PastOrdersLoaded {}';

  @override
  Stream<PastOrdersState> applyAsync(
      {PastOrdersState currentState, PastOrdersBloc bloc}) async* {
    try {
      await bloc.ordersRepository.getOrders();

      yield PastOrdersLoadSuccess(orders: []);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'PastOrdersLoaded', error: _, stackTrace: stackTrace);
      yield PastOrdersFailed(_?.toString());
    }
  }
}
