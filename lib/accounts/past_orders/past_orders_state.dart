import 'package:equatable/equatable.dart';

import 'past_orders.dart';

abstract class PastOrdersState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  PastOrdersState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class PastOrdersLoadSuccess extends PastOrdersState {
  PastOrdersLoadSuccess({this.orders = const []}) : super([orders]);

  final List<OrderDetails> orders;

  @override
  String toString() => 'PastOrdersLoadSuccess { orders: $orders }';
}

class PastOrdersFailed extends PastOrdersState {
  final String errorMessage;

  PastOrdersFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'PastOrdersError { errorMessage: $errorMessage }';
}
