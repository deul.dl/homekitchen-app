import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/customer/widgets/basket.dart';
import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/theme/colors.dart';

class StoreAppBar extends StatefulWidget implements PreferredSizeWidget {
  StoreAppBar({Key key, Color backgroundColor, this.title})
      : this.backgroundColor = backgroundColor ?? HomeKitchenColors.blackColor,
        preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  Color backgroundColor;
  final Widget title;

  @override
  final Size preferredSize;

  @override
  _StoreAppBarState createState() => _StoreAppBarState();
}

class _StoreAppBarState extends State<StoreAppBar> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
        builder: (BuildContext context, CartState state) {
      final int count =
          state is CartUninitialized ? 0 : (state as CartLoadSuccess).count;

      Widget basket = Basket(
        count: count,
        onTap: () => Navigator.of(context).pushNamed('/cart'),
      );

      return AppBar(
        elevation: 0,
        title: widget.title,
        backgroundColor: widget.backgroundColor,
        actions: <Widget>[basket],
      );
    });
  }
}
