import 'package:flutter/material.dart';

class Basket extends StatelessWidget {
  Basket({Key key, this.count, @required this.onTap}) : super(key: key);

  final int count;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    Widget result = Padding(
        padding: EdgeInsets.all(8.0),
        child: SizedBox(
          height: 24,
          width: 24,
          child: Icon(
            Icons.shopping_cart,
            color: Colors.white,
          ),
        ));

    return InkResponse(
        onTap: onTap,
        borderRadius: BorderRadius.circular(50),
        child: Stack(children: [
          Align(alignment: Alignment.center, child: result),
          if (count != 0)
            Positioned(
              right: 0,
              top: 5,
              child: Container(
                  height: 20,
                  width: 20,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      shape: BoxShape.circle),
                  child: Text(
                    count.toString(),
                    style: TextStyle(color: Colors.white, fontSize: 11),
                  )),
            )
        ]));
  }
}
