import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';
import 'package:home_kitchen/customer/payment_methods/payment_methods.dart';
import 'package:home_kitchen/order_status_repository/order_status_repository.dart';

import 'package:home_kitchen/theme/theme.dart';

import 'package:home_kitchen/customer/main/main.dart';

import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/customer/check_out/check_out.dart';
import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:home_kitchen/customer/my_orders/my_orders_bloc.dart';
import 'package:home_kitchen/accounts/accounts.dart';
import 'package:home_kitchen/customer/order_details/order_details.dart';
import 'package:home_kitchen/location/location.dart';
import 'package:home_kitchen/customer/categories/category.dart';

import 'package:home_kitchen/customer/items/items_repository.dart';

class HomeKitchenCustomer extends StatelessWidget {
  HomeKitchenCustomer({Key key, this.state}) : super(key: key);
  // This widget is the root of your application.
  final AuthState state;

  @override
  Widget build(BuildContext context) {
    final languageCode =
        WidgetsBinding.instance.window.locale.languageCode == 'ru'
            ? 'ru'
            : 'en';
    final FlutterI18nDelegate flutterI18nDelegate = FlutterI18nDelegate(
      translationLoader: NamespaceFileTranslationLoader(
        namespaces: [
          'common',
          'signup',
          'login',
          'addresses',
          'order',
          'cart',
          'checkOut',
          'account',
          'enterExecutor',
          'settings',
          'verificatePhoneNumber',
          'uploadImage',
          'stories',
          'items',
          'main',
          'category',
          'statistic'
        ],
        useCountryCode: false,
        basePath: 'assets/locales',
        fallbackDir: languageCode,
        forcedLocale: Locale(languageCode),
      ),
      missingTranslationHandler: (key, locale) {
        print('--- Missing Key: $key, languageCode: ${locale.languageCode}');
      },
    );
    WidgetsFlutterBinding.ensureInitialized();

    final addressesRepository =
        AddressesRepository(RepositoryProvider.of<WebClient>(context));
    final categoryRepository =
        CategoryRepository(RepositoryProvider.of<WebClient>(context));
    final storiesRepository =
        StoriesRepository(RepositoryProvider.of<WebClient>(context));
    final itemsRepository =
        ItemsRepository(RepositoryProvider.of<WebClient>(context));
    final myOrdersRepository =
        MyOrderRepository(RepositoryProvider.of<WebClient>(context));

    return MultiRepositoryProvider(
        providers: [
          RepositoryProvider<MyOrderRepository>(
            create: (context) => myOrdersRepository,
          ),
          RepositoryProvider<StoriesRepository>(
            create: (context) => storiesRepository,
          ),
          RepositoryProvider<AddressesRepository>(
              create: (context) => addressesRepository),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider<PaymentMethodBloc>(
              create: (_) => PaymentMethodBloc(),
            ),
            BlocProvider<CartBloc>(create: (_) => CartBloc()),
            BlocProvider<PaymentMethodBloc>(create: (_) => PaymentMethodBloc()),
            BlocProvider<CategoryBloc>(
                create: (_) =>
                    CategoryBloc(categoryRepository: categoryRepository)
                      ..add(LoadCategories())),
            BlocProvider<AddressesBloc>(
              create: (_) =>
                  AddressesBloc(addressRepository: addressesRepository),
            ),
            BlocProvider<MyOrdersBloc>(
                create: (_) => MyOrdersBloc(
                    ordersRepository: myOrdersRepository,
                    orderStatusRepository:
                        RepositoryProvider.of<OrderStatusRepository>(context)))
          ],
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: homeKitchenTheme,
            home: MultiBlocProvider(providers: [
              BlocProvider<HomeBloc>(
                  create: (_) => HomeBloc(
                      client: RepositoryProvider.of<WebClient>(context),
                      locationBloc: BlocProvider.of<LocationBloc>(context))),
            ], child: MainPage(authState: state)),
            routes: <String, WidgetBuilder>{
              StoriesPage.routeName: (BuildContext context) =>
                  BlocProvider<StoriesBloc>(
                    create: (context) => StoriesBloc(
                        locationBloc: BlocProvider.of<LocationBloc>(context),
                        storiesRepository: storiesRepository),
                    child: StoriesPage(),
                  ),
              StorePage.routeName: (BuildContext context) =>
                  BlocProvider<StoreBloc>(
                      create: (context) => StoreBloc(
                          storiesRepository: storiesRepository,
                          itemsRepository: itemsRepository),
                      child: StorePage()),
              // ItemsPage.routeName: (BuildContext context) => ItemsPage(),
              ItemPage.routeName: (BuildContext context) => ItemPage(),
              CheckOutPage.routeName: (BuildContext context) => CheckOutPage(),
              CartPage.routeName: (BuildContext context) => CartPage(),
              AddressesPage.routeName: (BuildContext context) =>
                  AddressesPage(),
              NewAddressPage.routeName: (BuildContext context) => BlocProvider<
                      AddressBloc>(
                  create: (_) => AddressBloc(
                      addressRepository:
                          RepositoryProvider.of<AddressesRepository>(context)),
                  child: NewAddressPage()),
              EditAddressPage.routeName: (BuildContext context) => BlocProvider<
                      AddressBloc>(
                  create: (_) => AddressBloc(
                      addressRepository:
                          RepositoryProvider.of<AddressesRepository>(context)),
                  child: EditAddressPage()),
              PaymentMethodsPage.routeName: (BuildContext context) =>
                  PaymentMethodsPage(),

              // Account
              // PastOrdersPage.routeName: (BuildContext context) =>
              //     BlocProvider<PastOrdersBloc>(
              //         create: (_) =>
              //             PastOrdersBloc(ordersRepository: ordersRepository),
              //         child: PastOrdersPage()),

              OrderDetailsPage.routeName: (BuildContext context) =>
                  BlocProvider<OrderDetailsBloc>(
                    create: (context) => OrderDetailsBloc(
                        orderStatusRepository:
                            RepositoryProvider.of<OrderStatusRepository>(
                                context),
                        ordersRepository: myOrdersRepository),
                    child: OrderDetailsPage(),
                  ),
              SettingsPage.routeName: (BuildContext context) => SettingsPage(),
            },
            localizationsDelegates: [
              flutterI18nDelegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
          ),
        ));
  }
}
