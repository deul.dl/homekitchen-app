import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import 'home.dart';

@immutable
abstract class HomeState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  HomeState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class InProgressHome extends HomeState {
  @override
  String toString() => 'InProgressHome {}';
}

class HomeLoaded extends HomeState {
  HomeLoaded({this.groupByPopularItems = const []})
      : super([
          groupByPopularItems,
        ]);

  final List<PopularItemsSection> groupByPopularItems;

  @override
  String toString() =>
      'HomeLoaded { groupByPopularItems: $groupByPopularItems }';
}

class HomeFailed extends HomeState {
  final String errorMessage;

  HomeFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'HomeFailed';
}
