import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/customer/constants/constants.dart';
import 'package:home_kitchen/customer/categories/category.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

class CategoryTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CategoryBloc, CategoryState>(builder: (
      BuildContext context,
      CategoryState currentState,
    ) {
      if (currentState is CategoryLoadSuccess) {
        return Container(
            child: ListView(
          children: ListTile.divideTiles(
            context: context,
            tiles: currentState.categories
                .map((category) => Container(
                    color: Colors.white,
                    child: ListTile(
                        onTap: () => Navigator.of(context).pushNamed('/stories',
                            arguments:
                                StoriesScreenArguments(category: category)),
                        key: Key(category.id.toString()),
                        title: Text(
                          FlutterI18n.translate(
                              context, 'category.${category.name}'),
                          style: Theme.of(context).textTheme.subtitle1,
                        ))))
                .toList(),
          ).toList(),
        ));
      } else if (currentState is CategoryFailure) {
        return ErrorScreen(
            errorMessage: currentState.errorMessage,
            onReload: () =>
                BlocProvider.of<CategoryBloc>(context).add(LoadCategories()));
      } else {
        return Container(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        );
      }
    });
  }
}
