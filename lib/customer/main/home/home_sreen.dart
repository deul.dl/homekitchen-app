import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_kitchen/location/location.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'widgets/popular_items.dart';
import 'category_tab_view.dart';
import 'home.dart';

class HomeSreen extends StatefulWidget {
  const HomeSreen({Key key, @required this.tabController}) : super(key: key);

  final TabController tabController;

  @override
  _HomeSreenState createState() => _HomeSreenState();
}

class _HomeSreenState extends State<HomeSreen> {
  @override
  Widget build(BuildContext context) {
    return TabBarView(
      controller: widget.tabController,
      children: [
        Container(
          child: _renderPopularItems(),
        ),
        CategoryTabView(),
      ],
    );
  }

  static Widget _renderPopularItems() {
    return BlocBuilder<HomeBloc, HomeState>(builder: (
      BuildContext context,
      HomeState currentState,
    ) {
      if (currentState is HomeLoaded) {
        return ListView(
            children: currentState.groupByPopularItems
                .map((e) => PopularItems(popularItemsSection: e))
                .toList());
      } else if (currentState is HomeFailed) {
        return ErrorScreen(
            errorMessage: currentState.errorMessage,
            onReload: () {
              final locality = (BlocProvider.of<LocationBloc>(context).state
                      as LocationSuccess)
                  .locality;
              BlocProvider.of<HomeBloc>(context).add(LoadHome(locality));
            });
      } else {
        return Container(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        );
      }
    });
  }
}
