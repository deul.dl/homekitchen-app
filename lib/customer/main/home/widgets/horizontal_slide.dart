import 'package:flutter/material.dart';

import 'package:home_kitchen/widgets/item_card.dart';
import 'package:home_kitchen/customer/constants/constants.dart';

import '../home.dart';

class HorizontalSlide extends StatelessWidget {
  HorizontalSlide({Key key, this.items}) : super(key: key);

  final List<Item> items;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ListView(
      scrollDirection: Axis.horizontal,
      children: items
          .map((item) => ItemCard(
              key: Key(item.uuid),
              height: 160,
              item: item,
              onTap: () => Navigator.of(context).pushNamed('/item',
                  arguments: ItemScreenArguments(item: item))))
          .toList(),
    ));
  }
}
