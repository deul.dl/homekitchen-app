import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/author_avatar.dart';

import 'horizontal_slide.dart';

import '../home.dart';

class PopularItems extends StatelessWidget {
  PopularItems({Key key, this.popularItemsSection}) : super(key: key);

  final PopularItemsSection popularItemsSection;

  @override
  Widget build(BuildContext context) {
    if (popularItemsSection.items.isEmpty) {
      return Container();
    }
    return Container(
      height: 300,
      child: Card(
        color: Colors.transparent,
        margin: EdgeInsets.symmetric(horizontal: 10.0),
        elevation: 0,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(children: <Widget>[
                Expanded(
                    child: AuthorAvatar(
                  store: popularItemsSection,
                )),
                InkWell(
                    child: Text(
                      FlutterI18n.translate(context, 'common.All'),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    onTap: () => Navigator.of(context).pushNamed('/store',
                        arguments: {'storeUUID': popularItemsSection.uuid}))
              ]),
              SizedBox(
                height: 20,
              ),
              HorizontalSlide(items: popularItemsSection.items)
            ]),
      ),
    );
  }
}
