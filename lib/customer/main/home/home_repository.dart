import 'dart:async';
import 'dart:core';

import 'package:home_kitchen/client/client.dart';
import 'home.dart';

abstract class Repository {
  Future<dynamic> loadData(String city);
}

class HomeRepository implements Repository {
  HomeRepository(client) : this._client = client;

  final WebClient _client;

  @override
  Future<List<PopularItemsSection>> loadData(String city) async {
    return _client
        .get('home?city=$city')
        .then((res) => res['data']['stories'])
        .then((stories) => stories == null
            ? null
            : (stories as List)
                .map((store) => PopularItemsSection.fromJson(store))
                .toList());
  }
}
