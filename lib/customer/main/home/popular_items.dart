import 'package:home_kitchen/models/store.dart';
import 'package:home_kitchen/models/item.dart';

class PopularItemsSection extends Store {
  PopularItemsSection(
      {String uuid,
      String name,
      String imageSource,
      int startedAt,
      int endedAt,
      this.items})
      : super(
            uuid: uuid,
            name: name,
            imageSource: imageSource,
            startedAt: startedAt,
            endedAt: endedAt);

  final List<Item> items;

  @override
  List<Object> get props =>
      [uuid, name, imageSource, startedAt, endedAt, items];

  @override
  factory PopularItemsSection.fromJson(Map<String, dynamic> json) {
    return PopularItemsSection(
        uuid: json['uuid'] as String,
        name: json['name'] as String,
        imageSource: json['imageSource'] as String,
        startedAt: json['startedAt'] as int,
        endedAt: json['endedAt'] as int,
        items: json['items'] != null
            ? (json['items'] as List)
                .map<Item>((item) => Item.fromJson(item))
                .toList()
            : []);
  }
}
