import 'dart:async';
import 'dart:io';

import 'package:home_kitchen/location/location.dart';
import 'package:meta/meta.dart';

import 'home.dart';

@immutable
abstract class HomeEvent {
  Stream<HomeState> applyAsync({HomeState currentState, HomeBloc bloc});
}

class LoadHome extends HomeEvent {
  LoadHome(this.city);
  final String city;

  @override
  String toString() => 'LoadedHome { city: $city }';

  @override
  Stream<HomeState> applyAsync({HomeState currentState, HomeBloc bloc}) async* {
    try {
      final groupByPopularItems = await bloc.homeRepository.loadData(city);

      yield HomeLoaded(
        groupByPopularItems: groupByPopularItems ?? [],
      );
    } on HttpException catch (_) {
      yield HomeFailed(_?.toString());
    }
  }
}

class StartHome extends HomeEvent {
  StartHome();

  @override
  String toString() => 'StartHome {}';

  @override
  Stream<HomeState> applyAsync({HomeState currentState, HomeBloc bloc}) async* {
    final locationState = bloc.locationBloc.state;
    if (locationState is LocationSuccess) {
      bloc.add(LoadHome(locationState.locality));
    }
  }
}
