import 'dart:async';

import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/location/location.dart';

import 'home.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  // todo: check singleton for logic in project
  final HomeRepository homeRepository;
  final LocationBloc locationBloc;

  HomeBloc({this.locationBloc, @required WebClient client})
      : this.homeRepository = HomeRepository(client),
        super(InProgressHome());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
