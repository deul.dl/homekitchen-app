import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/widgets/bottom_navigator.dart';
import 'package:home_kitchen/widgets/search_bar.dart';
import 'package:home_kitchen/widgets/expand_tab_bar.dart';
import 'package:home_kitchen/widgets/item_search.dart';
import 'package:home_kitchen/accounts/accounts.dart';
import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/location/location.dart';
import 'package:home_kitchen/signin/signin_page.dart';

import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';

import 'home/home.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, AuthState this.authState}) : super(key: key);

  final AuthState authState;

  static const String routeName = '/home';

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  int _selectedIndex = 0;

  static TabController _tabHomeController;
  static TabController _tabMyOrderController;

  @override
  void initState() {
    _tabHomeController = TabController(vsync: this, length: 2);
    _load();
    BlocProvider.of<HomeBloc>(context).add(StartHome());
    super.initState();
  }

  Widget _selectedAppBar(int index) {
    switch (index) {
      case 0:
        {
          return SearchBar(
              height: 130,
              bottom: ExpandTabBar(
                tabs: <Tab>[
                  Tab(text: FlutterI18n.translate(context, 'main.TabBar.Home')),
                  Tab(
                      text: FlutterI18n.translate(
                          context, 'main.TabBar.Categories')),
                ],
                controller: _tabHomeController,
              ),
              onNextSearch: () {
                showSearch(
                  context: context,
                  delegate: ItemSearch(),
                );
              });
        }
      case 1:
        {
          return AppBar(
            title: Text(FlutterI18n.translate(context, 'main.AppBar.MyOrders')),
          );
        }
      case 2:
        {
          return AppBar(
            title: Text(FlutterI18n.translate(context, 'main.AppBar.Cart')),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () =>
                      BlocProvider.of<CartBloc>(context).add(CleanCart()))
            ],
          );
        }
      case 3:
        {
          return AppBar(
            backgroundColor: Colors.black,
            title: Text(FlutterI18n.translate(context, 'main.AppBar.Account')),
          );
        }
      default:
        return AppBar(
          backgroundColor: Colors.black,
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _selectedAppBar(_selectedIndex),
      body: SafeArea(
          child: BlocListener<LocationBloc, LocationState>(
        listener: (BuildContext context, LocationState state) {
          if (state is LocationSuccess) {
            BlocProvider.of<HomeBloc>(context).add(LoadHome(state.locality));
          }
        },
        child: Container(
            color: Theme.of(context).backgroundColor,
            child: _renderTabView(_selectedIndex)),
      )),
      bottomNavigationBar: BottomNavigation(
        onChangeScreen: _onChangeScreen,
        selectedIndex: _selectedIndex,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: FlutterI18n.translate(context, 'main.BottomNavigator.Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.view_list),
            label:
                FlutterI18n.translate(context, 'main.BottomNavigator.MyOrders'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_basket),
            label: FlutterI18n.translate(context, 'main.BottomNavigator.Cart'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label:
                FlutterI18n.translate(context, 'main.BottomNavigator.Account'),
          ),
        ],
      ),
    );
  }

  Widget _renderTabView(int index) {
    switch (index) {
      case 1:
        return MyOrdersScreen();
      case 2:
        return CartScreen();
      case 3:
        return AccountScreen();
      default:
        return HomeSreen(
          tabController: _tabHomeController,
        );
    }
  }

  void _onChangeScreen(int index) {
    if (index == 1 && !widget.authState.isAuth) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => SignInPage()));
    } else if ((index == 3) && !widget.authState.isAuth) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => SignInPage()));
    } else {
      setState(() {
        _selectedIndex = index;
      });
    }
  }

  void _load() {
    BlocProvider.of<CartBloc>(context).add(LoadCart());
  }

  @override
  void dispose() {
    super.dispose();
    _tabHomeController?.dispose();
    _tabMyOrderController?.dispose();
  }
}
