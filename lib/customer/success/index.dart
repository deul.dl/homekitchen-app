import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class SuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.infinity,
          color: Theme.of(context).backgroundColor,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            Container(
              alignment: Alignment.centerRight,
              child: TextButton(
                child: Text(FlutterI18n.translate(context, 'common.Skip')),
                onPressed: () => Navigator.of(context).pushNamed('/home'),
              ),
            ),
            Container(
                height: 600,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.check_circle,
                        color: Colors.green,
                        size: 120,
                      ),
                      Text(
                        'Created order',
                        style: TextStyle(
                            fontSize: 21,
                            fontWeight: FontWeight.w400,
                            color: Colors.black54),
                      )
                    ]))
          ])),
    );
  }
}
