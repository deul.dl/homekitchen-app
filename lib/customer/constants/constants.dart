import 'package:home_kitchen/models/item.dart';
import 'package:home_kitchen/customer/models/models.dart';

class ItemsScreenArguments {
  final List<Item> items;

  ItemsScreenArguments({this.items});
}

class StoriesScreenArguments {
  final Category category;

  StoriesScreenArguments({this.category});
}

class StoreScreenArguments {
  final ExpandedStore store;

  StoreScreenArguments({this.store});
}

class ItemScreenArguments {
  final Item item;
  final String from;

  ItemScreenArguments({this.item, this.from = ''});
}
