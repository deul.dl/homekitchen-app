import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/customer/models/models.dart';

abstract class ItemListState extends Equatable {
  final List<Object> propss;
  ItemListState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class ItemListInProgress extends ItemListState {
  @override
  String toString() => 'ItemListInProgress {}';
}

class ItemListLoadSuccess extends ItemListState {
  ItemListLoadSuccess({@required this.items}) : super([]);
  final List<OrderItem> items;

  @override
  String toString() => '''
    ItemListLoadSuccess {
      items: $items,
    }''';
}

class ItemListFailed extends ItemListState {
  final String errorMessage;

  ItemListFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ItemListFailed {}';
}
