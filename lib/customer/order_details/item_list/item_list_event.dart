import 'dart:async';

import 'package:meta/meta.dart';

export 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/customer/main/home/home.dart';

import 'item_list.dart';

@immutable
abstract class ItemListEvent {
  Stream<ItemListState> applyAsync(
      {ItemListState currentState, ItemListBloc bloc});
}

class LoadItemList extends ItemListEvent {
  LoadItemList(this.orderUUID);
  final String orderUUID;

  @override
  String toString() => 'LoadItemList { orderUUID: $orderUUID}';

  @override
  Stream<ItemListState> applyAsync(
      {ItemListState currentState, ItemListBloc bloc}) async* {
    try {
      final items = await bloc.ordersRepository.getOrderItems(orderUUID);

      yield ItemListLoadSuccess(
        items: (items as List<OrderItem>) ?? [],
      );
    } catch (_) {
      yield ItemListFailed(_?.toString());
    }
  }
}

class CompleteOrder extends ItemListEvent {
  CompleteOrder({@required this.orderId});
  final String orderId;

  @override
  String toString() => 'CompleteOrder { orderId: $orderId}';

  @override
  Stream<ItemListState> applyAsync(
      {ItemListState currentState, ItemListBloc bloc}) async* {
    try {
      await bloc.orderStatusRepository.completeOrder(orderId);
    } catch (_) {
      yield ItemListFailed(_?.toString());
    }
  }
}
