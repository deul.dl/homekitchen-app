import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/customer/models/models.dart';

import 'widgets/item_card.dart';

class ItemListScreen extends StatefulWidget {
  ItemListScreen({Key key, this.items}) : super(key: key);

  final List<OrderItem> items;

  @override
  _ItemListState createState() => _ItemListState();
}

class _ItemListState extends State<ItemListScreen> {
  double get totalPrice => widget.items
      .fold(0, (previousValue, element) => previousValue + element.totalPrice);

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(children: [
        Expanded(
          child: ListView(
            children: widget.items
                .map((e) => ItemCard(key: Key(e.itemUUID), item: e))
                .toList(),
          ),
        ),
        Container(
            color: Colors.white,
            child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, 'order.TotalPrice'),
                  style: Theme.of(context).textTheme.subtitle2,
                ),
                trailing: Text(
                  formatCurrency.printPrice(totalPrice),
                  style: Theme.of(context).textTheme.headline6,
                )))
      ]),
    );
  }
}
