import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/customer/models/models.dart';

class ItemCard extends StatelessWidget {
  ItemCard({Key key, this.item}) : super(key: key);

  final OrderItem item;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (item.imageSource.isNotEmpty
          ? item.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    return Card(
        elevation: 0,
        child: Row(children: <Widget>[
          // CachedNetworkImage(
          //     imageUrl: urlImage,
          //     height: 100,
          //     width: 100),
          SizedBox(width: 20),
          Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.name,
                  style: TextStyle(
                    fontSize: 21,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      formatCurrency.printPrice(item.totalPrice),
                      style:
                          TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      ' (${formatCurrency.printPrice(item.price)})',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                Text(
                  'Count: ${item.count}',
                  style: TextStyle(fontSize: 16),
                ),
              ])
        ]));
  }
}
