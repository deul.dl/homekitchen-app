import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/customer/my_orders/my_orders.dart';
import 'package:home_kitchen/order_status_repository/order_status_repository.dart';

import 'item_list.dart';

class ItemListBloc extends Bloc<ItemListEvent, ItemListState> {
  ItemListBloc(
      {@required this.ordersRepository, @required this.orderStatusRepository})
      : super(ItemListInProgress());
  final MyOrderRepository ordersRepository;
  final OrderStatusRepositoryBase orderStatusRepository;

  @override
  Stream<ItemListState> mapEventToState(
    ItemListEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }
}
