import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/order_status_repository/order_status_repository.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'item_list_screen.dart';
import 'item_list.dart';

class ItemListPage extends StatefulWidget {
  ItemListPage({Key key, this.orderUUID}) : super(key: key);
  final String orderUUID;
  @override
  _ItemListState createState() => _ItemListState();
}

class _ItemListState extends State<ItemListPage> {
  ItemListBloc _itemsBloc;

  @override
  Widget build(BuildContext context) {
    _itemsBloc = ItemListBloc(
        orderStatusRepository:
            RepositoryProvider.of<OrderStatusRepository>(context),
        ordersRepository: RepositoryProvider.of<MyOrderRepository>(context))
      ..add(LoadItemList(widget.orderUUID));

    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
          title: Text(
            FlutterI18n.translate(context, 'order.Items'),
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: BlocBuilder<ItemListBloc, ItemListState>(
            bloc: _itemsBloc,
            builder: (context, state) {
              if (state is ItemListLoadSuccess) {
                return ItemListScreen(
                  items: state.items,
                );
              } else if (state is ItemListFailed) {
                return ErrorScreen(
                    errorMessage: state.errorMessage, onReload: _load);
              } else {
                return Container(child: CircularProgressIndicator());
              }
            }));
  }

  void _load() {
    _itemsBloc.add(LoadItemList(widget.orderUUID));
  }
}
