import 'dart:async';
import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:meta/meta.dart';

import 'order_details.dart';

@immutable
abstract class OrderDetailsEvent {
  Stream<OrderDetailsState> applyAsync(
      {OrderDetailsState currentState, OrderDetailsBloc bloc});
}

class LoadOrderDetails extends OrderDetailsEvent {
  LoadOrderDetails(this.orderUUID);
  final String orderUUID;

  @override
  String toString() => 'LoadOrderDetails { orderUUID: $orderUUID}';

  @override
  Stream<OrderDetailsState> applyAsync(
      {OrderDetailsState currentState, OrderDetailsBloc bloc}) async* {
    try {
      final statuses =
          await bloc.orderStatusRepository.getOrderStatuses(orderUUID);

      final orderPayment =
          await bloc.ordersRepository.getOrderPayment(orderUUID);

      yield OrderDetailsLoadSuccess(
          statuses: (statuses as List<OrderStatus>),
          orderPayment: (orderPayment as OrderPaymentMethod));
    } catch (_) {
      yield OrderDetailsFailed(_?.toString());
    }
  }
}

class CompleteOrder extends OrderDetailsEvent {
  CompleteOrder({@required this.orderId});
  final String orderId;

  @override
  String toString() => 'CompleteOrder { orderId: $orderId}';

  @override
  Stream<OrderDetailsState> applyAsync(
      {OrderDetailsState currentState, OrderDetailsBloc bloc}) async* {
    try {
      await bloc.orderStatusRepository.completeOrder(orderId);
    } catch (_) {
      yield OrderDetailsFailed(_?.toString());
    }
  }
}
