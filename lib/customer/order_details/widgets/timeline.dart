import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:timeline_tile/timeline_tile.dart';

import 'package:home_kitchen/customer/main/home/home.dart';

import 'package:home_kitchen/customer/order_details/order_details.dart';

class Timeline extends StatelessWidget {
  Timeline({Key key, this.state}) : super(key: key);
  final OrderDetailsLoadSuccess state;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TimelineTile(
          alignment: TimelineAlign.manual,
          lineX: 0.1,
          isFirst: true,
          indicatorStyle: IndicatorStyle(
            width: 20,
            color: state.hasStatus(RECEIVED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
            padding: EdgeInsets.all(6),
          ),
          rightChild: _RightChild(
            title:
                FlutterI18n.translate(context, 'order.Timeline.Received.Title'),
            message: FlutterI18n.translate(
                context, 'order.Timeline.Received.Message'),
          ),
          topLineStyle: LineStyle(
            color: state.hasStatus(RECEIVED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
          ),
        ),
        TimelineTile(
          alignment: TimelineAlign.manual,
          lineX: 0.1,
          indicatorStyle: IndicatorStyle(
            width: 20,
            color: state.hasStatus(ACCEPTED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
            padding: EdgeInsets.all(6),
          ),
          rightChild: _RightChild(
            title:
                FlutterI18n.translate(context, 'order.Timeline.Accepted.Title'),
            message: FlutterI18n.translate(
                context, 'order.Timeline.Accepted.Message'),
          ),
          topLineStyle: LineStyle(
            color: state.hasStatus(ACCEPTED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
          ),
        ),
        TimelineTile(
          alignment: TimelineAlign.manual,
          lineX: 0.1,
          indicatorStyle: IndicatorStyle(
            width: 20,
            color: state.hasStatus(PROCEESSED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
            padding: EdgeInsets.all(6),
          ),
          rightChild: _RightChild(
            title: FlutterI18n.translate(
                context, 'order.Timeline.InProgress.Title'),
            message: FlutterI18n.translate(
                context, 'order.Timeline.InProgress.Message'),
          ),
          topLineStyle: LineStyle(
            color: state.hasStatus(PROCEESSED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
          ),
        ),
        TimelineTile(
          alignment: TimelineAlign.manual,
          lineX: 0.1,
          indicatorStyle: IndicatorStyle(
            width: 20,
            color:
                state.hasStatus(DONE_STATUS) ? Color(0xFF27AA69) : Colors.grey,
            padding: EdgeInsets.all(6),
          ),
          rightChild: _RightChild(
            title: FlutterI18n.translate(context, 'order.Timeline.Done.Title'),
            message:
                FlutterI18n.translate(context, 'order.Timeline.Done.Message'),
          ),
          topLineStyle: LineStyle(
            color:
                state.hasStatus(DONE_STATUS) ? Color(0xFF27AA69) : Colors.grey,
          ),
        ),
        TimelineTile(
          alignment: TimelineAlign.manual,
          lineX: 0.1,
          isLast: true,
          indicatorStyle: IndicatorStyle(
            width: 20,
            color: state.hasStatus(COMPLETED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
            padding: EdgeInsets.all(6),
          ),
          rightChild: _RightChild(
            disabled: true,
            title: FlutterI18n.translate(
                context, 'order.Timeline.Delivered.Title'),
            message: FlutterI18n.translate(
                context, 'order.Timeline.Delivered.Message'),
          ),
          topLineStyle: LineStyle(
            color: state.hasStatus(COMPLETED_STATUS)
                ? Color(0xFF27AA69)
                : Colors.grey,
          ),
        ),
      ],
    );
  }
}

class _RightChild extends StatelessWidget {
  const _RightChild({
    Key key,
    this.title,
    this.message,
    this.disabled = false,
  }) : super(key: key);
  final String title;
  final String message;
  final bool disabled;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  // color: disabled
                  //     ? const Color(0xFFBABABA)
                  //     : const Color(0xFF636564),
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 6),
              Text(
                message,
                style: TextStyle(
                  // color: disabled
                  //     ? const Color(0xFFD5D5D5)
                  //     : const Color(0xFF636564),
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
