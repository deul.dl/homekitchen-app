import 'package:flutter/material.dart';

import 'package:home_kitchen/customer/order_details/order_details.dart';

class AddressView extends StatelessWidget {
  final OrderDetails order;

  AddressView({Key key, @required this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(
            color: Theme.of(context).backgroundColor,
            width: 1.0,
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
      alignment: Alignment.topLeft,
      child: Row(
        children: <Widget>[
          Icon(
            Icons.room,
            color: Colors.black87,
          ),
          Text(order.address.printOnlyAddress,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
        ],
      ),
    );
  }
}
