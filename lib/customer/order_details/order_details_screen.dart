import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:home_kitchen/widgets/price_list_item.dart';

import 'widgets/timeline.dart';
import 'widgets/address_view.dart';
import 'item_list/item_list.dart';
import 'order_details.dart';

class OrderDetailsScreen extends StatefulWidget {
  OrderDetailsScreen({Key key, this.order}) : super(key: key);

  final OrderDetails order;

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetailsScreen> {
  OrderDetails get order => widget.order;

  @override
  void initState() {
    _load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderDetailsBloc, OrderDetailsState>(
        builder: (context, state) {
      if (state is OrderDetailsLoadSuccess) {
        return _viewWrapper(context, state);
      } else if (state is OrderDetailsFailed) {
        return ErrorScreen(
          errorMessage: state.errorMessage,
          onReload: _load,
        );
      } else {
        return Container(
          alignment: Alignment.center,
          child: CircularProgressIndicator(),
        );
      }
    });
  }

  Widget _viewWrapper(BuildContext context, OrderDetailsLoadSuccess state) {
    return Container(
        color: Theme.of(context).backgroundColor,
        height: double.infinity,
        child: SingleChildScrollView(
            child: Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Timeline(state: state),
                    if (order.address != null) AddressView(order: order),
                    _listItem(order: order),
                    PriceListItem(
                        label: Text(
                            FlutterI18n.translate(
                                context, 'order.OrderTotal.Label'),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16)),
                        amount: state.orderPayment.amount),
                    if (state.orderPayment.deliveryPrice != 0)
                      PriceListItem(
                          label: Text(
                              FlutterI18n.translate(
                                  context, 'order.DeliveryPrice.Label'),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16)),
                          amount: state.orderPayment.deliveryPrice),
                    PriceListItem(
                        label: Text(
                            FlutterI18n.translate(
                                context, 'order.TotalPrice.Label'),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16)),
                        amount: state.orderPayment.totalPrice),
                    SizedBox(height: 160)
                  ],
                ))));
  }

  Widget _listItem({OrderDetails order}) {
    return Container(
        decoration: BoxDecoration(
          border: Border(
            top: BorderSide(
              color: Theme.of(context).backgroundColor,
              width: 1.0,
            ),
            bottom: BorderSide(
              color: Theme.of(context).backgroundColor,
              width: 1.0,
            ),
          ),
        ),
        alignment: Alignment.topLeft,
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'order.Items'),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
          trailing: Icon(
            Icons.chevron_right,
          ),
          onTap: () => {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) =>
                    ItemListPage(orderUUID: order.uuid)))
          },
        ));
  }

  void _load() {
    BlocProvider.of<OrderDetailsBloc>(context)
        .add(LoadOrderDetails(widget.order.uuid));
  }
}
