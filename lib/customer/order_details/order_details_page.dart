import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/cupertino.dart';

import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';

import 'order_details_screen.dart';
import 'order_details.dart';

class OrderDetailsPage extends StatelessWidget {
  OrderDetailsPage({Key key}) : super(key: key);

  static const String routeName = '/order-details';

  @override
  Widget build(BuildContext context) {
    final screenArguments =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final String orderUUID = screenArguments['orderUUID'];

    return BlocBuilder<MyOrdersBloc, MyOrdersState>(builder: (context, state) {
      final order = (state as MyOrdersLoadSuccess).orders.firstWhere(
          (element) => element.uuid == orderUUID,
          orElse: () => null);

      return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.white,
          title: Text(
            FlutterI18n.translate(context, 'order.Order'),
            style: TextStyle(color: Colors.black),
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.store),
                onPressed: () => Navigator.of(context).pushNamed('/store',
                    arguments: {'storeUUID': order.storeUUID}))
          ],
        ),
        body: OrderDetailsScreen(order: order),
        bottomSheet: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
            ),
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
                height: 100,
                child: Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: _bottomButton(context, order),
                ))),
      );
    });
  }

  static Widget _bottomButton(BuildContext context, OrderDetails order) {
    if (order.status == TRANSACTION_STATUS) {
      return SizedBox(
        height: 48,
        width: double.infinity,
        child: TextButton.icon(
            style: TextButton.styleFrom(backgroundColor: Colors.greenAccent),
            onPressed: () => BlocProvider.of<OrderDetailsBloc>(context)
                .add(CompleteOrder(orderId: order.uuid)),
            icon: Icon(
              Icons.done,
              color: Colors.white,
            ),
            label: Text(
              FlutterI18n.translate(context, 'order.Complete'),
              style: Theme.of(context).textTheme.button,
            )),
      );
    } else if (order.status == REFUSED_STATUS) {
      return SizedBox(
          height: 48,
          width: double.infinity,
          child: Container(
            alignment: Alignment.center,
            child: Text(
              FlutterI18n.translate(context, 'order.Reject'),
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ));
    } else if (order.status == ACCEPTED_STATUS ||
        order.status == RECEIVED_STATUS) {
      return SizedBox(
          height: 48,
          width: double.infinity,
          child: Container(
            alignment: Alignment.center,
            child: Text(
              FlutterI18n.translate(context, 'order.InWait'),
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ));
    } else {
      return SizedBox(
          height: 48,
          width: double.infinity,
          child: Container(
            alignment: Alignment.center,
            child: Text(
              FlutterI18n.translate(context, 'order.Finish'),
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ));
    }
  }
}
