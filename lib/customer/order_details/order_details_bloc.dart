import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/order_status_repository/order_status_repository.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';

import 'order_details.dart';

class OrderDetailsBloc extends Bloc<OrderDetailsEvent, OrderDetailsState> {
  OrderDetailsBloc(
      {@required this.ordersRepository, @required this.orderStatusRepository})
      : super(OrderDetailsUninitialized());

  final MyOrderRepository ordersRepository;
  final OrderStatusRepositoryBase orderStatusRepository;

  @override
  Stream<OrderDetailsState> mapEventToState(
    OrderDetailsEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }
}
