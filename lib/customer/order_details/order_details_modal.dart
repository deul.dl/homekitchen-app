import 'package:flutter/cupertino.dart';
import 'package:home_kitchen/customer/models/models.dart';
import 'package:home_kitchen/mixins/date.dart';

@immutable
class OrderDetails extends Order {
  OrderDetails(
      {uuid,
      number,
      description,
      address,
      type,
      status,
      totalPrice,
      createdAt,
      executorUUID,
      customerUUID,
      storeUUID,
      this.executor})
      : super(
            uuid: uuid,
            number: number,
            description: description,
            address: address,
            type: type,
            status: status,
            totalPrice: totalPrice,
            createdAt: createdAt,
            executorUUID: executorUUID,
            customerUUID: customerUUID,
            storeUUID: storeUUID);

  final Executor executor;

  List<Object> get props => [
        uuid,
        storeUUID,
        number,
        description,
        address,
        status,
        totalPrice,
        createdAt,
        executorUUID,
        customerUUID,
        executor,
      ];

  factory OrderDetails.fromJson(Map<String, dynamic> json) {
    return OrderDetails(
      uuid: json['uuid'] as String,
      storeUUID: json['storeUUID'] as String,
      executorUUID: json['executorUUID'] as String,
      customerUUID: json['customerUUID'] as String,
      status: json['status'] as String,
      description: json['description'] as String,
      totalPrice: json['totalPrice'] as num,
      address:
          json['address'] == null ? null : Address.fromJson(json['address']),
      createdAt: Date.formDartTime(
          DateTime.parse(json['time']).add(DateTime.now().timeZoneOffset)),
    );
  }

  OrderDetails updateStatus(String status) {
    return OrderDetails(
      uuid: uuid,
      storeUUID: storeUUID,
      executorUUID: executorUUID,
      customerUUID: customerUUID,
      address: address,
      status: status,
      description: description,
      totalPrice: totalPrice,
      createdAt: createdAt,
    );
  }
}
