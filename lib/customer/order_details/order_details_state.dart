import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/customer/models/models.dart';

abstract class OrderDetailsState extends Equatable {
  final List<Object> propss;
  OrderDetailsState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

@immutable
class OrderDetailsUninitialized extends OrderDetailsState {
  @override
  String toString() => 'OrderDetailsUninitialized {}';
}

@immutable
class OrderDetailsLoadSuccess extends OrderDetailsState {
  OrderDetailsLoadSuccess({
    @required this.statuses,
    @required this.orderPayment,
  }) : super([statuses, orderPayment]);
  final List<OrderStatus> statuses;
  final OrderPaymentMethod orderPayment;

  bool hasStatus(String status) =>
      statuses.any((element) => element.status == status);

  @override
  String toString() => '''
    OrderDetailsLoadSuccess {
      statuses: $statuses,
      orderPayment: $orderPayment,
    }''';
}

@immutable
class OrderDetailsFailed extends OrderDetailsState {
  final String errorMessage;

  OrderDetailsFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'OrderDetailsFailed {}';
}
