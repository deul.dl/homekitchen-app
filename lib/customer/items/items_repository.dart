import 'dart:async';
import 'dart:core';

import 'package:home_kitchen/models/models.dart';

import 'package:home_kitchen/client/client.dart';

class ItemsRepository {
  ItemsRepository(client) : this._client = client;

  final WebClient _client;

  Future<dynamic> getItemsByParam(String field, value) async {
    return _client
        .get('items?$field=$value')
        .then((res) => res['data']['items'])
        .then((items) {
      return items == null
          ? null
          : items.map<Item>((item) => Item.fromJson(item)).toList();
    });
  }
}
