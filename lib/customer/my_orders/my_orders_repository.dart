import 'dart:async';

import 'package:home_kitchen/customer/models/models.dart';
import 'package:home_kitchen/customer/order_details/order_details.dart';

import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/models/order_payment_method.dart';

abstract class OrderRepositoryBase {
  Future<dynamic> getOrders();

  Future<dynamic> getOrderItems(String orderUUID);

  Future<dynamic> createOrder(Map<String, dynamic> data);

  Future<dynamic> getOrderPayment(String orderUUID);
}

class MyOrderRepository extends OrderRepositoryBase {
  MyOrderRepository(client) : this._client = client;

  final WebClient _client;

  @override
  Future<dynamic> getOrders() async {
    return _client.get('customer/orders').then((res) {
      return res['data']['orders'];
    }).then((orders) => orders == null
        ? null
        : (orders as List)
            .map<OrderDetails>((order) => OrderDetails.fromJson(order))
            .toList());
  }

  @override
  Future<dynamic> getOrderItems(String ordeUUID) async {
    return _client
        .get('customer/orders/$ordeUUID/items')
        .then((res) => res['data']['orderItems'])
        .then((orderItems) => orderItems == null
            ? null
            : (orderItems as List)
                .map((orderUUID) => OrderItem.fromJson(orderUUID))
                .toList());
  }

  @override
  Future<dynamic> getOrderPayment(String ordeUUID) async {
    return _client
        .get('customer/orders/$ordeUUID/payment')
        .then((res) => res['data']['orderPaymentMethod'])
        .then((orderPaymentMethod) => orderPaymentMethod == null
            ? null
            : OrderPaymentMethod.fromJson(orderPaymentMethod));
  }

  @override
  Future<dynamic> createOrder(Map<String, dynamic> data) async {
    return _client
        .post('customer/orders/create', data)
        .then((res) => res['data']);
  }
}
