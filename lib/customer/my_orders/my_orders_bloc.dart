import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:home_kitchen/order_status_repository/order_status_repository.dart';
import 'package:meta/meta.dart';

import 'my_orders.dart';

class MyOrdersBloc extends Bloc<MyOrdersEvent, MyOrdersState> {
  MyOrdersBloc(
      {@required this.ordersRepository, @required this.orderStatusRepository})
      : super(MyOrdersUninitialized()) {
    _orderStatuses = orderStatusRepository.statuses.listen(
      (OrderStatus orderStatus) => add(OrdersStatusChanged(orderStatus)),
    );
  }
  final MyOrderRepository ordersRepository;
  final OrderStatusRepositoryBase orderStatusRepository;

  StreamSubscription<OrderStatus> _orderStatuses;

  @override
  Stream<MyOrdersState> mapEventToState(
    MyOrdersEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    _orderStatuses.cancel();
    await super.close();
  }
}
