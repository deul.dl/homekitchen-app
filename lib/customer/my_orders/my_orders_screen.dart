import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/widgets/error_sreen.dart';
import 'package:home_kitchen/widgets/order_list/order_list.dart';

import 'my_orders.dart';

class MyOrdersScreen extends StatefulWidget {
  MyOrdersScreen({Key key}) : super(key: key);

  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrdersScreen> {
  @override
  void initState() {
    super.initState();
    this._load();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: BlocBuilder<MyOrdersBloc, MyOrdersState>(
            builder: (BuildContext context, MyOrdersState state) {
          if (state is MyOrdersLoadSuccess) {
            return _simpleOrders(state.orders);
          } else if (state is MyOrdersFailed) {
            return ErrorScreen(
                errorMessage: state.errorMessage, onReload: _load);
          } else {
            return Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            );
          }
        }));
  }

  Widget _simpleOrders(List<Order> orders) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: OrderList(orders: orders),
    );
  }

  void _load() {
    BlocProvider.of<MyOrdersBloc>(context).add(LoadMyOrders());
  }
}
