import 'package:equatable/equatable.dart';

import 'package:home_kitchen/customer/order_details/order_details.dart';

abstract class MyOrdersState extends Equatable {
  final List<Object> propss;
  MyOrdersState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class MyOrdersUninitialized extends MyOrdersState {
  @override
  String toString() => 'MyOrdersUninitialized {}';
}

class MyOrdersLoadSuccess extends MyOrdersState {
  final List<OrderDetails> orders;

  MyOrdersLoadSuccess([this.orders = const []]) : super([orders]);

  @override
  String toString() => 'MyOrdersLoadSuccess {}';
}

class MyOrdersFailed extends MyOrdersState {
  final String errorMessage;

  MyOrdersFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'MyOrdersFailed {}';
}
