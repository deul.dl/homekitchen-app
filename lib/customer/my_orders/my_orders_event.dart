import 'dart:async';

import 'package:meta/meta.dart';

import 'package:home_kitchen/accounts/accounts.dart';
import 'package:home_kitchen/customer/main/home/home.dart';
import 'package:home_kitchen/customer/models/models.dart';

import 'my_orders.dart';

@immutable
abstract class MyOrdersEvent {
  Stream<MyOrdersState> applyAsync(
      {MyOrdersState currentState, MyOrdersBloc bloc});
}

class LoadMyOrders extends MyOrdersEvent {
  @override
  String toString() => 'LoadMyOrders {}';

  @override
  Stream<MyOrdersState> applyAsync(
      {MyOrdersState currentState, MyOrdersBloc bloc}) async* {
    try {
      final orders = await bloc.ordersRepository.getOrders();
      yield MyOrdersLoadSuccess((orders as List<OrderDetails>) ?? []);
    } catch (_) {
      yield MyOrdersFailed(_?.toString());
    }
  }
}

class OrdersStatusChanged extends MyOrdersEvent {
  final OrderStatus orderStatus;

  @override
  String toString() => 'OrdersStatusChanged { orderStatus: $orderStatus }';

  OrdersStatusChanged(this.orderStatus);

  @override
  Stream<MyOrdersState> applyAsync(
      {MyOrdersState currentState, MyOrdersBloc bloc}) async* {
    try {
      final orders = (currentState as MyOrdersLoadSuccess)
          .orders
          .map((OrderDetails e) => e.uuid == orderStatus.orderUUID
              ? e.updateStatus(orderStatus.status)
              : e)
          .toList();

      yield MyOrdersLoadSuccess(orders);
    } catch (_) {
      yield MyOrdersFailed(_?.toString());
    }
  }
}
