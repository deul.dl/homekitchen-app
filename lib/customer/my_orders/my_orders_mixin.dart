import 'package:collection/collection.dart';

import 'my_orders.dart';

class Orders {
  final List<Order> orders;

  Orders(this.orders);

  Future<Map> groupByDate() async {
    return groupBy(orders, (order) => order.createdAt.timeIndicators());
  }

  get() => orders;
}
