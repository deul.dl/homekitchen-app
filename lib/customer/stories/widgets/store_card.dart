import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:home_kitchen/customer/stories/stories.dart';

class StoreCard extends StatelessWidget {
  StoreCard({Key key, this.store, this.onTap}) : super(key: key);

  final Store store;
  final Function onTap;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (store.imageSource.isNotEmpty
          ? store.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    Widget card = Card(
        elevation: 0,
        margin: EdgeInsets.only(right: 10.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0),
              child: CachedNetworkImage(
                imageUrl: urlImage,
                height: 100,
                width: 100,
                placeholder: (context, url) =>
                    Image.asset('assets/images/placeholder.jpg'),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            SizedBox(width: 20),
            Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    store.name,
                    style: TextStyle(
                      fontSize: 21,
                    ),
                  ),
                  Text(
                    '${store.startTime.format(context)} - ${store.endTime.format(context)}',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                      width: MediaQuery.of(context).size.width - 135,
                      child: Text(
                        store.description,
                        style: TextStyle(color: Colors.black54),
                        maxLines: 8,
                        overflow: TextOverflow.ellipsis,
                      ))
                ])
          ],
        ));

    return GestureDetector(onTap: onTap, child: card);
  }
}
