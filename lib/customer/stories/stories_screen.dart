import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_kitchen/customer/categories/category.dart';

import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'widgets/store_card.dart';

class StoriesScreen extends StatefulWidget {
  StoriesScreen({Key key, this.category}) : super(key: key);

  final Category category;

  @override
  _StoriesState createState() => _StoriesState();
}

class _StoriesState extends State<StoriesScreen> {
  _StoriesState();

  @override
  void initState() {
    super.initState();
    this._load();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StoriesBloc, StoriesState>(builder: (
      BuildContext context,
      StoriesState currentState,
    ) {
      if (currentState is StoriesLoad) {
        return Container(
            child: ListView.separated(
                itemBuilder: (BuildContext context, int index) {
                  return StoreCard(
                      store: currentState.stories[index],
                      onTap: () {
                        Navigator.of(context).pushNamed('/store', arguments: {
                          'storeUUID': currentState.stories[index].uuid
                        });
                      });
                },
                separatorBuilder: (BuildContext context, int index) =>
                    Divider(),
                itemCount: currentState.stories.length));
      } else if (currentState is StoriesFailed) {
        return ErrorScreen(
            errorMessage: currentState.errorMessage, onReload: _load);
      } else {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    });
  }

  void _load() {
    BlocProvider.of<StoriesBloc>(context)
        .add(LoadStories(categoryId: widget.category.id));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
