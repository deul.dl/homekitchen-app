import 'dart:async';
import 'dart:core';

import 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/client/client.dart';
import 'package:home_kitchen/models/store_delivery.dart';

class StoriesRepository {
  StoriesRepository(client) : this._client = client;

  final WebClient _client;

  Future<dynamic> getStoriesByParams(int categoryId, String city) {
    return _client
        .get('stories/category/$categoryId?city=$city')
        .then((res) => res['data']['stories'])
        .then((stories) => stories == null
            ? null
            : (stories as List).map((e) => Store.fromJson(e)).toList());
  }

  Future<dynamic> getStoreByUUID(String storeUUID) {
    return _client
        .get('stories/$storeUUID')
        .then((res) => Store.fromJson(res['data']['store']));
  }

  Future<dynamic> getStoreByDelivery(String storeUUID) {
    return _client
        .get('stories/$storeUUID/delivery')
        .then((res) => StoreDelivery.fromJson(res['data']));
  }
}
