import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/location/location.dart';

import 'stories.dart';

class StoriesBloc extends Bloc<StoriesEvent, StoriesState> {
  StoriesRepository storiesRepository;
  final LocationBloc locationBloc;

  StoriesBloc({this.storiesRepository, this.locationBloc})
      : super(StoriesInProgress());

  @override
  Stream<StoriesState> mapEventToState(
    StoriesEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
