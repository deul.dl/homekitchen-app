import 'dart:async';

import 'package:home_kitchen/location/location.dart';
import 'package:meta/meta.dart';

import 'stories.dart';

@immutable
abstract class StoriesEvent {
  Stream<StoriesState> applyAsync(
      {StoriesState currentState, StoriesBloc bloc});
}

class LoadStories extends StoriesEvent {
  LoadStories({this.categoryId});

  final int categoryId;

  @override
  String toString() => 'LoadStories { categoryId: $categoryId }';

  @override
  Stream<StoriesState> applyAsync(
      {StoriesState currentState, StoriesBloc bloc}) async* {
    try {
      final state = bloc.locationBloc.state;
      yield StoriesInProgress();
      if (state is LocationSuccess) {
        final stories = await bloc.storiesRepository
            .getStoriesByParams(categoryId, state.locality);
        yield StoriesLoad((stories as List<Store>) ?? []);
      }
      if (state is LocationFailed) {
        yield StoriesFailed('Error depends on location!');
      }
    } catch (_) {
      yield StoriesFailed(_?.toString());
    }
  }
}
