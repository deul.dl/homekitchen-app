import 'package:equatable/equatable.dart';

import 'package:home_kitchen/customer/stories/stories.dart';

abstract class StoriesState extends Equatable {
  final List<Object> propss;
  StoriesState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class StoriesInProgress extends StoriesState {
  @override
  String toString() => 'StoriesInProgress';
}

class StoriesLoad extends StoriesState {
  final List<Store> stories;

  StoriesLoad([this.stories = const []]) : super([stories]);

  @override
  String toString() => 'StoriesLoad';
}

class StoriesFailed extends StoriesState {
  final String errorMessage;

  StoriesFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'StoriesError';
}
