import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/customer/widgets/store_app_bar.dart';
import 'package:home_kitchen/customer/stories/stories.dart';

import 'stories_screen.dart';

class StoriesPage extends StatelessWidget {
  StoriesPage({Key key}) : super(key: key);

  static const String routeName = '/stories';

  @override
  Widget build(BuildContext context) {
    final StoriesScreenArguments arguments =
        ModalRoute.of(context).settings.arguments;
    final Category category = arguments.category;
    return Scaffold(
      appBar: StoreAppBar(
        title: Text(FlutterI18n.translate(context, 'stories.Title')),
      ),
      body: Container(child: StoriesScreen(category: category)),
    );
  }
}
