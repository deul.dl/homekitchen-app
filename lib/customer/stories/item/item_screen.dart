import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:home_kitchen/common_settings.dart';
import 'item.dart';

class ItemScreen extends StatefulWidget {
  ItemScreen({Key key}) : super(key: key);

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<ItemScreen> {
  _ItemState();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    const padding = EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0);
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    final ItemScreenArguments screenArguments =
        ModalRoute.of(context).settings.arguments;
    final Item item = screenArguments.item;
    return Container(
        width: double.infinity,
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
            child: CachedNetworkImage(
              imageUrl: DotEnv().env['SERVER_URL'] +
                  ((item.imageSource.isNotEmpty
                      ? item.imageSource
                      : DotEnv().env['PLACEHOLDER_IMAGE'])),
              fit: BoxFit.cover,
              width: double.infinity,
              height: 400,
              placeholder: (context, url) =>
                  Image.asset('assets/images/placeholder.jpg'),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Padding(
            padding: padding,
            child: Row(children: [
              Text(
                formatCurrency.printPrice(item.price),
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )
            ]),
          ),
          Padding(
            padding: padding,
            child: Text(
              item.name,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Padding(
            padding: padding,
            child: Text(item.description),
          )
        ]));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
