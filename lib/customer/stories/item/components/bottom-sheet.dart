import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/customer/stories/item/components/item_counter.dart';
import 'package:home_kitchen/theme/colors.dart';
export 'package:home_kitchen/models/item.dart';

class ItemBottomSheet extends StatefulWidget {
  ItemBottomSheet({Key key, this.item, this.onAddCart, this.onMoveStore})
      : super(key: key);

  final Item item;
  final Function onAddCart;
  final Function onMoveStore;

  @override
  _ItemBottomSheetState createState() => _ItemBottomSheetState();
}

class _ItemBottomSheetState extends State<ItemBottomSheet> {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        height: 60,
        child: Row(children: [
          SizedBox(
            height: 60,
            width: MediaQuery.of(context).size.width * 0.3,
            child: TextButton(
                onPressed: widget.onMoveStore,
                child: Icon(
                  Icons.store,
                  color: Theme.of(context).primaryColor,
                )),
          ),
          SizedBox(
              height: 120,
              width: MediaQuery.of(context).size.width * 0.7,
              child: ActionCartItem(
                item: widget.item,
                onAddCart: widget.onAddCart,
              )),
        ]));
  }
}

class ActionCartItem extends StatelessWidget {
  ActionCartItem({this.item, this.onAddCart});
  final Function onAddCart;
  final Item item;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
      builder: (context, state) {
        final cartItem = (state is CartLoadSuccess)
            ? state.items.singleWhere((element) => element.uuid == item.uuid,
                orElse: () => null)
            : null;

        if (cartItem != null) {
          return ItemCounter(
            count: cartItem.count,
            onMinus: () => BlocProvider.of<CartBloc>(context)
                .add(DecrementCart(item: cartItem)),
            onPlus: () => BlocProvider.of<CartBloc>(context)
                .add(IncrementCart(item: cartItem)),
          );
        } else {
          return TextButton(
            onPressed: onAddCart,
            style: TextButton.styleFrom(
              backgroundColor: HomeKitchenColors.blackColor[200],
              shape: BeveledRectangleBorder(),
            ),
            child: Text(FlutterI18n.translate(context, 'items.AddToCart'),
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold)),
          );
        }
      },
    );
  }
}
