import 'package:flutter/material.dart';

import 'package:home_kitchen/theme/colors.dart';

class ItemCounter extends StatelessWidget {
  ItemCounter({this.count, this.onPlus, this.onMinus});
  final int count;
  final Function onPlus;
  final Function onMinus;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            color: HomeKitchenColors.blackColor[200],
            child: IconButton(icon: Icon(Icons.remove), onPressed: onMinus),
          ),
          Text(
            count.toString(),
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 16),
          ),
          Container(
            color: HomeKitchenColors.blackColor[200],
            child: IconButton(icon: Icon(Icons.add), onPressed: onPlus),
          ),
        ],
      ),
    );
  }
}
