import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/customer/cart/widgets/cart_actions.dart';
import 'package:home_kitchen/customer/widgets/basket.dart';

import 'components/bottom-sheet.dart';
import 'item_screen.dart';
import 'item.dart';

class ItemPage extends StatelessWidget {
  ItemPage();

  static const String routeName = '/item';

  @override
  Widget build(BuildContext context) {
    return CartActions(
        cartBloc: BlocProvider.of<CartBloc>(context),
        ctx: context,
        child: _ItemWidget());
  }
}

class _ItemWidget extends StatefulWidget {
  _ItemWidget();

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<_ItemWidget> {
  ItemScreenArguments _args;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _args = ModalRoute.of(context).settings.arguments;
    final item = _args.item;

    return BlocBuilder<CartBloc, CartState>(
        builder: (BuildContext context, CartState state) {
      return Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
              backgroundColor: Colors.transparent,
              title: Text(item.name),
              actions: [
                Basket(
                  count: (state is CartLoadSuccess) ? state.count : 0,
                  onTap: () => Navigator.of(context).pushNamed('/cart'),
                )
              ]),
          body: ItemScreen(),
          bottomSheet: ItemBottomSheet(
              item: item,
              onMoveStore: () => _args.from == 'store'
                  ? Navigator.of(context)
                      .popUntil(ModalRoute.withName('/store'))
                  : Navigator.of(context).pushReplacementNamed('/store',
                      arguments: {'storeUUID': item.storeUUID}),
              onAddCart: () => CartActions.of(context).addItemToCart(item)));
    });
  }
}
