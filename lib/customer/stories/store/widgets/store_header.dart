import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:home_kitchen/models/models.dart';

class StoreHeader extends StatelessWidget {
  StoreHeader({Key key, @required this.store, @required this.tap})
      : super(key: key);

  final Store store;
  final Widget tap;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (store.imageSource.isNotEmpty
          ? store.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 72),
      color: Colors.black,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: [
                    Container(
                        width: 70,
                        height: 70,
                        child: CachedNetworkImage(
                          imageUrl: urlImage,
                          placeholder: (context, url) =>
                              Image.asset('assets/images/placeholder.jpg'),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    Text(store.name,
                        style: TextStyle(color: Colors.white, fontSize: 16)),
                  ],
                ),
              ],
            ),
            Padding(
                padding: EdgeInsets.only(left: 90),
                child: Text(
                    '${store.startTime.format(context)} - ${store.endTime.format(context)}',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.bold))),
            tap
          ]),
    );
  }
}
