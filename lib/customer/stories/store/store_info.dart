import 'package:flutter/material.dart';

import 'package:home_kitchen/customer/stories/stories.dart';

class StoreInfo extends StatelessWidget {
  StoreInfo({Key key, this.store}) : super(key: key);

  final Store store;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (store.description.isNotEmpty)
            Text(store.description, style: TextStyle(fontSize: 16)),
        ],
      ),
    );
  }
}
