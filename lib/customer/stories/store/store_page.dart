import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/customer/categories/category.dart';
import 'package:home_kitchen/customer/stories/store/store_info.dart';
import 'package:home_kitchen/customer/widgets/store_app_bar.dart';
import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/customer/cart/widgets/cart_actions.dart';
import 'package:home_kitchen/widgets/error_sreen.dart';

import 'widgets/store_header.dart';
import 'all_items_tab.dart';

import 'store.dart';

class StorePage extends StatefulWidget {
  StorePage({Key key, this.store}) : super(key: key);
  final Store store;

  static const String routeName = '/store';

  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<StorePage> with SingleTickerProviderStateMixin {
  static TabController _tabController;

  static const List<Tab> storeTabs = <Tab>[
    Tab(text: 'Home'),
    Tab(text: 'About us'),
  ];

  @override
  void initState() {
    _tabController = TabController(length: storeTabs.length, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _load();
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: StoreAppBar(
          backgroundColor: Colors.transparent,
        ),
        body: SafeArea(
            top: false,
            child: BlocBuilder<StoreBloc, StoreState>(
                builder: (BuildContext context, StoreState state) {
              if (state is StoreLoad) {
                final store = state.store;
                return Container(
                    color: Theme.of(context).backgroundColor,
                    child: Column(children: [
                      StoreHeader(
                          store: store,
                          tap: Align(
                            alignment: Alignment.centerLeft,
                            child: TabBar(
                                indicatorSize: TabBarIndicatorSize.tab,
                                isScrollable: true,
                                labelColor: Colors.white,
                                unselectedLabelColor: Colors.white,
                                indicatorColor: Colors.white,
                                controller: _tabController,
                                tabs: storeTabs),
                          )),
                      Expanded(
                          child:
                              TabBarView(controller: _tabController, children: [
                        CartActions(
                            cartBloc: BlocProvider.of<CartBloc>(context),
                            ctx: context,
                            child: AllItemsTab(
                              store: store,
                              items: state.items,
                            )),
                        StoreInfo(
                          store: store,
                        )
                      ])),
                    ]));
              } else if (state is StoreError) {
                return ErrorScreen(
                    errorMessage: state.errorMessage, onReload: _load);
              } else {
                return Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                );
              }
            })));
  }

  void _load() {
    final _args =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    BlocProvider.of<StoreBloc>(context).add(LoadStore(_args['storeUUID']));
    BlocProvider.of<CategoryBloc>(context).add(LoadCategories());
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }
}
