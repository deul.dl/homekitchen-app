import 'dart:async';
import 'dart:developer' as developer;
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/customer/items/items_repository.dart';

import 'store.dart';

class StoreBloc extends Bloc<StoreEvent, StoreState> {
  StoreBloc({this.storiesRepository, this.itemsRepository})
      : super(StoreInProgressing());
  final StoriesRepository storiesRepository;
  final ItemsRepository itemsRepository;

  @override
  Stream<StoreState> mapEventToState(
    StoreEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_', name: 'StoreBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() {
    return super.close();
  }
}
