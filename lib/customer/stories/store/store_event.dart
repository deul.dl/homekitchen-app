import 'dart:async';
import 'dart:developer' as developer;

import 'package:home_kitchen/executor/my_store/create_store/create_store.dart';
import 'package:meta/meta.dart';

import 'store.dart';

@immutable
abstract class StoreEvent {
  Stream<StoreState> applyAsync({StoreState currentState, StoreBloc bloc});
}

class UnStore extends StoreEvent {
  @override
  Stream<StoreState> applyAsync(
      {StoreState currentState, StoreBloc bloc}) async* {}
}

class LoadStore extends StoreEvent {
  LoadStore(this.storeUUID);

  final String storeUUID;

  @override
  String toString() => 'LoadStore { storeUUID: $storeUUID }';

  @override
  Stream<StoreState> applyAsync(
      {StoreState currentState, StoreBloc bloc}) async* {
    try {
      yield StoreInProgressing();
      final store = await bloc.storiesRepository.getStoreByUUID(storeUUID);

      final items =
          await bloc.itemsRepository.getItemsByParam('store_uuid', storeUUID);

      yield StoreLoad(items: (items as List<Item>) ?? [], store: store);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'LoadStoreEvent', error: _, stackTrace: stackTrace);
      yield StoreError(_?.toString());
    }
  }
}
