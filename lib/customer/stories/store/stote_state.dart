import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'store.dart';

abstract class StoreState extends Equatable {
  final List<Object> propss;
  StoreState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class StoreInProgressing extends StoreState {
  @override
  String toString() => 'StoreInProgressing {}';
}

class StoreLoad extends StoreState {
  final Store store;
  final List<Item> items;

  StoreLoad({@required this.store, this.items}) : super([store, items]);

  @override
  String toString() => 'StoreLoad { store: $store, items: $items }';
}

class StoreError extends StoreState {
  final String errorMessage;

  StoreError(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'StoreError';
}
