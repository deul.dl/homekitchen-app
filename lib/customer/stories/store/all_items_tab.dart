import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/widgets/item_card.dart';
import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/customer/constants/constants.dart';
import 'package:home_kitchen/customer/categories/category.dart';

class AllItemsTab extends StatelessWidget {
  AllItemsTab({Key key, this.store, this.items}) : super(key: key);

  final Store store;
  final List<Item> items;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - 300) / 2;
    final double itemWidth = size.width / 2;
    final state = BlocProvider.of<CategoryBloc>(context).state;

    if (state is CategoryInProgress) {
      return Center(child: CircularProgressIndicator());
    }

    final categories = (state as CategoryLoadSuccess).categories;

    final List<CategoryWithItems> categoriesWithItems = categories.map((e) {
      final foundItems =
          items.where((element) => element.categoryId == e.id).toList();
      return CategoryWithItems(id: e.id, name: e.name, items: foundItems);
    }).toList();

    return ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: categoriesWithItems.length,
        itemBuilder: (BuildContext context, int index) {
          final CategoryWithItems category = categoriesWithItems[index];

          return Column(children: [
            ListTile(
              title: Text(category.name),
            ),
            Container(
              child: GridView.count(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  childAspectRatio: (itemWidth / itemHeight),
                  crossAxisCount: 2,
                  children: category.items
                      .map((item) => ItemCard(
                            showCartButton: true,
                            margin: EdgeInsets.all(5),
                            width: double.infinity,
                            item: item,
                            onTap: () => Navigator.of(context).pushNamed(
                                '/item',
                                arguments: ItemScreenArguments(
                                    item: item, from: 'store')),
                          ))
                      .toList()),
            )
          ]);
        });
  }
}
