import 'package:meta/meta.dart';

@immutable
abstract class PaymentMethodEvent {}

class ChangePaymentMethod extends PaymentMethodEvent {
  ChangePaymentMethod(this.paymentMethod);

  final String paymentMethod;

  @override
  String toString() => 'ChangePaymentMethod { paymentMethod: $paymentMethod }';
}
