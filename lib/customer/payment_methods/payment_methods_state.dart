import 'package:equatable/equatable.dart';

import 'package:home_kitchen/customer/payment_methods/payment_methods.dart';

class PaymentMethodState extends Equatable {
  PaymentMethodState({this.paymentMethod = IN_CASH}) : super();

  final String paymentMethod;

  @override
  List<Object> get props => [paymentMethod];

  @override
  String toString() =>
      'PaymentMethodLoadSuccess { paymentMethod: $paymentMethod }';
}
