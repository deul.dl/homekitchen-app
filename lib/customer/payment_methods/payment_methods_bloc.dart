import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';

import 'payment_methods.dart';

const IN_CASH = "in_cash";
const IN_CARD = "translate";

class PaymentMethodBloc extends Bloc<PaymentMethodEvent, PaymentMethodState> {
  PaymentMethodBloc() : super(PaymentMethodState());

  @override
  Stream<PaymentMethodState> mapEventToState(
    PaymentMethodEvent event,
  ) async* {
    if (event is ChangePaymentMethod) {
      yield PaymentMethodState(paymentMethod: event.paymentMethod);
    }
  }
}
