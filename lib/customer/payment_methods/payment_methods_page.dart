import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'payment_methods.dart';

class PaymentMethodsPage extends StatelessWidget {
  static const String routeName = "/payment-method";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Payment Methods'),
        ),
        body: Container(child:
            BlocBuilder<PaymentMethodBloc, PaymentMethodState>(
                builder: (BuildContext context, PaymentMethodState state) {
          return ListView(
            children: ListTile.divideTiles(context: context, tiles: [
              ListTile(
                onTap: () => BlocProvider.of<PaymentMethodBloc>(context)
                    .add(ChangePaymentMethod(IN_CASH)),
                title: Text(
                  'IN CASH',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                trailing:
                    state.paymentMethod == IN_CASH ? Icon(Icons.done) : null,
                selected: state.paymentMethod == IN_CASH,
              ),
              ListTile(
                onTap: () => BlocProvider.of<PaymentMethodBloc>(context)
                    .add(ChangePaymentMethod(IN_CARD)),
                title: Text(
                  'IN CARD',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                trailing:
                    state.paymentMethod == IN_CARD ? Icon(Icons.done) : null,
                selected: state.paymentMethod == IN_CARD,
              )
            ]).toList(),
          );
        })));
  }
}
