import 'package:home_kitchen/customer/models/models.dart';

abstract class ExpandedStore extends Store {
  ExpandedStore(
      {String name,
      String phoneNumber,
      String imageSource,
      String description,
      String userUUID})
      : super(
            name: name,
            phoneNumber: phoneNumber,
            imageSource: imageSource,
            description: description,
            userUUID: userUUID);
}
