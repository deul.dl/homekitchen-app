import 'package:home_kitchen/customer/models/models.dart';

abstract class ExpandedCategory extends Category {
  ExpandedCategory({id, name, imageSource, description})
      : super(id: id, name: name, imageSource: imageSource);
}

class CategoryWithItems extends ExpandedCategory {
  CategoryWithItems({id, name, imageSource, description, this.items})
      : super(id: id, name: name, imageSource: imageSource);

  final List<Item> items;
}
