import 'package:equatable/equatable.dart';

class OwnStore extends Equatable {
  final String own_id;
  final String store_name;

  OwnStore({this.own_id, this.store_name});

  @override
  List<Object> get props => [own_id, store_name];
}
