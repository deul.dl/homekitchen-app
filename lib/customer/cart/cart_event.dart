import 'dart:async';

import 'package:meta/meta.dart';

import 'cart.dart';

@immutable
abstract class CartEvent {
  Stream<CartState> applyAsync({CartState currentState, CartBloc bloc});
}

class LoadCart extends CartEvent {
  @override
  String toString() => 'LoadCart {}';

  LoadCart();

  @override
  Stream<CartState> applyAsync(
      {CartState currentState, CartBloc bloc}) async* {}
}

class AddCart extends CartEvent {
  AddCart({this.item, isClean}) : this.isClean = isClean ?? false;

  final CartItem item;
  final isClean;

  @override
  String toString() => 'AddCart { item: $item }';

  @override
  Stream<CartState> applyAsync({CartState currentState, CartBloc bloc}) async* {
    try {
      if (isClean || currentState is CartUninitialized) {
        yield CartLoadSuccess(items: [item]);
      } else if (currentState is CartLoadSuccess) {
        final List<CartItem> items = List.from(currentState.items)
          ..toList()
          ..add(item);
        yield CartLoadSuccess(items: items);
      }
    } catch (_) {
      yield CartError(_?.toString());
    }
  }
}

class IncrementCart extends CartEvent {
  IncrementCart({this.item});

  final CartItem item;

  @override
  String toString() => 'IncrementCart { item: $item } ';

  @override
  Stream<CartState> applyAsync({CartState currentState, CartBloc bloc}) async* {
    try {
      if (currentState is CartLoadSuccess) {
        final items = currentState.items
            .map((e) => e.uuid == item.uuid ? item.increment() : e)
            .toList();
        yield CartLoadSuccess(items: items);
      }
    } catch (_) {
      yield CartError(_?.toString());
    }
  }
}

class DecrementCart extends CartEvent {
  DecrementCart({this.item});

  final CartItem item;

  @override
  String toString() => 'DecrementCart { item: $item }';

  @override
  Stream<CartState> applyAsync({CartState currentState, CartBloc bloc}) async* {
    try {
      if (currentState is CartLoadSuccess && item.count > 1) {
        final items = currentState.items
            .map((e) => e.uuid == item.uuid ? item.descrement() : e)
            .toList();
        yield CartLoadSuccess(items: items);
      } else {
        bloc.add(DeleteCart(item: item));
      }
    } catch (_) {
      yield CartError(_?.toString());
    }
  }
}

class DeleteCart extends CartEvent {
  DeleteCart({this.item});

  final CartItem item;

  @override
  String toString() => 'DeleteCart { item: $item }';

  @override
  Stream<CartState> applyAsync({CartState currentState, CartBloc bloc}) async* {
    try {
      if (currentState is CartLoadSuccess) {
        final List<CartItem> items = List.from(currentState.items)
          ..toList()
          ..remove(item);
        yield CartLoadSuccess(items: items);
      }
    } catch (_) {
      yield CartError(_?.toString());
    }
  }
}

class CleanCart extends CartEvent {
  CleanCart();

  @override
  String toString() => 'CleanCart {}';

  @override
  Stream<CartState> applyAsync({CartState currentState, CartBloc bloc}) async* {
    yield CartUninitialized();
  }
}
