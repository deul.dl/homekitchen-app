import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'cart.dart';

class CartPage extends StatelessWidget {
  static const String routeName = '/cart';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(context, 'cart.Title')),
          backgroundColor: Colors.black,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.delete),
                onPressed: () =>
                    BlocProvider.of<CartBloc>(context).add(CleanCart()))
          ],
        ),
        body: Container(
            color: Theme.of(context).backgroundColor, child: CartScreen()));
  }
}
