import 'package:equatable/equatable.dart';

import 'cart.dart';

abstract class CartState extends Equatable {
  final List<Object> propss;
  CartState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

/// UnInitialized
class CartUninitialized extends CartState {
  CartUninitialized();

  @override
  String toString() => 'CartUninitialized {}';
}

class CartLoadSuccess extends CartState {
  final List<CartItem> items;

  CartLoadSuccess({
    this.items = const [],
  }) : super([items]);

  int get count =>
      items.fold(0, (previousValue, item) => previousValue + item.count);

  double get priceAmount => items.fold(
      0, (previousValue, CartItem item) => previousValue + item.totalPrice);

  @override
  String toString() => 'LoadedCartState { items: $items } ';
}

class CartError extends CartState {
  final String errorMessage;

  CartError(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'CartError {errorMessage: $errorMessage}';
}
