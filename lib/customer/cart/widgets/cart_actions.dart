import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/cart/cart.dart';

class CartActions extends InheritedWidget {
  CartActions({
    Key key,
    @required this.cartBloc,
    @required this.ctx,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  final CartBloc cartBloc;
  final BuildContext ctx;

  static CartActions of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<CartActions>();
  }

  @override
  Widget get child => super.child;

  CartState get state => cartBloc.state;

  Future<void> addItemToCart(Item item) async {
    if (state is CartUninitialized) {
      cartBloc.add(AddCart(
          item: CartItem(
              count: 1,
              uuid: item.uuid,
              name: item.name,
              price: item.price,
              cost: item.cost,
              description: item.description,
              userUUID: item.userUUID,
              storeUUID: item.storeUUID,
              imageSource: item.imageSource)));
      return;
    }

    if (state is CartLoadSuccess) {
      final isAnotherStore = (state as CartLoadSuccess)
          .items
          .any((element) => element.storeUUID != item.storeUUID);

      if (isAnotherStore) {
        return _showMyDialog(item);
      }

      _addItem(item);
    }
  }

  Future<void> _showMyDialog(Item item) async {
    return showDialog<void>(
      context: ctx,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(FlutterI18n.translate(
                    context, 'cart.ConfirmModal.Warning')),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
                child: Text(FlutterI18n.translate(context, 'Cancel')),
                onPressed: () {
                  Navigator.of(ctx).pop();
                }),
            TextButton(
              child: Text(FlutterI18n.translate(context, 'Ok')),
              onPressed: () {
                cartBloc.add(AddCart(
                    isClean: true,
                    item: CartItem(
                        count: 1,
                        uuid: item.uuid,
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        cost: item.cost,
                        userUUID: item.userUUID,
                        storeUUID: item.storeUUID,
                        imageSource: item.imageSource)));
                Navigator.of(ctx).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _addItem(Item item) {
    final CartItem cartItem = (state as CartLoadSuccess)
        .items
        .firstWhere((element) => element.uuid == item.uuid, orElse: () => null);

    if (cartItem == null) {
      cartBloc.add(AddCart(
          item: CartItem(
              count: 1,
              uuid: item.uuid,
              name: item.name,
              price: item.price,
              cost: item.cost,
              description: item.description,
              userUUID: item.userUUID,
              storeUUID: item.storeUUID,
              imageSource: item.imageSource)));
    } else {
      // updated
      cartBloc.add(IncrementCart(
        item: cartItem,
      ));
    }
  }

  @override
  bool updateShouldNotify(CartActions old) => false;
}
