import 'package:flutter/material.dart';

class CheckOutButton extends StatelessWidget {
  CheckOutButton({@required this.onPressed, @required this.label});

  final Widget label;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 48,
        child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
            ),
            child: label,
            onPressed: onPressed));
  }
}
