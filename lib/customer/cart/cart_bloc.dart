import 'dart:async';
import 'package:bloc/bloc.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'cart.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartUninitialized());

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    yield* event.applyAsync(currentState: state, bloc: this);
  }

  @override
  Future<void> close() async {
    return super.close();
  }
}
