import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/auth/auth.dart';
import 'package:home_kitchen/customer/constants/constants.dart';
import 'package:home_kitchen/signin/signin_page.dart';
import 'package:home_kitchen/widgets/empty_screen.dart';

import 'package:home_kitchen/common_settings.dart';

import 'cart.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(
        builder: (BuildContext context, CartState state) {
      if (state is CartLoadSuccess) {
        if (state.items.isEmpty) {
          return EmptyScreen();
        }
        return Column(
          children: <Widget>[
            Expanded(
              child: _listView(context, state),
            ),
            if (state.items.length != 0) _footer(context, state)
          ],
        );
      } else if (state is CartError) {
        return Container(
            child: Text(
          state.errorMessage,
          style: TextStyle(color: Colors.redAccent),
        ));
      } else {
        return EmptyScreen(
            icon: Icon(
              Icons.shopping_basket,
              color: Colors.black12,
              size: 150,
            ),
            text: FlutterI18n.translate(context, 'cart.Empty.Description'));
      }
    });
  }

  Widget _listView(BuildContext context, CartLoadSuccess state) {
    return ListView.separated(
      itemCount: state.items.length,
      itemBuilder: (BuildContext context, int index) {
        return ListCartItem(
          cartItem: state.items[index],
          onTap: () {
            Navigator.of(context).pushNamed('/item',
                arguments:
                    ItemScreenArguments(item: state.items[index].getItem()));
          },
          onDismissed: (direction) {
            final item = state.items[index];

            BlocProvider.of<CartBloc>(context).add(DeleteCart(item: item));

            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('$item remove')));
          },
          onMinus: () {
            BlocProvider.of<CartBloc>(context)
                .add(DecrementCart(item: state.items[index]));
          },
          onPlus: () {
            BlocProvider.of<CartBloc>(context)
                .add(IncrementCart(item: state.items[index]));
          },
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(),
    );
  }

  static Widget _footer(BuildContext context, CartLoadSuccess loadState) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;
    return BlocBuilder<AuthBloc, AuthState>(
        builder: (BuildContext context, AuthState auth) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              formatCurrency.printPrice(loadState.priceAmount),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            CheckOutButton(
                onPressed: () {
                  if (auth.isAuth) {
                    Navigator.of(context).pushNamed('/check-out');
                  } else {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SignInPage(
                              isPopMain: false,
                              hideExecutorButton: true,
                            )));
                  }
                },
                label: Text(
                  FlutterI18n.translate(context, 'cart.CheckOut',
                      translationParams: {'count': loadState.count.toString()}),
                  style: TextStyle(color: Colors.white),
                ))
          ],
        ),
      );
    });
  }
}
