import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/common_settings.dart';
import 'package:home_kitchen/customer/payment_methods/payment_methods.dart';

import '../../check_out.dart';

class PaymentMethodInfoBlock extends StatefulWidget {
  PaymentMethodInfoBlock({Key key, this.state}) : super(key: key);

  final CheckOutLoadSuccess state;

  @override
  _PaymentMethodInfoState createState() => _PaymentMethodInfoState();
}

class _PaymentMethodInfoState extends State<PaymentMethodInfoBlock> {
  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;
    return Card(
        elevation: 2,
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Column(children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      FlutterI18n.translate(
                          context, 'checkOut.OrderInfoFooter.OrderTotal'),
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      formatCurrency.printPrice(widget.state.calculatedPrice),
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              if (widget.state.storeDelivery.price == 0)
                Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          FlutterI18n.translate(context,
                              'checkOut.OrderInfoFooter.DeliveryPrice'),
                          style: TextStyle(fontSize: 16),
                        ),
                        Text(
                          formatCurrency
                              .printPrice(widget.state.storeDelivery.price),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        )
                      ],
                    )),
              Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                            child: Text(
                              FlutterI18n.translate(context,
                                  'checkOut.OrderInfoFooter.TotalPrice'),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        child: Text(
                          formatCurrency.printPrice(widget.state.totalPrice),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ))
            ])));
  }
}

class PaymentMethodCard extends StatelessWidget {
  PaymentMethodCard({Key key, this.onNextPayment}) : super(key: key);
  final Function onNextPayment;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PaymentMethodBloc, PaymentMethodState>(
        builder: (BuildContext context, PaymentMethodState state) {
      return Card(
          elevation: 2,
          color: Colors.white,
          child: Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Text(
                      FlutterI18n.translate(
                          context, 'checkOut.${state.paymentMethod}'),
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  TextButton(
                      onPressed: onNextPayment,
                      child: Text(FlutterI18n.translate(context,
                          'checkOut.OrderInfoFooter.SelectPaymentMethod')))
                ],
              )));
    });
  }
}
