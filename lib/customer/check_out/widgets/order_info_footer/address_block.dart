import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/models/order.dart';
import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/check_out/check_out.dart';

class AddressBlock extends StatefulWidget {
  AddressBlock({Key key, this.currentState, this.onChangeOrderType})
      : super(key: key);

  final CheckOutLoadSuccess currentState;
  final Function onChangeOrderType;

  @override
  _AddressState createState() => _AddressState();
}

class _AddressState extends State<AddressBlock> {
  @override
  void initState() {
    _load();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AddressesBloc, AddressesState>(builder: (
      BuildContext context,
      AddressesState currentState,
    ) {
      if (currentState is AddressesError) {
        return _error(context, currentState);
      } else {
        return Card(
            child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Switch(
                      value: widget.currentState.type == DELIVERY_TYPE,
                      onChanged: widget.onChangeOrderType),
                  Text(FlutterI18n.translate(context,
                      'checkOut.OrderInfoFooter.Address.Type.${widget.currentState.type}'))
                ],
              ),
              if (widget.currentState.type == DELIVERY_TYPE)
                _renderAddress(context, currentState)
            ],
          ),
        ));
      }
    });
  }

  Widget _renderAddress(BuildContext context, AddressesState currentState) {
    if (currentState is AddressesLoadSuccess) {
      final selectedAddress = currentState.getPrimary();

      return Container(
          alignment: Alignment.bottomLeft,
          padding: EdgeInsets.only(top: 5),
          child: selectedAddress != null
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                      Text(
                        selectedAddress.printOnlyAddress,
                      ),
                      TextButton(
                        onPressed: () =>
                            {Navigator.of(context).pushNamed('/addresses')},
                        child: Text(FlutterI18n.translate(context,
                            'checkOut.OrderInfoFooter.Address.Select')),
                      )
                    ])
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      FlutterI18n.translate(
                        context,
                        'checkOut.OrderInfoFooter.Address.Label',
                      ),
                      style: TextStyle(color: Colors.grey),
                    ),
                    TextButton(
                      onPressed: () =>
                          {Navigator.of(context).pushNamed('/addresses')},
                      child: Text(FlutterI18n.translate(
                          context, 'checkOut.OrderInfoFooter.Address.Select')),
                    )
                  ],
                ));
    }
    return Container(
        padding: EdgeInsets.only(top: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(FlutterI18n.translate(
                context, 'checkOut.OrderInfoFooter.Address.Label')),
            TextButton(
              onPressed: () => Navigator.of(context).pushNamed('/addresses'),
              child: Text(FlutterI18n.translate(
                  context, 'checkOut.OrderInfoFooter.Address.Select')),
            )
          ],
        ));
  }

  Widget _error(BuildContext context, AddressesError currentState) {
    return Card(
        elevation: 2,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          child: Text('Error: ${currentState.errorMessage}',
              style: TextStyle(color: Colors.redAccent)),
        ));
  }

  void _load() {
    BlocProvider.of<AddressesBloc>(context).add(LoadAddresses());
  }
}
