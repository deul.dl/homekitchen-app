import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/models/order.dart';
import 'package:home_kitchen/customer/models/own_store.dart';
import 'package:home_kitchen/widgets/expanded_flat_button.dart';

import 'payment_method_info_block.dart';
import 'address_block.dart';
import '../../check_out.dart';

class OrderInfoFooter extends StatelessWidget {
  OrderInfoFooter(
      {Key key,
      this.executor,
      @required this.checkOutBloc,
      @required this.onCreateOrder,
      @required this.onNextPaymentMethod,
      isAmount})
      : super(key: key);
  final OwnStore executor;

  final CheckOutBloc checkOutBloc;
  final Function onCreateOrder;
  final Function onNextPaymentMethod;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CheckOutBloc, CheckOutState>(
        bloc: checkOutBloc,
        builder: (
          BuildContext context,
          CheckOutState currentState,
        ) {
          if (currentState is CheckOutLoadSuccess &&
              currentState.status == STATUS.STARTED) {
            return Container(
                color: Theme.of(context).backgroundColor,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AddressBlock(
                        currentState: currentState,
                        onChangeOrderType: (bool isDelivery) {
                          if (isDelivery) {
                            checkOutBloc.add(ChangeTypeOfOrder(DELIVERY_TYPE));
                          } else {
                            checkOutBloc.add(ChangeTypeOfOrder(IN_STORE_TYPE));
                          }
                        }),
                    PaymentMethodCard(
                      onNextPayment: onNextPaymentMethod,
                    ),
                    PaymentMethodInfoBlock(
                      state: currentState,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 15.0),
                      alignment: Alignment.bottomCenter,
                      child: ExpandedFlatButton(
                          label: FlutterI18n.translate(
                              context, 'checkOut.OrderInfoFooter.CreateOrder'),
                          onPressed: onCreateOrder),
                    )
                  ],
                ));
          } else if (currentState is CheckOutFailed) {
            return _error(context, currentState);
          } else {
            return Container();
          }
        });
  }

  static Widget _error(BuildContext context, CheckOutFailed currentState) {
    return Card(
        elevation: 2,
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          child: Text('Error: ${currentState.errorMessage}',
              style: TextStyle(color: Colors.redAccent)),
        ));
  }
}
