import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/common_settings.dart';

class ListItemCard extends StatelessWidget {
  ListItemCard({
    @required this.cartItem,
    this.onTap,
  });

  final CartItem cartItem;
  final Function onTap;

  String get urlImage =>
      DotEnv().env['SERVER_URL'] +
      (cartItem.imageSource.isNotEmpty
          ? cartItem.imageSource
          : DotEnv().env['PLACEHOLDER_IMAGE']);

  @override
  Widget build(BuildContext context) {
    final formatCurrency = CommonSettings.of(context).formatCurrency;

    Widget leftSide = Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            cartItem.name,
            textAlign: TextAlign.left,
          ),
          Text(formatCurrency.printPrice(cartItem.totalPrice),
              textAlign: TextAlign.left,
              style: TextStyle(
                  height: 1.5, fontWeight: FontWeight.bold, fontSize: 16)),
          Text(cartItem.count.toString(),
              textAlign: TextAlign.left,
              style: TextStyle(
                  height: 1.5, fontWeight: FontWeight.bold, fontSize: 16)),
        ],
      ),
    );

    Widget content = Padding(
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: CachedNetworkImage(
                imageUrl: urlImage,
                width: 100,
                height: 100,
                placeholder: (context, url) =>
                    Image.asset('assets/images/placeholder.jpg'),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            SizedBox(width: 20),
            leftSide,
          ],
        ));

    return GestureDetector(
        onTap: onTap,
        child: Card(
          child: content,
        ));
  }
}
