import 'package:meta/meta.dart';

@immutable
abstract class CheckOutEvent {}

class LoadStoreDelivery extends CheckOutEvent {
  LoadStoreDelivery();

  @override
  String toString() => 'CreateOrder { }';
}

class ChangeTypeOfOrder extends CheckOutEvent {
  ChangeTypeOfOrder(this.type);
  final String type;

  @override
  String toString() => 'ChangeTypeOfOrder { }';
}

class CreateOrder extends CheckOutEvent {
  CreateOrder();

  @override
  String toString() => 'CreateOrder { }';
}
