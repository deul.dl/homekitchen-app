import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:home_kitchen/models/store_delivery.dart';
import 'package:meta/meta.dart';

import 'package:home_kitchen/models/order.dart';
import 'package:home_kitchen/customer/payment_methods/payment_methods.dart';
import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';

import 'check_out.dart';

class CheckOutBloc extends Bloc<CheckOutEvent, CheckOutState> {
  CheckOutBloc({
    @required this.orderRepository,
    @required this.addressesBloc,
    @required this.cartBloc,
    @required this.paymentMethodBloc,
    @required this.storeRepository,
  }) : super(null);

  final MyOrderRepository orderRepository;
  final StoriesRepository storeRepository;
  final AddressesBloc addressesBloc;
  final CartBloc cartBloc;
  final PaymentMethodBloc paymentMethodBloc;

  @override
  Stream<CheckOutState> mapEventToState(
    CheckOutEvent event,
  ) async* {
    if (event is LoadStoreDelivery) {
      yield* _mapCheckOutLoadedStoreDeliveryToStore(event);
    }
    if (event is CreateOrder) {
      yield* _mapCheckOutCreatedOrderToState(event);
    }
    if (event is ChangeTypeOfOrder) {
      yield* _mapCheckOutChangedTypeOfOrderToState(event);
    }
  }

  Stream<CheckOutState> _mapCheckOutChangedTypeOfOrderToState(
    ChangeTypeOfOrder event,
  ) async* {
    yield (state as CheckOutLoadSuccess).copyWith(type: event.type);
  }

  Stream<CheckOutState> _mapCheckOutLoadedStoreDeliveryToStore(
    CheckOutEvent event,
  ) async* {
    try {
      final cart = (cartBloc.state as CartLoadSuccess);

      final storeDelivery =
          await storeRepository.getStoreByDelivery(cart.items[0].storeUUID);

      yield CheckOutLoadSuccess(
          storeDelivery: (storeDelivery as StoreDelivery) ?? null,
          calculatedPrice: cart.priceAmount,
          status: STATUS.STARTED,
          type: IN_STORE_TYPE);
    } catch (_) {
      yield CheckOutFailed(_?.toString());
    }
  }

  Stream<CheckOutState> _mapCheckOutCreatedOrderToState(
    CheckOutEvent event,
  ) async* {
    try {
      final addressesState = addressesBloc.state;
      yield CheckOutInProgress();

      final items = (cartBloc.state as CartLoadSuccess).items;
      DateTime now = DateTime.now();

      final Map<String, dynamic> order = {
        'items': items.map((e) => e.toJson()).toList(),
        'description': '',
        'paymentMethod': paymentMethodBloc.state.paymentMethod,
        'time': now.toUtc().toIso8601String(),
        'executorUUID': items[0].userUUID,
        'storeUUID': items[0].storeUUID,
      };

      if (addressesState is AddressesLoadSuccess &&
          addressesState.addresses.isNotEmpty &&
          (state as CheckOutLoadSuccess).type == DELIVERY_TYPE) {
        final address = addressesState.getPrimary();
        order['addressUUID'] = address.uuid;
      }

      await orderRepository
          .createOrder(order)
          .whenComplete(() => cartBloc.add(CleanCart()));

      yield CheckOutLoadSuccess(status: STATUS.SENDED);
    } catch (_) {
      yield CheckOutFailed(_?.toString());
    }
  }
}
