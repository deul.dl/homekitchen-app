import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import 'package:home_kitchen/models/order.dart';
import 'package:home_kitchen/models/store_delivery.dart';

enum STATUS { STARTED, SENDED }

abstract class CheckOutState extends Equatable {
  final List<Object> propss;
  CheckOutState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

@immutable
class CheckOutInProgress extends CheckOutState {
  @override
  String toString() => 'CheckOutInProgress {}';
}

@immutable
class CheckOutLoadSuccess extends CheckOutState {
  CheckOutLoadSuccess(
      {this.storeDelivery,
      this.calculatedPrice,
      this.status,
      this.type = IN_STORE_TYPE})
      : super([storeDelivery, calculatedPrice, status, type]);

  final StoreDelivery storeDelivery;
  final double calculatedPrice;
  final String type;
  final STATUS status;

  double get totalPrice => storeDelivery.price + calculatedPrice;

  @override
  String toString() => '''
      CheckOutLoadSuccess {
        storeDelivery: $storeDelivery,
        calculatedPrice: $calculatedPrice,
        status: $status,
        type: $type
      }''';

  CheckOutLoadSuccess copyWith(
      {StoreDelivery storeDelivery,
      double calculatedPrice,
      STATUS status,
      String type}) {
    return CheckOutLoadSuccess(
        storeDelivery: storeDelivery ?? this.storeDelivery,
        calculatedPrice: calculatedPrice ?? this.calculatedPrice,
        status: status ?? this.status,
        type: type ?? this.type);
  }
}

@immutable
class CheckOutFailed extends CheckOutState {
  final String errorMessage;

  CheckOutFailed(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'CheckOutFailed {}';
}
