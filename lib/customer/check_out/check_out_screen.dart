import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:home_kitchen/customer/cart/cart.dart';
import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/theme/colors.dart';

import 'widgets/list_Item.dart';

class CheckOutScreen extends StatefulWidget {
  CheckOutScreen();

  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOutScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartBloc, CartState>(builder: (
      BuildContext context,
      CartState currentState,
    ) {
      if (currentState is CartLoadSuccess) {
        return ListView.separated(
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return ListItemCard(
                cartItem: currentState.items[index],
              );
            },
            separatorBuilder: (BuildContext context, int index) => Divider(),
            itemCount: currentState.items.length);
      } else {
        return Container(
            color: HomeKitchenColors.whiteLilac,
            child: CircularProgressIndicator());
      }
    });
  }
}
