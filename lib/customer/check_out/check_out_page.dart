import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:home_kitchen/addresses/addresses.dart';
import 'package:home_kitchen/customer/my_orders/my_orders.dart';
import 'package:home_kitchen/customer/payment_methods/payment_methods.dart';
import 'package:home_kitchen/customer/stories/stories.dart';
import 'package:home_kitchen/customer/check_out/widgets/order_info_footer/order_info_footer.dart';
import 'package:home_kitchen/customer/check_out/check_out.dart';
import 'check_out_screen.dart';

class CheckOutPage extends StatefulWidget {
  CheckOutPage();

  static const String routeName = '/check-out';

  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOutPage> {
  CheckOutBloc _checkOutBloc;

  @override
  void initState() {
    _checkOutBloc = CheckOutBloc(
        storeRepository: RepositoryProvider.of<StoriesRepository>(context),
        orderRepository: RepositoryProvider.of<MyOrderRepository>(context),
        cartBloc: BlocProvider.of<CartBloc>(context),
        addressesBloc: BlocProvider.of<AddressesBloc>(context),
        paymentMethodBloc: BlocProvider.of<PaymentMethodBloc>(context))
      ..add(LoadStoreDelivery());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(context, 'checkOut.Title')),
        ),
        body: BlocListener<CheckOutBloc, CheckOutState>(
          bloc: _checkOutBloc,
          listener: (context, state) {
            if (state is CheckOutLoadSuccess && state.status == STATUS.SENDED) {
              Navigator.of(context).popUntil(ModalRoute.withName('/'));
            } else if (state is CheckOutFailed) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(content: Text(state.errorMessage)),
                );
            }
          },
          child: SafeArea(
              child: Container(
            color: Theme.of(context).backgroundColor,
            child: Column(children: [
              Expanded(child: CheckOutScreen()),
              OrderInfoFooter(
                  checkOutBloc: _checkOutBloc,
                  onNextPaymentMethod: () =>
                      Navigator.of(context).pushNamed('/payment-method'),
                  isAmount: true,
                  onCreateOrder: () => _checkOutBloc.add(CreateOrder()))
            ]),
          )),
        ));
  }

  @override
  void dispose() {
    _checkOutBloc.close();
    super.dispose();
  }
}
