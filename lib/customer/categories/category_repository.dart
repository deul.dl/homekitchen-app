import 'dart:async';

import 'package:home_kitchen/models/models.dart';
import 'package:home_kitchen/client/client.dart';

class CategoryRepository {
  CategoryRepository(client) : this._client = client;

  final WebClient _client;

  Future<dynamic> loadData() {
    return _client
        .get('categories/not-empty')
        .then((res) => res['data']['categories'])
        .then((categories) => categories == null
            ? null
            : (categories as List)
                .map((category) => Category.fromJson(category))
                .toList());
  }
}
