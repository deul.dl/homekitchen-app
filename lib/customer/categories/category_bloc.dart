import 'dart:async';
import 'dart:developer' as developer;

import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'category.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc({@required this.categoryRepository}) : super(null);
  final CategoryRepository categoryRepository;

  @override
  Stream<CategoryState> mapEventToState(
    CategoryEvent event,
  ) async* {
    try {
      yield* event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'CategoryBloc', error: _, stackTrace: stackTrace);
    }
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
