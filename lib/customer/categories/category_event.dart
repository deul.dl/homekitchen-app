import 'dart:async';

import 'package:meta/meta.dart';

import 'category.dart';

@immutable
abstract class CategoryEvent {
  Stream<CategoryState> applyAsync(
      {CategoryState currentState, CategoryBloc bloc});
}

class LoadCategories extends CategoryEvent {
  LoadCategories();

  @override
  String toString() => 'LoadCategories';

  @override
  Stream<CategoryState> applyAsync(
      {CategoryState currentState, CategoryBloc bloc}) async* {
    try {
      yield CategoryInProgress();
      final categories = await bloc.categoryRepository.loadData();
      yield CategoryLoadSuccess(
          categories: (categories as List<Category>) ?? []);
    } catch (_) {
      yield CategoryFailure(_?.toString());
    }
  }
}
