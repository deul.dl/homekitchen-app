import 'package:equatable/equatable.dart';

import 'package:home_kitchen/models/models.dart';

abstract class CategoryState extends Equatable {
  /// notify change state without deep clone state
  final List propss;

  CategoryState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);
}

class CategoryInProgress extends CategoryState {
  CategoryInProgress();

  @override
  String toString() => 'CategoryInProgress';
}

class CategoryLoadSuccess extends CategoryState {
  final List<Category> categories;

  CategoryLoadSuccess({this.categories}) : super([categories]);

  @override
  String toString() => 'CategoryLoadSuccess { categories: $categories }';
}

class CategoryFailure extends CategoryState {
  final String errorMessage;

  CategoryFailure(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'CategoryFailure { errorMessage: $errorMessage }';
}
