import 'package:flutter/material.dart';

import 'widgets/ButtonCircle.dart';

class NewOrderPage extends StatelessWidget {
  static const String routeName = '/new-order';

  @override
  Widget build(BuildContext context) {
    final container = Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ButtonCircle(
              icon: Icon(
                Icons.store,
                color: Theme.of(context).primaryColor,
                size: 48.0,
              ),
              onClick: () => Navigator.pushNamed(context, '/stories'),
              title: 'Choose store'),
          ButtonCircle(
            icon: Icon(
              Icons.add,
              color: Theme.of(context).primaryColor,
              size: 48.0,
            ),
            onClick: () => Navigator.pushNamed(context, '/special-order',
                arguments: {'isMore': false}),
            title: 'Order special order',
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black)),
      body: SafeArea(
        child: container,
      ),
    );
  }
}
